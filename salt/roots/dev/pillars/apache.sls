apache:
  sites:
    9gag.la:
      ServerName: 9gag.la
      DocumentRoot: /var/www/9gag/web
      Directory:
        /var/www/9gag/web:
          Options: Indexes FollowSymLinks
          AllowOverride: None
          Require: all granted
          Include: /var/www/9gag/web/.htaccess
        /var/www/9gag/app:
          Options: Indexes FollowSymLinks
          AllowOverride: None
          Require: all granted
          Include: /var/www/9gag/app/.htaccess
        /var/www/9gag/src:
          Options: Indexes FollowSymLinks
          AllowOverride: None
          Require: all granted
          Include: /var/www/9gag/src/.htaccess
  modules:
    enabled:
      - rewrite