nginx:
  openresty:
    version: 1.7.7.2
    checksum: sha1=837f1d740121ba22db8f9cbae0d142ce7a498c3a
  ng:
    service:
      enable: True
    lookup:
      conf_file: /usr/local/openresty/nginx/conf/nginx.conf
      vhost_available: /usr/local/openresty/nginx/sites-available
      vhost_enabled: /usr/local/openresty/nginx/sites-enabled
      vhost_use_symlink: True
    server:
      config:
        pid: /usr/local/openresty/nginx/logs/nginx.pid
        worker_processes: auto
        worker_rlimit_nofile: 8192
        events:
          worker_connections: 8000
        http:
          server_tokens: 'off'
          default_type: application/octet-stream
          charset_types:
            - text/xml
            - text/plain
            - text/vnd.wap.wml
            - application/x-javascript
            - application/rss+xml
            - text/css
            - application/javascript
            - application/json
          log_format:  main  '$remote_addr - $remote_user [$time_local] "$request" $status $body_bytes_sent "$http_referer" "$http_user_agent" "$http_x_forwarded_for"'
          keepalive_timeout: 20
          sendfile: 'on'
          tcp_nopush: 'on'
          gzip: 'on'
          gzip_comp_level: 5
          gzip_min_length: 256
          gzip_proxied: any
          gzip_vary: 'on'
          gzip_types:
            - application/atom+xml
            - application/javascript
            - application/json
            - application/rss+xml
            - application/vnd.ms-fontobject
            - application/x-font-ttf
            - application/x-web-app-manifest+json
            - application/xhtml+xml
            - application/xml
            - font/eot
            - font/opentype
            - image/svg+xml
            - image/x-icon
            - text/javascript
            - text/css
            - text/plain
            - text/x-component
          client_body_buffer_size: 10K
          client_header_buffer_size: 1k
          client_max_body_size: 8m
          large_client_header_buffers: 2 1k
          include:
            - /usr/local/openresty/nginx/mime.types
            - /usr/local/openresty/nginx/conf.d/*.conf
            - /usr/local/openresty/nginx/sites-available/*
    vhosts:
      managed:
        default:
          enabled: False
          config:
            -
        no-default:
          enabled: True
          config:
            - server:
              - listen:
                - 80
                - default_server
              - return: 444
              - error_log: /var/log/nginx/no-default_error.log
              - access_log: /var/log/nginx/no-default_access.log
        3paaye:
          enabled: True
          config:
            - server:
              - server_name:
                - a1.statics.3paaye.com
                - i1.statics.3paaye.com
                - i2.statics.3paaye.com
                - i3.statics.3paaye.com
              - listen:
                - 80
              - root: /var/www/9gag/web
              - error_page: 403 /404.html
              - error_page: 404 /404.html
              - error_page: 405 /404.html
              - error_page: 500 501 502 503 504 /404.html
              - location @404:
                - expires: max
                - index: 404.html
                - log_not_found: 'off'
                - access_log: 'off'
                - add_header: Cache-Control "public"
              - location ~ ~$:
                - deny: all
              - location ~ \.php$:
                - deny: all
              - location ~* .(ogg|ogv|svg|svgz|eot|otf|woff|mp4|ttf|css|rss|atom|js|jpg|jpeg|gif|png|ico|zip|tgz|gz|rar|bz2|doc|xls|exe|ppt|tar|mid|midi|wav|bmp|rtf|html|txt|htm)$:
                - expires: max
                - log_not_found: 'off'
                - access_by_lua_file: /usr/local/openresty/nginx/conf/cdn.lua
                - access_log: 'off'
                - add_header: Cache-Control "public"
                - fastcgi_hide_header: Set-Cookie
                - set: $cors "true"
                - if ($request_method = 'OPTIONS'):
                  - set: $cors "${cors}options"
                - if ($request_method = 'GET'):
                  - set: $cors "${cors}get"
                - if ($request_method = 'POST'):
                  - set: $cors "${cors}post"
                - if ($cors = "true"):
                  - add_header: "'Access-Control-Allow-Origin' '*'"
                - if ($cors = "trueget"):
                  - add_header: "'Access-Control-Allow-Origin' '*'"
                  - add_header: "'Access-Control-Allow-Credentials' 'true'"
                  - add_header: "'Access-Control-Allow-Methods' 'GET, POST, OPTIONS'"
                  - add_header: "'Access-Control-Allow-Headers' 'DNT,X-Mx-ReqToken,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type'"
                - if ($cors = "trueoptions"):
                  - add_header: "'Access-Control-Allow-Origin' '*'"
                  - add_header: "'Access-Control-Allow-Credentials' 'true'"
                  - add_header: "'Access-Control-Allow-Methods' 'GET, POST, OPTIONS'"
                  - add_header: "'Access-Control-Allow-Headers' 'DNT,X-Mx-ReqToken,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type'"
                  - add_header: "'Access-Control-Max-Age' 1728000"
                  - add_header: "'Content-Type' 'text/plain charset=UTF-8'"
                  - add_header: "'Content-Length' 0"
                  - return: 204
                - if ($cors = "truepost"):
                  - add_header: "'Access-Control-Allow-Origin' '*'"
                  - add_header: "'Access-Control-Allow-Credentials' 'true'"
                  - add_header: "'Access-Control-Allow-Methods' 'GET, POST, OPTIONS'"
                  - add_header: "'Access-Control-Allow-Headers' 'DNT,X-Mx-ReqToken,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type'"
              - include: /usr/local/openresty/nginx/conf/h5bp/location/protect-system-files.conf
            - server:
              - server_name:
                - www.3paaye.com
              - listen:
                - 80
              - return: 301 $scheme://3paaye.com$request_uri
            - server:
              - server_name:
                - 3paaye.com
              - listen:
                - 80
              - root: /var/www/9gag/web
              - charset: utf-8
              - 'if ($http_user_agent ~* LWP::Simple|wget|libwww-perl)':
                - return: 403
              - location /:
                - index: app.php
                - try_files:
                  - $uri
                  - /app.php$is_args$args
              - 'location ~ ^/(app_dev|config)\.php(/|$)':
                - fastcgi_pass: unix:/var/run/php5-fpm.sock
                - fastcgi_split_path_info: ^(.+\.php)(/.*)$
                - include: fastcgi_params
                - fastcgi_param: SCRIPT_FILENAME $document_root$fastcgi_script_name
                - fastcgi_param: HTTPS off
              - 'location ~ ^/app\.php(/|$)':
                - fastcgi_pass: unix:/var/run/php5-fpm.sock
                - fastcgi_split_path_info: ^(.+\.php)(/.*)$
                - include: fastcgi_params
                - fastcgi_param: SCRIPT_FILENAME $document_root$fastcgi_script_name
                - fastcgi_param: HTTPS off
                # Prevents URIs that include the front controller. This will 404:
                # http://domain.tld/app.php/some-path
                # Remove the internal directive to allow URIs like this
                - internal
              - error_log: /var/log/nginx/3paaye_error.log
              - access_log: /var/log/nginx/3paaye_access.log
              - include: /usr/local/openresty/nginx/conf/h5bp/basic.conf