base:
  '*':
#    - nginx.openresty
#    - nginx.ng.config
#    - nginx.ng.vhosts_config
#    - nginx.ng.vhosts
#    - nginx.ng.service
#    - h5bp-nginx-configs
#    - nginx_cdn_config
#    - nginx-openresty-config
    - apache
    - apache.modules
    - apache.vhosts.standard
    - php.ng
#    - php.ng.fpm.install
#    - php.ng.fpm.config
#    - php.ng.fpm.service
    - php.ng.cli
    - php.ng.intl
    - php.ng.imagick
    - php.ng.json
    - php.ng.gd
    - php.ng.curl
    - php.ng.mcrypt
    - php.ng.mysql
    - php.ng.redis
    - php.ng.soap
    - git
    - mysql
    - node
    - rabbitmq.latest
    - redis
    - java
    - imagemagick
    - ffmpeg
    - composer
    - symfony.composer.install
    - symfony.cache.clear
    - fix_permissions
    - symfony.doctrine.update
    - symfony.assets.install
    - symfony.assetic.dump