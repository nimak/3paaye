{% from "java/openjdk_map.jinja" import openjdk with context %}
openjdk7:
  pkg:
    - installed
    - name: {{ openjdk.package.v7 }}