base:
  pkgrepo.managed:
    - humanname: Ubuntu Multimedia for Trusty PPA
    - name: ppa:mc3man/trusty-media
    - dist: trusty
    - file: /etc/apt/sources.list.d/ffmpeg.list
    - keyid: ED8E640A
    - keyserver: keyserver.ubuntu.com
    - require_in:
      - pkg: ffmpeg

ffmpeg:
  pkg.installed