{% set env = pillar.get('symfony', {}).get('environment') %}
{% if env == "prod" %}
symfony.composer.install:
  cmd.run:
    - name: composer install --no-dev --optimize-autoloader
    - cwd: /var/www/9gag
    - onlyif:
      - command -v composer
{% endif %}