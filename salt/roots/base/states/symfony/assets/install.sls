{% set env = pillar.get('symfony', {}).get('environment') %}
symfony.assets.install:
  cmd.run:
    {% if env == "prod" %}
    - name: php app/console assets:install --env={{ env }}
    {% else %}
    - name: php app/console assets:install --symlink --env={{ env }}
    {% endif %}
    - cwd: /var/www/9gag
    - onlyif:
      - command -v php