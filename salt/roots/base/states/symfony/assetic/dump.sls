{% set env = pillar.get('symfony', {}).get('environment') %}
symfony.assetic.dump:
  cmd.run:
    - name: php app/console assetic:dump --env={{ env }}
    - cwd: /var/www/9gag
    - onlyif:
      - command -v php