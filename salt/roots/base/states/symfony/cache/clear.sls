{% set env = pillar.get('symfony', {}).get('environment') %}
symfony.cache.clear:
  cmd.run:
    - name: php app/console cache:clear --env={{ env }}
    - cwd: /var/www/9gag
    - onlyif:
      - command -v php