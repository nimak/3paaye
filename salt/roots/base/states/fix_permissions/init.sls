acl:
  pkg.installed

add_user_to_www_group:
  cmd.run:
    - names:
      - gpasswd -a "$USER" www-data
      - usermod -a -G www-data "$USER"
fix_main_permissions:
  cmd.run:
    - onlyif: ls /var/www/9gag
    - names:
#      - find /var/www/9gag -type d -exec chmod 755 {} \;
#      - find /var/www/9gag -type f -exec chmod 644 {} \;
      - chgrp www-data /var/www/9gag
      - chown "$USER":www-data /var/www/9gag/ -R
      - chmod g+ws /var/www/9gag/ -R
set_main_acl:
  cmd.run:
    - onlyif: ls /var/www/9gag
    - names:
      - setfacl -Rd -m g:www-data:rw /var/www/9gag/
    - require:
      - pkg: acl
fix_cache_permissions:
  cmd.run:
    - onlyif: ls /dev/shm/9gag
    - names:
      - chmod a+rw /dev/shm/9gag/ -R
      - chown "$USER":www-data /dev/shm/9gag/ -R
      - chmod g+ws /dev/shm/9gag/ -R
set_cache_acl:
  cmd.run:
    - onlyif: ls /dev/shm/9gag
    - names:
      - setfacl -Rd -m g:www-data:rwx /dev/shm/9gag/
    - require:
      - pkg: acl