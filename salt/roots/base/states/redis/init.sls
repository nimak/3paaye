redis_install:
  pkgrepo.managed:
      - ppa: chris-lea/redis-server
      - require_in:
            - pkg: redis-server
  pkg.installed:
    - name: redis-server
    - version: 2:2.8.19-1chl1~trusty1
    - refresh: True

redis_config:
  file.managed:
    - name: /etc/redis/redis.conf
    - template: jinja
    - source: salt://redis/templates/redis-2.8.conf.jinja
    - require:
      - pkg: redis-server

redis_service:
  service.running:
    - name: redis-server
    - enable: True
    - watch:
      - file: /etc/redis/redis.conf
    - require:
      - pkg: redis-server