/etc/init.d/nginx:
  file.managed:
    - source: salt://nginx-openresty-config/files/nginx
    - user: root
    - group: root
    - mode: 755
startup:
  cmd.wait:
    - cwd: /etc/init.d
    - names:
      - update-rc.d -f nginx defaults
    - watch:
      - file: /etc/init.d/nginx
