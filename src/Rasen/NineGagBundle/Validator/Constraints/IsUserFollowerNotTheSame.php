<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/10/2014
 * Time: 1:15 PM
 */

namespace Rasen\NineGagBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
/**
 * Class IsUserFollowerNotTheSame
 *
 * Checks if a user and it's follower are not the same.
 *
 * @package Rasen\NineGagBundle\Validator\Constraints
 *
 * @Annotation
 *
 */
class IsUserFollowerNotTheSame extends Constraint
{
	public $message = 'A user can not follow itself.';

	/**
	 * {@inheritdoc}
	 */
	public function __construct($options = null)
	{
		parent::__construct( $options );
	}

	/**
	 * {@inheritdoc}
	 */
	public function validatedBy()
	{
		return 'is_user_follower_not_the_same';
	}

	/**
	 * {@inheritdoc}
	 */
	public function getTargets()
	{
		return self::CLASS_CONSTRAINT;
	}
} 