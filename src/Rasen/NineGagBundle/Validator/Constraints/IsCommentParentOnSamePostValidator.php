<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/10/2014
 * Time: 1:25 PM
 */

namespace Rasen\NineGagBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use JMS\DiExtraBundle\Annotation as DI;
use Rasen\NineGagBundle\Entity\Notification;
/**
 * Class IsCommentParentOnSamePostValidator
 *
 * A validation constraint to check if the comment's parent is on the same post as the comment.
 *
 * @package Rasen\NineGagBundle\Validator\Constraints
 *
 * @DI\Validator("is_comment_parent_on_same_post")
 */
class IsCommentParentOnSamePostValidator extends ConstraintValidator
{
	/**
	 * @param \Rasen\NineGagBundle\Entity\Comment $comment
	 * @param Constraint $constraint
	 */
	public function validate($comment, Constraint $constraint)
	{
		if ($comment->getParent() && !($comment->getPost() === $comment->getParent()->getPost())) {
			$this->context->addViolationAt(
				'parent',
				$constraint->message,
				array(),
				null
			);
		}
	}
}