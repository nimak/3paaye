<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/10/2014
 * Time: 1:25 PM
 */

namespace Rasen\NineGagBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use JMS\DiExtraBundle\Annotation as DI;
use Rasen\NineGagBundle\Entity\Notification;
/**
 * Class IsObjectIdValidValidator
 *
 * A validation constraint to check if an object id is valid by querying the database for that object. (depending on object type)
 * Useful for Notification entity where the object id for multiple object types is stored in a field with no foreign key constraint.
 *
 * @package Rasen\NineGagBundle\Validator\Constraints
 *
 * @DI\Validator("is_object_id_valid")
 */
class IsObjectIdValidValidator extends ConstraintValidator
{
	/**
	 * @var \Doctrine\Common\Persistence\ObjectManager
	 *
	 * @DI\Inject("doctrine.orm.entity_manager")
	 */
	private $em;

	/**
	 * @param \Rasen\NineGagBundle\Entity\Notification $notification
	 * @param Constraint $constraint
	 */
	public function validate($notification, Constraint $constraint)
	{
		/**
		 * {
		 *   **0** : Post
		 *   **1** : Comment
		 *   **2** : User
		 * }
		 */
		$objectType = $notification->getObjectType();

		$objectId = $notification->getObjectId();

		switch ($objectType)
		{
			case Notification::OBJECT_TYPE_POST:
				//Post
				$object = $this->em->getRepository('RasenNineGagBundle:Post')->find($objectId);
				break;
			case Notification::OBJECT_TYPE_COMMENT:
				//Comment
				$object = $this->em->getRepository('RasenNineGagBundle:Comment')->find($objectId);
				break;
			case Notification::OBJECT_TYPE_USER:
				//User
				$object = $this->em->getRepository('RasenNineGagBundle:User')->find($objectId);
				break;
			default:
				//none
				$object = false;
				break;
		}

		if (!$object) {
			$this->context->addViolationAt(
				'objectId',
				$constraint->message,
				array(),
				null
			);
		}
	}
}