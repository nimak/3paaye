<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/10/2014
 * Time: 1:25 PM
 */

namespace Rasen\NineGagBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use JMS\DiExtraBundle\Annotation as DI;
use Rasen\NineGagBundle\Entity\Notification;
/**
 * Class IsNotificationVerbValidValidator
 *
 * Checks if a notification verb is related to the object type provided.
 * In other words if an objectType is "Post", the verb can not be "Followed" since that only makes sense for Users.
 *
 * @package Rasen\NineGagBundle\Validator\Constraints
 *
 * @DI\Validator("is_notification_verb_valid")
 */
class IsNotificationVerbValidValidator extends ConstraintValidator
{
	/**
	 * @param \Rasen\NineGagBundle\Entity\Notification $notification
	 * @param Constraint $constraint
	 */
	public function validate($notification, Constraint $constraint)
	{
		/**
		 * {
		 *   **0** : Post
		 *   **1** : Comment
		 *   **2** : User
		 * }
		 */
		$objectType = $notification->getObjectType();

		/**
		 * {
		 *   **0** : _DownVoted_ a Post
		 *   **1** : _UpVoted_ a Post
		 *   **2** : _Commented_ on a Post
		 *   **3** : _Followed_ another User
		 * }
		 */
		$verb = $notification->getVerb();

		switch ($objectType)
		{
			case Notification::OBJECT_TYPE_POST :
				//Post
				$validVerbs = array(Notification::VERB_DOWNVOTED, Notification::VERB_UPVOTED, Notification::VERB_COMMENTED);
				break;
			case Notification::OBJECT_TYPE_COMMENT:
				//Comment
				$validVerbs = $validVerbs = array(Notification::VERB_DOWNVOTED, Notification::VERB_UPVOTED, Notification::VERB_COMMENTED);
				break;
			case Notification::OBJECT_TYPE_USER:
				//User
				$validVerbs = $validVerbs = array(Notification::VERB_FOLLOWED);
				break;
			default:
				//none
				$validVerbs = false;
				break;
		}

		if (!in_array($verb, $validVerbs)) {
			$this->context->addViolationAt(
				'verb',
				$constraint->message,
				array(),
				null
			);
		}
	}
}