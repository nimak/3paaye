<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/10/2014
 * Time: 1:15 PM
 */

namespace Rasen\NineGagBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
/**
 * Class IsNotificationVerbValid
 *
 * Checks if a notification verb is related to the object type provided.
 * In other words if an objectType is "Post", the verb can not be "Followed" since that only makes sense for Users.
 *
 * @package Rasen\NineGagBundle\Validator\Constraints
 *
 * @Annotation
 *
 */
class IsNotificationVerbValid extends Constraint
{
	public $message = 'The provided verb is not related to the provided object type.';

	/**
	 * {@inheritdoc}
	 */
	public function __construct($options = null)
	{
		parent::__construct( $options );
	}

	/**
	 * {@inheritdoc}
	 */
	public function validatedBy()
	{
		return 'is_notification_verb_valid';
	}

	/**
	 * {@inheritdoc}
	 */
	public function getTargets()
	{
		return self::CLASS_CONSTRAINT;
	}
} 