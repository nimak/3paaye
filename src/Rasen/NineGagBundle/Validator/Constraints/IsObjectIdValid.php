<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/10/2014
 * Time: 1:15 PM
 */

namespace Rasen\NineGagBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
/**
 * Class IsObjectIdValid
 *
 * A validation constraint to check if an object id is valid by querying the database for that object. (depending on object type)
 * Useful for Notification entity where the object id for multiple object types is stored in a field with no foreign key constraint.
 *
 * @package Rasen\NineGagBundle\Validator\Constraints
 *
 * @Annotation
 *
 */
class IsObjectIdValid extends Constraint
{
	public $message = 'No object with this Id exists.';

	/**
	 * {@inheritdoc}
	 */
	public function __construct($options = null)
	{
		parent::__construct( $options );
	}

	/**
	 * {@inheritdoc}
	 */
	public function validatedBy()
	{
		return 'is_object_id_valid';
	}

	/**
	 * {@inheritdoc}
	 */
	public function getTargets()
	{
		return self::CLASS_CONSTRAINT;
	}
} 