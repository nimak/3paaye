<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/10/2014
 * Time: 1:15 PM
 */

namespace Rasen\NineGagBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
/**
 * Class IsCommentParentOnSamePost
 *
 * A validation constraint to check if the comment's parent is on the same post as the comment.
 *
 * @package Rasen\NineGagBundle\Validator\Constraints
 *
 * @Annotation
 *
 */
class IsCommentParentOnSamePost extends Constraint
{
	public $message = 'Invalid Parent!';

	/**
	 * {@inheritdoc}
	 */
	public function __construct($options = null)
	{
		parent::__construct( $options );
	}

	/**
	 * {@inheritdoc}
	 */
	public function validatedBy()
	{
		return 'is_comment_parent_on_same_post';
	}

	/**
	 * {@inheritdoc}
	 */
	public function getTargets()
	{
		return self::CLASS_CONSTRAINT;
	}
} 