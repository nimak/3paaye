<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/24/2014
 * Time: 6:15 PM
 */

namespace Rasen\NineGagBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Hashids\Hashids;

/**
 * Class GenerateHashIdsCommand
 * @package Rasen\NineGagBundle\Command
 */
class GenerateHashIdsCommand extends ContainerAwareCommand
{
	/**
	 * {@inheritdoc}
	 */
	protected function configure()
	{
		$this
			->setName('rasen:hashids:generate')
			->setDescription('Generates hashids for entities currently stored in database and updates them.')
		;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		/**
		 * $container ContainerInterface
		 */
		$container = $this->getContainer();

		/**
		 * @var ObjectManager $em
		 */
		$em = $container->get('doctrine')->getManager();

		/**
		 * @var Hashids $hashIds
		 */
		$hashIds = $container->get('hashids');

		$output->writeln('Updating posts\' ids...');
		$postRepo = $em->getRepository('RasenNineGagBundle:Post');
		$getAllPostsQ = $postRepo->createQueryBuilder('p')
			->select('p.id as postId')
			->getQuery();
			;
		$allPostsIds = $getAllPostsQ->getScalarResult();

		$postsHashIdsCaseQ = "";
		foreach ( $allPostsIds as $post ) {
			$postId = $post['postId'];
			$postsHashIdsCaseQ .= "WHEN " . $postId . " THEN '" . $hashIds->encode($postId) . "' ";
		}

		$postUpdateQuery = "UPDATE RasenNineGagBundle:Post p SET ";
		if ($postsHashIdsCaseQ !== "") {
			$postUpdateQuery .=
				"p.hashId = CASE p.id ".
				$postsHashIdsCaseQ .
				" ELSE '' END "
			;
		}
		$updatePostsQ = $em->createQuery($postUpdateQuery);
		$updatePosts = $updatePostsQ->execute();

		/////////////////////////////////////////////////////////////////////////

		$output->writeln('Updating comments\' ids...');
		$CommentRepo = $em->getRepository('RasenNineGagBundle:Comment');
		$getAllCommentsQ = $CommentRepo->createQueryBuilder('c')
			->select('c.id as commentId')
			->getQuery();
			;
		$allCommentsIds = $getAllCommentsQ->getScalarResult();

		$CommentsHashIdsCaseQ = "";
		foreach ( $allCommentsIds as $comment) {
			$commentId = $comment['commentId'];
			$CommentsHashIdsCaseQ .= "WHEN " . $commentId . " THEN '" . $hashIds->encode($commentId) . "' ";
		}

		$CommentUpdateQuery = "UPDATE RasenNineGagBundle:Comment c SET ";
		if ($CommentsHashIdsCaseQ !== "") {
			$CommentUpdateQuery .=
				"c.hashId = CASE c.id ".
				$CommentsHashIdsCaseQ .
				" ELSE '' END "
			;
		}
		$updateCommentsQ = $em->createQuery($CommentUpdateQuery);
		$updateComments = $updateCommentsQ->execute();


		$output->writeln('Done! ;)');
	}
} 