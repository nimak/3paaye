<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/24/2014
 * Time: 6:15 PM
 */

namespace Rasen\NineGagBundle\Command;

use Doctrine\ORM\EntityManager;
use Rasen\NineGagBundle\Entity\Post;
use Rasen\NineGagBundle\Entity\PostImage;
use Rasen\NineGagBundle\Lib\PostImageProcessor;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Hashids\Hashids;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Class CalculateUploadsChecksum
 * @package Rasen\NineGagBundle\Command
 */
class CalculateUploadsChecksumCommand extends ContainerAwareCommand
{
	/**
	 * {@inheritdoc}
	 */
	protected function configure()
	{
		$this
			->setName('rasen:post:checksum')
			->setDescription('Calculates and stores posts checksum (sha1).');
	}

	/**
	 * {@inheritdoc}
	 */
	protected function execute(InputInterface $input, OutputInterface $output)
	{

        /**
         * $container ContainerInterface
         */
        $container = $this->getContainer();

        /**
         * @var EntityManager $em
         */
        $em = $container->get('doctrine')->getManager();

        $selectDQL = "SELECT p FROM RasenNineGagBundle:PostImage p";
        $selectQ = $em->createQuery($selectDQL);
        $results = $selectQ->iterate();
        $i = 1;
        $batchSize = 20;
        $output->writeln('Calculating...');

        foreach ($results as $row) {
            $post = $row[0];
            if ($post instanceof PostImage)
            {
                $postImagePath = $this->getPostImagePath($post);

                $sha1 = sha1_file($postImagePath);
                $post->setChecksum($sha1);

                if (($i % $batchSize) === 0) {
                    $output->writeln('Updating batch #'.$i/$batchSize.'...');
                    $em->flush();
                    $em->clear();
                    $output->writeln('Calculating batch #'.(($i/$batchSize)+1).'...');
                }
            }
            ++$i;
        }
        $output->writeln('Finishing...');
        $em->flush();
        $output->writeln('Done :)');
	}

    private function getPostImagePath(PostImage $post)
    {
        $uploadPath = $this->getContainer()->getParameter('kernel.root_dir').PostImageProcessor::POST_UPLOAD_DIR;
        return realpath(PostImageProcessor::join_paths($uploadPath, $post->getImageUrl()));
    }

} 