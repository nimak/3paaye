'use strict';

/* App Module */

var userProfileApp = angular.module('userProfileApp', [
    'ngRoute',
    'userProfileAppControllers',
    'userProfileAppServices',
    'approveStatusApp',
    'postsManagerApp',
    'timeApp',
    'userApp',
    'userConfigApp',
    'userProfileConfigApp'
]).run(['$http', '$templateCache', function($http, $templateCache){
    var templates = [
        '/bundles/rasenninegag/js/user_profile_app/views/posts_list.html',
        '/bundles/rasenninegag/js/user_profile_app/views/users_list.html'
    ];
    var cacheTemplate = function (templateUrl) {
        $http.get(templateUrl).success(function (t) {
            $templateCache.put(templateUrl, t);
        });
    };
    for(var i=0; i<templates.length; i++) {
        cacheTemplate(templates[i]);
    }
}]);

userProfileApp.config(['$interpolateProvider', '$routeProvider', '$locationProvider',
    function($interpolateProvider, $routeProvider, $locationProvider) {
        $interpolateProvider.startSymbol('[[').endSymbol(']]');

        $routeProvider.
            when('/posts', {
                templateUrl: '/bundles/rasenninegag/js/user_profile_app/views/posts_list.html',
                controller: 'PostsCtrl'
            }).
            when('/upvotes', {
                templateUrl: '/bundles/rasenninegag/js/user_profile_app/views/posts_list.html',
                controller: 'PostsUpvotedCtrl'
            }).
            when('/comments', {
                templateUrl: '/bundles/rasenninegag/js/user_profile_app/views/posts_list.html',
                controller: 'PostsCommentedCtrl'
            }).
            when('/followers', {
                templateUrl: '/bundles/rasenninegag/js/user_profile_app/views/users_list.html',
                controller: 'FollowersCtrl'
            }).
            when('/followings', {
                templateUrl: '/bundles/rasenninegag/js/user_profile_app/views/users_list.html',
                controller: 'FollowingsCtrl'
            }).
            when('/stream', {
                templateUrl: '/bundles/rasenninegag/js/user_profile_app/views/posts_list.html',
                controller: 'StreamCtrl'
            }).
            otherwise({
                redirectTo: '/posts'
            });
        $locationProvider.html5Mode(true);
    }]);

angular.element().ready(function() {
    angular.bootstrap('.main-wrapper', ['userProfileApp']);
});

