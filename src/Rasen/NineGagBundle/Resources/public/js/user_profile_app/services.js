'use strict';

/* Services */

angular.module('userProfileAppServices', []).
    service('UserProfileService', [
        'UsersManager',
        'User',
        'Users',
        'ajaxConfig',
        'profileConstants',
        'Posts', function (
            UsersManager,
            User,
            Users,
            ajaxConfig,
            profileConstants,
            Posts)
        {
        var service = {
            user: new User(profileConstants.user),
            own: profileConstants.own,
            getUserPosts: getUserPosts,
            getUserPostsUpvoted: getUserPostsUpvoted,
            getUserPostsCommented: getUserPostsCommented,
            getStreamPosts: getStreamPosts,
            getUserFollowers: getUserFollowers,
            getUserFollowings: getUserFollowings
        };

        var userPostsUrl = service.user._links.posts.href;
        function getUserPosts() {
            return new Posts(userPostsUrl);
        }

        var userPostsUpvotedUrl = service.user._links.upvotes.href;
        function getUserPostsUpvoted() {
            return new Posts(userPostsUpvotedUrl);
        }

        var userPostsCommentedUrl = service.user._links.commented.href;
        function getUserPostsCommented() {
            return new Posts(userPostsCommentedUrl);
        }

        var streamPostsUrl = service.user._links.stream.href;
        function getStreamPosts() {
            return new Posts(streamPostsUrl);
        }

        var userFollowersUrl = service.user._links.followers.href;
        function getUserFollowers() {
            return new Users(userFollowersUrl);
        }

        var userFollowingsUrl = service.user._links.followings.href;
        function getUserFollowings() {
            return new Users(userFollowingsUrl);
        }

        return service;
    }])
;