'use strict';

/* Controllers */


var userProfileAppControllers = angular.module('userProfileAppControllers', []);

userProfileAppControllers
    .controller('ProfileCtrl', ['$scope', 'User', '$rootScope', 'UserProfileService',
        function($scope, User, $rootScope, UserProfileService)
        {
            $scope.own = UserProfileService.own;
            $scope.user = UserProfileService.user;

            $rootScope.$on('$routeChangeSuccess', function(e, current, pre) {
                if (current.$$route != undefined) {
                    $scope.activeTab = current.$$route.originalPath;
                }
            });
        }])
    .controller('PostsCtrl', ['$scope', 'User', 'UserProfileService',
        function($scope, User, UserProfileService)
        {
            $scope.own = UserProfileService.own;
            $scope.user = UserProfileService.user;
            $scope.posts = UserProfileService.getUserPosts();
            $scope.posts.load();

        }])
    .controller('PostsUpvotedCtrl', ['$scope', 'User', 'UserProfileService',
        function($scope, User, UserProfileService)
        {
            $scope.own = UserProfileService.own;
            $scope.user = UserProfileService.user;
            $scope.posts = UserProfileService.getUserPostsUpvoted();
            $scope.posts.load();
        }])
    .controller('PostsCommentedCtrl', ['$scope', 'User', 'UserProfileService',
        function($scope, User, UserProfileService)
        {
            $scope.own = UserProfileService.own;
            $scope.user = UserProfileService.user;
            $scope.posts = UserProfileService.getUserPostsCommented();
            $scope.posts.load();

        }])
    .controller('FollowersCtrl', ['$scope', 'User', 'UserProfileService',
        function($scope, User, UserProfileService)
        {
            $scope.own = UserProfileService.own;
            $scope.user = UserProfileService.user;
            $scope.users = UserProfileService.getUserFollowers();
            $scope.users.load();
        }])
    .controller('FollowingsCtrl', ['$scope', 'User', 'UserProfileService',
        function($scope, User, UserProfileService)
        {
            $scope.own = UserProfileService.own;
            $scope.user = UserProfileService.user;
            $scope.users = UserProfileService.getUserFollowings();
            $scope.users.load();
        }])
    .controller('StreamCtrl', ['$scope', 'User', 'UserProfileService',
        function($scope, User, UserProfileService)
        {
            $scope.own = UserProfileService.own;
            $scope.user = UserProfileService.user;
            $scope.posts = UserProfileService.getStreamPosts();
            $scope.posts.load();
        }]);