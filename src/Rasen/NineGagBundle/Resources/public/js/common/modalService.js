/**
 * Created by Nima on 11/14/2014.
 */
angular.module('modalServiceApp', ['ui.bootstrap'])
    .run(['$http', '$templateCache', function($http, $templateCache){
        var cacheTemplate = function (templateUrl) {
            $http.get(templateUrl).success(function (t) {
                $templateCache.put(templateUrl, t);
            });
        };
        cacheTemplate('/bundles/rasenninegag/js/common/views/modal.html');
    }]).
    config(['$interpolateProvider',
        function($interpolateProvider) {
            $interpolateProvider.startSymbol('[[').endSymbol(']]');
        }]).
    service('modalService', ['$modal',
        function ($modal) {

            var modalDefaults = {
                backdrop: true,
                keyboard: true,
                modalFade: false,
                windowClass: 'small-modal',
                templateUrl: '/bundles/rasenninegag/js/common/views/modal.html'
            };

            var modalOptions = {
                closeButtonText: 'بستن',
                actionButtonText: 'تایید',
                headerText: null,
                bodyText: 'انجام این کار؟',
                okBtnClass: 'btn-primary'
            };

            this.showModal = function (customModalDefaults, customModalOptions) {
                if (!customModalDefaults) customModalDefaults = {};
                customModalDefaults.backdrop = 'static';
                return this.show(customModalDefaults, customModalOptions);
            };

            this.show = function (customModalDefaults, customModalOptions) {
                //Create temp objects to work with since we're in a singleton service
                var tempModalDefaults = {};
                var tempModalOptions = {};

                //Map angular-ui modal custom defaults to modal defaults defined in service
                angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

                //Map modal.html $scope custom properties to defaults defined in service
                angular.extend(tempModalOptions, modalOptions, customModalOptions);

                if (!tempModalDefaults.controller) {
                    tempModalDefaults.controller = ['$scope', '$modalInstance', function ($scope, $modalInstance) {
                        $scope.modalOptions = tempModalOptions;
                        $scope.modalOptions.ok = function (result) {
                            $modalInstance.close(result);
                        };
                        $scope.modalOptions.close = function (result) {
                            $modalInstance.dismiss('cancel');
                        };
                    }]
                }

                return $modal.open(tempModalDefaults).result;
            };

        }]);