/**
 * Created by Nima on 11/20/2014.
 */
'use strict';

/* App Module */

var userApp = angular.module('userApp', [
    'userConfigApp'
]).run(['$http', '$templateCache', function($http, $templateCache){
    var cacheTemplate = function (templateUrl) {
        $http.get(templateUrl).success(function (t) {
            $templateCache.put(templateUrl, t);
        });
    };
    cacheTemplate('/bundles/rasenninegag/js/common/views/users_list.html');
}]).
    factory('User', ['$q', '$http', 'modalService', function ($q, $http, modalService)
    {
        var User = function(user)
        {
            this.id = this.username = user.username;
            this.fullName = user.full_name;
            this.firstName = user.first_name;
            this.lastName = user.last_name;
            this.href = user.href;
            this.imageUrl = user.image_url;
            this.followed = user.followed;
            this._links = user._links;
            this.badges = user._embedded.badges;
        };

        User.prototype.follow = function() {
            if (typeof this._links.follow === 'undefined') return false;
            var userFollowUrl = this._links.follow.href;
            this.followedStatus = 'pending';
            var self = this;
            $http.post(userFollowUrl,{}).then(function(){
                self.followedStatus = 'done';
                self.followed = true;
            }, function(){
                self.followedStatus = 'done';
            })
        };

        User.prototype.unfollow = function () {
            if (typeof this._links.unfollow === 'undefined') return false;
            var userUnFollowUrl = this._links.unfollow.href;

            var modalOptions = {
                closeButtonText: 'انصراف',
                actionButtonText: 'دنبال نکردن این کاربر',
                bodyText: 'آیا از دنبال نکردن این کاربر اطمینان دارید؟',
                okBtnClass: 'btn-primary'
            };

            var self = this;

            modalService.showModal({}, modalOptions).then(function (result) {
                self.followedStatus = 'pending';
                $http.post(userUnFollowUrl,{}).then(function(){
                    self.followedStatus = 'done';
                    self.followed = false;
                }, function(){
                    self.followedStatus = 'done';
                })
            });
        };

        User.prototype.toggleFollow = function () {
            if (this.followed) {
                this.unfollow();
            } else {
                this.follow();
            }
        };

        return User;

    }]).
    factory('Users', ['$q', '$http', 'User', function ($q, $http, User) {
        var Users = function(sourceUrl) {
            this.sourceUrl = sourceUrl;
            this.hasMore = true;
            this.loadingMore = false;
            this.loading = false;
            this.loadingError = false;
            this.after = null;
            this.items = [];
            this.loaded = false;
            this.loadedUsersIds = [];
        };

        Users.prototype.load = function() {
            if (!this.hasMore || this.loading) {
                return false;
            }
            this.loading = true;
            var self = this;
            $http.get(this.sourceUrl, {params: {}}).success(function(data){
                var result = data.result;
                if (typeof result === 'undefined' || typeof result._embedded === 'undefined' || typeof result._embedded.users === 'undefined' ) {
                    self.loading = false;
                    self.loaded = true;
                    self.hasMore = false;
                    self.items = [];
                    return false;
                }
                var users = result._embedded.users;
                var user;
                for (var i = 0; i < users.length; i ++) {
                    if ($.inArray(users[i].username, self.loadedUsersIds) > -1) continue;
                    user = new User(users[i]);
                    user.followers = new Users(user._links.followers.href);
                    user.followings = new Users(user._links.followings.href);
                    self.items.push(user);
                    self.loadedUsersIds.push(user.id);
                }
                self.hasMore = (typeof result._links.next != 'undefined');
                self.sourceUrl = self.hasMore ? result._links.next.href : self.sourceUrl;
                self.loading = false;
                self.loaded = true;
                self.loadingError = false;
            }).
                error(function() {
                    self.loadingError = true;
                    self.loading = false;
                });
        };

        return Users;
    }]).
    service('UsersManager', ['User', '$q', '$http', 'ajaxConfig', function (User, $q, $http, ajaxConfig) {
        var service = {
            getUser: getUser,
            getCurrentUser: getCurrentUser,
            isLoggedIn: (ajaxConfig.currentUser && ajaxConfig.loggedIn),
            checkLoggedIn: checkLoggedIn
        };

        function getCurrentUser() {
            if (ajaxConfig.currentUser) {
                return new User(ajaxConfig.currentUser);
            } else {
                return false;
            }
        }

        function checkLoggedIn() {
            if (service.isLoggedIn) {
                return true;
            } else {
                $('#loginModal').modal();
                return false;
            }
        }

        function getUser(username) {
            var userBaseUrl = ajaxConfig.baseUrl + '/users/' + username;
            var deferred = $q.defer();
            $http.get(userBaseUrl).success(function(data){
                var user = new User(data.result);
                deferred.resolve(user);
            }).
                error(function() {
                    var user = null;
                    deferred.reject(user);
                });
            return deferred.promise;
        }

        return service;
    }]).
    directive('followButton', function () {
        return {
            scope: {
                user: '=',
                small: '@'
            },
            controller: ['$scope', 'UsersManager', function($scope, UsersManager) {
                $scope.canFollow = function(user){
                    if (UsersManager.checkLoggedIn) {
                        var currentUser;
                        if (currentUser = UsersManager.getCurrentUser()) {
                            return !(currentUser.id == user.id);
                        }
                    }
                };
                $scope.followIcon = '<i class="fa fa-user-plus"></i>';
                $scope.followTxt = 'دنبال کردن این کاربر';
                $scope.unfollowIcon = '<i class="fa fa-user-times"></i>';
                $scope.unfollowTxt = 'دنبال نکردن این کاربر';
            }],
            template: '<button type="button" class="btn btn-follow" data-ng-show="canFollow(user)" data-ng-disabled="(user.followedStatus == \'pending\')" data-ng-class="{\'btn-danger\': !user.followed, \'btn-primary\': user.followed}" data-ng-click="user.toggleFollow()">'+
            '<span data-ng-if="small" data-ng-bind-html="user.followed ? unfollowIcon : followIcon"></span>'+
            '<span data-ng-if="!small" data-ng-bind="user.followed ? unfollowTxt : followTxt"></span>'+
            '</button>'
        }
    }).
    directive('usersList', function () {
        return {
            scope: {
                users: '=',
                loadUsers: '&',
                smallFollow: '@'
            },
            templateUrl: '/bundles/rasenninegag/js/common/views/users_list.html'
        }
    });