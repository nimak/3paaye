/**
 * Created by Nima on 11/18/2014.
 */
angular.module('commentsManagerApp', ['modalServiceApp', 'timeApp', 'approveStatusApp', 'userConfigApp','userApp'])
    .run(['$http', '$templateCache', function($http, $templateCache){
        var templates = [
            '/bundles/rasenninegag/js/common/views/comment_list.html',
            '/bundles/rasenninegag/js/common/views/comment_item.html',
            '/bundles/rasenninegag/js/common/views/comment_replies_list.html',
            '/bundles/rasenninegag/js/common/views/comment_reply_item.html'
        ];
        var cacheTemplate = function (templateUrl) {
            $http.get(templateUrl).success(function (t) {
                $templateCache.put(templateUrl, t);
            });
        };
        for(var i=0; i<templates.length; i++) {
            cacheTemplate(templates[i]);
        }
    }]).
    factory('Comment', ['$q', '$http', 'ajaxConfig', 'modalService', 'UsersManager',
        function ($q, $http, ajaxConfig, modalService, UsersManager)
    {
        var baseUrl = ajaxConfig.baseUrl;
        var Comment = function(comment)
        {
            this.id = comment.id;
            this.content = comment.content ? comment.content : (comment.deleted ? 'این نظر حذف شده است.': '' );
            this.totalVotes = comment.total_votes ? comment.total_votes : 0;
            this.upvotesNo = comment.upvotes_no ? comment.upvotes_no : 0;
            this.downvotesNo = comment.downvotes_no ? comment.downvotes_no : 0;
            this.repliesNo = comment.replies_no ? comment.replies_no : 0;
            this.vote = comment.vote;
            this.reported = comment.reported;
            this.own = comment.own;
            this.canEdit = comment.can_edit;
            this.canDelete = comment.can_delete;
            this.createdTime = comment.created_time;
            this.deleted = comment.deleted;

            this.approved = comment.approved;

            this.createdBy = comment.created_by;

            this.lastEditedTime = comment.last_edited_time;
            this.lastEditedBy = comment.last_edited_by;

            if (comment.deleted) {
                this.createdBy.href = '#';
            }

            this.hidden = comment.reported && !comment.own && !comment.deleted;

            this.canReport = UsersManager.isLoggedIn && !comment.reported && !comment.own && !comment.deleted;

            this._links = comment._links;
        };

        Comment.prototype.toggleHidden = function () {
            this.hidden = !this.hidden;
        };

        Comment.prototype.updateVoteCounter = function (voteBefore, voteNow) {
            if (voteBefore == null && voteNow == null) {
                return false;
            } else if (voteBefore == null && voteNow != null) {
                if (voteNow) {
                    this.upvotesNo++;
                } else if (!voteNow) {
                    this.downvotesNo++;
                }
            } else if (voteBefore != null && voteNow == null) {
                if (voteBefore) {
                    this.upvotesNo--;
                } else if (!voteBefore) {
                    this.downvotesNo--;
                }
            } else if (voteBefore != null && voteNow != null) {
                if (!voteBefore && voteNow) {
                    this.upvotesNo++;
                    this.downvotesNo--;
                } else if (voteBefore && !voteNow) {
                    this.upvotesNo--;
                    this.downvotesNo++;
                }
            }
            this.totalVotes = this.upvotesNo - this.downvotesNo;
        };

        Comment.prototype.postVote = function(vote) {
            if (!UsersManager.checkLoggedIn() || (typeof this._links.votes === 'undefined')) {
                return false;
            }
            var voteBefore = this.vote;
            this.vote = vote;
            this.loading = true;
            var votesUrl = this._links.votes.href;
            var self = this;
            $http.post(votesUrl,{vote:vote}).then(function(){

                self.updateVoteCounter(voteBefore, vote);
                self.loading = false;
            }, function(){
                self.vote = voteBefore;
                self.loading = false;
            })
        };

        Comment.prototype.removeVote = function() {
            if (!UsersManager.checkLoggedIn() || (typeof this._links.votes === 'undefined')) {
                return false;
            }
            var voteBefore = this.vote;
            this.vote = null;
            this.loading = true;
            var votesUrl = this._links.votes.href;
            var self = this;
            $http.delete(votesUrl,{}).then(function(){
                self.updateVoteCounter(voteBefore, null);
                self.loading = false;
            }, function(){
                self.vote = voteBefore;
                self.loading = false;
            })
        };

        Comment.prototype.upVote = function() {
            if (this.vote == true) {
                this.removeVote();
            } else {
                this.postVote(true);
            }
        };

        Comment.prototype.downVote = function() {
            if (this.vote == false) {
                this.removeVote();
            } else {
                this.postVote(false);
            }
        };

        Comment.prototype.report = function () {
            if (!UsersManager.checkLoggedIn()) {
                return false;
            }
            if (this.reported == true || this.own) return false;
            var modalOptions = {
                closeButtonText: 'انصراف',
                actionButtonText: 'گزارش نظر',
                bodyText: 'بعد از گزارش این نظر، دیگر آن را مشاهده نمی‌کنید. آیا از این کار اطمینان دارید؟'
            };
            var _self = this;
            modalService.show({}, modalOptions).then(function (result) {
                _self.reported = _self.hidden = true;
                var reportsUrl = baseUrl+'/comments/'+_self.id+'/reports';
                $http.post(reportsUrl,{}).success(function(resp){
                })
            });
        };

        Comment.prototype.remove = function(report) {
            if (!UsersManager.checkLoggedIn() || !this.canDelete) {
                return false;
            }
            var actionButtonText = 'حذف نظر';
            var bodyText = 'آیا از حذف این نظر اطمینان دارید؟';
            if (report == true) {
                actionButtonText = 'حذف و گزارش نظر';
                bodyText = 'آیا از حذف و گزارش این نظر اطمینان دارید؟'
            }
            var modalOptions = {
                closeButtonText: 'انصراف',
                actionButtonText: actionButtonText,
                bodyText: bodyText,
                okBtnClass: 'btn-danger'
            };
            var _self = this;
            modalService.show({}, modalOptions).then(function (result) {
                var removeUrl = _self._links.self.href;
                $http.delete(removeUrl,{params:{report: report}}).success(function(resp){
                    _self.deleted = true;
                    _self.content = 'این نظر حذف شده است.';
                })
            });
        };

        Comment.prototype.update = function (content) {
            var deferred = $q.defer();
            if (!UsersManager.checkLoggedIn() || !this.canEdit) {
                return false;
            }
            this.updating = true;
            var _self = this;
            var editUrl = _self._links.self.href;
            $http.put(editUrl,{content: content}).success(function(){
                _self.updating = false;
                _self.content = content;
                _self.lastEditedTime = Date.now();
                _self.lastEditedBy = UsersManager.getCurrentUser();
                deferred.resolve();
            }).error(function(resp){
                _self.updating = false;
                deferred.reject(resp.result);
            });
            return deferred.promise;
        };

        return Comment;

    }]).
    factory('CommentsManager', ['$q', '$http', 'ajaxConfig', 'Comment', 'UsersManager', 'User', function ($q, $http, ajaxConfig, Comment, UsersManager, User) {
        var baseUrl = ajaxConfig.baseUrl;
        var CommentsManager = function(sourceUrl, totalCount, hidden, fillComments) {
            this.commentsUrl = sourceUrl;
            this.sourceUrl = sourceUrl;
            this.hasMore = false;
            this.loadingMore = false;
            this.loading = false;
            this.loadingError = false;
            this.offset = 0;
            this.totalCount = totalCount;
            this.totalListCount = totalCount;
            this.totalFullCount = totalCount;
            this.firstComments = {};
            this.comments = [];
            this.hidden = hidden;
            this.loaded = false;
            this.loadedIds = [];
            this.mentionables = [];

            if (typeof fillComments !== 'undefined') {
                this.loadPaginatedComments(fillComments);
                this.loaded = true;
            }
        };

        CommentsManager.prototype.addComment = function (comment, unshift) {
            if (typeof comment._links.replies !== 'undefined' ) {
                comment.replies = new CommentsManager(comment._links.replies.href, comment.repliesNo, true);
            }
            if (unshift) {
                this.comments.unshift(comment);
            } else {
                this.comments.push(comment);
            }
        };

        CommentsManager.prototype.loadPaginatedComments = function (result) {
            if (typeof result._embedded === 'undefined' || typeof result._embedded.comments === 'undefined' ) {
                return false;
            }
            var items = result._embedded.comments;
            var comments = [];
            if (items == undefined || items[0] == undefined || items[0]['id'] == undefined ) {
                return comments;
            }
            for (var i = 0; i < items.length; i ++) {
                if ($.inArray(items[i].id, this.loadedIds) > -1) continue;
                var comment = new Comment(items[i]);
                this.addComment(comment, true);
                comments.unshift(comment);
                this.loadedIds.unshift(comment.id);
            }
            this.offset = parseInt(result.offset) + items.length;
            this.totalCount = parseInt(result.total);
            this.totalListCount = this.loadedIds.length;
            this.hasMore = (typeof result._links.next !== 'undefined');
            return comments;
        };

        CommentsManager.prototype.fetchComments = function (params) {
            var deferred = $q.defer();
            var self = this;
            $http.get(this.sourceUrl, {params: params}).success(function(data){
                var comments = self.loadPaginatedComments(data.result);
                deferred.resolve(comments);
            }).
                error(function() {
                    deferred.reject([]);
                });
            return deferred.promise;
        };

        CommentsManager.prototype.fetchPostOverviewComments = function(limit) {
            var params = {offset: this.offset, limit: limit};
            return this.fetchComments(params);
        };

        CommentsManager.prototype.fetchPostMoreComments = function () {
            var params = {offset: this.offset, limit: 10};
            return this.fetchComments(params);
        };

        CommentsManager.prototype.loadComments = function (limit){
            var deferred = $q.defer();
            this.hidden = false;
            this.loading = true;
            this.loadingError = false;
            var self = this;
            this.fetchPostOverviewComments(limit).then(function(comments) {
                if (comments.length > 0) {
                    deferred.resolve(comments);
                } else {
                    deferred.reject(false);
                }
                self.loading = false;
                self.loaded = true;
                self.loadingError = false;
            }, function(comments) {
                self.comments = comments;
                self.loading = false;
                self.loadingError = true;
                deferred.reject(false);
            });
            return deferred.promise;
        };

        CommentsManager.prototype.toggleCommentsView = function (){
            this.hidden = !this.hidden;
        };

        CommentsManager.prototype.loadMoreComments = function(){
            this.loadingMore = true;
            var self = this;
            this.fetchPostMoreComments().then(function(comments) {
                self.loadingMore = false;
            }, function(comments) {
            });
        };

        CommentsManager.prototype.postComment = function (content) {
            var deferred = $q.defer();
            if (!UsersManager.checkLoggedIn()) {
                deferred.reject({'error': 'forbidden'});
                return deferred.promise;
            }
            if (content.trim() == '' || content.length > 500) {
                deferred.reject({'error': 'blank'});
                return deferred.promise;
            }
            var url = this.commentsUrl;
            var self = this;
            $http.post(url, {content: content}).success(function(data){
                var result = data.result;
                if(typeof result.id !== "undefined" && result.id !== null ) {
                    self.addComment(new Comment(result), false);
                    self.totalCount++;
                    self.totalFullCount++;
                    deferred.resolve(result);
                } else {
                    deferred.reject(result);
                }
            }).
                error(function() {
                    deferred.reject(false);
                });
            return deferred.promise;

        };

        CommentsManager.prototype.fetchMentionables = function (q) {
            var deferred = $q.defer();
            var self = this;
            $http.get(this.commentsUrl + '/mentionables', {params: {q: q, limit: 10}}).success(function(data){
                var mentionables=[];
                var items = data.result;
                if (items == undefined || items[0] == undefined || items[0]['username'] == undefined ) {
                    deferred.resolve([]);
                    return false;
                }
                for (var i=0; i<items.length; i++) {
                    var user = new User(items[i]);
                    mentionables.push(user);
                }
                deferred.resolve(mentionables);
            }).
                error(function() {
                    deferred.reject([]);
                });
            return deferred.promise;
        };

        return CommentsManager;
    }]).
    directive('commentItem', function () {
        return {
            scope: {
                comment: '='
            },
            controller: ['$scope', 'UsersManager', function($scope, UsersManager) {
                $scope.isLoggedIn = UsersManager.isLoggedIn;
                $scope.initEdit = function(){
                    $scope.editable = true;
                    $scope.comment.editedContent = $scope.comment.content;
                };
                $scope.cancelEdit = function(){
                    $scope.editable = false;
                };
                $scope.saveEdit = function(){
                    $scope.comment.update($scope.comment.editedContent).then(function(){
                        $scope.editable = false;
                    }, function(){
                    });
                };
                $scope.remove = function(){
                    if ($scope.comment.own) {
                        $scope.comment.remove(false);
                    } else {
                        $scope.comment.remove(true);
                    }
                };
                $scope.report = function(){
                    if ($scope.comment.canDelete) {
                        $scope.comment.remove(true);
                    } else {
                        $scope.comment.report();
                    }
                };
            }],
            templateUrl: '/bundles/rasenninegag/js/common/views/comment_item.html'
        }
    }).
    directive('commentsList', function () {
        return {
            scope: {
                commentsManager: "=",
                limit: "="
            },
            controller: ['$scope', 'UsersManager', function($scope, UsersManager) {
                $scope.newComment = {content: '', status: 0};
                var canLoadMore = ($scope.limit > $scope.commentsManager.totalListCount && $scope.limit < $scope.commentsManager.totalCount);
                if (!$scope.commentsManager.loaded || canLoadMore) {
                    $scope.commentsManager.loadComments($scope.limit - $scope.commentsManager.totalListCount).then(function(){}, function(){
                        if (!UsersManager.isLoggedIn) { //instead we can use checkLoggedIn to show user register/login modal
                            $scope.commentsManager.hidden = true;
                            return false;
                        }
                    });
                }
                $scope.currentUser = UsersManager.getCurrentUser();
                $scope.isLoggedIn = UsersManager.isLoggedIn;
                $scope.canComment = !($scope.newComment.status == 1) && $scope.isLoggedIn;
                $scope.showLogin = function(){
                    $('#loginModal').modal('show');
                };
                $scope.showRegister = function(){
                    $('#registerModal').modal('show');
                };

                $scope.postNewComment = function () {
                    if (!UsersManager.checkLoggedIn() || $scope.newComment.status == 1) {
                        return false;
                    }
                    var content = $scope.newComment.content;
                    if (content.trim() == '' || content.length > 500) {
                        return false
                    }
                    $scope.newComment.status = 1;
                    $scope.canComment = false;
                    var self = this;
                    $scope.commentsManager.postComment($scope.newComment.content).then(function (data) {
                        $scope.newComment.content = '';
                        $scope.newComment.status = 0;
                        $scope.canComment = true;
                    }, function () {
                        $scope.newComment.status = -1;
                        $scope.canComment = true;
                    });
                };
            }],
            templateUrl: '/bundles/rasenninegag/js/common/views/comment_list.html'
        }
    }).
    directive('replyItem', function () {
        return {
            scope: {
                comment: '='
            },
            controller: ['$scope', function($scope) {
                $scope.initEdit = function(){
                    $scope.editable = true;
                    $scope.comment.editedContent = $scope.comment.content;
                };
                $scope.cancelEdit = function(){
                    $scope.editable = false;
                };
                $scope.saveEdit = function(){
                    $scope.comment.update($scope.comment.editedContent).then(function(){
                        $scope.editable = false;
                    }, function(){
                    });
                };
                $scope.remove = function(){
                    if ($scope.comment.own) {
                        $scope.comment.remove(false);
                    } else {
                        $scope.comment.remove(true);
                    }
                };
                $scope.report = function(){
                    if ($scope.comment.canDelete) {
                        $scope.comment.remove(true);
                    } else {
                        $scope.comment.report();
                    }
                };
            }],
            templateUrl: '/bundles/rasenninegag/js/common/views/comment_reply_item.html'
        }
    }).
    directive('repliesList', function () {
        return {
            scope: {
                comment: "="
            },
            controller: ['$scope', 'UsersManager', function($scope, UsersManager) {
                $scope.newComment = {content: '', status: 0};

                $scope.currentUser = UsersManager.getCurrentUser();
                $scope.isLoggedIn = UsersManager.isLoggedIn;

                if (typeof $scope.comment.replies !== 'undefined')
                {
                    $scope.canComment = !($scope.newComment.status == 1) && $scope.isLoggedIn;

                    if (!$scope.comment.replies.loaded) {
                        $scope.comment.replies.loadComments(5).then(function(){}, function(){
                            if (!UsersManager.isLoggedIn) { //instead we can use checkLoggedIn to show user register/login modal
                                $scope.comment.replies.hidden = true;
                                return false;
                            }
                        });
                    }

                    $scope.postNewComment = function () {
                        if ($scope.newComment.status == 1) {
                            return false;
                        }
                        var content = $scope.newComment.content;
                        if (content.trim() == '' || content.length > 500) {
                            return false
                        }
                        $scope.newComment.status = 1;
                        $scope.canComment = false;
                        var self = this;
                        $scope.comment.replies.postComment($scope.newComment.content).then(function (data) {
                            $scope.newComment.content = '';
                            $scope.newComment.status = 0;
                            $scope.canComment = true;
                        }, function () {
                            $scope.newComment.status = -1;
                            $scope.canComment = true;
                        });
                    };
                }

            }],
            templateUrl: '/bundles/rasenninegag/js/common/views/comment_replies_list.html'
        }
    })
;