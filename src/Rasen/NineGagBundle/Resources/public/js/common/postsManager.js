/**
 * Created by Nima on 11/14/2014.
 */

var EVENTS = {};
EVENTS.VISIBLITY_CHANGED = 'visiblity-changed';

angular.module('postsManagerApp', [
    'ui.bootstrap',
    'modalServiceApp',
    'commentsManagerApp',
    'timeApp',
    'approveStatusApp',
    'userConfigApp',
    'userApp',
    'ngSanitize',
    'com.2fdevs.videogular',
    'com.2fdevs.videogular.plugins.overlayplay',
    'com.2fdevs.videogular.plugins.poster',
    'info.vietnamcode.nampnq.videogular.plugins.flash',
    'infinite-scroll',
    'djds4rce.angular-socialshare'
])
    .config(['$tooltipProvider', function($tooltipProvider){
        $tooltipProvider.options({
            placement: 'top',
            animation: true,
            popupDelay: 0,
            appendToBody: true
        });
    }])
    .run(['$http', '$templateCache', function($http, $templateCache){
        var templates = [
            '/bundles/rasenninegag/js/common/views/post_card.html',
            '/bundles/rasenninegag/js/common/views/post_item.html',
            '/bundles/rasenninegag/js/common/views/ad_item.html',
            '/bundles/rasenninegag/js/common/views/post_video_content.html',
            '/bundles/rasenninegag/js/common/views/modal_upvotes.html'
        ];
        var cacheTemplate = function (templateUrl) {
            $http.get(templateUrl).success(function (t) {
                $templateCache.put(templateUrl, t);
            });
        };
        for(var i=0; i<templates.length; i++) {
            cacheTemplate(templates[i]);
        }
    }]).
    value('baseUrl', Routing.getBaseUrl() + '/rest').
    factory('Ad', [ '$sce', function ($sce)
    {
        var Ad = function(ad) {
            this.content = $sce.trustAsHtml(ad.content);
            this.title = ad.advertisement_title;
            this.link = ad.link;
            this.type = ad.type;

            this.ptype = (this.type === 'ad');
        };

        return Ad;
    }]).
    factory('Post', ['$rootScope', '$q', '$http', 'ajaxConfig', 'modalService', 'CommentsManager', 'UsersManager', 'User', 'Users', function ($rootScope, $q, $http, ajaxConfig, modalService, CommentsManager, UsersManager, User, Users)
    {
        var baseUrl = ajaxConfig.baseUrl;
        var Post = function(post)
        {
            this.id = post.id;
            this.type = post.type;
            this.href = post.href;
            this.image = post.image;
            this.imageBig = post.image_big;
            this.isAnimated = post.is_animated;
            this.isProcessed = post.is_processed;
            this.gifUrl = post.gif_url;
            this.mp4Url = post.mp4_url;
            this.webmUrl = post.webm_url;
            this.postTitle = post.post_title;
            this.content = post.content;
            this.totalVotes = post.total_votes;
            this.upvotesNo = post.upvotes_no;
            this.downvotesNo = post.downvotes_no;
            this.commentsNo = post.comments_no;
            this.approvedCommentsNo = post.approved_comments_no;
            this.viewsNo = post.views_no;
            this.downloadsNo = post.downloads_no;
            this.sharesNo = post.shares_no;
            this.vote = post.vote;
            this.reported = post.reported;
            this.nsfw = post.nsfw;
            this.own = post.own;
            this.canEdit = post.can_edit;
            this.canDelete = post.can_delete;
            this.createdTime = post.created_time;
            this.approved = post.is_approved;
            this.createdBy = post.created_by;
            this.hidden = post.reported && !post.own;
            this.category = post.category;

            if (this.image) {
                this.shareMediaUrl = this.isAnimated ? (this.mp4Url?this.mp4Url:this.image.url) : this.image.url;
            }

            this.commentsManager = new CommentsManager(post._links.comments.href, this.commentsNo, false, post._embedded.comments);

            this._links = post._links;

            this.upvotes = post.upvotes ? post.upvotes : new Users( this._links.votes.href);
        };

        Post.prototype.toggleHidden = function () {
            this.hidden = !this.hidden;
        };

        Post.prototype.checkProcessed = function() {
            if (this.isAnimated) {
                return (this.mp4Url && this.webmUrl);
            } else {
                return (this.image.url);
            }
        };

        Post.prototype.refreshMedia = function() {
            var deferred = $q.defer();

            var postUrl = this._links.self.href;
            var self = this;
            self.refreshLoading = true;
            //window.console.log('refreshing');
            $http.get(postUrl,{}).
                success(function(data, status, headers, config) {
                    var result = data.result;
                    self.mp4Url = result.mp4_url;
                    self.webmUrl = result.webm_url;
                    self.image = result.image;
                    self.imageBig = result.image_big;
                    self.isProcessed = result.is_processed;
                    self.refreshLoading = false;
                    deferred.resolve(self);
                }).error(function(data, status, headers, config) {
                    self.refreshLoading = false;
                    deferred.reject(self);
                });
            return deferred.promise;
        };

        Post.prototype.updateVoteCounter = function (voteBefore, voteNow) {
            if (voteBefore === null && voteNow === null) {
                return false;
            } else if (voteBefore === null && voteNow !== null) {
                if (voteNow) {
                    this.upvotesNo++;
                } else if (!voteNow) {
                    this.downvotesNo++;
                }
            } else if (voteBefore !== null && voteNow === null) {
                if (voteBefore) {
                    this.upvotesNo--;
                } else if (!voteBefore) {
                    this.downvotesNo--;
                }
            } else if (voteBefore !== null && voteNow !== null) {
                if (!voteBefore && voteNow) {
                    this.upvotesNo++;
                    this.downvotesNo--;
                } else if (voteBefore && !voteNow) {
                    this.upvotesNo--;
                    this.downvotesNo++;
                }
            }
            this.totalVotes = this.upvotesNo - this.downvotesNo;
        };

        Post.prototype.postVote = function(vote) {
            if (!UsersManager.checkLoggedIn() || this.loading) {
                return false;
            }
            var voteBefore = this.vote;
            this.vote = vote;
            this.loading = true;
            var self = this;
            var votesUrl = this._links.votes.href;
            $http.post(votesUrl,{vote:vote}).then(function(){
                self.updateVoteCounter(voteBefore, vote);
                self.loading = false;
            }, function(){
                self.vote = voteBefore;
                self.loading = false;
            });
            return true;
        };

        Post.prototype.removeVote = function() {
            if (!UsersManager.checkLoggedIn() || this.loading) {
                return false;
            }
            var voteBefore = this.vote;
            this.vote = null;
            this.loading = true;
            var self = this;
            var votesUrl = this._links.votes.href;
            $http.delete(votesUrl,{}).then(function(){
                self.updateVoteCounter(voteBefore, null);
                self.loading = false;
            }, function(){
                self.vote = voteBefore;
                self.loading = false;
            });
            return true;
        };

        Post.prototype.upVote = function() {
            if (this.vote !== true) {
                return this.postVote(true);
            }
            return true;
        };

        Post.prototype.downVote = function() {
            if (this.vote !== false) {
                return this.postVote(false);
            }
            return true;
        };

        Post.prototype.upVoteToggle = function() {
            if (this.vote === true) {
                this.removeVote();
            } else {
                this.upVote();
            }
        };

        Post.prototype.downVoteToggle = function() {
            if (this.vote === false) {
                this.removeVote();
            } else {
                this.downVote();
            }
        };

        Post.prototype.addReport = function () {
            if (!UsersManager.checkLoggedIn()) {
                return false;
            }
            if (this.reported == true || this.own) return false;
            var modalOptions = {
                closeButtonText: 'انصراف',
                actionButtonText: 'گزارش پست',
                bodyText: 'بعد از گزارش این پست، دیگر این پست را مشاهده نمی‌کنید. آیا از این کار اطمینان دارید؟'
            };
            var _self = this;
            modalService.show({}, modalOptions).then(function (result) {
                _self.reported = _self.hidden = true;
                var reportsUrl = baseUrl+'/posts/'+_self.id+'/reports';
                $http.post(reportsUrl,{}).success(function(resp){
                });
            });
        };

        Post.prototype.update = function (postTitle, category) {
            var deferred = $q.defer();
            if (!UsersManager.checkLoggedIn() || !this.canEdit) {
                return false;
            }
            this.updating = true;
            var _self = this;
            var editUrl = this._links.self.href;
            $http.put(editUrl,{title: postTitle, category: category.slug}).success(function(){
                _self.updating = false;
                _self.postTitle = postTitle;
                _self.category = category;
                deferred.resolve();
            }).error(function(resp){
                _self.updating = false;
                deferred.reject(resp.result);
            });
            return deferred.promise;
        };

        Post.prototype.showUpvotes = function () {
            var self = this;
            this.upvotes.load();
            modalService.show({templateUrl: '/bundles/rasenninegag/js/common/views/modal_upvotes.html', windowClass:'upvotes-modal'}, {users: self.upvotes, headerText: 'رای‌های مثبت'});
        };

        Post.prototype.remove = function() {
            if (!UsersManager.checkLoggedIn() || !this.canDelete) {
                return false;
            }
            var actionButtonText = 'حذف پست';
            var bodyText = 'آیا از حذف این پست اطمینان دارید؟';
            var modalOptions = {
                closeButtonText: 'انصراف',
                actionButtonText: actionButtonText,
                bodyText: bodyText,
                okBtnClass: 'btn-danger'
            };
            var _self = this;
            modalService.show({}, modalOptions).then(function (result) {
                var removeUrl = _self._links.self.href;
                $http.delete(removeUrl,{}).success(function(resp){
                    $rootScope.$emit('post.delete', _self);
                });
            });
        };

        Post.prototype.download = function(){
            if (this.downloaded) {return false;}
            var _self = this;
            $http.post(this._links.self.href + '/downloads' ,{}).then(function(){
                _self.downloaded = true;
            }, function(){
            });
        };
        Post.prototype.share = function(){
            if (this.shared) {return false;}
            var _self = this;
            $http.post(this._links.self.href + '/shares' ,{}).then(function(){
                _self.shared = true;
            }, function(){
            });
        };

        return Post;

    }]).
    factory('Posts', ['$rootScope', '$q', '$http', 'Post', 'Ad', 'ajaxConfig', function ($rootScope, $q, $http, Post, Ad, ajaxConfig) {
        var baseUrl = ajaxConfig.baseUrl;
        var Posts = function(sourceUrl) {
            this.sourceUrlOrigin = sourceUrl;
            this.sourceUrl = sourceUrl;
            this.hasMore = true;
            this.loadingMore = false;
            this.loading = false;
            this.loadingError = false;
            this.page = 1;
            this.after = null;
            this.items = [];
            this.loaded = false;
            this.loadedPostsIds = [];

            var _self = this;
            $rootScope.$on('post.delete', function (event, post) {
                _self.remove(post);
            });
        };

        Posts.prototype.remove = function(post) {
            this.items = $.grep(this.items, function(e){ return e.id !== post.id; });
        };

        Posts.prototype.load = function() {
            if (!this.hasMore || this.loading) {
                return false;
            }
            this.loading = true;
            var newPosts = [];
            var self = this;
            $http.get(this.sourceUrl, {params: {}}).success(function(data){
                var result = data.result;
                if (typeof result === 'undefined' || typeof result._embedded === 'undefined' || typeof result._embedded.posts === 'undefined' ) {
                    self.loading = false;
                    self.loaded = true;
                    self.hasMore = false;
                    self.items = [];
                    return false;
                }
                var posts = result._embedded.posts;
                var post;
                for (var i = 0; i < posts.length; i ++) {
                    if (posts[i].type !== 'ad' && $.inArray(posts[i].id, self.loadedPostsIds) > -1) continue;
                    //console.log(posts[i]);
                    if (posts[i].type === "ad") {
                        post = new Ad(posts[i]);
                    } else {
                        post = new Post(posts[i]);
                    }
                    self.items.push(post);
                    self.loadedPostsIds.push(post.id);
                    newPosts.push(post);
                }

                ga('send', 'pageview', self.sourceUrlOrigin + '/#page' + self.page);

                self.hasMore = (typeof result._links.next != 'undefined');
                self.page = self.hasMore ? self.page+1 : self.page;
                self.sourceUrl = self.hasMore ? result._links.next.href : self.sourceUrl;
                self.loading = false;
                self.loaded = true;
                self.loadingError = false;
            }).
                error(function() {
                    console.log('err');
                    self.loadingError = true;
                    self.loading = false;
                });
        };

        return Posts;
    }]).
    directive('postImage', function () {

        function link(scope, element, attrs) {

            var imageContainer = element.find('.post-image-content');

            if (scope.post.isAnimated) {
                element.on('click', function(e) {
                    e.preventDefault();
                });
                //var videoTagId = "vid_" + scope.post.id;

                /*var animationImg = element.find('.post-image-animation');
                 element.on('click', function(e){
                 e.preventDefault();
                 if (!animationImg.hasClass('loaded')) {
                 element.find('.animation-play-overlay').hide();
                 //animationImg.attr('src', animationImg.data('src'));
                 animationImg.addClass('loaded').show();
                 element.find('.thumb').hide();
                 } else if (animationImg.hasClass('paused')) {
                 } else {
                 // element.find('.thumb').show();
                 // animationImg.removeClass('loaded').hide();
                 //animationImg.attr('src', null);
                 element.find('.animation-play-overlay').show();
                 }
                 });*/
            }

            var resizeCard = function(){
                var postHeight = scope.post.image.height;
                if ((postHeight < 2500 || scope.big) && !imageContainer.hasClass('big-post')) {
                    //imageContainer.height($(this).height());
                } else {
                    imageContainer.height(500);
                    imageContainer.append('<a href="'+scope.post.href+'" class="view-more">نمایش کامل تصویر</a>');
                    imageContainer.addClass('big-post');
                }
            };
            var thumb = imageContainer.find('img.thumb');
            var setHeight = function(){
                if ((imageContainer.height() < 2500 || scope.big) && !imageContainer.hasClass('big-post')) {
                    imageContainer.height(thumb.height());
                } else {
                    imageContainer.height(500);
                    imageContainer.addClass('big-post');
                }
            };
            var debouncedSetHeight = _.debounce(setHeight, 200);
            thumb.on('load', resizeCard);
            resizeCard();
            $(window).on('resize', debouncedSetHeight);

            var cContainer = element.find('.post-image-content');
            cContainer.on('dblclick doubletap', function(e){
                e.preventDefault();
                if (scope.post.upVote()) {
                    cContainer.find('.upvote-big-icon').addClass('pop');
                    window.setTimeout(function(){
                        cContainer.find('.upvote-big-icon').removeClass('pop');
                    },700);
                }
            });
        }
        return {
            scope: {
                post: '=',
                big: '='
            },
            controller: ["$scope", "$interval", "$rootScope", function ($scope, $interval, $rootScope) {
                $scope.image = $scope.big ? $scope.post.imageBig : $scope.post.image;

                $scope.processed = false;
                $scope.processTrouble = false;
                var isChecking = false;
                var checks = 0;
                $scope.checkPostProcessed = function() {
                    if (!isChecking) {
                        isChecking = true;
                        $scope.processTrouble = false;
                        $scope.post.refreshMedia().then(function(){
                            if ($scope.post.checkProcessed()) {
                                setup();
                            } else {
                                if (checks > 10) {
                                    $scope.processTrouble = true;
                                }
                            }
                            isChecking = false;
                        }).then(function(){
                            if (checks > 10) {
                                $scope.processTrouble = true;
                            }
                            isChecking = false;
                        });
                    }
                };
                var processing = $interval(function() {
                    $scope.checkPostProcessed();
                    checks++;
                    if (checks > 10) {
                        $scope.processTrouble = true;
                        if (angular.isDefined(processing)) {
                            $interval.cancel(processing);
                            processing = undefined;
                        }
                    }
                }, 2000);

                var setup = function() {
                    $scope.image = $scope.big ? $scope.post.imageBig : $scope.post.image;
                    $rootScope.$emit('post.processed', $scope.post);
                    if (angular.isDefined(processing)) {
                        $interval.cancel(processing);
                        processing = undefined;
                    }
                    $scope.processed = true;
                };

                if ($scope.post.checkProcessed()) {
                    if (angular.isDefined(processing)) {
                        $interval.cancel(processing);
                        processing = undefined;
                    }
                    $scope.processed = true;
                }

                $scope.checkProcessed = function() {
                    $scope.post.refreshMedia().then(function(){
                        setup();
                    });
                };

                $scope.$on('$destroy', function() {
                    // Make sure that the interval is destroyed too
                    if (angular.isDefined(processing)) {
                        $interval.cancel(processing);
                        processing = undefined;
                    }
                });
            }],
            template: '<div class="post-image-content" data-ng-class="{\'animation\': post.isAnimated}">'+
                '<div data-ng-if="processed">'+
                '<div class="icon-overlay upvote-big-icon"><div class="icon-content"><i class="fa fa-thumbs-up"></i></div></div>'+
                '<div class="post-image post-image-animation" data-post-video="" data-post="post" data-big="big" data-ng-if="post.isAnimated"/>' +
                '<img class="post-image thumb" itemprop="contentUrl" width="[[ ::image.width ]]" height="[[ ::image.height ]]" data-ng-src="[[ ::image.url ]]"/>'+
                '</div>' +
                '<div class="video-not-ready" data-ng-if="!processed">' +
                '<div data-ng-switch="processTrouble">' +
                '<div data-ng-switch-when="true">' +
                '<h4>تصویر آماده نیست :(</h4>' +
                '<a href="javascript:void(0);" data-ng-click="checkPostProcessed()" class="btn btn-default btn-sm">دوباره چک کن!</a>' +
                '</div>' +
                '<div data-ng-switch-default>' +
                '<h4>در حال پردازش ...</h4>' +
                '<i class="ion-looping"></i>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>'
            ,
            link: link
        }
    }).
    directive('postVideo', function () {
        function link(scope, element, attrs) {
            element.find('.mobile-only').on('click', function(e) {
                e.preventDefault();
            });
        }
        return {
            scope: {
                post: '=',
                big: '='
            },
            controller: ["$sce", "$scope", "$rootScope", function ($sce, $scope, $rootScope) {
                $scope.API = null;
                $scope.onPlayerReady = function(API) {
                    $scope.API = API;
                };
                $scope.$on(EVENTS.VISIBLITY_CHANGED, function(e, visible) {
                    if (!$scope.API) return false;
                    if (visible) {
                        //$scope.API.play()
                    } else {
                        $scope.API.pause()
                    }
                });


                $scope.config = {};
                var setup = function(){
                    $scope.config = {
                        sources: [
                            {src: $sce.trustAsResourceUrl($scope.post.mp4Url), type: "video/mp4"},
                            {src: $sce.trustAsResourceUrl($scope.post.webmUrl), type: "video/webm"}
                        ],
                        plugins: {
                            poster: $scope.big ? $scope.post.imageBig.url : $scope.post.image.url
                        }
                    };
                };

                $rootScope.$on('post.processed', function (event, post) {
                    if (post.id === $scope.post.id) {
                        setup();
                    }
                });

                if ($scope.post.checkProcessed()) {
                    setup();
                }


            }],
            templateUrl: '/bundles/rasenninegag/js/common/views/post_video_content.html',
            link: link
        }
    }).
    directive('postItem', function () {
        function link(scope, element, attrs) {
            var listItem = element.parent('.list-item');
            listItem.find('.mobile-only').on('click', function(e) {
                e.preventDefault();
            });
            listItem.find('.post-dl-btn').on('click', function(e) {
                ga('send', 'event', 'post-dl-button', 'click', scope.post.id, 1);
                scope.post.download();
            });
            listItem.find('.post-share-btn').on('click', function(e) {
                ga('send', 'event', 'post-share-button', 'click', scope.post.id, 1);
                scope.post.share();
            });
            var postInfo = listItem.find('.post-info');
            if (scope.post.downloadsNo <= 0) {
                postInfo.find('.post-dl-btn, .sep.post-dl').remove();
            }
            if (scope.post.sharesNo <= 0) {
                postInfo.find('.post-share-btn, .sep.post-share').remove();
            }

            scope.post.appeared = false;
            var $window = $(window);
            function isAppeared(el, offsetTop) {
                if (!el.is(':visible')) {
                    return false;
                }


                var topOffset = el.data('appear-top-offset') ? el.data('appear-top-offset') : -offsetTop;
                var leftOffset = el.data('appear-left-offset') ? el.data('appear-left-offset') : 0;

                var window_left = $window.scrollLeft();
                var window_top = $window.scrollTop();
                var offset = el.offset();
                var left = offset.left;
                var top = offset.top;

                return (top + el.height() + topOffset >= window_top &&
                top - topOffset <= window_top + $window.height() &&
                left + el.width() >= window_left &&
                left - leftOffset <= window_left + $window.width());
            }
            var on_check = function() {
                if (isAppeared(listItem, 130)) {
                    scope.post.appeared = true;
                    scope.$emit(EVENTS.VISIBLITY_CHANGED, true);
                } else if(scope.post.appeared == true) {
                    scope.post.appeared = false;
                    scope.$emit(EVENTS.VISIBLITY_CHANGED, false);
                }
                if (isAppeared(listItem, -2000)) {
                    scope.post.hideable = false;
                } else {
                    scope.post.hideable = true;
                }
            };
            var throttledCheck = _.throttle(on_check, 200);
            $window.scroll(throttledCheck).resize(throttledCheck);
        }
        return {
            scope: {
                post: '='
            },
            controller: ['$scope', 'UsersManager', 'ajaxConfig', function($scope, UsersManager, ajaxConfig) {
                $scope.currentUser = UsersManager.getCurrentUser();
                $scope.isLoggedIn = UsersManager.isLoggedIn;
                $scope.categories = ajaxConfig.categories;
                $scope.initEdit = function(){
                    $scope.editable = true;
                    $scope.post.editedCategory = $scope.post.category;
                    $scope.post.editedPostTitle = $scope.post.postTitle;
                };
                $scope.cancelEdit = function(){
                    $scope.editable = false;
                };
                $scope.saveEdit = function(){
                    $scope.post.update($scope.post.editedPostTitle, $scope.post.editedCategory).then(function(){
                        $scope.editable = false;
                    }, function(){

                    });

                };
            }],
            templateUrl: '/bundles/rasenninegag/js/common/views/post_item.html',
            link: link
        }
    }).
    directive('postsList', function () {
        return {
            scope: {
                posts: '='
            },
            controller: ['$scope', 'UsersManager', function($scope, UsersManager) {
                $scope.currentUser = UsersManager.getCurrentUser();
                $scope.isLoggedIn = UsersManager.isLoggedIn;
            }],
            templateUrl: '/bundles/rasenninegag/js/common/views/post_card.html'
        };
    }).
    directive('bsTooltip', function(){
        return {
            restrict: 'A',
            link: function(scope, element, attrs){
                $(element).tooltip({container: "body", placement: "top"});
            }
        };
    }).
    directive('txtInput', function(){
        return {
            scope: {
                model: '=',
                disabled: '=',
                submit: '&'
            },
            link: function(scope, element, attrs){
                element.keypress(function (e) {
                    if (e.which == 13) {
                        scope.submit();
                        return false;
                    }
                });
            },
            template: '<input type="text" maxlength="100" name="title" data-ng-model="model" data-ng-disabled="disabled" placeholder="عنوان عکس (حداکثر 100 حرف)">'
        };
    }).
    directive('adItem', function () {
        return {
            scope: {
                ad: '='
            },
            controller: ['$scope', function($scope) {

            }],
            templateUrl: '/bundles/rasenninegag/js/common/views/ad_item.html'
        };
    })
;