/**
 * Created by Nima on 11/14/2014.
 */
angular.module('approveStatusApp', []).
    directive('approveStatus', function () {
        return {
            scope: {
                approved: '='
            },
            controller: ['$scope', function ($scope){
                $scope.getStatusIcon = function(approved) {
                    if (approved == true) {
                        return 'fa-check-circle';
                    } else if (approved == false) {
                        return 'fa-times-circle';
                    } else {
                        return 'fa-dot-circle-o';
                    }
                };
                $scope.getStatusClass = function(approved) {
                    if (approved == true) {
                        return 'approved';
                    } else if (approved == false) {
                        return 'not-approved';
                    } else {
                        return 'pending';
                    }
                };
                $scope.getStatusText = function(approved) {
                    if (approved == true) {
                        return 'تایید شده';
                    } else if (approved == false) {
                        return 'تایید نشده';
                    } else {
                        return 'در انتظار تایید';
                    }
                };
            }],
            template:
            '<span title="[[ getStatusText(approved) ]]" class="approval-status" data-ng-class="getStatusClass(approved)">'+
            '<i class="fa" data-ng-class="getStatusIcon(approved)"></i>'+
            '</span>'
        }
    });