'use strict';

/* Services */

angular.module('postsCategoryAppServices', []).
    service('PostsService', ['Posts', 'ajaxConfig', function (Posts, ajaxConfig) {
        var baseUrl = ajaxConfig.baseUrl;
        var service = {
            getCategoryPosts: getCategoryPosts
        };
        var basePostsUrl =  baseUrl + '/posts';
        function getCategoryPosts(category, filter) {
            if (filter == null || filter == undefined || filter == '') filter = 'fresh';
            if (category == null || category == undefined || category == '') category = category;
            var categoryPostsUrl = basePostsUrl + '/category/' + category + '/' + filter;
            return new Posts(categoryPostsUrl);
        }

        return service;
    }])
;