'use strict';

/* Controllers */

var postsCategoryAppControllers = angular.module('postsCategoryAppControllers', []);

postsCategoryAppControllers
    .controller('MainCtrl', ['$scope',
        function($scope)
        {
            $scope.loading = false;
        }])
    .controller('PostsCategoryCtrl', ['$scope', '$rootScope', '$stateParams', 'PostsService',
        function($scope, $rootScope, $stateParams, PostsService)
        {
            $scope.category = category;
            $scope.filter = $stateParams.filter;
            $scope.posts = PostsService.getCategoryPosts($scope.category, $scope.filter);
            $scope.posts.load();
        }]);