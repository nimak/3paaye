'use strict';

/* App Module */

var postsCategoryListApp = angular.module('postsCategoryListApp', [
    'ui.router',
    'postsCategoryAppServices',
    'postsCategoryAppControllers',
    'postsManagerApp',
    'userConfigApp'
]);

postsCategoryListApp
    .run(
    ['$http', '$templateCache', '$rootScope', '$state', '$stateParams',
        function ($http, $templateCache, $rootScope, $state, $stateParams) {
            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;

            var cacheTemplate = function (templateUrl) {
                $http.get(templateUrl).success(function (t) {
                    $templateCache.put(templateUrl, t);
                });
            };
            cacheTemplate('/bundles/rasenninegag/js/posts_app/views/posts_list.html');
        }
    ]
)
    .config(['$interpolateProvider', '$stateProvider', '$urlRouterProvider', '$locationProvider',
        function($interpolateProvider, $stateProvider, $urlRouterProvider, $locationProvider) {
            $interpolateProvider.startSymbol('[[').endSymbol(']]');

            $urlRouterProvider.when('/', '/fresh');
            $urlRouterProvider.otherwise("/fresh");

            $stateProvider
                .state('category', {
                    template: '<div data-ui-view=""></div>',
                    controller: 'PostsCategoryCtrl'
                })
                .state('category.list', {
                    url: "/:filter",
                    templateUrl: '/bundles/rasenninegag/js/posts_app/views/posts_list.html',
                    controller: 'PostsCategoryCtrl'
                })
            ;
            $locationProvider.html5Mode(true);
        }]);

angular.element().ready(function() {
    angular.bootstrap('.main-wrapper', ['postsCategoryListApp']);
});

