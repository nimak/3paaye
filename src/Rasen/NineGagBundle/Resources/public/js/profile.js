'use strict';

$(document).ready(function() {
    var profileTabs = $('#profileTabs');
    if( $(window).width() > 467 ) {
        profileTabs.affix({offset: {
            top: profileTabs.offset().top - 50
        }});
    }
    var mainContainer = $('div.posts-list.profile');
    var setContainerPos = function() {
        mainContainer.css('margin-top', profileTabs.outerHeight() + 20);
    };
    var debouncedSCP = _.debounce(setContainerPos, 100);
    $(window).resize(debouncedSCP);
    $( window ).load(function() {
        setContainerPos()
    });
});