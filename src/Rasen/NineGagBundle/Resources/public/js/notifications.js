/**
 * Created by Nima on 11/8/2014.
 */
'use strict';

$(document).ready(function() {

    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (settings.type == 'POST' || settings.type == 'PUT' || settings.type == 'DELETE') {
                if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                    // Only send the token to relative URLs i.e. locally.
                    xhr.setRequestHeader("X-XSRF-TOKEN", docCookies.getItem('XSRF-TOKEN'));
                }
            }
        }
    });

    var data, authPayload, notifId;

    var docTitle = $('title').data('title');

    makeClickable($('.notification-item'));

    function notificationTemplate(id, notification)
    {
        notifId = "n"+id;
        //var m = moment(notification.timestamp);
        //var jTimestamp = m.format('jD jMMMM jYYYY HH:mm:ss');
        return '<div id="' + notifId + '" class="list-group-item notification-item' + (notification.read ? '':' new') + '" data-href="' + notification.link + '">' +
        '<div class="notification-image">' + notification.image + '</div>' +
        '<div class="notification-content"><div class="content">' + notification.content + '</div>' +
        '<div class="clearfix">' +
            '<div class="notification-icon">' + notification.icon  + '</div>' +
            '<div class="notification-timestamp" title="'+ notification.timestamp + '">'+ notification.timestamp + '</div>' +
        '</div>' +
        '</div>' +
        '</div>';

    }

    var notificationDropdown = $('#notificationDropdown');
    var notifLoadingSpinner = notificationDropdown.find('.loading');
    var notificationList = $('.notification-list');
    var notificationDropdownList = $( "#notificationList" );
    var notificationEmptyMsg = notificationDropdown.find('.empty-msg');
    var unreadContainer = notificationDropdown.find('.unread-no');
    var unreadCount = 0;
    var firstLoad = true;
    var loading = false;

    function makeClickable(item){
        var f = function(e){
            var target = $(e.target);
            if (target.data("href")) {
                location.href = target.data("href");
            } else if (target.parents('.notification-item').data("href")) {
                location.href = target.parents('.notification-item').data("href");
            }
        };
        item.on('click', f);
        item.find('.notification-image', '.notification-content').on('click', f);
    }

    function markNotNew()
    {
        var notif = notificationDropdownList.find('.notification-item');
        window.setTimeout(function(){
            notif.animate({
                backgroundColor: '#fff'
            }, 500, function(){
                notif.removeClass('new').removeAttr( 'style' );
            });
        },500);
    }

    function markAllRead()
    {
        if (unreadCount > 0) {
            $.ajax({
                type: "POST",
                url: Routing.generate('ajax_notification_mark_all_read', [], true),
                cache: false,
                converters: {
                    "text json": secureJsonConverter
                },
                headers: {"X-XSRF-TOKEN": docCookies.getItem('XSRF-TOKEN')},
                data: { _token: ajaxToken }
            }).success(function(){
                setUnread(0);
                updateUnreadCounter(0);
                markNotNew();
            });
        }
    }

    function increaseUnread()
    {
        var currentNo = parseInt(unreadCount);
        setUnread(currentNo+1);
        updateUnreadCounter(currentNo+1);
    }
    function setUnread(count)
    {
        unreadCount = parseInt(count);
    }
    function updateUnreadCounter(count)
    {
        unreadContainer.text(count);
        if (count > 0) {
            unreadContainer.show();
            document.title = '(' + count + ') ' + docTitle
        } else {
            unreadContainer.hide();
            document.title = docTitle;
        }
    }

    if (firstLoad && !loading) {
        $.ajax({
            url: Routing.generate('ajax_notification_latest', [], true),
            cache: false,
            dataType: "json",
            converters: {
                "text json": secureJsonConverter
            },
            headers: {"X-XSRF-TOKEN": docCookies.getItem('XSRF-TOKEN')},
            beforeSend: function() {
                notificationDropdownList.hide();
                notificationEmptyMsg.hide();
                notifLoadingSpinner.show();
                loading = true;
            }
        }).success(function(data) {
            var result = data.result;
            notifLoadingSpinner.hide();
            notificationEmptyMsg.hide();
            var notifications = result['messages'];
            if (notifications.length > 0) {
                notificationDropdownList.show();
                $.each( notifications, function( key, notification ) {
                    notificationDropdownList.append(notificationTemplate(key, notification));
                });
                var notifs = notificationDropdownList.find('.notification-item');
                makeClickable(notifs);
                notificationDropdownList.find('.notification-timestamp').timeago();
                firstLoad = false;
            } else {
                notificationEmptyMsg.show();
            }
            setUnread(result['totalUnreadCount']);
            notificationDropdownList.perfectScrollbar({
                suppressScrollX: true
            });
        }).error(function(xhr, textStatus, errorThrown) {
            console.log(textStatus);
            loading = false;
        }).complete(function(){
            loading = false;
        });
    }

    $('#notificationDropdownBtn').on('click', function (event) {
        if ($(this).parent().hasClass("open")) markAllRead();
    });

    authPayload = {'token': token};
    var sock = new SockJS('https://social.demo.sepehrdata.com:8080/socks');
    sock.onopen = function () {
        console.log('open');
        sock.send(JSON.stringify(authPayload));
    };
    sock.onmessage = function (e) {
        data = JSON.parse(e.data);
        if (data.data_type == 'notification') {
            $('.notification-list').prepend(
                notificationTemplate(data.id, data)
            );
            notificationList.find('#n'+data.id).find('.notification-timestamp').timeago();
            makeClickable(notificationList.find('#n'+data.id));
            if (!notificationDropdown.hasClass('open')) increaseUnread();
            notificationDropdownList.find('.notification-item').slice(-1).remove();
        } else if (data.data_type == 'server_msg') {
            console.log(data.content);
        }

    };
    sock.onclose = function () {
        console.log('close');
    };
});