var showFiltersBtn, filtersContainer;
$(document).ready(function() {
    showFiltersBtn = $('#showFilters');
    filtersContainer = $('#filtersContainer');
    showFiltersBtn.on('click', function(e){
        if (filtersContainer.is(':visible')) {
            filtersContainer.slideUp();
            showFiltersBtn.text('نمایش فیلتر‌ها')
            showFiltersBtn.removeClass('active');
        } else {
            filtersContainer.slideDown();
            showFiltersBtn.text('مخفی کردن فیلتر‌ها')
            showFiltersBtn.addClass('active');
        }
    });
    $(".colorpicker").spectrum({
        preferredFormat: "name",
        allowEmpty: true,
        showAlpha: true,
        showButtons: false
    });
    tinymce.init({
        selector: "textarea.tinymce",
        directionality : 'rtl',
        plugins: "code preview link charmap table contextmenu paste insertdatetime visualblocks searchreplace",
        toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link preview code"
    });
});
