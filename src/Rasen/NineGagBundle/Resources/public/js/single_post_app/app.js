'use strict';

/* App Module */

var singlePostApp = angular.module('singlePostApp', [
    'singlePostAppControllers',
    'approveStatusApp',
    'postsManagerApp',
    'timeApp',
    'commentsManagerApp'
]);

singlePostApp.config(['$interpolateProvider',
    function($interpolateProvider) {
        $interpolateProvider.startSymbol('[[').endSymbol(']]');
    }]);

angular.element().ready(function() {
    angular.bootstrap('.main-wrapper', ['singlePostApp']);
});

