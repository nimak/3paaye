'use strict';

/* Controllers */


var singlePostAppControllers = angular.module('singlePostAppControllers', []);

singlePostAppControllers
    .controller('PostCtrl', ['$rootScope', '$scope', 'Post', 'ajaxConfig', 'UsersManager',
        function($rootScope, $scope, Post, ajaxConfig, UsersManager)
        {
            $scope.post = new Post(post);
            $scope.currentUser = UsersManager.getCurrentUser();
            $scope.isLoggedIn = UsersManager.isLoggedIn;
            $scope.categories = ajaxConfig.categories;
            $scope.initEdit = function(){
                $scope.editable = true;
                $scope.post.editedCategory = $scope.post.category;
                $scope.post.editedPostTitle = $scope.post.postTitle;
            };
            $scope.cancelEdit = function(){
                $scope.editable = false;
            };
            $scope.saveEdit = function(){
                $scope.post.update($scope.post.editedPostTitle, $scope.post.editedCategory).then(function(){
                    $scope.editable = false;
                }, function(){

                });

            };
            $rootScope.$on('post.delete', function (event, post) {
                window.location.href = post.category.href;
            });
            var postInfo = $('.post-info');
            if ($scope.post.downloadsNo <= 0) {
                postInfo.find('.post-dl-btn, .sep.post-dl').css('cssText', 'display:none !important');
            }
            if ($scope.post.sharesNo <= 0) {
                postInfo.find('.post-share-btn, .sep.post-share').css('cssText', 'display:none !important');
            }
        }])
;