'use strict';

/* Controllers */


var postsAppControllers = angular.module('postsAppControllers', []);

postsAppControllers
    .controller('MainCtrl', ['$scope', '$rootScope',
        function($scope, $rootScope)
        {
            $scope.loading = false;
        }])
    .controller('PostsCtrl', ['$scope', '$rootScope', 'PostsService',
        function($scope, $rootScope, PostsService)
        {
            $scope.filter = null;
            $scope.posts = PostsService.getPosts($scope.filter);
            $scope.posts.load();

        }])
    .controller('PostsFreshCtrl', ['$scope', '$rootScope', 'PostsService',
        function($scope, $rootScope, PostsService)
        {
            $scope.filter = 'fresh';
            $scope.posts = PostsService.getPosts($scope.filter);
            $scope.posts.load();

        }])
    .controller('PostsNsfwCtrl', ['$scope', '$rootScope','PostsService',
        function($scope, $rootScope, PostsService)
        {
            $scope.filter = 'nsfw';
            $scope.posts = PostsService.getPosts($scope.filter);
            $scope.posts.load();

        }]);