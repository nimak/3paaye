'use strict';

/* Services */

angular.module('postsAppServices', []).
    service('PostsService', ['ajaxConfig', 'Posts', function (ajaxConfig, Posts) {
        var baseUrl = ajaxConfig.baseUrl;
        var service = {
            getPosts: getPosts,
            getCategoryPosts: getCategoryPosts
        };
        var basePostsUrl =  baseUrl + '/posts';

        function getPosts(filter) {
            if (filter == null || filter == undefined || filter == '') filter = 'fresh';
            var postsUrl = basePostsUrl + '/' + filter;
            return new Posts(postsUrl);
        }

        function getCategoryPosts(category, filter) {
            if (filter == null || filter == undefined || filter == '') filter = 'fresh';
            var categoryPostsUrl = basePostsUrl + '/category/' + category + '/' + filter;
            return new Posts(categoryPostsUrl);
        }

        return service;
    }])
;