<?php
/**
 * Created by PhpStorm.
 * User: AmiR
 * Date: 10/24/14
 * Time: 11:16 PM
 */
namespace  Rasen\NineGagBundle\Security\Core\User;

use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\FOSUBUserProvider as BaseClass;
use Rasen\NineGagBundle\Entity\User;
use Symfony\Component\Security\Core\User\UserInterface;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class FOSUBUserProvider
 *
 * @package Rasen\NineGagBundle\Security\Core\User
 */
class FOSUBUserProvider extends BaseClass
{

    /**
     * {@inheritdoc}
     */
    public function connect(UserInterface $user, UserResponseInterface $response)
    {
        $property = $this->getProperty($response);
        $username = $response->getUsername();

        //on connect - get the access token and the user ID
        $service = $response->getResourceOwner()->getName();

        $setter = 'set'.ucfirst($service);
        $setter_id = $setter.'Id';
        $setter_token = $setter.'AccessToken';

        //we "disconnect" previously connected users
        if (null !== $previousUser = $this->userManager->findUserBy(array($property => $username))) {
            $previousUser->$setter_id(null);
            $previousUser->$setter_token(null);
            $this->userManager->updateUser($previousUser);
        }

        //we connect current user
        $user->$setter_id($username);
        $user->$setter_token($response->getAccessToken());

        $this->userManager->updateUser($user);
    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
        $username = $response->getUsername();
        $useremail = $response->getEmail();	// get facebook email id
        $user = $this->userManager->findUserByEmail($useremail);
        //$user = $this->userManager->findUserBy(array($this->getProperty($response) => $useremail));
        //when the user is registrating
        if (null === $user) {
            $service = $response->getResourceOwner()->getName();
            $setter = 'set'.ucfirst($service);
            $setter_id = $setter.'Id';
            $setter_token = $setter.'AccessToken';
            // create new user here
            $user = $this->userManager->createUser();
            $user->$setter_id($username);
            $user->$setter_token($response->getAccessToken());

            $parts = explode('@', $useremail);
            $newUsername = $parts[0];
            while (true) {
                if ($this->userManager->findUserByUsername($newUsername)) {
                    $newUsername = $parts[0].rand('1','99999');
                } else {
                    $user->setUsername($newUsername);
                    break;
                }
            }

            $realName = trim($response->getRealName());
            $firstName = strstr($realName, ' ', true) != '' ? strstr($realName, ' ', true) : $realName;
            $lastName = strstr($realName, ' ');
            $user->setFirstName(trim($firstName));
            $user->setLastName(trim($lastName));
            $user->setIsTrustedUser(true);
            $user->setGender(User::GENDER_NOT_KNOWN);
            $user->setEmail($useremail);
            $user->setPassword($username);
            $user->setEnabled(true);
            $this->userManager->updateUser($user);
            return $user;
        }

        $user = $this->userManager->findUserByEmail($useremail);

        $serviceName = $response->getResourceOwner()->getName();
        $setter = 'set' . ucfirst($serviceName) . 'AccessToken';

        //update access token
        $user->$setter($response->getAccessToken());

        return $user;
    }
}