<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/6/2014
 * Time: 10:57 PM
 */

namespace Rasen\NineGagBundle\Security\Authorization\Voter;

use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Authorization\Voter\RoleHierarchyVoter;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class CommentVoter
 *
 * This class decides user authorization for Comment. (Some kind of ACL)
 *
 * @package Rasen\NineGagBundle\Security\Authorization\Voter
 *
 * @DI\Service("security.access.comment_voter", public=false)
 * @DI\Tag("security.voter")
 */
class CommentVoter implements VoterInterface
{
    const EDIT = 'edit';
    const DELETE = 'delete';

    /**
     * @var RoleHierarchyVoter $roleHierarchyVoter
     */
    private $roleHierarchyVoter;

    /**
     * @DI\InjectParams({
     *     "roleHierarchyVoter" = @DI\Inject("security.access.role_hierarchy_voter")
     * })
     * @param RoleHierarchyVoter $roleHierarchyVoter
     */
    public function __construct(RoleHierarchyVoter $roleHierarchyVoter){
        $this->roleHierarchyVoter = $roleHierarchyVoter;
    }

    /**
     * Checks if the voter supports the given attribute.
     *
     * @param string $attribute
     * @return bool
     */
    public function supportsAttribute($attribute)
    {
        return in_array($attribute, array(
                self::EDIT,
                self::DELETE,
            ));
    }

    /**
     * Checks if the voter supports the class.
     *
     * @param string $class
     * @return bool
     */
    public function supportsClass($class)
    {
        $supportedClass = 'Rasen\NineGagBundle\Entity\Comment';

        return $supportedClass === $class || is_subclass_of($class, $supportedClass);
    }

    /**
     *
     * @var \Rasen\NineGagBundle\Entity\Comment $comment
     * @param TokenInterface $token
     * @param \Rasen\NineGagBundle\Entity\Comment $comment
     * @param array $attributes
     * @return int
     */
    public function vote(TokenInterface $token, $comment, array $attributes)
    {
        // check if class of this object is supported by this voter
        if (!$this->supportsClass(get_class($comment))) {
            return VoterInterface::ACCESS_ABSTAIN;
        }

        // check if the voter is used correct
        if(1 !== count($attributes)) {
            throw new \InvalidArgumentException(
                'Only one attribute is allowed'
            );
        }

        // set the attribute to check against
        $attribute = $attributes[0];

        // check if the given attribute is covered by this voter
        if (!$this->supportsAttribute($attribute)) {
            return VoterInterface::ACCESS_ABSTAIN;
        }

        // get current logged in user
        $user = $token->getUser();

        // make sure there is a user object (i.e. that the user is logged in)
        if (!$user instanceof UserInterface) {
            return VoterInterface::ACCESS_DENIED;
        }

        switch($attribute) {
            case self::EDIT:
                $hasRoleEditComments = ($this->roleHierarchyVoter->vote($token, $comment, array('ROLE_EDIT_COMMENTS')) == VoterInterface::ACCESS_GRANTED);
                if ($user->getId() === $comment->getCreatedBy()->getId() || $hasRoleEditComments) {
                    return VoterInterface::ACCESS_GRANTED;
                }
                break;
            case self::DELETE:
                $hasRoleDeleteComments = ($this->roleHierarchyVoter->vote($token, $comment, array('ROLE_DELETE_COMMENTS')) == VoterInterface::ACCESS_GRANTED);
                if ($user->getId() === $comment->getCreatedBy()->getId() ||
                    $user->getId() === $comment->getPost()->getCreatedBy()->getId() ||
                    $hasRoleDeleteComments
                ) {
                    return VoterInterface::ACCESS_GRANTED;
                }
                break;
        }

        return VoterInterface::ACCESS_DENIED;
    }
} 