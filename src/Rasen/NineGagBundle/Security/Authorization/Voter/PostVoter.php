<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/6/2014
 * Time: 11:09 PM
 */

namespace Rasen\NineGagBundle\Security\Authorization\Voter;

use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Authorization\Voter\RoleHierarchyVoter;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class PostVoter
 *
 * This class decides user authorization for Post.
 *
 * @package Rasen\NineGagBundle\Security\Authorization\Voter
 *
 * @DI\Service("security.access.post_voter", public=false)
 * @DI\Tag("security.voter")
 */
class PostVoter implements VoterInterface
{

    const EDIT = 'edit';
    const DELETE = 'delete';

    /**
     * @var RoleHierarchyVoter $roleHierarchyVoter
     */
    private $roleHierarchyVoter;

    /**
     * @DI\InjectParams({
     *     "roleHierarchyVoter" = @DI\Inject("security.access.role_hierarchy_voter")
     * })
     * @param RoleHierarchyVoter $roleHierarchyVoter
     */
    public function __construct(RoleHierarchyVoter $roleHierarchyVoter){
        $this->roleHierarchyVoter = $roleHierarchyVoter;
    }

    /**
     * Checks if the voter supports the given attribute.
     *
     * @param string $attribute
     * @return bool
     */
    public function supportsAttribute($attribute)
    {
        return in_array($attribute, array(
                self::EDIT,
                self::DELETE,
            ));
    }

    /**
     * Checks if the voter supports the class.
     *
     * @param string $class
     * @return bool
     */
    public function supportsClass($class)
    {
        $supportedClass = 'Rasen\NineGagBundle\Entity\Post';

        return $supportedClass === $class || is_subclass_of($class, $supportedClass);
    }

    /**
     *
     * @var \Rasen\NineGagBundle\Entity\Post $post
     * @param TokenInterface $token
     * @param \Rasen\NineGagBundle\Entity\Post $post
     * @param array $attributes
     * @return int
     */
    public function vote(TokenInterface $token, $post, array $attributes)
    {
        // check if class of this object is supported by this voter
        if (!$this->supportsClass(get_class($post))) {
            return VoterInterface::ACCESS_ABSTAIN;
        }

        // check if the voter is used correct
        if(1 !== count($attributes)) {
            throw new \InvalidArgumentException(
                'Only one attribute is allowed'
            );
        }

        // set the attribute to check against
        $attribute = $attributes[0];

        // check if the given attribute is covered by this voter
        if (!$this->supportsAttribute($attribute)) {
            return VoterInterface::ACCESS_ABSTAIN;
        }

        // get current logged in user
        $user = $token->getUser();

        // make sure there is a user object (i.e. that the user is logged in)
        if (!$user instanceof UserInterface) {
            return VoterInterface::ACCESS_DENIED;
        }

        switch($attribute) {
            case self::EDIT:
                $hasRoleEditPosts = ($this->roleHierarchyVoter->vote($token, $post, array('ROLE_EDIT_POSTS')) == VoterInterface::ACCESS_GRANTED);
                if ($user->getId() === $post->getCreatedBy()->getId() || $hasRoleEditPosts) {
                    return VoterInterface::ACCESS_GRANTED;
                }
                break;
            case self::DELETE:
                $hasRoleDeletePosts = ($this->roleHierarchyVoter->vote($token, $post, array('ROLE_DELETE_POSTS')) == VoterInterface::ACCESS_GRANTED);
                if ($user->getId() === $post->getCreatedBy()->getId() || $hasRoleDeletePosts) {
                    return VoterInterface::ACCESS_GRANTED;
                }
                break;
        }

        return VoterInterface::ACCESS_DENIED;
    }
} 