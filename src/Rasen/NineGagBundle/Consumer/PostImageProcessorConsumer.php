<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 11/28/2014
 * Time: 2:08 AM
 */

namespace Rasen\NineGagBundle\Consumer;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\QueryException;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use JMS\DiExtraBundle\Annotation as DI;
use Rasen\NineGagBundle\Entity\PostImage;

/**
 * Class PostImageProcessorConsumer
 *
 * @DI\Service("rasen_ninegag.post_image_processor_consumer")
 *
 * @package Rasen\NineGagBundle\Consumer
 */
class PostImageProcessorConsumer implements ConsumerInterface
{
	/**
	 * @var EntityManager $em
	 */
	private $em;

	/**
	 * * @DI\InjectParams({
	 *     "em" = @DI\Inject("doctrine.orm.entity_manager")
	 * })
	 * @param EntityManager $em
	 */
	public function __construct(EntityManager $em)
	{
		$this->em = $em;
	}
	public function execute(AMQPMessage $msg)
	{
		//Process picture upload.
		//$msg will be an instance of `PhpAmqpLib\Message\AMQPMessage` with the $msg->body being the data sent over RabbitMQ.

		$postInfo = json_decode($msg->body);

		$postId = $postInfo->postId;
		$width = $postInfo->width;
		$height = $postInfo->height;
		$size = $postInfo->size;

		$sizeKb = intval(str_replace('B', '', $size)) / 1024;

		echo 'Received Info for Post #'.$postId . PHP_EOL;

		$q = $this->em->getRepository('RasenNineGagBundle:PostImage')->createQueryBuilder('p');

		$q = $q->update('RasenNineGagBundle:PostImage', 'p')
			->where('p.id = :postId')
			->set('p.width', ':width')
			->set('p.height', ':height')
			->set('p.size', ':size')
			->set('p.isProcessed', true)
			->getQuery();

		$q = $q
			->setParameter(':postId', $postId)
			->setParameter(':width', $width)
			->setParameter(':height', $height)
			->setParameter(':size', $sizeKb);
			;

		try {
			$result = $q->execute();
			echo 'Done!' . PHP_EOL;
			return true;
		} catch(QueryException $e) {
			echo 'Query Error' . PHP_EOL;
			return false;
		}



	}
}