<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/9/2014
 * Time: 3:26 PM
 */

namespace Rasen\NineGagBundle\Entity;

use FOS\OAuthServerBundle\Entity\Client as BaseClient;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="clients")
 * @ORM\Entity
 *
 * @author Nima Karimi <nima.k68@gmail.com>
 */
class Client extends BaseClient
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	public function __construct()
	{
		parent::__construct();

	}
}