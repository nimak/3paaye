<?php

namespace Rasen\NineGagBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

use JMS\Serializer\Annotation as Serializer;

/**
 * Advertisement
 * @ORM\Table(name="advertisement")
 * @ORM\Entity(repositoryClass="Rasen\NineGagBundle\Entity\AdvertisementRepository")
 *
 * @Vich\Uploadable
 *
 * @Serializer\ExclusionPolicy("none")
 *
 * @author Amir Eslami <amir_eslami_k@yahoo.com>
 */
class Advertisement
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="smallint", options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @Serializer\Exclude
     */
    private $id;

    /**
     * @var string
     *
     * @Serializer\Expose
     * @Serializer\Type("string")
     *
     * @ORM\Column(name="content", type="text", nullable=false)
     *
     * @Assert\NotBlank(message = "advertisement.content.not_blank")
     */
    private $content;


	/**
	 * @var string
	 *
	 * @Serializer\Groups({"default", "own"})
	 *
	 * @Assert\Length(
	 *      min = 1,
	 *      max = 100,
	 *      minMessage = "advertisement.title.length_min",
	 *      maxMessage = "advertisement.title.length_max"
	 * )
	 *
	 * @ORM\Column(name="advertisement_title", type="string", length=100, nullable=true)
	 *
	 */
	protected $advertisementTitle;

    /**
     * @var string
     *
     * @Serializer\Groups({"default", "own"})
     *
     * @ORM\Column(name="link", type="string", length=100, nullable=true)
     *
     */
    private $link;

    /**
     * Ad position
     *
     * @var integer
     *
     * @Serializer\Exclude
     *
     * @ORM\Column(name="position", type="integer", nullable=true, options={"unsigned":true})
     */
	protected $position;

    /**
     * Probability of an ad appearing in the posts list.
     *
     * @var integer
     *
     * @Serializer\Exclude
     *
     * @ORM\Column(name="appearance_chance", type="integer", nullable=true, options={"unsigned":true})
     */
	protected $appearanceChance;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false, options={"unsigned":true, "default":0})
     */
    private $active;



    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     *
     * @Serializer\Exclude
     *
     * @ORM\Column(name="last_modified_time", type="datetime", nullable=true)
     */
    protected $lastModifiedTime;

    /**
     * @var \Rasen\NineGagBundle\Entity\User
     *
     * @Gedmo\Blameable(on="update")
     *
     * @ORM\ManyToOne(targetEntity="Rasen\NineGagBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="last_modified_by", referencedColumnName="id", nullable=true)
     * })
     *
     * @Serializer\Exclude
     *
     */
    protected $lastModifiedBy;


    /**
     * @var \Rasen\NineGagBundle\Entity\User
     *
     * @Gedmo\Blameable(on="create")
     *
     * @Serializer\Exclude
     *
     * @ORM\ManyToOne(targetEntity="Rasen\NineGagBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="created_by", referencedColumnName="id", nullable=false)
     * })
     */
    private $createdBy;


    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     *
     * @Serializer\Exclude
     *
     * @ORM\Column(name="created_time", type="datetime", nullable=false)
     */
    private $createdTime;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Notice
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set advertisementTitle
     *
     * @param string $advertisementTitle
     * @return Advertisement
     */
    public function setAdvertisementTitle($advertisementTitle)
    {
        $this->advertisementTitle = $advertisementTitle;

        return $this;
    }

    /**
     * Get advertisementTitle
     *
     * @return string
     */
    public function getAdvertisementTitle()
    {
        return $this->advertisementTitle;
    }

    /**
     * Set link
     *
     * @param string $link
     * @return Advertisement
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return Advertisement
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get appearanceChance
     *
     * @return integer
     */
    public function getAppearanceChance()
    {
        return $this->appearanceChance;
    }

    /**
     * Set appearanceChance
     *
     * @param integer $appearanceChance
     * @return Advertisement
     */
    public function setAppearanceChance($appearanceChance)
    {
        $this->appearanceChance = $appearanceChance;

        return $this;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Advertisement
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set lastModifiedTime
     *
     * @param \DateTime $lastModifiedTime
     * @return Advertisement
     */
    public function setLastModifiedTime($lastModifiedTime)
    {
        $this->lastModifiedTime = $lastModifiedTime;

        return $this;
    }

    /**
     * Get lastModifiedTime
     *
     * @return \DateTime
     */
    public function getLastModifiedTime()
    {
        return $this->lastModifiedTime;
    }

    /**
     * Set lastModifiedBy
     *
     * @param \Rasen\NineGagBundle\Entity\User $lastModifiedBy
     * @return Advertisement
     */
    public function setLastModifiedBy(\Rasen\NineGagBundle\Entity\User $lastModifiedBy = null)
    {
        $this->lastModifiedBy = $lastModifiedBy;

        return $this;
    }

    /**
     * Get lastModifiedBy
     *
     * @return \Rasen\NineGagBundle\Entity\User
     */
    public function getLastModifiedBy()
    {
        return $this->lastModifiedBy;
    }

    /**
     * Set createdTime
     *
     * @param \DateTime $createdTime
     * @return Post
     */
    public function setCreatedTime($createdTime)
    {
        $this->createdTime = $createdTime;

        return $this;
    }

    /**
     * Get createdTime
     *
     * @return \DateTime
     */
    public function getCreatedTime()
    {
        return $this->createdTime;
    }

    /**
     * Set createdBy
     *
     * @param \Rasen\NineGagBundle\Entity\User $createdBy
     * @return Advertisement
     */
    public function setCreatedBy(\Rasen\NineGagBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \Rasen\NineGagBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

}
