<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 11/01/2014
 * Time: 9:17 PM
 */

namespace Rasen\NineGagBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * Class AdvertisementRepository
 * @package Rasen\NineGagBundle\Entity
 */
class AdvertisementRepository extends EntityRepository
{

	public function findAds($limit = null, $random = false)
	{
        $count = $this->createQueryBuilder('a')
            ->select('COUNT(a)')
            ->where('a.active = true')
            ->getQuery()
            ->getSingleScalarResult();
        $q = $this->createQueryBuilder('a')
        ->where('a.active = true');
        if ($limit !== null) {
            $q = $q->setMaxResults($limit);
        }
        if ($random) {
            $q = $q->setFirstResult(rand(0, $count - 1));
        }
        return $q->getQuery()
            ->getResult()
            ;
	}

} 