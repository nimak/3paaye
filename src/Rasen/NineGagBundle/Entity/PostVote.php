<?php

namespace Rasen\NineGagBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * PostVote
 *
 * @ORM\Table(name="posts_votes", uniqueConstraints={@ORM\UniqueConstraint(name="post_id_voted_by_UNIQUE", columns={"post_id", "voted_by"})}, indexes={@ORM\Index(name="posts_votes_post_id_idx", columns={"post_id"}), @ORM\Index(name="posts_votes_voted_by_idx", columns={"voted_by"})})
 * @ORM\Entity
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 *
 * @UniqueEntity(
 *     fields={"post", "votedBy"},
 *     errorPath="post",
 *     message="post.voted_by.unique"
 * )
 *
 * @author Nima Karimi <nima.k68@gmail.com>
 */
class PostVote
{
	const DOWNVOTE = 0;
	const UPVOTE = 1;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     *
     * @ORM\Column(name="voted_time", type="datetime", nullable=false)
     */
    private $votedTime;

    /**
     * UpVote or DownVote
     *
     * Possible values are:
     * {
     *   **0** : DownVote
     *   **1** : UpVote
     * }
     *
     * @var boolean
     *
     * @ORM\Column(name="vote", type="boolean", nullable=false, options={"unsigned":true, "default":0})
     */
    private $vote;

    /**
     * @var \Rasen\NineGagBundle\Entity\Post
     *
     * @ORM\ManyToOne(targetEntity="Rasen\NineGagBundle\Entity\Post", inversedBy="votes", cascade={"remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="post_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * })
     */
    private $post;

    /**
     * @var \Rasen\NineGagBundle\Entity\User
     *
     * @Gedmo\Blameable(on="update")
     *
     * @ORM\ManyToOne(targetEntity="Rasen\NineGagBundle\Entity\User", cascade={"remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="voted_by", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * })
     */
    private $votedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    protected $deletedAt;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set votedTime
     *
     * @param \DateTime $votedTime
     * @return PostVote
     */
    public function setVotedTime($votedTime)
    {
        $this->votedTime = $votedTime;

        return $this;
    }

    /**
     * Get votedTime
     *
     * @return \DateTime 
     */
    public function getVotedTime()
    {
        return $this->votedTime;
    }

    /**
     * Set vote
     *
     * @param boolean $vote
     * @return PostVote
     */
    public function setVote($vote)
    {
        $this->vote = $vote;

        return $this;
    }

    /**
     * Get vote
     *
     * @return boolean 
     */
    public function getVote()
    {
        return $this->vote;
    }

    /**
     * Set post
     *
     * @param \Rasen\NineGagBundle\Entity\Post $post
     * @return PostVote
     */
    public function setPost(\Rasen\NineGagBundle\Entity\Post $post = null)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Get post
     *
     * @return \Rasen\NineGagBundle\Entity\Post 
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * Set votedBy
     *
     * @param \Rasen\NineGagBundle\Entity\User $votedBy
     * @return PostVote
     */
    public function setVotedBy(\Rasen\NineGagBundle\Entity\User $votedBy = null)
    {
        $this->votedBy = $votedBy;

        return $this;
    }

    /**
     * Get votedBy
     *
     * @return \Rasen\NineGagBundle\Entity\User 
     */
    public function getVotedBy()
    {
        return $this->votedBy;
    }

    /**
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime $deletedAt
     * @return PostVote
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     *
     * @return boolean
     */
    public function isDeleted()
    {
        return ($this->deletedAt !== null);
    }
}
