<?php

namespace Rasen\NineGagBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * NotificationMeta
 *
 * Stores meta variable needed to render notification messages (e.g. How much points a user received)
 *
 * @ORM\Table(name="notifications_meta", indexes={@ORM\Index(name="notifications_meta_notification_id_idx", columns={"notification_id"})})
 * @ORM\Entity
 *
 * @UniqueEntity(
 *     fields={"notification", "name"},
 *     errorPath="name",
 *     message="notification_meta.notification_meta_name.unique"
 * )
 *
 * @author Nima Karimi <nima.k68@gmail.com>
 */
class NotificationMeta
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Rasen\NineGagBundle\Entity\Notification", inversedBy="meta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="notification_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * })
     */
    private $notification;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=256, nullable=false)
     *
     * @Assert\NotBlank(message = "notification_meta.name.not_blank")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=256, nullable=false)
     *
     * @Assert\NotBlank(message = "notification_meta.value.not_blank")
     */
    private $value;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return integer
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param $name
     *
     * @return Notification
     */
    public function setName($name)
    {
	    $this->name = $name;

	    return $this;
    }

    /**
     * Get value
     *
     * @return integer
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set value
     *
     * @param $value
     *
     * @return Notification
     */
    public function setValue($value)
    {
	    $this->value = $value;

	    return $this;
    }

    /**
     * Set notification
     *
     * @param \Rasen\NineGagBundle\Entity\Notification $notification
     * @return Notification
     */
    public function setNotification(\Rasen\NineGagBundle\Entity\Notification $notification = null)
    {
        $this->notification = $notification;

        return $this;
    }

    /**
     * Get notification
     *
     * @return \Rasen\NineGagBundle\Entity\Notification
     */
    public function getNotification()
    {
        return $this->notification;
    }
}
