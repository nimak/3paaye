<?php

namespace Rasen\NineGagBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Rasen\NineGagBundle\Validator\Constraints as RasenAssert;
use \Rasen\NineGagBundle\Entity\NotificationMeta;
/**
 * Notification
 *
 * Notifications are stored with a set of predefined **verbs** which may take place on objects specified with a set of predefined **objectTypes**.
 * This way we can store multiple actions (e.g. Commenting, Vote) on multiple objects (e.g. Comment, Post, User) in a single table.
 *
 * @ORM\Table(name="notifications", indexes={@ORM\Index(name="notifications_user_id_idx", columns={"user_id"}), @ORM\Index(name="notifications_actor_id_idx", columns={"actor_id"})})
 * @ORM\Entity(repositoryClass="Rasen\NineGagBundle\Entity\NotificationRepository")
 *
 * @RasenAssert\IsObjectIdValid(message = "notification.object_id.valid")
 * @RasenAssert\IsNotificationVerbValid(message = "notification.verb.valid")
 *
 * @author Nima Karimi <nima.k68@gmail.com>
 */
class Notification
{
	const READ_STATUS_NOT_READ = 0;
	const READ_STATUS_READ = 1;

	const OBJECT_TYPE_POST = 'post';
	const OBJECT_TYPE_COMMENT = 'comment';
	const OBJECT_TYPE_USER = 'user';
	const OBJECT_TYPE_BADGE = 'badge';
	const OBJECT_TYPE_POINT = 'point';

	const VERB_DOWNVOTED = 'downvoted';
	const VERB_UPVOTED = 'upvoted';
	const VERB_COMMENTED = 'commented';
	const VERB_REPLIED = 'replied';
	const VERB_FOLLOWED = 'followed';
	const VERB_RECEIVED_BADGE = 'received_badge';
	const VERB_RECEIVED_POINT = 'received_point';
	const VERB_MENTIONED = 'mentioned';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     *
     * @ORM\Column(name="created_time", type="datetime", nullable=false)
     */
    private $createdTime;

    /**
     * Specifies whether a notification has been read or not.
     *
     * Possible values are:
     * {
     *   **0** : Not Read
     *   **1** : Read
     * }
     *
     * @var boolean
     *
     * @ORM\Column(name="read_status", type="boolean", nullable=false, options={"default":0})
     *
     * @Assert\NotBlank(message = "notification.read_status.not_blank")
     */
    private $readStatus;

    /**
     * The object which this notification is about.
     *
     * It could be any of the following values:
     * {
     *   **post** : Post
     *   **comment** : Comment
     *   **user** : User
     *   **badge** : UserBadge
     *   **point** : UserPoint
     * }
     *
     * @var integer
     *
     * @ORM\Column(name="object_type", type="string", length=64, nullable=false)
     *
     * @Assert\NotBlank(message = "notification.object_type.not_blank")
     * @Assert\Choice(
     *  choices = {"post","comment","user","badge","point"},
     *  message = "notification.object_type.choice"
     * )
     */
    private $objectType;

    /**
     * ID of the object this notification is about.
     *
     * @var integer
     *
     * @ORM\Column(name="object_id", type="bigint", nullable=false, options={"unsigned":true})
     *
     * @Assert\NotBlank(message = "notification.object_id.not_blank")
     */
    private $objectId;

    /**
     * The action which the user (actor) has taken to cause this notification.
     *
     * This specifies what the user (actor) has done that has caused this notification, and it may have the following values:
     * {
     *   **downvoted** : _DownVoted_ a Post/Comment
     *   **upvoted** : _UpVoted_ a Post/Comment
     *   **commented** : _Commented_ on a Post/Comment
     *   **followed** : _Followed_ another User
     *   **received_badge** : User Received a _Badge_
     *   **received_point** : User Received a _Point_
     *   **mentioned** : _Mentioned_ a user in a Comment
     * }
     *
     * @var integer
     *
     * @ORM\Column(name="verb", type="string", length=64, nullable=false)
     *
     * @Assert\NotBlank(message = "notification.verb.not_blank")
     * @Assert\Choice(
     *  choices = {"downvoted","upvoted","commented","followed","received_badge","received_point","mentioned"},
     *  message = "notification.verb.choice"
     * )
     */
    private $verb;

    /**
     * The user which this notification belongs to.
     *
     * @var \Rasen\NineGagBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Rasen\NineGagBundle\Entity\User", cascade={"remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * })
     */
    private $user;

    /**
     * The user who caused this notification.
     *
     * @var \Rasen\NineGagBundle\Entity\User
     *
     * @Gedmo\Blameable(on="create")
     *
     * @ORM\ManyToOne(targetEntity="Rasen\NineGagBundle\Entity\User", cascade={"remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="actor_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $actor;


	/**
	 * @ORM\OneToMany(targetEntity="NotificationMeta", mappedBy="notification", cascade={"persist", "remove"}, orphanRemoval=true)
	 */
	private $meta;


	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->meta = new \Doctrine\Common\Collections\ArrayCollection();
	}

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdTime
     *
     * @param \DateTime $createdTime
     * @return Notification
     */
    public function setCreatedTime($createdTime)
    {
        $this->createdTime = $createdTime;

        return $this;
    }

    /**
     * Get createdTime
     *
     * @return \DateTime 
     */
    public function getCreatedTime()
    {
        return $this->createdTime;
    }

    /**
     * Set readStatus
     *
     * @param boolean $readStatus
     * @return Notification
     */
    public function setReadStatus($readStatus)
    {
        $this->readStatus = $readStatus;

        return $this;
    }

    /**
     * Get readStatus
     *
     * @return boolean 
     */
    public function getReadStatus()
    {
        return $this->readStatus;
    }

    /**
     * Set objectType
     *
     * @param integer $objectType
     * @return Notification
     */
    public function setObjectType($objectType)
    {
        $this->objectType = $objectType;

        return $this;
    }

    /**
     * Get objectType
     *
     * @return integer 
     */
    public function getObjectType()
    {
        return $this->objectType;
    }

    /**
     * Set objectId
     *
     * @param integer $objectId
     * @return Notification
     */
    public function setObjectId($objectId)
    {
        $this->objectId = $objectId;

        return $this;
    }

    /**
     * Get objectId
     *
     * @return integer 
     */
    public function getObjectId()
    {
        return $this->objectId;
    }

    /**
     * Set verb
     *
     * @param integer $verb
     * @return Notification
     */
    public function setVerb($verb)
    {
        $this->verb = $verb;

        return $this;
    }

    /**
     * Get verb
     *
     * @return integer 
     */
    public function getVerb()
    {
        return $this->verb;
    }

    /**
     * Set user
     *
     * @param \Rasen\NineGagBundle\Entity\User $user
     * @return Notification
     */
    public function setUser(\Rasen\NineGagBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Rasen\NineGagBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set actor
     *
     * @param \Rasen\NineGagBundle\Entity\User $actor
     * @return Notification
     */
    public function setActor(\Rasen\NineGagBundle\Entity\User $actor = null)
    {
        $this->actor = $actor;

        return $this;
    }

    /**
     * Get actor
     *
     * @return \Rasen\NineGagBundle\Entity\User 
     */
    public function getActor()
    {
        return $this->actor;
    }


	/**
	 * Add notificationMeta
	 *
	 * @param \Rasen\NineGagBundle\Entity\NotificationMeta $meta
	 * @return User
	 */
	public function addNotificationMeta(\Rasen\NineGagBundle\Entity\NotificationMeta $meta)
	{
		$meta->setNotification($this);
		$this->meta[] = $meta;

		return $this;
	}

	/**
	 * Remove notificationMeta
	 *
	 * @param \Rasen\NineGagBundle\Entity\NotificationMeta $meta
	 */
	public function removeNotificationMeta(\Rasen\NineGagBundle\Entity\NotificationMeta $meta)
	{
		$this->meta->removeElement($meta);
	}

	/**
	 * Get notificationMeta
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getNotificationMeta()
	{
		return $this->meta;
	}

	/**
	 * Add a new meta data for a notification
	 *
	 * @param $metaName
	 * @param $metaValue
	 */
	public function addMeta($metaName, $metaValue)
	{
		$meta = new NotificationMeta();
		$meta->setName($metaName);
		$meta->setValue($metaValue);
		$meta->setNotification($this);
		$this->addNotificationMeta($meta);
	}
}
