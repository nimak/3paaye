<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 11/01/2014
 * Time: 9:17 PM
 */

namespace Rasen\NineGagBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * Class PostCategoryRepository
 * @package Rasen\NineGagBundle\Entity
 */
class PostCategoryRepository extends EntityRepository
{

	/**
	 * Find all categories and return an array (not an array of objects) [for better performance]
	 * @return array
	 */
	public function findAllArray()
	{
		$q = $this->createQueryBuilder('pc')
			->select('pc')
            ->orderBy('pc.menuOrder', 'ASC')
        ->getQuery();
		return $q->getArrayResult();
	}

} 