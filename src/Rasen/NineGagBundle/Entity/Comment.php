<?php

namespace Rasen\NineGagBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Rasen\NineGagBundle\Model\VotableReportable;
use Symfony\Component\Validator\Constraints as Assert;
use Rasen\NineGagBundle\Validator\Constraints as RasenAssert;

use JMS\Serializer\Annotation as Serializer;
use Hateoas\Configuration\Annotation as Hateoas;
/**
 * Comment
 *
 * @ORM\Table(name="comments", indexes={@ORM\Index(name="comments_post_id_idx", columns={"post_id"}), @ORM\Index(name="created_by_idx", columns={"created_by"}), @ORM\Index(name="last_modified_by_idx", columns={"last_modified_by"}), @ORM\Index(name="approved_by_idx", columns={"approved_by"})})
 * @ORM\Entity(repositoryClass="Rasen\NineGagBundle\Entity\CommentRepository") @ORM\HasLifecycleCallbacks
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 *
 * @Serializer\ExclusionPolicy("all")
 *
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "ajax_comment_get_comment",
 *          parameters = { "commentId" = "expr(service('hashids').encode(object.getId()))" }
 *      )
 * )
 * @Hateoas\Relation(
 *      "replies",
 *      href = @Hateoas\Route(
 *          "ajax_comment_get_comment_replies",
 *          parameters = { "commentId" = "expr(service('hashids').encode(object.getId()))" }
 *      )
 * )
 * @Hateoas\Relation(
 *      "votes",
 *      href = @Hateoas\Route(
 *          "ajax_comment_post_vote",
 *          parameters = { "hashId" = "expr(service('hashids').encode(object.getId()))" }
 *      )
 * )
 *
 * @RasenAssert\IsCommentParentOnSamePost(message = "comment.parent.valid")
 *
 * @author Nima Karimi <nima.k68@gmail.com>
 */
class Comment extends VotableReportable
{
    /**
     * @var integer
     *
     * @Serializer\Exclude
     *
     * @ORM\Column(name="id", type="bigint", options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @Serializer\Accessor(getter="getContentSerialized",setter="setContent")
     * @Serializer\Expose
     * @Serializer\Type("string")
     *
     * @ORM\Column(name="content", type="string", length=500, nullable=false)
     *
     * @Assert\NotBlank(message = "comment.content.not_blank")
     * @Assert\Length(
     *      min = 1,
     *      max = 500,
     *      minMessage = "comment.content.length_min",
     *      maxMessage = "comment.content.length_max"
     * )
     */
    private $content;

    /**
     * @var \DateTime
     *
     * @Serializer\Expose
     * @Serializer\Type("DateTime")
     *
     * @Gedmo\Timestampable(on="create")
     *
     * @ORM\Column(name="created_time", type="datetime", nullable=false)
     */
    private $createdTime;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     *
     * @ORM\Column(name="last_modified_time", type="datetime", nullable=false)
     */
    private $lastModifiedTime;

    /**
     * @var \DateTime
     *
     * @Serializer\Accessor(getter="getLastEditedTimeSerialized",setter="setLastEditedTime")
     * @Serializer\Expose
     *
     * @Gedmo\Timestampable(on="change", field={"content"})
     *
     * @ORM\Column(name="last_edited_time", type="datetime", nullable=true)
     */
    private $lastEditedTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * The comment author's IP address
     *
     * @var string
     *
     * @ORM\Column(name="author_ip", type="string", length=45, nullable=true)
     *
     * @Assert\Ip(message = "comment.author_ip.ip")
     */
    private $authorIP;

    /**
     * Whether the comment has been approved by an administrator.
     *
     * @Serializer\Exclude
     *
     * @var boolean
     *
     * @ORM\Column(name="approved", type="boolean", nullable=true)
     */
    private $approved;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="change", field="approved", value="1")
     *
     * @ORM\Column(name="approved_time", type="datetime", nullable=true)
     */
    private $approvedTime;

    /**
     * @var \Rasen\NineGagBundle\Entity\Post
     *
     * @ORM\ManyToOne(targetEntity="Rasen\NineGagBundle\Entity\Post", inversedBy="comments", cascade={"remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="post_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * })
     */
    private $post;

    /**
     * @var \Rasen\NineGagBundle\Entity\User
     *
     * @Gedmo\Blameable(on="create")
     *
     * @Serializer\Accessor(getter="getCreatedBySerialized",setter="setCreatedBy")
     * @Serializer\Expose
     * @Serializer\Type("Rasen\NineGagBundle\Entity\User")
     *
     * @ORM\ManyToOne(targetEntity="Rasen\NineGagBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="created_by", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * })
     */
    private $createdBy;

    /**
     * @var \Rasen\NineGagBundle\Entity\User
     *
     * @Gedmo\Blameable(on="update")
     *
     * @ORM\ManyToOne(targetEntity="Rasen\NineGagBundle\Entity\User", cascade={"remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="last_modified_by", referencedColumnName="id", onDelete="SET NULL")
     * })
     */
    private $lastModifiedBy;

    /**
     * @var \Rasen\NineGagBundle\Entity\User
     *
     * @Serializer\Accessor(getter="getLastEditedBySerialized",setter="setLastEditedBy")
     * @Serializer\Expose
     * @Serializer\Type("Rasen\NineGagBundle\Entity\User")
     *
     * @Gedmo\Blameable(on="change", field={"content"})
     *
     * @ORM\ManyToOne(targetEntity="Rasen\NineGagBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="last_edited_by", referencedColumnName="id", onDelete="SET NULL")
     * })
     */
    private $lastEditedBy;

    /**
     * @var \Rasen\NineGagBundle\Entity\User
     *
     * @Gedmo\Blameable(on="change", field="approved", value="1")
     *
     * @ORM\ManyToOne(targetEntity="Rasen\NineGagBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="approved_by", referencedColumnName="id")
     * })
     */
    private $approvedBy;

    /**
     * @var \Rasen\NineGagBundle\Entity\Comment
     *
     * @ORM\ManyToOne(targetEntity="Rasen\NineGagBundle\Entity\Comment", inversedBy="replies")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     * })
     */
    private $parent;

	/**
	 * Stores this comment's reply count
	 *
	 * @var integer
	 *
	 * @Serializer\Expose
	 * @Serializer\Groups({"default", "own"})
	 *
	 * @ORM\Column(name="replies_no", type="integer", nullable=true, options={"unsigned":true, "default":0})
	 */
	protected $repliesNo;


    /**
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="parent", cascade={"remove"}, orphanRemoval=true, fetch="EXTRA_LAZY")
     *
     * @Serializer\Exclude
     *
     */
    private $replies;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->replies = new \Doctrine\Common\Collections\ArrayCollection();
	    $this->resetCounters();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Comment
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContentSerialized()
    {
        return $this->isDeleted() ? null : $this->content;
    }

    /**
     * Set createdTime
     *
     * @param \DateTime $createdTime
     * @return Comment
     */
    public function setCreatedTime($createdTime)
    {
        $this->createdTime = $createdTime;

        return $this;
    }

    /**
     * Get createdTime
     *
     * @return \DateTime 
     */
    public function getCreatedTime()
    {
        return $this->createdTime;
    }

    /**
     * Set lastModifiedTime
     *
     * @param \DateTime $lastModifiedTime
     * @return Comment
     */
    public function setLastModifiedTime($lastModifiedTime)
    {
        $this->lastModifiedTime = $lastModifiedTime;

        return $this;
    }

    /**
     * Get lastModifiedTime
     *
     * @return \DateTime 
     */
    public function getLastModifiedTime()
    {
        return $this->lastModifiedTime;
    }

    /**
     * Set lastEditedTime
     *
     * @param \DateTime $lastEditedTime
     * @return Comment
     */
    public function setLastEditedTime($lastEditedTime)
    {
        $this->lastEditedTime = $lastEditedTime;

        return $this;
    }

    /**
     * Get lastEditedTime
     *
     * @return \DateTime
     */
    public function getLastEditedTime()
    {
        return $this->lastEditedTime;
    }

    /**
     * Get lastEditedTime
     *
     * @return \DateTime
     */
    public function getLastEditedTimeSerialized()
    {
        return $this->isDeleted() ? null : $this->lastEditedTime;
    }

    /**
     * Set authorIP
     *
     * @param string $authorIP
     * @return Comment
     */
    public function setAuthorIP($authorIP)
    {
        $this->authorIP = $authorIP;

        return $this;
    }

    /**
     * Get authorIP
     *
     * @return string 
     */
    public function getAuthorIP()
    {
        return $this->authorIP;
    }

    /**
     * Set approved
     *
     * @param boolean $approved
     * @return Comment
     */
    public function setApproved($approved)
    {
        $this->approved = $approved;

        return $this;
    }

    /**
     * Get approved
     *
     * @return boolean 
     */
    public function getApproved()
    {
        return $this->approved;
    }

    /**
     * Set approvedTime
     *
     * @param \DateTime $approvedTime
     * @return Comment
     */
    public function setApprovedTime($approvedTime)
    {
        $this->approvedTime = $approvedTime;

        return $this;
    }

    /**
     * Get approvedTime
     *
     * @return \DateTime 
     */
    public function getApprovedTime()
    {
        return $this->approvedTime;
    }

    /**
     * Set post
     *
     * @param \Rasen\NineGagBundle\Entity\Post $post
     * @return Comment
     */
    public function setPost(\Rasen\NineGagBundle\Entity\Post $post = null)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Get post
     *
     * @return \Rasen\NineGagBundle\Entity\Post 
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * Set createdBy
     *
     * @param \Rasen\NineGagBundle\Entity\User $createdBy
     * @return Comment
     */
    public function setCreatedBy(\Rasen\NineGagBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \Rasen\NineGagBundle\Entity\User 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Get createdBy
     *
     * @return \Rasen\NineGagBundle\Entity\User
     */
    public function getCreatedBySerialized()
    {
        if ($this->isDeleted()) {
            $emptyUser = new User();
            $emptyUser->makeDummy();
            return $emptyUser;
        }
        return $this->createdBy;
    }

    /**
     * Set lastModifiedBy
     *
     * @param \Rasen\NineGagBundle\Entity\User $lastModifiedBy
     * @return Comment
     */
    public function setLastModifiedBy(\Rasen\NineGagBundle\Entity\User $lastModifiedBy = null)
    {
        $this->lastModifiedBy = $lastModifiedBy;

        return $this;
    }

    /**
     * Get lastModifiedBy
     *
     * @return \Rasen\NineGagBundle\Entity\User 
     */
    public function getLastModifiedBy()
    {
        return $this->lastModifiedBy;
    }

    /**
     * Set lastEditedBy
     *
     * @param \Rasen\NineGagBundle\Entity\User $lastEditedBy
     * @return Comment
     */
    public function setLastEditedBy(\Rasen\NineGagBundle\Entity\User $lastEditedBy = null)
    {
        $this->lastEditedBy = $lastEditedBy;

        return $this;
    }

    /**
     * Get lastEditedBy
     *
     * @return \Rasen\NineGagBundle\Entity\User
     */
    public function getLastEditedBy()
    {
        return $this->lastEditedBy;
    }

    /**
     * Get lastEditedBy
     *
     * @return \Rasen\NineGagBundle\Entity\User
     */
    public function getLastEditedBySerialized()
    {
        return $this->isDeleted() ? null : $this->lastEditedBy;
    }

    /**
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime $deletedAt
     * @return Comment
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     *
     * @return boolean
     */
    public function isDeleted()
    {
        return ($this->deletedAt !== null);
    }


    /**
     * Set approvedBy
     *
     * @param \Rasen\NineGagBundle\Entity\User $approvedBy
     * @return Comment
     */
    public function setApprovedBy(\Rasen\NineGagBundle\Entity\User $approvedBy = null)
    {
        $this->approvedBy = $approvedBy;

        return $this;
    }

    /**
     * Get approvedBy
     *
     * @return \Rasen\NineGagBundle\Entity\User 
     */
    public function getApprovedBy()
    {
        return $this->approvedBy;
    }

    /**
     * Set parent
     *
     * @param \Rasen\NineGagBundle\Entity\Comment $parent
     * @return Comment
     */
    public function setParent(\Rasen\NineGagBundle\Entity\Comment $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \Rasen\NineGagBundle\Entity\Comment
     */
    public function getParent()
    {
        return $this->parent;
    }

	/**
	 * Set repliesNo
	 *
	 * @param integer $repliesNo
	 * @return Post
	 */
	public function setRepliesNo($repliesNo)
	{
		$this->repliesNo = $repliesNo;

		return $this;
	}

	/**
	 * Get repliesNo
	 *
	 * @return integer
	 */
	public function getRepliesNo()
	{
		return $this->repliesNo;
	}

	/**
	 * Update repliesNo
	 *
	 * Increase or Decrease one step
	 *
	 * @param bool $decrease
	 * @return VotableReportable
	 */
	public function updateRepliesNo($decrease = false)
	{
		if ($this->repliesNo == null) $this->repliesNo = 0;
		$this->repliesNo = $decrease ? $this->repliesNo - 1 : $this->repliesNo + 1;

		return $this;
	}

	/**
	 * Reset counters
	 */
	public function resetCounters()
	{
		parent::resetCounters();
		$this->repliesNo = 0;
	}

	/**
	 * @return string
	 */
	public function __toString()
	{
		return $this->content;
	}


	/**
	 * @ORM\PreUpdate
	 */
	public function removeApproveBlame()
	{
		if ($this->approved == null) {
			$this->approvedBy = $this->approvedTime = null;
		}
	}
}
