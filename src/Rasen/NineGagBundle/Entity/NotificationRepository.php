<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/14/2014
 * Time: 12:05 PM
 */

namespace Rasen\NineGagBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;

/**
 * Class NotificationRepository
 * @package Rasen\NineGagBundle\Entity
 */
class NotificationRepository extends EntityRepository
{
	/**
	 * Returns all notifications for a user (Query)
	 *
	 * @param $userId
	 * @param bool $onlyUnread
	 * @param integer $limit
	 *
	 * @return \Doctrine\ORM\NativeQuery
	 */
	public function findAllByUserIdQuery($userId, $onlyUnread = true, $limit = null)
	{
		$em = $this->getEntityManager();
		$rsm = $this->createResultSetMappingBuilder('n');
		$rsm->addScalarResult('notificationIds', 'notificationIds');
		$rsm->addScalarResult('lastNotificationId', 'lastNotificationId');
		$rsm->addScalarResult('lastCreatedTime', 'lastCreatedTime');
		$rsm->addScalarResult('actorCount', 'actorCount');
		$rsm->addScalarResult('totalRead', 'totalRead');
		$rsm->addScalarResult('totalNotifications', 'totalNotifications');
		$rsm->addScalarResult('unreadNotificationsId', 'unreadNotificationsId');
		//$rsm->addJoinedEntityFromClassMetadata('RasenNineGagBundle:User', 'u', 'n', 'actor');
		$selectClause = $rsm->generateSelectClause(array(
			'n' => 'n',
		));
		$querySQL = "SELECT " . $selectClause . ", ".
		            "CONCAT_WS(',', n.id) as notificationIds, ".
		            "MAX(n.id) as lastNotificationId, ".
		            "COUNT(DISTINCT n.actor_id) as actorCount, ".
		            "SUM(n.read_status) as totalRead, ".
		            "COUNT(n.id) as totalNotifications, ".
		            "MAX(n.created_time) as lastCreatedTime, ".
		            "GROUP_CONCAT(DISTINCT n.id SEPARATOR ',') as unreadNotificationsId ".
		            "FROM notifications n ".
		            "WHERE n.user_id = :userId AND n.actor_id != :userId ".
		            "GROUP BY n.object_type, n.object_id, n.verb ";
		if ($onlyUnread) $querySQL .= "HAVING totalRead < totalNotifications ";
		$querySQL .= "ORDER BY lastCreatedTime DESC ";
		if ($limit !== null) $querySQL .= "LIMIT :limit ";
		$query = $em->createNativeQuery($querySQL, $rsm);
		$query->setParameter('userId', $userId);
		$query->setParameter('limit', $limit);
		return $query;
	}

	/**
	 * Returns all notifications for a user
	 *
	 * @param $userId
	 * @param bool $onlyUnread
	 * @param integer $limit
	 *
	 * @return array
	 */
	public function findAllByUserId($userId, $onlyUnread = true, $limit = null)
	{
		$query = $this->findAllByUserIdQuery($userId, $onlyUnread, $limit);
		return $query->getResult();
	}

	/**
	 * Return the total unread notifications count for a user
	 *
	 * @param $userId
	 *
	 * @return mixed
	 */
	public function countAllUnreadByUserId($userId)
	{
		$em = $this->getEntityManager();
		$rsm = $this->createResultSetMappingBuilder('n');
		$rsm->addScalarResult('unreadCount', 'unreadCount');
		$querySQL = "SELECT COUNT(n.id) as unreadCount ".
		            "FROM notifications n ".
		            "WHERE n.user_id = :userId AND n.actor_id != :userId AND n.read_status = 0 "
		;
		$query = $em->createNativeQuery($querySQL, $rsm);
		$query->setParameter('userId', $userId);
		return $query->getScalarResult()[0]['unreadCount'];
	}

	/**
	 * Marks all notifications as read
	 *
	 * @param $userId
	 *
	 * @return mixed
	 */
	public function markAllAsRead($userId)
	{
		$em = $this->getEntityManager();
		$dql = "UPDATE RasenNineGagBundle:Notification n ".
		       "SET n.readStatus = 1".
		       "WHERE n.user = :userId AND n.readStatus = 0 "
		;
		$query = $em->createQuery($dql)
			->setParameter('userId', $userId);
		return $query->execute();
	}

} 