<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/9/2014
 * Time: 3:27 PM
 */

namespace Rasen\NineGagBundle\Entity;

use FOS\OAuthServerBundle\Entity\AccessToken as BaseAccessToken;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="access_tokens", indexes={@ORM\Index(name="client_id_idx", columns={"client_id"}), @ORM\Index(name="user_id_idx", columns={"user_id"}) })
 * @ORM\Entity
 *
 * @author Nima Karimi <nima.k68@gmail.com>
 */
class AccessToken extends BaseAccessToken
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @ORM\ManyToOne(targetEntity="Client")
	 * @ORM\JoinColumn(name="client_id", referencedColumnName="id", nullable=false)
	 */
	protected $client;

	/**
	 * @ORM\ManyToOne(targetEntity="Rasen\NineGagBundle\Entity\User")
	 * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
	 */
	protected $user;
}