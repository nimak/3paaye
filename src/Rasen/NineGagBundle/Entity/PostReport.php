<?php

namespace Rasen\NineGagBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * PostReport
 *
 * Stores inappropriate reports about posts.
 *
 * @ORM\Table(name="posts_reports", uniqueConstraints={@ORM\UniqueConstraint(name="post_id_reporter_id_UNIQUE", columns={"post_id", "reporter_id"})}, indexes={@ORM\Index(name="posts_reports_post_id_idx", columns={"post_id"}), @ORM\Index(name="posts_reports_reporter_id_idx", columns={"reporter_id"})})
 * @ORM\Entity
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 *
 * @UniqueEntity(
 *     fields={"post", "reporter"},
 *     errorPath="post",
 *     message="post.report.unique"
 * )
 *
 * @author Nima Karimi <nima.k68@gmail.com>
 */
class PostReport
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     *
     * @ORM\Column(name="reported_time", type="datetime", nullable=false)
     */
    private $reportedTime;

    /**
     * @var \Rasen\NineGagBundle\Entity\Post
     *
     * @ORM\ManyToOne(targetEntity="Rasen\NineGagBundle\Entity\Post", cascade={"remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="post_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * })
     */
    private $post;

    /**
     * @var \Rasen\NineGagBundle\Entity\User
     *
     * @Gedmo\Blameable(on="update")
     *
     * @ORM\ManyToOne(targetEntity="Rasen\NineGagBundle\Entity\User", cascade={"remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="reporter_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * })
     */
    private $reporter;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    protected $deletedAt;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reportedTime
     *
     * @param \DateTime $reportedTime
     * @return PostReport
     */
    public function setReportedTime($reportedTime)
    {
        $this->reportedTime = $reportedTime;

        return $this;
    }

    /**
     * Get reportedTime
     *
     * @return \DateTime 
     */
    public function getReportedTime()
    {
        return $this->reportedTime;
    }

    /**
     * Set post
     *
     * @param \Rasen\NineGagBundle\Entity\Post $post
     * @return PostReport
     */
    public function setPost(\Rasen\NineGagBundle\Entity\Post $post = null)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Get post
     *
     * @return \Rasen\NineGagBundle\Entity\Post 
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * Set reporter
     *
     * @param \Rasen\NineGagBundle\Entity\User $reporter
     * @return PostReport
     */
    public function setReporter(\Rasen\NineGagBundle\Entity\User $reporter = null)
    {
        $this->reporter = $reporter;

        return $this;
    }

    /**
     * Get reporter
     *
     * @return \Rasen\NineGagBundle\Entity\User 
     */
    public function getReporter()
    {
        return $this->reporter;
    }

    /**
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime $deletedAt
     * @return PostVote
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     *
     * @return boolean
     */
    public function isDeleted()
    {
        return ($this->deletedAt !== null);
    }
}
