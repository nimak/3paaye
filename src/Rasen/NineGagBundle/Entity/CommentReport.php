<?php

namespace Rasen\NineGagBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * CommentReport
 *
 * Stores inappropriate reports about comments.
 *
 * @ORM\Table(name="comments_reports", uniqueConstraints={@ORM\UniqueConstraint(name="comment_id_reporter_id_UNIQUE", columns={"comment_id", "reporter_id"})}, indexes={@ORM\Index(name="comments_reports_comment_id_idx", columns={"comment_id"}), @ORM\Index(name="comments_reports_reporter_id_idx", columns={"reporter_id"})})
 * @ORM\Entity
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 *
 * @UniqueEntity(
 *     fields={"comment", "reporter"},
 *     errorPath="comment",
 *     message="comment.report.unique"
 * )
 *
 * @author Nima Karimi <nima.k68@gmail.com>
 */
class CommentReport
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     *
     * @ORM\Column(name="reported_time", type="datetime", nullable=false)
     */
    private $reportedTime;

    /**
     * @var \Rasen\NineGagBundle\Entity\Comment
     *
     * @ORM\ManyToOne(targetEntity="Rasen\NineGagBundle\Entity\Comment")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="comment_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * })
     */
    private $comment;

    /**
     * @var \Rasen\NineGagBundle\Entity\User
     *
     * @Gedmo\Blameable(on="update")
     *
     * @ORM\ManyToOne(targetEntity="Rasen\NineGagBundle\Entity\User", cascade={"remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="reporter_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * })
     */
    private $reporter;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    protected $deletedAt;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reportedTime
     *
     * @param \DateTime $reportedTime
     * @return CommentReport
     */
    public function setReportedTime($reportedTime)
    {
        $this->reportedTime = $reportedTime;

        return $this;
    }

    /**
     * Get reportedTime
     *
     * @return \DateTime 
     */
    public function getReportedTime()
    {
        return $this->reportedTime;
    }

    /**
     * Set comment
     *
     * @param \Rasen\NineGagBundle\Entity\Comment $comment
     * @return CommentReport
     */
    public function setComment(\Rasen\NineGagBundle\Entity\Comment $comment = null)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return \Rasen\NineGagBundle\Entity\Comment 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set reporter
     *
     * @param \Rasen\NineGagBundle\Entity\User $reporter
     * @return CommentReport
     */
    public function setReporter(\Rasen\NineGagBundle\Entity\User $reporter = null)
    {
        $this->reporter = $reporter;

        return $this;
    }

    /**
     * Get reporter
     *
     * @return \Rasen\NineGagBundle\Entity\User 
     */
    public function getReporter()
    {
        return $this->reporter;
    }

    /**
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime $deletedAt
     * @return CommentReport
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     *
     * @return boolean
     */
    public function isDeleted()
    {
        return ($this->deletedAt !== null);
    }
}
