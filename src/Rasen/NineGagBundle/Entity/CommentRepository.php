<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/14/2014
 * Time: 12:05 PM
 */

namespace Rasen\NineGagBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;

/**
 * Class CommentRepository
 * @package Rasen\NineGagBundle\Entity
 */
class CommentRepository extends EntityRepository
{

	private function getCommentsCreatedByQuery($userId, $dateFrom=null, $dateTo=null)
	{
		$q = $this->createQueryBuilder('c')
		          ->where('c.createdBy = :userId')
		          ->setParameter('userId', $userId);
		if ($dateFrom == null && $dateTo == null) {
			//Nothing to see here!
		} elseif ($dateFrom == null) {
			$q = $q->andWhere($q->expr()->gte('c.createdTime', ':dateFrom'))
			       ->setParameter('dateFrom', $dateFrom);
		} elseif ($dateTo == null) {
			$q = $q->andWhere($q->expr()->lte('c.createdTime', ':dateTo'))
			       ->setParameter('dateTo', $dateTo);
		} else {
			$q = $q->andWhere($q->expr()->between('c.createdTime', ':dateFrom', ':dateTo'))
			       ->setParameters(array('dateFrom'=>$dateFrom, 'dateTo'=>$dateTo));
		}
		return $q;
	}

	public function findCommentsCreatedByQ($userId, $dateFrom=null, $dateTo=null)
	{
		$q = $this->getCommentsCreatedByQuery($userId, $dateFrom, $dateTo);

		return $q->getQuery();
	}

	public function findCommentsCreatedByCount($userId, $dateFrom=null, $dateTo=null)
	{
		$q = $this->getCommentsCreatedByQuery($userId, $dateFrom, $dateTo);
		$q = $q->select('COUNT(c)');
		return $q->getQuery()->getSingleScalarResult();
	}

	public function countAllApprovedByPost($post, $currentUserId = null)
	{
		$dql = "SELECT COUNT(c) as totalCount ".
		       "FROM RasenNineGagBundle:Comment c ".
		       "WHERE c.post = :post AND c.parent IS NULL ";
		/*if ($currentUserId != null) {
			$dql .= "AND IF(c.createdBy = :currentUser, true, c.approved) = true ";
		} else {
			$dql .= "AND c.approved = true ";
		}*/

		$q = $this->getEntityManager()->createQuery($dql);

		$q->setParameter('post', $post);
		/*if ($currentUserId != null) {
			$q->setParameter('currentUser', $currentUserId);
		}*/

		return $q->getScalarResult()[0]['totalCount'];
	}

	private function getAllApprovedDQL($currentUserId = null){
		$dql = "SELECT c, (CAST(c.upvotesNo as SIGNED) - CAST(c.downvotesNo as SIGNED)) as totalVotes ".
		       "FROM RasenNineGagBundle:Comment c ".
		       "WHERE c.post = :post AND c.parent IS NULL ";
		/*if ($currentUserId != null) {
			$dql .= "AND IF(c.createdBy = :currentUser, true, c.approved) = true ";
		} else {
			$dql .= "AND c.approved = true ";
		}*/
		return $dql;
	}

	public function countAllByParent($parent)
	{
		$dql = "SELECT COUNT(c) as totalCount ".
		       "FROM RasenNineGagBundle:Comment c ".
		       "WHERE c.parent = :parent ";
		/*if ($currentUserId != null) {
			$dql .= "AND IF(c.createdBy = :currentUser, true, c.approved) = true ";
		} else {
			$dql .= "AND c.approved = true ";
		}*/

		$q = $this->getEntityManager()->createQuery($dql);

		$q->setParameter('parent', $parent);
		/*if ($currentUserId != null) {
			$q->setParameter('currentUser', $currentUserId);
		}*/

		return $q->getScalarResult()[0]['totalCount'];
	}

	private function getAllRepliesDQL(){
		$dql = "SELECT c, (CAST(c.upvotesNo as SIGNED) - CAST(c.downvotesNo as SIGNED)) as totalVotes ".
		       "FROM RasenNineGagBundle:Comment c ".
		       "WHERE c.parent = :parent ";
		return $dql;
	}

	public function findAllByParentQ($parent, $limit = null, $offset = null)
	{
		$dql = $this->getAllRepliesDQL();
		$dql .= "ORDER BY c.createdTime DESC ";

		$q = $this->getEntityManager()->createQuery($dql);

		if ($limit != null) {
			$q->setMaxResults($limit);
		}
		if ($offset != null) {
			$q->setFirstResult($offset);
		}
		$q->setParameter('parent', $parent);

		return $q;
	}

	public function findAllByParent($parent, $limit = null, $offset = null)
	{
		$q = $this->findAllByParentQ($parent, $limit, $offset);
		$results = $q->getResult();
		$comments = array();
		foreach($results as $comment) {
			$comments[] = $comment[0];
		}

		$totalCount = $this->countAllByParent($parent);

		$finalResults = array();
		$finalResults['items'] = $comments;
		$finalResults['totalCount'] = $totalCount;
		$finalResults['offset'] = $offset + count($comments);

		return $finalResults;
	}

	public function findAllByParentOrderDescQ($parent, $limit = null, $offset = null)
	{
		$dql = $this->getAllRepliesDQL();
		$dql .= "ORDER BY c.createdTime DESC ";

		$q = $this->getEntityManager()->createQuery($dql);

		if ($limit != null) {
			$q->setMaxResults($limit);
		}
		if ($offset != null) {
			$q->setFirstResult($offset);
		}
		$q->setParameter('parent', $parent);

		return $q;
	}

	public function findAllByParentOrderDesc($parent, $limit = null, $offset = null)
	{
		$q = $this->findAllByParentOrderDescQ($parent, $limit, $offset);
		$results = $q->getResult();
		$comments = array();
		foreach($results as $comment) {
			$comments[] = $comment[0];
		}
		return $comments;
	}

	public function findOverviewByParent($parent)
	{
		$results = $this->findAllByParent($parent);
		$resultsCount = count($results);
		$comments = array('items'=>array(), 'totalCount' => $resultsCount, 'offset' => 2);
		if ($resultsCount < 5) {
			$comments['items'] = $results;
			return $comments;
		}

		$comments['items'][0] = $results[0];
		$comments['items'][1] = $results[1];
		$comments['items'][2] = $results[$resultsCount - 2];
		$comments['items'][3] = $results[$resultsCount - 1];

		return $comments;
	}

	public function findAllApprovedByPostQ($post, $currentUserId = null, $limit = null, $offset = null)
	{
		$dql = $this->getAllApprovedDQL($currentUserId);
		$dql .= "ORDER BY c.createdTime DESC ";

		$q = $this->getEntityManager()->createQuery($dql);

		if ($limit != null) {
			$q->setMaxResults($limit);
		}
		if ($offset != null) {
			$q->setFirstResult($offset);
		}

		$q->setParameter('post', $post);
		/*if ($currentUserId != null) {
			$q->setParameter('currentUser', $currentUserId);
		}*/

		return $q;
	}

	public function findAllApprovedByPost($post, $currentUserId = null, $limit = null, $offset = null)
	{
		$q = $this->findAllApprovedByPostQ($post, $currentUserId, $limit, $offset);
		$results = $q->getResult();
		$comments = array();
		foreach($results as $comment) {
			$comments[] = $comment[0];
		}

		$totalCount = $this->countAllApprovedByPost($post, $currentUserId);

		$finalResults = array();
		$finalResults['items'] = $comments;
		$finalResults['totalCount'] = $totalCount;
		$finalResults['offset'] = $offset + count($comments);

		return $finalResults;
	}

	public function findAllApprovedByPostOrderDescQ($post, $currentUserId = null, $limit = null, $offset = null)
	{
		$dql = $this->getAllApprovedDQL($currentUserId);
		$dql .= "ORDER BY c.createdTime DESC ";

		$q = $this->getEntityManager()->createQuery($dql);

		if ($limit != null) {
			$q->setMaxResults($limit);
		}
		if ($offset != null) {
			$q->setFirstResult($offset);
		}

		$q->setParameter('post', $post);
		/*if ($currentUserId != null) {
			$q->setParameter('currentUser', $currentUserId);
		}*/

		return $q;
	}

	public function findAllApprovedByPostOrderDesc($post, $currentUserId = null, $limit = null, $offset = null)
	{
		$q = $this->findAllApprovedByPostOrderDescQ($post, $currentUserId, $limit, $offset);
		$results = $q->getResult();
		$comments = array();
		foreach($results as $comment) {
			$comments[] = $comment[0];
		}
		return $comments;
	}


	public function findOverviewApprovedByPost($postId, $currentUserId = null)
	{
		/*$em = $this->getEntityManager();
		$rsm = $this->createResultSetMappingBuilder('c');
		$rsm->addRootEntityFromClassMetadata('RasenNineGagBundle:Comment', 'c');
		$select = $rsm->generateSelectClause(array(
			'c' => 'c'
		));
		$sql = "SELECT ".$select.", (c.upvotes_no - c.downvotes_no) as totalVotes ".
		       "FROM comments c ".
		       "WHERE c.post_id = :postId ";
		if ($currentUserId != null) {
			$sql .= "AND IF(c.created_by = :currentUser, true, c.approved) = true ";
		} else {
			$sql .= "AND c.approved = true ";
		}
		$sqlFirst = $sql . "ORDER BY totalVotes DESC, c.created_time ASC LIMIT 2";
		$sqlLast = $sql . "ORDER BY totalVotes ASC, c.created_time DESC LIMIT 2";

		$sqlOverview = "(" . $sqlFirst . ")" . " UNION " . "(" . $sqlLast . ")" ;

		$q = $em->createNativeQuery($sqlOverview, $rsm);

		$q->setParameter('postId', $postId);
		if ($currentUserId != null) {
			$q->setParameter('currentUser', $currentUserId);
		}*/

		$results = $this->findAllApprovedByPost($postId, $currentUserId);
		$resultsCount = count($results);
		$comments = array('items'=>array(), 'totalCount' => $resultsCount, 'offset' => 2);
		if ($resultsCount < 5) {
			$comments['items'] = $results;
			return $comments;
		}

		$comments['items'][0] = $results[0];
		$comments['items'][1] = $results[1];
		$comments['items'][2] = $results[$resultsCount - 2];
		$comments['items'][3] = $results[$resultsCount - 1];

		return $comments;
	}
} 