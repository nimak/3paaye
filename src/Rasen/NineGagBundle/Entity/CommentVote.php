<?php

namespace Rasen\NineGagBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * CommentVote
 *
 * Stores UpVotes and DownVotes for comments.
 *
 * @ORM\Table(name="comments_votes", uniqueConstraints={@ORM\UniqueConstraint(name="comment_id_voted_by_UNIQUE", columns={"comment_id", "voted_by"})}, indexes={@ORM\Index(name="comments_votes_comment_id_idx", columns={"comment_id"}), @ORM\Index(name="comments_votes_voted_by_idx", columns={"voted_by"})})
 * @ORM\Entity
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 *
 * @UniqueEntity(
 *     fields={"comment", "votedBy"},
 *     errorPath="comment",
 *     message="comment.voted_by.unique"
 * )
 *
 * @author Nima Karimi <nima.k68@gmail.com>
 */
class CommentVote
{
	const DOWNVOTE = 0;
	const UPVOTE = 1;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     *
     * @ORM\Column(name="voted_time", type="datetime", nullable=false)
     */
    private $votedTime;

    /**
     * UpVote or DownVote
     *
     * Possible values are:
     * {
     *   **0** : DownVote
     *   **1** : UpVote
     * }
     *
     * @var boolean
     *
     * @ORM\Column(name="vote", type="boolean", nullable=false)
     */
    private $vote;

    /**
     * @var \Rasen\NineGagBundle\Entity\Comment
     *
     * @ORM\ManyToOne(targetEntity="Rasen\NineGagBundle\Entity\Comment")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="comment_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $comment;

    /**
     * @var \Rasen\NineGagBundle\Entity\User
     *
     * @Gedmo\Blameable(on="update")
     *
     * @ORM\ManyToOne(targetEntity="Rasen\NineGagBundle\Entity\User", cascade={"remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="voted_by", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $votedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    protected $deletedAt;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set votedTime
     *
     * @param \DateTime $votedTime
     * @return CommentVote
     */
    public function setVotedTime($votedTime)
    {
        $this->votedTime = $votedTime;

        return $this;
    }

    /**
     * Get votedTime
     *
     * @return \DateTime 
     */
    public function getVotedTime()
    {
        return $this->votedTime;
    }

    /**
     * Set vote
     *
     * @param boolean $vote
     * @return CommentVote
     */
    public function setVote($vote)
    {
        $this->vote = $vote;

        return $this;
    }

    /**
     * Get vote
     *
     * @return boolean 
     */
    public function getVote()
    {
        return $this->vote;
    }

    /**
     * Set comment
     *
     * @param \Rasen\NineGagBundle\Entity\Comment $comment
     * @return CommentVote
     */
    public function setComment(\Rasen\NineGagBundle\Entity\Comment $comment = null)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return \Rasen\NineGagBundle\Entity\Comment 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set votedBy
     *
     * @param \Rasen\NineGagBundle\Entity\User $votedBy
     * @return CommentVote
     */
    public function setVotedBy(\Rasen\NineGagBundle\Entity\User $votedBy = null)
    {
        $this->votedBy = $votedBy;

        return $this;
    }

    /**
     * Get votedBy
     *
     * @return \Rasen\NineGagBundle\Entity\User 
     */
    public function getVotedBy()
    {
        return $this->votedBy;
    }

    /**
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime $deletedAt
     * @return CommentVote
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     *
     * @return boolean
     */
    public function isDeleted()
    {
        return ($this->deletedAt !== null);
    }
}
