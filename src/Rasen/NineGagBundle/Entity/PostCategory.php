<?php

namespace Rasen\NineGagBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation as Serializer;

/**
 * PostCategory
 *
 * @ORM\Table(name="posts_categories", uniqueConstraints={@ORM\UniqueConstraint(name="slug_UNIQUE", columns={"slug"})})
 * @ORM\Entity(repositoryClass="Rasen\NineGagBundle\Entity\PostCategoryRepository")
 *
 * @UniqueEntity(
 *     fields={"slug"},
 *     errorPath="slug",
 *     message="post_category.slug.unique"
 * )
 *
 * @Serializer\ExclusionPolicy("all")
 *
 * @author Nima Karimi <nima.k68@gmail.com>
 */
class PostCategory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"default", "own"})
     *
     * @Assert\NotBlank(message = "post_category.name.not_blank")
     * @Assert\Length(
     *      min = 1,
     *      max = 255,
     *      minMessage = "post_category.name.length_min",
     *      maxMessage = "post_category.name.length_max"
     * )
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, nullable=false)
     * 
     * @Serializer\Expose
     * @Serializer\Groups({"default", "own"})
     *
     * @Assert\NotBlank(message = "post_category.slug.not_blank")
     * @Assert\Length(
     *      min = 1,
     *      max = 255,
     *      minMessage = "post_category.slug.length_min",
     *      maxMessage = "post_category.slug.length_max"
     * )
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

	/**
	 * @var boolean
     * @Serializer\Exclude
	 *
	 * @ORM\Column(name="main_menu", type="boolean", nullable=true)
	 */
	private $mainMenu;

    /**
     * @var \Rasen\NineGagBundle\Entity\Watermark
     *
     * @ORM\ManyToOne(targetEntity="Rasen\NineGagBundle\Entity\Watermark")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="watermark_id", referencedColumnName="id", nullable=true)
     * })
     *
     */
    protected $watermark;

    /**
     *
     * @var integer
     *
     * @Serializer\Exclude
     *
     * @ORM\Column(name="menuOrder", type="integer", nullable=true, options={"unsigned":true})
     */
    protected $menuOrder;

    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return PostCategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return PostCategory
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return PostCategory
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

	/**
	 * Set mainMenu
	 *
	 * @param string $mainMenu
	 * @return PostCategory
	 */
	public function setMainMenu($mainMenu)
	{
		$this->mainMenu = $mainMenu;

		return $this;
	}

	/**
	 * Get mainMenu
	 *
	 * @return string
	 */
	public function getMainMenu()
	{
		return $this->mainMenu;
	}

    /**
     * Set watermark
     *
     * @param \Rasen\NineGagBundle\Entity\Watermark $watermark
     * @return PostCategory
     */
    public function setWatermark(\Rasen\NineGagBundle\Entity\Watermark $watermark = null)
    {
        $this->watermark = $watermark;

        return $this;
    }

    /**
     * Get watermark
     *
     * @return \Rasen\NineGagBundle\Entity\Watermark
     */
    public function getWatermark()
    {
        return $this->watermark;
    }

	/**
	 * @return string
	 */
	public function __toString()
	{
		return $this->name;
	}

    /**
     * Set menuOrder
     *
     * @param integer $menuOrder
     * @return PostCategory
     */
    public function setMenuOrder($menuOrder)
    {
        $this->menuOrder = $menuOrder;

        return $this;
    }

    /**
     * Get MenuOrder
     *
     * @return integer
     */
    public function getMenuOrder()
    {
        return $this->menuOrder;
    }
}
