<?php

namespace Rasen\NineGagBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

use JMS\Serializer\Annotation as Serializer;
/**
 * PostText
 *
 * @ORM\Table(name="posts_text", indexes={@ORM\Index(name="posts_text_post_id_idx", columns={"post_id"})})
 * @ORM\Entity(repositoryClass="Rasen\NineGagBundle\Entity\PostRepository")
 *
 * @Serializer\ExclusionPolicy("none")
 *
 * @author Nima Karimi <nima.k68@gmail.com>
 */
class PostText extends Post
{

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=false)
     *
     * @Serializer\Groups({"default", "own"})
     *
     * @Assert\NotBlank(message = "post_text.content.not_blank")
     * @Assert\Length(
     *      min = 1,
     *      max = 200,
     *      minMessage = "post_text.content.length_min",
     *      maxMessage = "post_text.content.length_max"
     * )
     */
	protected $content;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="change", field={"category", "nsfw", "content"})
     *
     * @Serializer\Exclude
     *
     * @ORM\Column(name="last_modified_time", type="datetime", nullable=true)
     */
    protected $lastModifiedTime;

    /**
     * TODO: Fix blamable and timestampable to "on Change" instead of "on Update"
     * @var \Rasen\NineGagBundle\Entity\User
     *
     * @Gedmo\Blameable(on="change", field={"category", "nsfw", "content"})
     *
     * @ORM\ManyToOne(targetEntity="Rasen\NineGagBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="last_modified_by", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     * })
     *
     * @Serializer\Exclude
     *
     */
    protected $lastModifiedBy;




    /**
     * Set content
     *
     * @param string $content
     * @return PostText
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set lastModifiedTime
     *
     * @param \DateTime $lastModifiedTime
     * @return PostText
     */
    public function setLastModifiedTime($lastModifiedTime)
    {
        $this->lastModifiedTime = $lastModifiedTime;

        return $this;
    }

    /**
     * Get lastModifiedTime
     *
     * @return \DateTime
     */
    public function getLastModifiedTime()
    {
        return $this->lastModifiedTime;
    }

    /**
     * Set lastModifiedBy
     *
     * @param \Rasen\NineGagBundle\Entity\User $lastModifiedBy
     * @return PostText
     */
    public function setLastModifiedBy(\Rasen\NineGagBundle\Entity\User $lastModifiedBy = null)
    {
        $this->lastModifiedBy = $lastModifiedBy;

        return $this;
    }

    /**
     * Get lastModifiedBy
     *
     * @return \Rasen\NineGagBundle\Entity\User
     */
    public function getLastModifiedBy()
    {
        return $this->lastModifiedBy;
    }
}
