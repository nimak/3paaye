<?php

namespace Rasen\NineGagBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use FOS\UserBundle\Entity\User as BaseUser;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

use JMS\Serializer\Annotation as Serializer;
use Hateoas\Configuration\Annotation as Hateoas;
/**
 * User
 *
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="Rasen\NineGagBundle\Entity\UserRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 *
 * @Vich\Uploadable
 *
 * @Serializer\ExclusionPolicy("all")
 *
 * @UniqueEntity(
 *     fields={"username"},
 *     errorPath="username",
 *     message="user.username.unique"
 * )
 * @UniqueEntity(
 *     fields={"email"},
 *     errorPath="email",
 *     message="user.email.unique"
 * )
 *
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "ajax_user_get_user_profile",
 *          parameters = { "username" = "expr(object.getUsername())" }
 *      ),
 *      exclusion = @Hateoas\Exclusion(
 *          excludeIf = "expr(object.isDeleted())"
 *      )
 * )
 * @Hateoas\Relation(
 *      "posts",
 *      href = @Hateoas\Route(
 *          "ajax_post_get_user_posts",
 *          parameters = { "username" = "expr(object.getUsername())" }
 *      ),
 *      exclusion = @Hateoas\Exclusion(
 *          excludeIf = "expr(object.isDeleted())"
 *      )
 * )
 * @Hateoas\Relation(
 *      "upvotes",
 *      href = @Hateoas\Route(
 *          "ajax_post_get_user_posts_upvoted",
 *          parameters = { "username" = "expr(object.getUsername())" }
 *      ),
 *      exclusion = @Hateoas\Exclusion(
 *          excludeIf = "expr(object.isDeleted())"
 *      )
 * )
 * @Hateoas\Relation(
 *      "commented",
 *      href = @Hateoas\Route(
 *          "ajax_post_get_user_posts_commented",
 *          parameters = { "username" = "expr(object.getUsername())" }
 *      ),
 *      exclusion = @Hateoas\Exclusion(
 *          excludeIf = "expr(object.isDeleted())"
 *      )
 * )
 * @Hateoas\Relation(
 *      "stream",
 *      href = @Hateoas\Route(
 *          "ajax_post_get_user_posts_stream",
 *          parameters = { "username" = "expr(object.getUsername())" }
 *      ),
 *      exclusion = @Hateoas\Exclusion(
 *          excludeIf = "expr(object.isDeleted())"
 *      )
 * )
 * @Hateoas\Relation(
 *      "followers",
 *      href = @Hateoas\Route(
 *          "ajax_user_get_user_followers",
 *          parameters = { "username" = "expr(object.getUsername())" }
 *      ),
 *      exclusion = @Hateoas\Exclusion(
 *          excludeIf = "expr(object.isDeleted())"
 *      )
 * )
 * @Hateoas\Relation(
 *      "followings",
 *      href = @Hateoas\Route(
 *          "ajax_user_get_user_following",
 *          parameters = { "username" = "expr(object.getUsername())" }
 *      ),
 *      exclusion = @Hateoas\Exclusion(
 *          excludeIf = "expr(object.isDeleted())"
 *      )
 * )
 * @Hateoas\Relation(
 *      "follow",
 *      href = @Hateoas\Route(
 *          "ajax_user_follow_user",
 *          parameters = { "username" = "expr(object.getUsername())" }
 *      ),
 *      exclusion = @Hateoas\Exclusion(
 *          excludeIf = "expr(object.isDeleted())"
 *      )
 * )
 * @Hateoas\Relation(
 *      "unfollow",
 *      href = @Hateoas\Route(
 *          "ajax_user_un_follow_user",
 *          parameters = { "username" = "expr(object.getUsername())" }
 *      ),
 *      exclusion = @Hateoas\Exclusion(
 *          excludeIf = "expr(object.isDeleted())"
 *      )
 * )
 * @Hateoas\Relation(
 *      "badges",
 *      href = @Hateoas\Route(
 *          "ajax_user_get_user_badges",
 *          parameters = { "username" = "expr(object.getUsername())" }
 *      ),
 *      embedded = "expr(service('rasen_ninegag.serialization_helper').getUserBadges(object))",
 *      exclusion = @Hateoas\Exclusion(
 *          excludeIf = "expr(object.isDeleted())"
 *      )
 * )
 *
 * @author Nima Karimi <nima.k68@gmail.com>
 */
class User extends BaseUser
{
	const TRUST_POST_COUNT = 20;
	const TRUST_COMMENT_COUNT = 20;

	const GENDER_NOT_KNOWN = 0;
	const GENDER_MALE = 1;
	const GENDER_FEMALE = 2;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /** @ORM\Column(name="facebook_id", type="string", length=255, nullable=true) */
    protected $facebook_id;

    /** @ORM\Column(name="facebook_access_token", type="string", length=255, nullable=true) */
    protected $facebook_access_token;

    /** @ORM\Column(name="google_id", type="string", length=255, nullable=true) */
    protected $google_id;

    /** @ORM\Column(name="google_access_token", type="string", length=255, nullable=true) */
    protected $google_access_token;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=255, nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"default", "own"})
     *
     * @Assert\Length(
     *      min = 1,
     *      max = 255,
     *      minMessage = "user.first_name.length_min",
     *      maxMessage = "user.first_name.length_max"
     * )
     */
    protected $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=255, nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"default", "own"})
     *
     * @Assert\Length(
     *      min = 1,
     *      max = 255,
     *      minMessage = "user.last_name.length_min",
     *      maxMessage = "user.last_name.length_max"
     * )
     */
    protected $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=45, nullable=true)
     *
     * @Assert\Regex(
     *  pattern = "/^(\+\d{1,2}\s?)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/",
     *  message = "user.phone.regex"
     * )
     */
    protected $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="profile_pic_name", type="string", length=255, nullable=true)
     *
     */
    protected $profilePicName;

	/**
	 * @Vich\UploadableField(mapping="profile_pics", fileNameProperty="profilePicName")
	 *
	 * This is not a mapped field of entity metadata, just a simple property.
	 *
	 * @var File $profilePic
	 */
	protected $profilePic;

    /**
     * @var string
     *
     * @ORM\Column(name="profile_header_pic_name", type="string", length=255, nullable=true)
     *
     */
    protected $profileHeaderPicName;

	/**
	 * @Vich\UploadableField(mapping="profile_header_pics", fileNameProperty="profileHeaderPicName")
	 *
	 * This is not a mapped field of entity metadata, just a simple property.
	 *
	 * @var File $profileHeaderPic
	 */
	protected $profileHeaderPic;

    /**
     * A short description or slogan text (appears on top of user's profile page)
     *
     * @var string
     *
     * @ORM\Column(name="profile_bio", type="string", length=255, nullable=true)
     *
     * @Assert\Length(
     *      min = 1,
     *      max = 255,
     *      minMessage = "user.profile_bio.length_min",
     *      maxMessage = "user.profile_bio.length_max"
     * )
     */
    protected $profileBio;

    /**
     * Is the user an trusted user
     *
     * This defines whether an user is trusted.
     * Active users can post without moderation, meaning their posts does not need an administrator's approval in order to appear on the stream.
     *
     * @var boolean
     *
     * @ORM\Column(name="is_trusted_user", type="boolean", nullable=false)
     */
    protected $isTrustedUser;

    /**
     * Specifies user's gender based on ISO/IEC 5218 standard
     *
     * Possible values:
     * {
     *   **0** = Not Known
     *   **1** = Male
     *   **2** = Female
     * }
     *
     * @var integer
     *
     * @ORM\Column(name="gender", type="smallint", nullable=false, options={"unsigned":true, "default":0})
     *
     * @Assert\Choice(
     *  choices = {0, 1, 2},
     *  message = "user.gender.choice"
     * )
     */
    protected $gender;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthday", type="date", nullable=true)
     *
     * @Assert\Date(message = "user.birthday.date")
     */
    protected $birthday;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     *
     * @ORM\Column(name="created_time", type="datetime", nullable=true)
     */
    protected $createdTime;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     *
     * @ORM\Column(name="last_modified_time", type="datetime", nullable=true)
     */
    protected $lastModifiedTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * Stores the user's followers count.
     *
     * @var integer
     *
     * @ORM\Column(name="followers_no", type="integer", nullable=true, options={"unsigned":true, "default":0})
     */
    protected $followersNo;

    /**
     * Stores the user's following count.
     *
     * @var integer
     *
     * @ORM\Column(name="following_no", type="integer", nullable=true, options={"unsigned":true, "default":0})
     */
    protected $followingNo;

    /**
     * Stores the user's total posts count.
     *
     * @var integer
     *
     * @ORM\Column(name="posts_no", type="integer", nullable=true, options={"unsigned":true, "default":0})
     */
    protected $postsNo;

    /**
     * Stores the user's total approved posts count.
     *
     * @var integer
     *
     * @ORM\Column(name="approved_posts_no", type="integer", nullable=true, options={"unsigned":true, "default":0})
     */
    protected $approvedPostsNo;

    /**
     * Stores the user's total comments count.
     *
     * @var integer
     *
     * @ORM\Column(name="comments_no", type="integer", nullable=true, options={"unsigned":true, "default":0})
     */
    protected $commentsNo;

	/**
	 * Stores the user's total approved comments count.
	 *
	 * @var integer
	 *
	 * @ORM\Column(name="approved_comments_no", type="integer", nullable=true, options={"unsigned":true, "default":0})
	 */
	protected $approvedCommentsNo;

    /**
     * Stores the user's total points (scores) count.
     *
     * @var integer
     *
     * @ORM\Column(name="points_no", type="integer", nullable=true, options={"unsigned":true, "default":0})
     */
    protected $pointsNo;

    /**
     * Stores the user's total upvotes count.
     *
     * @var integer
     *
     * @ORM\Column(name="upvotes_no", type="integer", nullable=true, options={"unsigned":true, "default":0})
     */
    protected $upvotesNo;

	/**
	 * Reports (inappropriate flags) count
	 *
	 * @var integer
	 *
	 * @ORM\Column(name="reports_no", type="integer", nullable=true, options={"unsigned":true, "default":0})
	 */
	protected $reportsNo;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
	    $this->resetCounters();
    }



    /**
     * Set facebook_id
     *
     * @param string $facebookId
     * @return User
     */
    public function setFacebookId($facebookId)
    {
        $this->facebook_id = $facebookId;

        return $this;
    }

    /**
     * Get facebook_id
     *
     * @return string
     */
    public function getFacebookId()
    {
        return $this->facebook_id;
    }
    /**
     * Set facebook_access_token
     *
     * @param string $facebookAccessToken
     * @return User
     */
    public function setFacebookAccessToken($facebookAccessToken)
    {
        $this->facebook_access_token = $facebookAccessToken;

        return $this;
    }

    /**
     * Get facebook_access_token
     *
     * @return string
     */
    public function getFacebookAccessToken()
    {
        return $this->facebook_access_token;
    }



    /**
     * Set google_id
     *
     * @param string $googleId
     * @return User
     */
    public function setGoogleId($googleId)
    {
        $this->google_id = $googleId;

        return $this;
    }

    /**
     * Get google_id
     *
     * @return string
     */
    public function getGoogleId()
    {
        return $this->google_id;
    }
    /**
     * Set google_access_token
     *
     * @param string $googleAccessToken
     * @return User
     */
    public function setGoogleAccessToken($googleAccessToken)
    {
        $this->google_access_token = $googleAccessToken;

        return $this;
    }

    /**
     * Get google_access_token
     *
     * @return string
     */
    public function getGoogleAccessToken()
    {
        return $this->google_access_token;
    }

	/**
	 * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
	 * of 'UploadedFile' is injected into this setter to trigger the  update. If this
	 * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
	 * must be able to accept an instance of 'File' as the bundle will inject one here
	 * during Doctrine hydration.
	 *
	 * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
	 */
	public function setProfilePic(File $image)
	{
		$this->profilePic = $image;

		if ($image) {
			// It is required that at least one field changes if you are using doctrine
			// otherwise the event listeners won't be called and the file is lost
			$this->lastModifiedTime = new \DateTime('now');
		}
	}

	/**
	 * @return File
	 */
	public function getProfilePic()
	{
		return $this->profilePic;
	}

	/**
	 * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
	 * of 'UploadedFile' is injected into this setter to trigger the  update. If this
	 * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
	 * must be able to accept an instance of 'File' as the bundle will inject one here
	 * during Doctrine hydration.
	 *
	 * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
	 */
	public function setProfileHeaderPic(File $image)
	{
		$this->profileHeaderPic = $image;

		if ($image) {
			// It is required that at least one field changes if you are using doctrine
			// otherwise the event listeners won't be called and the file is lost
			$this->lastModifiedTime = new \DateTime('now');
		}
	}

	/**
	 * @return File
	 */
	public function getProfileHeaderPic()
	{
		return $this->profileHeaderPic;
	}

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set profilePicName
     *
     * @param string $profilePicName
     * @return User
     */
    public function setProfilePicName($profilePicName)
    {
        $this->profilePicName = $profilePicName;

        return $this;
    }

    /**
     * Get profilePicName
     *
     * @return string 
     */
    public function getProfilePicName()
    {
        return $this->profilePicName;
    }

    /**
     * Set profileHeaderPicName
     *
     * @param string $profileHeaderPicName
     * @return User
     */
    public function setProfileHeaderPicName($profileHeaderPicName)
    {
        $this->profileHeaderPicName = $profileHeaderPicName;

        return $this;
    }

    /**
     * Get profileHeaderPicName
     *
     * @return string
     */
    public function getProfileHeaderPicName()
    {
        return $this->profileHeaderPicName;
    }

    /**
     * Set profileBio
     *
     * @param string $profileBio
     * @return User
     */
    public function setProfileBio($profileBio)
    {
        $this->profileBio = $profileBio;

        return $this;
    }

    /**
     * Get profileBio
     *
     * @return string 
     */
    public function getProfileBio()
    {
        return $this->profileBio;
    }

    /**
     * Set isTrustedUser
     *
     * @param boolean $isTrustedUser
     * @return User
     */
    public function setIsTrustedUser($isTrustedUser)
    {
        $this->isTrustedUser = $isTrustedUser;

        return $this;
    }

    /**
     * Get isTrustedUser
     *
     * @return boolean 
     */
    public function getIsTrustedUser()
    {
        return $this->isTrustedUser;
    }

    /**
     * Set gender
     *
     * @param integer $gender
     * @return User
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return integer 
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Get gender
     *
     * @return integer
     */
    public static function getGenderList()
    {
        return array(
	        User::GENDER_NOT_KNOWN,
	        User::GENDER_MALE,
	        User::GENDER_FEMALE
        );
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     * @return User
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime 
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set createdTime
     *
     * @param \DateTime $createdTime
     * @return User
     */
    public function setCreatedTime($createdTime)
    {
        $this->createdTime = $createdTime;

        return $this;
    }

    /**
     * Get createdTime
     *
     * @return \DateTime 
     */
    public function getCreatedTime()
    {
        return $this->createdTime;
    }

    /**
     * Set lastModifiedTime
     *
     * @param \DateTime $lastModifiedTime
     * @return User
     */
    public function setLastModifiedTime($lastModifiedTime)
    {
        $this->lastModifiedTime = $lastModifiedTime;

        return $this;
    }

    /**
     * Get lastModifiedTime
     *
     * @return \DateTime 
     */
    public function getLastModifiedTime()
    {
        return $this->lastModifiedTime;
    }

    /**
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime $deletedAt
     * @return User
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     *
     * @return boolean
     */
    public function isDeleted()
    {
        return ($this->deletedAt !== null);
    }

    /**
     * Set followersNo
     *
     * @param integer $followersNo
     * @return User
     */
    public function setFollowersNo($followersNo)
    {
        $this->followersNo = $followersNo;

        return $this;
    }

    /**
     * Get followersNo
     *
     * @return integer 
     */
    public function getFollowersNo()
    {
        return $this->followersNo;
    }

    /**
     * Set followingNo
     *
     * @param integer $followingNo
     * @return User
     */
    public function setFollowingNo($followingNo)
    {
        $this->followingNo = $followingNo;

        return $this;
    }

    /**
     * Get followingNo
     *
     * @return integer 
     */
    public function getFollowingNo()
    {
        return $this->followingNo;
    }

    /**
     * Set postsNo
     *
     * @param integer $postsNo
     * @return User
     */
    public function setPostsNo($postsNo)
    {
        $this->postsNo = $postsNo;

        return $this;
    }

    /**
     * Get postsNo
     *
     * @return integer 
     */
    public function getPostsNo()
    {
        return $this->postsNo;
    }

    /**
     * Set approvedPostsNo
     *
     * @param integer $approvedPostsNo
     * @return User
     */
    public function setApprovedPostsNo($approvedPostsNo)
    {
        $this->approvedPostsNo = $approvedPostsNo;

        return $this;
    }

    /**
     * Get approvedPostsNo
     *
     * @return integer
     */
    public function getApprovedPostsNo()
    {
        return $this->approvedPostsNo;
    }

    /**
     * Set pointsNo
     *
     * @param integer $pointsNo
     * @return User
     */
    public function setPointsNo($pointsNo)
    {
        $this->pointsNo = $pointsNo;

        return $this;
    }

    /**
     * Get commentsNo
     *
     * @return integer
     */
    public function getCommentsNo()
    {
        return $this->commentsNo;
    }

    /**
     * Set commentsNo
     *
     * @param integer $commentsNo
     * @return User
     */
    public function setCommentsNo($commentsNo)
    {
        $this->commentsNo = $commentsNo;

        return $this;
    }

	/**
	 * Set approvedCommentsNo
	 *
	 * @param integer $approvedCommentsNo
	 * @return User
	 */
	public function setApprovedCommentsNo($approvedCommentsNo)
	{
		$this->approvedCommentsNo = $approvedCommentsNo;

		return $this;
	}

	/**
	 * Get approvedCommentsNo
	 *
	 * @return integer
	 */
	public function getApprovedCommentsNo()
	{
		return $this->approvedCommentsNo;
	}

    /**
     * Get pointsNo
     *
     * @return integer 
     */
    public function getPointsNo()
    {
        return $this->pointsNo;
    }

	/**
	 * Set upvotesNo
	 *
	 * @param integer $upvotesNo
	 * @return User
	 */
	public function setUpvotesNo($upvotesNo)
	{
		$this->upvotesNo = $upvotesNo;

		return $this;
	}

	/**
	 * Get upvotesNo
	 *
	 * @return integer
	 */
	public function getUpvotesNo()
	{
		return $this->upvotesNo;
	}

	/**
	 * Set reportsNo
	 *
	 * @param integer $reportsNo
	 * @return User
	 */
	public function setReportsNo($reportsNo)
	{
		$this->reportsNo = $reportsNo;

		return $this;
	}

	/**
	 * Get reportsNo
	 *
	 * @return integer
	 */
	public function getReportsNo()
	{
		return $this->reportsNo;
	}

	/**
	 * Update followersNo
	 *
	 * Increase or Decrease one step
	 *
	 * @param bool $decrease
	 * @return User
	 */
	public function updateFollowersNo($decrease = false)
	{
		if ($this->followersNo == null) $this->followersNo = 0;
		$this->followersNo = $decrease ? $this->followersNo - 1 : $this->followersNo + 1;

		return $this;
	}

	/**
	 * Update followingNo
	 *
	 * Increase or Decrease one step
	 *
	 * @param bool $decrease
	 * @return User
	 */
	public function updateFollowingNo($decrease = false)
	{
		if ($this->followingNo == null) $this->followingNo = 0;
		$this->followingNo = $decrease ? $this->followingNo - 1 : $this->followingNo + 1;

		return $this;
	}

	/**
	 * Update postsNo
	 *
	 * Increase or Decrease one step
	 *
	 * @param bool $decrease
	 * @return User
	 */
	public function updatePostsNo($decrease = false)
	{
		if ($this->postsNo == null) $this->postsNo = 0;
		$this->postsNo = $decrease ? $this->postsNo - 1 : $this->postsNo + 1;

		return $this;
	}

	/**
	 * Update approvedPostsNo
	 *
	 * Increase or Decrease one step
	 *
	 * @param bool $decrease
	 * @return User
	 */
	public function updateApprovedPostsNo($decrease = false)
	{
		if ($this->approvedPostsNo == null) $this->approvedPostsNo = 0;
		$this->approvedPostsNo = $decrease ? $this->approvedPostsNo - 1 : $this->approvedPostsNo + 1;

		return $this;
	}

	/**
	 * Update commentsNo
	 *
	 * Increase or Decrease one step
	 *
	 * @param bool $decrease
	 * @return User
	 */
	public function updateCommentsNo($decrease = false)
	{
		if ($this->commentsNo == null) $this->commentsNo = 0;
		$this->commentsNo = $decrease ? $this->commentsNo - 1 : $this->commentsNo + 1;

		return $this;
	}

	/**
	 * Update approvedCommentsNo
	 *
	 * Increase or Decrease one step
	 *
	 * @param bool $decrease
	 * @return User
	 */
	public function updateApprovedCommentsNo($decrease = false)
	{
		if ($this->approvedCommentsNo == null) $this->approvedCommentsNo = 0;
		$this->approvedCommentsNo = $decrease ? $this->approvedCommentsNo - 1 : $this->approvedCommentsNo + 1;

		return $this;
	}

	/**
	 * Update pointsNo
	 *
	 * Increase or Decrease one step
	 *
	 * @param int $point
	 * @param bool $decrease
	 * @return User
	 */
	public function updatePointsNo($point, $decrease)
	{
		if ($this->pointsNo == null) $this->pointsNo = 0;
		$this->pointsNo = $decrease ? $this->pointsNo - $point : $this->pointsNo + $point;

		return $this;
	}

	/**
	 * Update reportsNo
	 *
	 * Increase or Decrease one step
	 *
	 * @param bool $decrease
	 * @return User
	 */
	public function updateReportsNo($decrease = false)
	{
		if ($this->reportsNo == null) $this->reportsNo = 0;
		$this->reportsNo = $decrease ? $this->reportsNo - 1 : $this->reportsNo + 1;

		return $this;
	}

	/**
	 * Update upvotesNo
	 *
	 * Increase or Decrease one step
	 *
	 * @param bool $decrease
	 * @return User
	 */
	public function updateUpvotesNo($decrease = false)
	{
		if ($this->upvotesNo == null) $this->upvotesNo = 0;
		$this->upvotesNo = $decrease ? $this->upvotesNo - 1 : $this->upvotesNo + 1;

		return $this;
	}

	/**
	 * Create new notification
	 *
	 * @return Notification
	 */
	public function newNotification()
	{
		$notif = new Notification();
		$notif->setUser($this);
		$notif->setReadStatus(false);

		return $notif;
	}

	/**
	 * Reset counters
	 */
	public function resetCounters()
	{
		$this->followersNo =
		$this->followingNo =
		$this->pointsNo =
		$this->postsNo =
		$this->approvedPostsNo =
		$this->upvotesNo =
		$this->commentsNo =
		$this->approvedCommentsNo =
		$this->reportsNo =
			0;
	}

	/**
	 * Checks if a user has more than "TRUST_POST_COUNT" approved posts.
	 *
	 * @return bool
	 */
	public function hasEarnedTrustToPost()
	{
		return ($this->getApprovedPostsNo() >= self::TRUST_POST_COUNT);
	}

	/**
	 * If the user is a trusted user AND it has more than "TRUST_POST_COUNT" approved posts, then he/she will be considered
	 * a TrustedToPost user. Only then his/her's post will be automatically approved.
	 *
	 * @return bool
	 */
	public function isTrustedToPost()
	{
		return ($this->getIsTrustedUser() && $this->hasEarnedTrustToPost());
	}

	/**
	 * @param $approvedBefore
	 * @param $approvedAfter
	 */
	public function changeApprovedPostsNoOnUpdate( $approvedBefore, $approvedAfter )
	{
		if ( $approvedBefore && ! $approvedAfter ) {
			$this->updateApprovedPostsNo( true ); //approvedPostNo--
		} elseif ( ! $approvedBefore && $approvedAfter ) {
			$this->updateApprovedPostsNo( false ); //approvedPostNo++
		}

	}

	/**
	 * @param $approvedBefore
	 * @param $approvedAfter
	 */
	public function changeApprovedCommentsNoOnUpdate( $approvedBefore, $approvedAfter )
	{
		if ( $approvedBefore && ! $approvedAfter ) {
			$this->updateApprovedCommentsNo( true ); //approvedCommentsNo--
		} elseif ( ! $approvedBefore && $approvedAfter ) {
			$this->updateApprovedCommentsNo( false ); //approvedCommentsNo++
		}

	}

	/**
	 * Updates reports counter on entity updates
	 *
	 * @param User $objBefore
	 * @param User $objAfter
	 */
	public function changeReportsNoOnUpdate(User $objBefore, User $objAfter)
	{
		if ($objBefore !== $objAfter) {
			$objBefore->updateReportsNo(true); // decrease last user's report count
			$objAfter->updateReportsNo(false); // increase new user's report count
		}
	}

	/**
	 * Update vote
	 *
	 * Increase or Decrease upVote/downVote one step depending on the vote
	 *
	 * @param bool $decrease
	 * @return User
	 */
	public function updateVotesNo($vote, $decrease = false)
	{
		if ($vote == true) {
			$this->updateUpvotesNo($decrease);
		} else {
			//$this->updateDownvotesNo($decrease);
		}

		return $this;
	}

	/**
	 * Updates votes counter on entity updates
	 *
	 * @param User $objBefore
	 * @param User $objAfter
	 * @param $voteBefore
	 * @param $voteAfter
	 */
	public function changeVotesNoOnUpdate(User $objBefore, User $objAfter, $voteBefore, $voteAfter)
	{
		if ($objBefore !== $objAfter) {
			$objBefore->updateVotesNo($voteBefore, true); // decrease last post's vote count
			$objAfter->updateVotesNo($voteAfter, false); // increase new post's vote count
		} else {
			if ($voteBefore && !$voteAfter) {
				$this->updateUpvotesNo(true); // upVotes--
				//$this->updateDownvotesNo(false); // downVotes++
			} elseif (!$voteBefore && $voteAfter) {
				$this->updateUpvotesNo(false); // upVotes++
				//$this->updateDownvotesNo(true); // downVotes--
			}

		}
	}

	/**
	 * Updates follower/following counter on entity updates
	 *
	 * @param User $objBefore
	 * @param User $objAfter
	 * @param User $followerBefore
	 * @param User $followerAfter
	 */
	public function changeFollowersNoOnUpdate(User $objBefore, User $objAfter, User $followerBefore, User $followerAfter)
	{
		if ( $objBefore !== $objAfter ) {
			$objBefore->updateFollowersNo( true ); // decrease last user's followers count
			$objAfter->updateFollowersNo( false ); // increase new user's followers count
		}
		if ( $followerBefore !== $followerAfter ) {
			$followerBefore->updateFollowingNo( true ); // decrease last user's following count
			$followerAfter->updateFollowingNo( false ); // increase new user's following count
		}
	}

	/**
	 * Updates points counter on entity updates
	 *
	 * @param User $objBefore
	 * @param User $objAfter
	 * @param $pointBefore
	 * @param $pointAfter
	 */
	public function changePointsNoOnUpdate(User $objBefore, User $objAfter, $pointBefore, $pointAfter)
	{
		if ($objBefore !== $objAfter) {
			$objBefore->updatePointsNo($pointBefore, true); // decrease last user's points
			$objAfter->updatePointsNo($pointAfter, false); // increase new user's points
		} else {
			$point = $pointAfter - $pointBefore;
			$this->updatePointsNo($point, false);
		}
	}

	/**
	 * Returns name (used in building notification message)
	 * @return string
	 */
	public function getName()
	{
		$name = $this->firstName != null ? $this->firstName : $this->username;
		return $name;
	}

	/**
	 * Returns full name
	 *
	 * @Serializer\VirtualProperty
	 * @Serializer\Groups({"default", "own"})
	 *
	 * @return string
	 */
	public function getFullName()
	{
		if ($this->firstName != null && $this->lastName != null) {
			$name = $this->firstName . ' ' . $this->lastName;
		} elseif ($this->firstName != null) {
			$name = $this->firstName;
		} elseif ($this->lastName != null) {
			$name = $this->lastName;
		} else {
			$name = $this->username;
		}
		return $name;
	}

    /**
     * @return User
     */
    public function makeDummy() {
        return $this->setUsername('-')->setFirstName('')->setLastName('')->setDeletedAt(new \DateTime());
    }
}
