<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 12/8/2014
 * Time: 2:15 AM
 */

namespace Rasen\NineGagBundle\DoctrineExtensions\DBAL\Types;

use Doctrine\DBAL\Types\DateTimeType;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;

/**
 * Class UTCDateTimeType
 * @package Rasen\NineGagBundle\DoctrineExtensions\DBAL\Types
 */
class UTCDateTimeType extends DateTimeType
{
	static private $utc = null;

	/**
	 * {@inheritdoc}
	 */
	public function convertToDatabaseValue($value, AbstractPlatform $platform)
	{
		if ($value === null) {
			return null;
		}

		if (is_null(self::$utc)) {
			self::$utc = new \DateTimeZone('UTC');
		}

		$value->setTimeZone(self::$utc);

		return $value->format($platform->getDateTimeFormatString());
	}

	/**
	 * {@inheritdoc}
	 */
	public function convertToPHPValue($value, AbstractPlatform $platform)
	{
		if ($value === null) {
			return null;
		}

		if (is_null(self::$utc)) {
			self::$utc = new \DateTimeZone('UTC');
		}

		$val = \DateTime::createFromFormat($platform->getDateTimeFormatString(), $value, self::$utc);

		if (!$val) {
			throw ConversionException::conversionFailed($value, $this->getName());
		}

		return $val;
	}
}