<?php
/**
 * Created by PhpStorm.
 * User: AmiR
 * Date: 10/29/14
 * Time: 10:42 AM
 */
namespace Rasen\NineGagBundle\Handler;

use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Translation\LoggingTranslator;
use Symfony\Component\Translation\Translator;

use JMS\DiExtraBundle\Annotation as DI;
/**
 * Class AuthenticationHandler
 *
 * @DI\Service("rasen_ninegag.authentication_handler")
 *
 * @package Rasen\NineGagBundle\Handler
 */
class AuthenticationHandler implements AuthenticationSuccessHandlerInterface, AuthenticationFailureHandlerInterface
{

    protected $router;

    protected $security;

    protected $userManager;

    protected $translator;

	/**
	 *
	 * * @DI\InjectParams({
	 *     "router" = @DI\Inject("router"),
	 *     "security" = @DI\Inject("security.context"),
	 *     "userManager" = @DI\Inject("fos_user.user_manager"),
	 *     "translator" = @DI\Inject("translator")
	 * })
	 *
	 * @param RouterInterface $router
	 * @param SecurityContext $security
	 * @param $userManager
	 * @param $translator
	 *
	 */
    public function __construct(RouterInterface $router, SecurityContext $security, $userManager, $translator)
    {
        $this->router = $router;
        $this->security = $security;
        $this->userManager = $userManager;
        $this->translator = $translator;

    }
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        if ($request->isXmlHttpRequest()) {

            $result = array('success' => true);
            $response = new Response(json_encode($result));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
        else {
            // Create a flash message with the authentication error message
            //$request->getSession()->getFlashBag()->set('error', $exception->getMessage());
            $url = $this->router->generate('rasen_ninegag_main_home');
            return new RedirectResponse($url);
        }
    }
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception) {

        if ($request->isXmlHttpRequest()) {
            $result = array(
                'success' => false,
                'function' => 'onAuthenticationFailure',
                'error' => true,
                'message' => $this->translator->trans($exception->getMessage(), array(), 'FOSUserBundle')
            );
            $response = new Response(json_encode($result));
            $response->headers->set('Content-Type', 'application/json');

            return $response;
        }
        else {
            $request->getSession()->set(SecurityContext::AUTHENTICATION_ERROR, $exception);
            $url = $this->router->generate('fos_user_security_login');
            return new RedirectResponse($url);
        }
    }
}