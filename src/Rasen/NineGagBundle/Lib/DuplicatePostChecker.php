<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 3/23/2015
 * Time: 11:08 PM
 */

namespace Rasen\NineGagBundle\Lib;

use Rasen\NineGagBundle\Entity\Post;
use Rasen\NineGagBundle\Entity\PostImage;
use Rasen\NineGagBundle\Entity\User;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class DuplicatePostChecker
 *
 * @DI\Service("rasen_ninegag.duplicate_post_checker")
 *
 * @package Rasen\NineGagBundle\Lib
 */
class DuplicatePostChecker
{

    private $redis;

    /**
     * * @DI\InjectParams({
     *     "redis" = @DI\Inject("snc_redis.default"),
     * })
     * @param $redis
     */
    public function __construct($redis)
    {
        $this->redis = $redis;
    }

    public function isDuplicate($checksum, User $uploadedBy)
    {
        $userId = $uploadedBy->getId();
        if ($this->redis->exists($userId.':uploaded:'.$checksum)){
            return $this->redis->get($userId.':uploaded:'.$checksum);
        }
        return false;
    }

    public function markAsUploaded(PostImage $post)
    {
        $userId = $post->getCreatedBy()->getId();
        $timeout = 60 * 60; // 1 hour
        $this->redis->set($userId.':uploaded:'.$post->getChecksum(), $post->getId(), $timeout);
    }

    public function calculateFileChecksum($filePath)
    {
        return sha1_file($filePath);
    }
}