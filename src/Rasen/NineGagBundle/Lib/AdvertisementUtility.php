<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 01/24/2015
 * Time: 10:20 PM
 */

namespace Rasen\NineGagBundle\Lib;

use Doctrine\ORM\EntityManager;
use \Rasen\NineGagBundle\Entity\User;
use \Rasen\NineGagBundle\Entity\Advertisement;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class AdvertisementUtility
 *
 *
 * @DI\Service("rasen_ninegag.advertisement")
 *
 * @package Rasen\NineGagBundle\Lib
 */
class AdvertisementUtility
{

	/**
	 * @var EntityManager
	 */
	private $em;

	/**
	 * * @DI\InjectParams({
	 *     "em" = @DI\Inject("doctrine.orm.entity_manager"),
	 * })
	 * @param EntityManager $em
	 */
	public function __construct(EntityManager $em)
	{
		$this->em = $em;
	}

	public function getAds($limit = null)
	{
        $ads = $this->em->getRepository('RasenNineGagBundle:Advertisement')->findAds($limit);
        return $ads;
	}

	public function mixAds($posts, $limit = null)
	{
        $ads = $this->getAds($limit);
        if (empty($ads)) {
            return $posts;
        }
        /**
         * @var Advertisement $ad
         */
        foreach ($ads as $ad)
        {
            if ($ad->getPosition() !== null) {
                $index = $ad->getPosition();
            } else {
                $index = rand(0, count($posts) - 1);
            }

            if ($ad->getAppearanceChance() !== null) {
                $chance = $ad->getAppearanceChance();
                $rnd = rand(1,100);
                if ($rnd <= $chance) {
                    array_splice($posts, $index, 0, array($ad));
                }
            } else {
                array_splice($posts, $index, 0, array($ad));
            }
        }

        return $posts;
	}















} 