<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 01/24/2015
 * Time: 10:20 PM
 */

namespace Rasen\NineGagBundle\Lib;

use Doctrine\ORM\EntityManager;
use Hashids\Hashids;
use \Rasen\NineGagBundle\Entity\User;
use \Rasen\NineGagBundle\Entity\Post;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class PostStatsUtility
 *
 *
 * @DI\Service("rasen_ninegag.post_stats_utility", scope="request")
 *
 * @package Rasen\NineGagBundle\Lib
 */
class PostStatsUtility
{

	/**
	 * @var EntityManager
	 */
	private $em;

	/**
	 * @var Hashids
	 */
	private $hashids;

	/**
	 * @var Request $request
	 */
	private $request;

	/**
	 * @var \Redis $redis
	 */
	private $redis;

	/**
	 * * @DI\InjectParams({
	 *     "em" = @DI\Inject("doctrine.orm.entity_manager"),
	 *     "hashids" = @DI\Inject("hashids"),
	 *     "request" = @DI\Inject("request"),
	 *     "redis" = @DI\Inject("snc_redis.default")
	 * })
	 * @param EntityManager $em
	 * @param Hashids $hashids
	 * @param Request $request
	 * @param \Redis $redis
	 */
	public function __construct(EntityManager $em,
								Hashids $hashids,
								Request $request,
								$redis)
	{
		$this->em = $em;
		$this->hashids = $hashids;
		$this->request = $request;
		$this->redis = $redis;
	}

	private function checkUnique()
	{
		/**
		 * if postId in in visitedCookie return false
		 * visitors = get_post_visitors_list(postId)
		 * If (user is logged in) {
		 * 		if (user id is in visitors) {
		 * 			return false
		 * 		} else {
		 * 			add_visitor(postId, userId, never expire)
		 * 		}
		 * }
		 * if (fingerprint is in visitors) {
		 * 		return false
		 * } else {
		 * 		add_visitor(postId, userId, never expire)
		 * }
		 * if (ip is in visitors) {
		 * 		return false
		 * } else {
		 * 		add_visitor(postId, Ip, expire 1 day)
		 * }
		 */

	}

	private function isUniqueUserVisit(User $user, Post $post)
	{
		$redisKey = $this->getUserVisitRedisKey($post);
		$userId = $user->getId();
		return !$this->redis->getBit($redisKey, $userId);
	}

	private function getUserVisitRedisKey(Post $post, \DateTime $date=null)
	{
		if ($date != null && $date instanceof \DateTime) {
			$timestamp = $date->format('Y-m-d');
			return 'post:' . $post->getId() . ':user-visit:' . $timestamp;
		}
		return 'post:' . $post->getId() . ':user-visit';
	}

	private function doUserVisit(User $user, Post $post)
	{
		if (!$this->isUniqueUserVisit($user, $post)) {
			return false;
		}

		$date = new \DateTime();
		$redisKey = $this->getUserVisitRedisKey($post, $date);

		$userId = $user->getId();

		$this->redis->setBit($redisKey, $userId, 1);
		return true;
	}

	private function doVisit($who, Post $post)
	{
		if ($who instanceof User) {
			$this->doUserVisit($who, $post);
		}
	}

    /**
     * @param array $posts
     * @param $user
     * @return bool|Cookie
     */
	public function increaseView($posts, $user)
	{
		if (!$this->request->cookies->has('PHPSESSID')) return false;

        if (empty($posts)) return false;

        $viewedPosts = array();
        if ($this->request->cookies->has('vds')) {
            $viewedPostsCookie = $this->request->cookies->get('vds', '');
            try {
                $viewedPostsCookie = gzinflate(self::base64url_decode($viewedPostsCookie));
                $viewedPosts = explode(',', $viewedPostsCookie);
            } catch (\Exception $e) {

            }
        }

        $posts = array_filter($posts, array($this, 'isPost'));

        $postHashIds = array_map(array($this, 'getPostHashId'), $posts);
        if (!empty($viewedPosts)) {
            $notVisitedHashIds = array_diff($postHashIds, $viewedPosts);
        } else {
            $notVisitedHashIds = $postHashIds;
        }
        if (empty($notVisitedHashIds)) return false;

        $viewedPosts = array_merge($notVisitedHashIds, $viewedPosts);

        $this->updatePostsViews($notVisitedHashIds);

        if (count($viewedPosts) > 75) {
            $viewedPosts = array_slice($viewedPosts, 0, 75);
        }

		$viewedPostsCookie = implode(',', $viewedPosts);
		$viewedPostsCookie = trim(trim($viewedPostsCookie), ',');
        $compressedCookie = self::base64url_encode(gzdeflate($viewedPostsCookie));
		$cookie = new Cookie('vds', $compressedCookie, time() + 315360000, '/', null, false, false);
		return $cookie;
	}

    /**
     * @param $postHashIds
     * @return bool
     */
    private function updatePostsViews($postHashIds)
    {
        if (count($postHashIds) < 1) return false;
        $postIds = array_values(array_map(array($this, 'decodeHashId'), $postHashIds));
        $postIdsComma = implode(' ', $postIds);

        $sql = "UPDATE posts p SET p.views_no = p.views_no + 1 WHERE p.id IN (:postIds)";
        $this->em->getConnection()->executeUpdate($sql, array('postIds' => $postIds), array('postIds' => \Doctrine\DBAL\Connection::PARAM_INT_ARRAY));

        return true;
    }

    /**
     * @param Post $post
     */
    public function increaseDownload(Post $post)
    {
        $sql = "UPDATE posts p SET p.downloads_no = p.downloads_no + 1 WHERE p.id = :postId";
        $this->em->getConnection()->executeUpdate($sql, array(
            'postId' => $post->getId()
        ));
    }

    /**
     * @param Post $post
     */
    public function increaseShare(Post $post)
    {
        $sql = "UPDATE posts p SET p.shares_no = p.shares_no + 1 WHERE p.id = :postId";
        $this->em->getConnection()->executeUpdate($sql, array(
            'postId' => $post->getId()
        ));
    }

    /**
     * Returns post
     *
     * @param $hashId
     *
     * @return Post
     */
    private function getPostObject($hashId)
    {
        $id = $this->hashids->decode($hashId);
        /**
         * @var Post $post
         */
        $post = $this->em->getRepository('RasenNineGagBundle:Post')->findOneBy(array(
            'id' => $id
        ));
        if (!$post) {
            throw new NotFoundHttpException();
        }
        return $post;
    }

    private function getPostHashId(Post $post)
    {
        return $this->hashids->encode($post->getId());
    }

    private function isPost($object)
    {
        return $object instanceof Post;
    }

    private function decodeHashId($hashId)
    {
        return $this->hashids->decode($hashId)[0];
    }

    private static function base64url_encode($data) {
        return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
    }

    private static function base64url_decode($data) {
        return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
    }
} 