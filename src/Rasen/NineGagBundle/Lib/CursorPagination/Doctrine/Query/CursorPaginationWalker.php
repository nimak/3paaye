<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 2/5/2015
 * Time: 5:44 PM
 */

namespace Rasen\NineGagBundle\Lib\CursorPagination\Doctrine\Query;

use Doctrine\ORM\Query\AST\AggregateExpression;
use Doctrine\ORM\Query\AST\ArithmeticExpression;
use Doctrine\ORM\Query\AST\ComparisonExpression;
use Doctrine\ORM\Query\AST\ConditionalExpression;
use Doctrine\ORM\Query\AST\ConditionalPrimary;
use Doctrine\ORM\Query\AST\ConditionalTerm;
use Doctrine\ORM\Query\AST\Literal;
use Doctrine\ORM\Query\AST\OrderByClause;
use Doctrine\ORM\Query\AST\OrderByItem;
use Doctrine\ORM\Query\AST\PathExpression;
use Doctrine\ORM\Query\AST\SelectExpression;
use Doctrine\ORM\Query\AST\SelectStatement;
use Doctrine\ORM\Query\AST\SimpleArithmeticExpression;
use Doctrine\ORM\Query\AST\WhereClause;
use Doctrine\ORM\Query\TreeWalkerAdapter;

/**
 * Class CursorPaginationWalker
 * @package Rasen\NineGagBundle\Lib\CursorPagination\Doctrine\Query
 */
class CursorPaginationWalker extends TreeWalkerAdapter
{
    /**
     * Time hint name
     */
    const HINT_PAGINATOR_BEFORE = 'paginate.before';
    const HINT_PAGINATOR_AFTER = 'paginate.after';

    /**
     * Walks down a SelectStatement AST node, thereby generating the appropriate SQL.
     *
     * @param SelectStatement $AST
     *
     * @return string The SQL.
     */
    public function walkSelectStatement(SelectStatement $AST)
    {
        $parent = null;
        $parentName = null;
        foreach ($this->_getQueryComponents() as $dqlAlias => $qComp) {
            if ($qComp['parent'] === null && $qComp['nestingLevel'] == 0) {
                $parent = $qComp;
                $parentName = $dqlAlias;
                break;
            }
        }

        $pathExpression = new PathExpression(
            PathExpression::TYPE_STATE_FIELD | PathExpression::TYPE_SINGLE_VALUED_ASSOCIATION, $parentName,
            $parent['metadata']->getSingleIdentifierFieldName()
        );
        $pathExpression->type = PathExpression::TYPE_STATE_FIELD;

        $arithmeticExpression = new ArithmeticExpression();
        $arithmeticExpression->simpleArithmeticExpression = new SimpleArithmeticExpression(
            array($pathExpression)
        );

        $before = $this->_getQuery()->getHint(self::HINT_PAGINATOR_BEFORE);
        $after = $this->_getQuery()->getHint(self::HINT_PAGINATOR_AFTER);

        if (($after != null && is_int($after)) || ($before != null && is_int($before))) {
            if ($after != null && is_int($after)) {
                $literal = new Literal(Literal::NUMERIC, $after);
                $operator = '<';
            } else {
                $literal = new Literal(Literal::NUMERIC, $before);
                $operator = '>';
            }
            $literalArithmeticExpression = new ArithmeticExpression();
            $literalArithmeticExpression->simpleArithmeticExpression = $literal;
            $comparisonExpression = new ComparisonExpression($arithmeticExpression, $operator, $literalArithmeticExpression);

            $conditionalPrimary = new ConditionalPrimary();
            $conditionalPrimary->simpleConditionalExpression = $comparisonExpression;

            if ($AST->whereClause) {
                if ($AST->whereClause->conditionalExpression instanceof ConditionalTerm) {
                    $AST->whereClause->conditionalExpression->conditionalFactors[] = $conditionalPrimary;
                } elseif ($AST->whereClause->conditionalExpression instanceof ConditionalPrimary) {
                    $AST->whereClause->conditionalExpression = new ConditionalExpression(array(
                        new ConditionalTerm(array(
                            $AST->whereClause->conditionalExpression,
                            $conditionalPrimary
                        ))
                    ));
                } elseif ($AST->whereClause->conditionalExpression instanceof ConditionalExpression) {
                    $tmpPrimary = new ConditionalPrimary;
                    $tmpPrimary->conditionalExpression = $AST->whereClause->conditionalExpression;
                    $AST->whereClause->conditionalExpression = new ConditionalTerm(array(
                        $tmpPrimary,
                        $conditionalPrimary
                    ));
                }
            } else {
                $AST->whereClause = new WhereClause(
                    new ConditionalExpression(array(
                        new ConditionalTerm(array(
                            $conditionalPrimary
                        ))
                    ))
                );
            }

            //die(var_dump($AST->orderByClause));
        }

        $order = new OrderByItem($pathExpression);
        $order->type = "DESC";
        if ($AST->orderByClause !== null) {
            $AST->orderByClause->orderByItems = array($order);
        } else {
            $AST->orderByClause = new OrderByClause(array($order));
        }

        //$AST->whereClause->conditionalExpression = $conditionalPrimary;
        //die(var_dump($AST->whereClause->conditionalExpression));
    }
}