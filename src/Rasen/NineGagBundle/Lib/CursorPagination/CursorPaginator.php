<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 2/4/2015
 * Time: 12:40 AM
 */

namespace Rasen\NineGagBundle\Lib\CursorPagination;

use Doctrine\ORM\Query;
use Hashids\Hashids;
use Hateoas\Representation\CollectionRepresentation;
use Rasen\NineGagBundle\Hateoas\Representation\CursorPaginatedRepresentation;
use Rasen\NineGagBundle\Lib\AdvertisementUtility;
use Rasen\NineGagBundle\Lib\CursorPagination\Doctrine\Query\CursorPaginationWalker;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Router;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * @TODO: Complete this cursor paginator for future versions (this is the current method used by facebook, twitter, etc.)
 * Class CursorPaginator
 *
 * @DI\Service("rasen_ninegag.cursor_paginator", scope="request")
 *
 * @package Rasen\NineGagBundle\Lib\CursorPagination
 */
class CursorPaginator
{
    /**
     * @var Request $request
     */
    private $request;

    /**
     * @var Router $router
     */
    private $router;

    /**
     * @var Hashids $hashids
     */
    private $hashids;

    /**
     * @var AdvertisementUtility $advertisementUtil
     */
    private $advertisementUtil;

    /**
     * * @DI\InjectParams({
     *     "request" = @DI\Inject("request"),
     *     "router" = @DI\Inject("router"),
     *     "hashids" = @DI\Inject("hashids"),
     *     "advertisementUtil" = @DI\Inject("rasen_ninegag.advertisement"),
     * })
     * @param Request $request
     * @param Router $router
     * @param Hashids $hashids
     * @param AdvertisementUtility $advertisementUtil
     */
    public function __construct(Request $request, Router $router, Hashids $hashids, AdvertisementUtility $advertisementUtil)
    {
        $this->request = $request;
        $this->router = $router;
        $this->hashids = $hashids;
        $this->advertisementUtil = $advertisementUtil;
    }

    /**
     * @param Query $query
     * @param Cursor $cursor
     * @param int $limit
     * @return array
     */
    public function paginate(Query $query, Cursor $cursor, $limit = 10)
    {
        if ($cursor->getAfter()) {
            $after = $this->hashids->decode($cursor->getAfter());
            if (count($after) > 0) {
                $query->setHint(CursorPaginationWalker::HINT_PAGINATOR_AFTER, $after[0]);
            }
        } elseif ($cursor->getBefore()) {
            $before = $this->hashids->decode($cursor->getBefore());
            if (count($before) > 0) {
                $query->setHint(CursorPaginationWalker::HINT_PAGINATOR_BEFORE, $before[0]);
            }
        }

        $query->setHint(Query::HINT_CUSTOM_TREE_WALKERS, array('Rasen\NineGagBundle\Lib\CursorPagination\Doctrine\Query\CursorPaginationWalker'));

        $query->setMaxResults($limit);
        $items = $query->getResult();

        $paginationData = array(
            'limit' => $limit
        );

        $cursors = array();
        if (count($items) > 0) {
            $firstPost = reset($items);
            $cursors['before'] = $this->hashids->encode($firstPost->getId());

            $lastPost = end($items);
            $cursors['after'] = $this->hashids->encode($lastPost->getId());

            if (array_key_exists('after', $cursors) && $cursors['after'] && count($items) >= $limit){
                $currentRouteParams = $this->request->get('_route_params');
                $paginationParams = array(
                    'after' => $cursors['after'],
                    'limit' => $limit
                );
                $paginationData['next'] = $this->router->generate($this->request->get('_route'), array_merge($currentRouteParams, $paginationParams));
            }
        } else {
            if ($cursor->getAfter()) {
                $cursors['before'] = $cursors['after'] = $cursor->getAfter();
            } elseif ($cursor->getBefore()) {
                $cursors['before'] = $cursors['after'] = $cursor->getBefore();
            }
        }

        if (array_key_exists('before', $cursors) && $cursors['before'])  {
            $currentRouteParams = $this->request->get('_route_params');
            $paginationParams = array(
                'before' => $cursors['before'],
                'limit' => $limit
            );
            $paginationData['previous'] = $this->router->generate($this->request->get('_route'), array_merge($currentRouteParams, $paginationParams));
        }

        $paginationData['cursors'] = $cursors;

        $result = array(
            'items' => $items,
            'paginationData' => $paginationData
        );

        return $result;
    }

    /**
     * @param Request $request
     * @param Query $query
     * @param string $what Name of the embedded collection of objects (usually class name)
     * @param boolean $includeAds
     * @return array
     */
    public function cursorPaginate(Request $request, Query $query, $what = 'items', $includeAds = false)
    {
        $limit = $request->query->get('limit', 25);

        $cursor = new Cursor();

        if ($request->query->has('after')) {
            $afterHashId = $request->query->get('after');
            $cursor->setAfter($afterHashId);
        } elseif ($request->query->has('before')) {
            $beforeHashId = $request->query->get('before');
            $cursor->setBefore($beforeHashId);
        }

        $pagination = $this->paginate(
            $query,
            $cursor,
            $limit
        );

        if ($includeAds) {
            $pagination['items'] = $this->advertisementUtil->mixAds($pagination['items']);
        }

        $collectionRepresentation = new CollectionRepresentation(
            $pagination['items'],
            $what
        );
        $cursorPaginatedRepresentation = new CursorPaginatedRepresentation(
            $collectionRepresentation,
            $request->get('_route'),
            $request->get('_route_params'),
            $pagination['paginationData']
        );

        return $cursorPaginatedRepresentation;
    }
}