<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 2/4/2015
 * Time: 1:14 PM
 */

namespace Rasen\NineGagBundle\Lib\CursorPagination;

/**
 * Class Cursor
 * @package Rasen\NineGagBundle\Lib\CursorPagination
 */
class Cursor
{
    /**
     * after cursor value.
     *
     * @var mixed
     */
    protected $after;

    /**
     * before cursor value.
     *
     * @var mixed
     */
    protected $before;

    /**
     * Create a new Cursor instance.
     *
     * @param mixed $after
     * @param mixed $before
     */
    public function __construct($after = null, $before = null)
    {
        $this->after = $after;
        $this->before = $before;
    }

    /**
     * Get the after cursor value.
     *
     * @return mixed
     */
    public function getAfter()
    {
        return $this->after;
    }

    /**
     * Set the after cursor value.
     *
     * @param int $after
     *
     * @return Cursor
     */
    public function setAfter($after)
    {
        $this->after = $after;
        return $this;
    }

    /**
     * Get the before cursor value.
     *
     * @return mixed
     */
    public function getBefore()
    {
        return $this->before;
    }

    /**
     * Set the before cursor value.
     *
     * @param int $before
     *
     * @return Cursor
     */
    public function setBefore($before)
    {
        $this->before = $before;
        return $this;
    }
}