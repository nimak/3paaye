<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 11/28/2014
 * Time: 6:28 PM
 */

namespace Rasen\NineGagBundle\Lib;

use Doctrine\ORM\EntityManager;
use JMS\DiExtraBundle\Annotation as DI;
use Rasen\NineGagBundle\Entity\PostImage;
use Rasen\NineGagBundle\Entity\Watermark;
use Rasen\NineGagBundle\Lib\ImageProcessor;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Routing\RouterInterface;
use Vich\UploaderBundle\Storage\StorageInterface;

/**
 * Class PostImageProcessor
 *
 * @DI\Service("rasen_ninegag.post_image_processor")
 *
 * @package Rasen\NineGagBundle\Lib
 */
class PostImageProcessor
{
	const WATERMARK_UPLOAD_DIR = '/../web/uploads/watermarks/';
	const ADS_UPLOAD_DIR = '/../web/uploads/ads/';
	const POST_UPLOAD_DIR = '/../web/uploads/posts/';
	const VIDEO_CACHE_DIR = 'videos';
	const THUMB_CACHE_DIR = 'thumbs';

	/**
	 * @var EntityManager
	 */
	private $em;

	private $kernelRootDir;

	private $rootUploadDir;

	private $watermarkUploadDir;

	/**
	 * @var ImageProcessor
	 */
	private $imageProcessor;

	/**
	 * @var string
	 */
	private $postCacheDir;

	/**
	 * @var string
	 */
	private $postCacheUrl;

	/**
	 * @var Router $router
	 */
	private $router;

	/**
	 * @var CDNHostChoose $cdnHostChoose
	 */
	private $cdnHostChoose;

	private $environment;

	/**
	 * * @DI\InjectParams({
	 *     "em" = @DI\Inject("doctrine.orm.entity_manager"),
	 *     "kernelRootDir" = @DI\Inject("%kernel.root_dir%"),
	 *     "imageProcessor" = @DI\Inject("rasen_ninegag.image_processor"),
	 *     "postCacheDir" = @DI\Inject("%rasen_ninegag.post_cache_dir%"),
	 *     "postCacheUrl" = @DI\Inject("%rasen_ninegag.post_cache_url%"),
	 *     "router" = @DI\Inject("router"),
	 *     "cdnHostChoose" = @DI\Inject("rasen_ninegag.cdn_host_choose"),
	 *     "environment" = @DI\Inject("%kernel.environment%")
	 * })
	 * @param EntityManager $em
	 * @param $kernelRootDir
	 * @param ImageProcessor $imageProcessor
	 * @param string $postCacheDir
	 * @param $postCacheUrl
	 * @param Router $router
	 * @param CDNHostChoose $cdnHostChoose
	 * @param $environment
	 */
	public function __construct(
        EntityManager $em,
		$kernelRootDir,
        ImageProcessor $imageProcessor,
		$postCacheDir,
		$postCacheUrl,
        Router $router,
        CDNHostChoose $cdnHostChoose,
        $environment)
	{
		$this->em = $em;
		$this->kernelRootDir = $kernelRootDir;
		$this->imageProcessor = $imageProcessor;
		$this->postCacheDir = $postCacheDir;
		$this->postCacheUrl = $postCacheUrl;
		$this->router = $router;
		$this->cdnHostChoose = $cdnHostChoose;
		$this->environment = $environment;

		$this->rootUploadDir = $kernelRootDir.self::POST_UPLOAD_DIR;
		$this->watermarkUploadDir = $kernelRootDir.self::WATERMARK_UPLOAD_DIR;
	}

	public function process(PostImage $postImage)
	{
		/*$fileDir = dirname($imagePath);
		$lastSubDir = str_replace(dirname($fileDir), '', $fileDir);*/

		$postImage->setIsProcessed(false);
		$this->em->flush();

		$saveToPath = $this->getPostImageCachePath($postImage);
		$fileAbsoulutePath = $this->getPostImageUploadPath($postImage);

        $watermark = null;
        if ($postImage->getCategory()->getWatermark()) {
            $watermark = array(
                'path' => $this->getWatermarkUploadPath($postImage->getCategory()->getWatermark()),
                'width' => $postImage->getCategory()->getWatermark()->getWidth(),
                'height' => $postImage->getCategory()->getWatermark()->getHeight()
            );
        }

		$this->imageProcessor->process(
            $postImage->getId(),
            $fileAbsoulutePath,
            $saveToPath,
            $watermark
        );
	}

	public function purge(PostImage $postImage)
	{
		$cachePath = $this->getPostImageCachePath($postImage);

		$fileNameNoExt = pathinfo($postImage->getImageUrl(), PATHINFO_FILENAME);

		$this->imageProcessor->purge($cachePath, $fileNameNoExt);
	}

	private function getPostImageUploadPath(PostImage $postImage)
	{
		$filePath = $postImage->getImageUrl();
		return $this->rootUploadDir.$filePath;
	}

	private function getWatermarkUploadPath(Watermark $watermark)
	{
		$filePath = $watermark->getImageName();
		return $this->watermarkUploadDir.$filePath;
	}

	public function getProcessedImage(PostImage $postImage, $filter)
	{
		if ($this->isProcessed($postImage, $filter)) {
			return $this->generateUrl($postImage, $filter);
		} else {
			$this->process($postImage);
			return false;
		}

	}

	public function isProcessed(PostImage $postImage, $filter)
	{
		$cachePath = $this->postCacheDir;
		$fileName = $this->generateFileName($postImage, $filter);

		$path = self::join_paths($cachePath,$fileName);

		return file_exists($path);
	}

    private function getHost($fileName)
    {
        return $this->router->getContext()->getHost();

        /*if (in_array($this->environment, array('dev', 'test'))) {
            return $this->router->getContext()->getHost();
        }
        return $this->cdnHostChoose->getCDNHost($fileName).'.'.$this->router->getContext()->getHost();*/
    }

	public function generateUrl($postImage, $filter)
	{
        $fileName = $this->generateFileName($postImage, $filter);

        $baseUrl = $this->router->getContext()->getBaseUrl();
        if ('.php' == substr($this->router->getContext()->getBaseUrl(), -4)) {
            $baseUrl = pathinfo($this->router->getContext()->getBaseurl(), PATHINFO_DIRNAME);
        }
        $baseUrl = rtrim($baseUrl, '/\\');

		$siteUrl = $this->router->getContext()->getScheme() . '://' . $this->getHost($fileName) . $baseUrl;
		$path = $siteUrl . self::join_paths($this->postCacheUrl,$fileName);
		return $path;
	}

	public function generateFileName($postImage, $filter)
	{
		$filePath = $this->getPostImageUploadPath($postImage);
		$fileDir = dirname($filePath);
		$lastSubDir = str_replace(dirname($fileDir), '', $fileDir);

		$fileNameNoExt = pathinfo($filePath, PATHINFO_FILENAME);

		if ($filter == ImageProcessor::FILTER_MP4) {
			$cacheSubDir = self::VIDEO_CACHE_DIR;
			$fileExt = '.mp4';
		} elseif ($filter == ImageProcessor::FILTER_WEBM) {
			$cacheSubDir = self::VIDEO_CACHE_DIR;
			$fileExt = '.webm';
		} elseif ($filter == ImageProcessor::FILTER_THUMB_700) {
			$cacheSubDir = self::THUMB_CACHE_DIR;
			$fileExt = '_700.jpg';
		} elseif ($filter == ImageProcessor::FILTER_THUMB_500) {
			$cacheSubDir = self::THUMB_CACHE_DIR;
			$fileExt = '_500.jpg';
		} elseif ($filter == ImageProcessor::FILTER_THUMB_FEATURE) {
            $cacheSubDir = self::THUMB_CACHE_DIR;
            $fileExt = '_feature.jpg';
        } elseif ($filter == ImageProcessor::FILTER_THUMB_FEATURE_SQ) {
			$cacheSubDir = self::THUMB_CACHE_DIR;
			$fileExt = '_feature_sq.jpg';
		} else {
			return false;
		}
		return self::join_paths($lastSubDir,$cacheSubDir,$fileNameNoExt.$fileExt);
	}

	private function getPostImageCachePath(PostImage $postImage)
	{
		$filePath = $postImage->getImageUrl();
		$fileDir = dirname($filePath);

		return self::join_paths($this->postCacheDir,$fileDir);
	}


	public static function join_paths() {
		$paths = array();

		foreach (func_get_args() as $arg) {
			if ($arg !== '') { $paths[] = $arg; }
		}

		return preg_replace('#/+#','/',join('/', $paths));
	}

} 