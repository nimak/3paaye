<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 2/14/2015
 * Time: 10:21 PM
 */

namespace Rasen\NineGagBundle\Lib;

use Hashids\Hashids;
use JMS\DiExtraBundle\Annotation as DI;
use Rasen\NineGagBundle\Entity\Post;
use Rasen\NineGagBundle\Entity\PostImage;
use Symfony\Component\Routing\Router;

/**
 * Class UrlSlugGenerator
 *
 * @DI\Service("rasen_ninegag.url_slug_generator")
 *
 * @package Rasen\NineGagBundle\Lib
 */
class UrlSlugGenerator
{

    /**
     * @var Hashids
     */
    protected $hashids;

    /**
     * @var Router $router
     */
    protected $router;

    /**
     * @DI\InjectParams({
     *     "hashids" = @DI\Inject("hashids"),
     *     "router" = @DI\Inject("router")
     * })
     * @param Hashids $hashids
     * @param Router $router
     */
    public function __construct(Hashids $hashids,
                                Router $router)
    {
        $this->hashids = $hashids;
        $this->router = $router;
        setlocale(LC_ALL, 'en_US.UTF8');
    }

    /**
     * @param $str
     * @param array $replace
     * @param string $delimiter
     * @return mixed|string
     */
    public function toAscii($str, $replace=array(), $delimiter='-') {
        if( !empty($replace) ) {
            $str = str_replace((array)$replace, ' ', $str);
        }

        $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
        $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
        $clean = strtolower(trim($clean, '-'));
        $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

        return $clean;
    }

    /**
     * Transliterate English numbers in a string to localized Farsi numbers and vice versa.
     *
     * @param $str
     * @param bool $toFa
     * @return mixed
     */
    public function enFaNumbers($str, $toFa = FALSE) {
        $en = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
        $localized = array('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹');
        $localized2 = array('٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩');

        $str = $toFa ? str_replace($en, $localized, $str) : str_replace($localized, $en, $str);
        $str = $toFa ? str_replace($en, $localized2, $str) : str_replace($localized2, $en, $str);
        return $str;
    }

    public function cleanPersianString($str)
    {
        $str .= '';

        $arab = array(
            '¬', 'ك', 'ي', 'إ', 'أ', 'ؤ', 'ئ', 'ة', 'ؼ', 'ؼ', 'ؽ', 'ؾ', 'ؿ',);
        $persian = array(
            '‌', 'ک', 'ی', 'ا', 'ا', 'و', 'ی', 'ه', 'ک', 'ک', 'ی', 'ی', 'ی',);
        $str = str_replace($arab, $persian, $str);

        $nonWords = array(
            'ـ', 'ً', 'ٌ', 'ٍ', 'َ', 'ُ', 'ِ', 'ّ', 'إ', 'ء', 'ْ', 'ٓ', 'ٔ', 'ٕ', 'ٖ', 'ٗ', '٘', 'ٙ', 'ٚ', 'ٛ', 'ٜ', 'ٝ', 'ٞ', 'ٟ',

        );
        $str = str_replace($nonWords, '', $str);


        $arab2 = array(
            'ؠ','ػ','ٵ','ٲ','ٳ','ٱ','ٶ','ٷ','ٸ','ٹ','ٺ','ٻ','ټ','ٽ','پ','ٿ','ڀ','ځ','ڂ','ڃ','ڄ','څ','ڇ','ڈ','ډ','ڊ','ڋ','ڌ','ڍ','ڎ','ڏ','ڐ','ڑ','ڒ','ړ','ڔ','ڕ','ږ','ڗ','ڙ','ښ','ڛ','ڜ','ڝ','ڞ','ڟ','ڠ','ڡ','ڢ','ڣ','ڤ','ڥ','ڦ','ڨ','ڧ','ڪ','ګ','ڬ','ڭ','ڮ','ڰ','ڱ','ڲ','ڳ','ڴ','ڵ','ڶ','ڷ','ڸ','ڹ','ں','ڻ','ڼ','ڽ','ڿ','ۀ','ہ','ۂ','ۃ','ۄ','ۅ','ۆ','ۇ','ۈ','ۉ','ۊ','ۋ','ۍ','ێ','ۏ','ې','ۑ','ۺ','ۻ','ۼ','۾','ۿ','ݐ','ݑ','ݒ','ݓ','ݔ','ݕ','ݖ','ݗ','ݘ','ݙ','ݚ','ݛ','ݜ','ݝ','ݞ','ݟ','ݠ','ݡ','ݢ','ݣ','ݤ','ݥ','ݦ','ݧ','ݨ','ݩ','ݪ','ݫ','ݬ','ݭ','ݮ','ݯ','ݰ','ݱ','ݲ','ݳ','ݴ','ݵ','ݶ','ݷ','ݸ','ݹ','ݼ','ݽ','ݾ','ݿ','ࢠ','ࢢ','ࢣ','ࢤ','ࢥ','ࢦ','ࢧ','ࢨ','ࢩ','ࢪ','ࢫ','ࢬ',
        );
        $persian2 = array(
            'ی', 'ک', 'ا', 'ا', 'ا', 'ا', 'و', 'و', 'ی', 'ث', 'ت', 'ب', 'ت', 'ت', 'پ', 'ت', 'ح', 'خ', 'ج', 'چ', 'ح', 'چ', 'د', 'د', 'د', 'ذ', 'ذ', 'ذ', 'ذ', 'ژ', 'ز', 'ر', 'ر', 'ر', 'ر', 'ز', 'ژ', 'س', 'س', 'ش', 'ص', 'ض', 'ظ', 'غ', 'ف', 'ف', 'ف', 'ف', 'ق', 'ق', 'ک', 'گ', 'ک', 'ک', 'ک', 'گ', 'گ', 'گ', 'گ', 'گ', 'ل', 'ل', 'ل', 'ل', 'ل', 'ن', 'ن', 'ن', 'ن', 'ن', 'چ', 'ه', 'ه', 'ه', 'ه', 'و', 'و', 'و', 'و', 'و', 'و', 'و', 'و', 'ی', 'ی', 'و', 'ی', 'ی', 'ش', 'ض', 'غ', 'م', 'ه', 'پ', 'ث', 'پ', 'ت', 'ت', 'پ', 'ت', 'ح', 'چ', 'د', 'د', 'ر', 'ش', 'ع', 'غ', 'غ', 'غ', 'ف', 'ف', 'ک', 'ک', 'ک', 'م', 'م', 'ن', 'ن', 'ن', 'ل', 'ز', 'ز', 'س', 'چ', 'چ', 'ش', 'ژ', 'ح', 'ا', 'ا', 'ی', 'ی', 'ی', 'و', 'و', 'چ', 'ش', 'ش', 'ک', 'ب', 'ج', 'ط', 'ف', 'ق', 'ل', 'م', 'ی', 'ی', 'ر', 'و', 'م',
        );
        $str = str_replace($arab2, $persian2, $str);

        $nonWords2 = array(        '؀','؁','؂','؃','؄','؆','؇','؈','؉','؊','؋','،','؍','؎','؏','ؑ','ؐ','ؒ','ؓ','ؔ','ؕ','ؖ','ؗ','ؘ','ؙ','ؚ','؛','؞','؟',
            '٪','٭','٫','٬','ٰ','ٱ','ٴ',
            'ے','ۓ','۔','ە','ۖ','ۗ','ۘ','ۙ','ۚ','ۛ','ۜ','۝','۞','۟','۠','ۢ','ۡ','ۣ','ۤ','ۥ','ۦ','ۧ','ۨ','۩','۪','۫','۬','ۭ','۽','ࢬ','ݺ','ݻ','ࣤ','ࣥ','ࣦ','ࣧ','ࣨ','ࣩ','࣪','࣫','࣬','࣭','࣮','࣯','ࣰ','ࣱ','ࣲ','ࣳ','ࣴ','ࣵ','ࣶ','ࣷ','ࣸ','ࣹ','ࣺ','ࣻ','ࣼ','ࣽ','ࣾ',

        );
        $str = str_replace($nonWords2, '', $str);
        $str = $this->enFaNumbers($str);

        if (! class_exists("Normalizer", $autoload = false))
            return $str;

        $str = \Normalizer::normalize($str, \Normalizer::FORM_C);
        return $str;
    }

    public function slugify($str, $maxLength = 100)
    {
        //$t1 = microtime(true);
        $str = $this->cleanPersianString($str);
        $text = preg_replace('/[^\\pL\d]+/u', '-', $str);
        $text = trim($text, '-');
        $text = preg_replace('/[^-\w]+/u', '', $text);
        $text = strtolower($text);
        $text = substr($text, 0 ,$maxLength);

        //$t2 = microtime(true);
        //die(var_dump(($t2-$t1)*1000000));
        return $text;
    }

    public function generateUrl(Post $post)
    {
        $hashid = $this->hashids->encode($post->getId());
        if ($post instanceof PostImage) {
            $slug = $this->slugify($post->getPostTitle());
            if ($slug != '') {
                $postUrl = $this->router->generate('rasen_ninegag_post_viewslug', array('hashId' => $hashid, 'slug'=>$slug),true);
            } else {
                $postUrl = $this->router->generate('rasen_ninegag_post_view', array('hashId' => $hashid),true);
            }
        } else {
            $postUrl = $this->router->generate('rasen_ninegag_post_view', array('hashId' => $hashid),true);
        }
        return $postUrl;
    }
}