<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 2/23/2015
 * Time: 8:47 PM
 */

namespace Rasen\NineGagBundle\Lib;

use JMS\DiExtraBundle\Annotation as DI;
use Liip\ImagineBundle\Imagine\Cache\Resolver\WebPathResolver;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Routing\RequestContext;

/**
 * Class CDNAssetsResolver
 * @DI\Service("rasen_ninegag.cdn_assets_resolver")
 * @DI\Tag("liip_imagine.cache.resolver", attributes = {"resolver" = "cdn_assets_resolver"})
 * @package Rasen\NineGagBundle\Lib
 */
class CDNAssetsResolver extends WebPathResolver
{
    protected $path;

    /**
     * @var CDNHostChoose
     */
    protected $cdnHostChoose;

    private $environment;

    /**
     * @DI\InjectParams({
     *     "filesystem" = @DI\Inject("filesystem"),
     *     "requestContext" = @DI\Inject("router.request_context"),
     *     "rootDir" = @DI\Inject("%kernel.root_dir%"),
     *     "cdnHostChoose" = @DI\Inject("rasen_ninegag.cdn_host_choose"),
     *     "environment" = @DI\Inject("%kernel.environment%")
     * })
     * @param Filesystem $filesystem
     * @param RequestContext $requestContext
     * @param string $rootDir
     * @param CDNHostChoose $cdnHostChoose
     * @param $environment
     */
    public function __construct(
        Filesystem $filesystem,
        RequestContext $requestContext,
        $rootDir,
        CDNHostChoose $cdnHostChoose,
        $environment
    )
    {
        $webRoot = $rootDir.'/../web';
        parent::__construct($filesystem, $requestContext, $webRoot, 'media/cache');
        $this->cdnHostChoose = $cdnHostChoose;
        $this->environment = $environment;
    }

    protected function getCDNHost()
    {
        if (in_array($this->environment, array('dev', 'test'))) {
            //return $this->requestContext->getHost();
        }
        $fileNameNoExt = pathinfo($this->path, PATHINFO_FILENAME);
        $cdnHost = $this->cdnHostChoose->getCDNHost($fileNameNoExt);
        return $cdnHost.'.'.$this->requestContext->getHost();
    }

    /**
     * {@inheritDoc}
     */
    public function resolve($path, $filter)
    {
        $this->path = $path;
        return sprintf('%s/%s',
            $this->getBaseUrl(),
            $this->getFileUrl($path, $filter)
        );
    }

    /**
     * @return string
     */
    protected function getBaseUrl()
    {
        $port = '';
        if ('https' == $this->requestContext->getScheme() && $this->requestContext->getHttpsPort() != 443) {
            $port =  ":{$this->requestContext->getHttpsPort()}";
        }

        if ('http' == $this->requestContext->getScheme() && $this->requestContext->getHttpPort() != 80) {
            $port =  ":{$this->requestContext->getHttpPort()}";
        }

        $baseUrl = $this->requestContext->getBaseUrl();
        if ('.php' == substr($this->requestContext->getBaseUrl(), -4)) {
            $baseUrl = pathinfo($this->requestContext->getBaseurl(), PATHINFO_DIRNAME);
        }
        $baseUrl = rtrim($baseUrl, '/\\');

        return sprintf('%s://%s%s%s',
            $this->requestContext->getScheme(),
            $this->getCDNHost(),
            $port,
            $baseUrl
        );
    }
/*
* CUSTOMIZED CACHE FOLDER NAMES

    protected function getFileUrl($path, $filter)
    {
        // crude way of sanitizing URL scheme ("protocol") part
        $path = str_replace('://', '---', $path);

        $fileName = pathinfo($path, PATHINFO_BASENAME);
        $dirName = trim(str_replace('/', '', pathinfo($path, PATHINFO_DIRNAME)), '/');
        $folderName = $filter.$dirName;
        $hashedFolder = $this::base64url_encode(substr(sha1($folderName), 0, 20));

        return $this->cachePrefix.'/'.$hashedFolder.'/'.trim($fileName, '/');
    }

    private static function base64url_encode($data) {
        return rtrim(base64_encode($data), '=');
    }*/
}