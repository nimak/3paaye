<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 11/22/2014
 * Time: 3:12 PM
 */

namespace Rasen\NineGagBundle\Lib;

use OldSound\RabbitMqBundle\RabbitMq\Producer;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AbstractConnection;

/**
 * Class JsonProducer
 *
 * Custom Producer class for RabbitMQ bundle, this class sends json messages to
 * consumers.
 *
 * @package Rasen\NineGagBundle\Lib
 */
class JsonProducer extends Producer
{
	/**
	 * {@inheritdoc}
	 */
	public function __construct(AbstractConnection $conn, AMQPChannel $ch = null, $consumerTag = null)
	{
		parent::__construct($conn, $ch, $consumerTag);
		$this->setContentType('application/json');
	}

	/**
	 * {@inheritdoc}
	 */
	public function publish($msgBody, $routingKey = '', $additionalProperties = array())
	{
		parent::publish(json_encode($msgBody), $routingKey, $additionalProperties);
	}
} 