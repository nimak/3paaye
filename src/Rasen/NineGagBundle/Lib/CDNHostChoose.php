<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 2/24/2015
 * Time: 12:15 AM
 */

namespace Rasen\NineGagBundle\Lib;

use JMS\DiExtraBundle\Annotation as DI;
/**
 * Class CDNHostChoose
 * @DI\Service("rasen_ninegag.cdn_host_choose")
 * @package Rasen\NineGagBundle\Lib
 */
class CDNHostChoose
{
    public function getCDNHost($fileName)
    {
        $hosts = array(
            1 => 'i1',
            2 => 'i2',
            3 => 'i3',
        );
        $fileNameNoExt = str_replace(array('_','.','-'), '',$fileName);
        $middleChar = substr($fileNameNoExt, intval(ceil(strlen($fileNameNoExt)/2)), 1);
        if (ctype_xdigit($middleChar)) {
            $hexToDec = hexdec($middleChar);
            $charCode = $hexToDec / 15;
        } else {
            $charCode = ord($middleChar) / 128;
        }
        $i = intval(ceil(($charCode) * 3));
        $i = min($i, 3);
        $i = max($i, 1);
        return $hosts[$i].'.statics';
    }
}