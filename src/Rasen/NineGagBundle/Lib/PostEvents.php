<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 3/21/2015
 * Time: 12:47 PM
 */

namespace Rasen\NineGagBundle\Lib;

/**
 * Class PostEvents
 * @package Rasen\NineGagBundle\Lib
 */
class PostEvents
{
    const POST_VIEW = 'post.view';
}