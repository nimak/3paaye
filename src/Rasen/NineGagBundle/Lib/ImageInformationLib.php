<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 3/13/2015
 * Time: 9:48 PM
 */

namespace Rasen\NineGagBundle\Lib;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class ImageInformationLib
 *
 * @DI\Service("rasen_ninegag.image_information")
 *
 * @package Rasen\NineGagBundle\Lib
 */
class ImageInformationLib
{

    public function getFileSize($filePath)
    {
        return round(filesize($filePath) / 1024, 2);
    }

    public function getImageDimensions($imagePath)
    {
        return getimagesize($imagePath);
    }

    public function isAnimated($imagePath)
    {
        $ext = pathinfo($imagePath, PATHINFO_EXTENSION);

        $isAnimated = false;
        if (strtolower($ext) == 'gif') {
            $isAnimated = $this->is_ani($imagePath);
        }
        return $isAnimated;
    }

    /**
     * Check whether an image is animated or not
     *
     * @param $filename
     * @return bool
     */
    private function is_ani($filename) {
        if(!($fh = @fopen($filename, 'rb')))
            return false;
        $count = 0;
        //an animated gif contains multiple "frames", with each frame having a
        //header made up of:
        // * a static 4-byte sequence (\x00\x21\xF9\x04)
        // * 4 variable bytes
        // * a static 2-byte sequence (\x00\x2C) (some variants may use \x00\x21 ?)

        // We read through the file til we reach the end of the file, or we've found
        // at least 2 frame headers
        while(!feof($fh) && $count < 2) {
            $chunk = fread($fh, 1024 * 100); //read 100kb at a time
            $count += preg_match_all('#\x00\x21\xF9\x04.{4}\x00(\x2C|\x21)#s', $chunk, $matches);
        }

        fclose($fh);
        return $count > 1;
    }
}