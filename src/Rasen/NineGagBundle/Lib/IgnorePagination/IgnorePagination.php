<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 2/8/2015
 * Time: 11:01 AM
 */

namespace Rasen\NineGagBundle\Lib\IgnorePagination;

/**
 * @TODO: Implement this (like medium.com pagination)
 *
 * Class IgnorePagination
 *
 * Paginates through items ignoring already paginated items
 *
 * @package Rasen\NineGagBundle\Lib\IgnorePagination
 */
class IgnorePagination {

}