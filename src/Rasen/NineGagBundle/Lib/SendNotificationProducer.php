<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 11/5/2014
 * Time: 8:07 PM
 */

namespace Rasen\NineGagBundle\Lib;

use OldSound\RabbitMqBundle\RabbitMq\Producer;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AbstractConnection;

/**
 * Class SendNotificationProducer
 *
 * Custom Producer class for RabbitMQ bundle, this class sends json notification messages to
 * consumers.
 *
 * @package Rasen\NineGagBundle\Lib
 */
class SendNotificationProducer extends Producer
{
	/**
	 * {@inheritdoc}
	 */
	public function __construct(AbstractConnection $conn, AMQPChannel $ch = null, $consumerTag = null)
	{
		parent::__construct($conn, $ch, $consumerTag);
		$this->setContentType('application/json');
	}

	/**
	 * {@inheritdoc}
	 */
	public function publish($msgBody, $routingKey = '', $additionalProperties = array())
	{
		parent::publish($msgBody, $routingKey, $additionalProperties);
	}
} 