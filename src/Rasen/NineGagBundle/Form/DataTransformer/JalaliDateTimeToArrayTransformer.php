<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 11/14/2014
 * Time: 7:44 PM
 */

namespace Rasen\NineGagBundle\Form\DataTransformer;

use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Form\Exception\UnexpectedTypeException;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Extension\Core\DataTransformer\BaseDateTimeTransformer;

class JalaliDateTimeToArrayTransformer extends BaseDateTimeTransformer
{
	private $pad;

	private $fields;

	private $formatter;

	/**
	 * Constructor.
	 *
	 * @param string $inputTimezone  The input timezone
	 * @param string $outputTimezone The output timezone
	 * @param array  $fields         The date fields
	 * @param bool   $pad            Whether to use padding
	 *
	 * @throws UnexpectedTypeException if a timezone is not a string
	 */
	public function __construct($inputTimezone = null, $outputTimezone = null, array $fields = null, $pad = false)
	{
		parent::__construct($inputTimezone, $outputTimezone);

		if (null === $fields) {
			$fields = array('year', 'month', 'day', 'hour', 'minute', 'second');
		}

		$this->fields = $fields;
		$this->pad = (bool) $pad;

		$this->formatter = new \IntlDateFormatter(
			'en_US@calendar=persian',
			\IntlDateFormatter::SHORT,
			\IntlDateFormatter::NONE,
			'UTC',
			\IntlDateFormatter::TRADITIONAL
		);
	}

	/**
	 * Transforms a normalized date into a localized date.
	 *
	 * @param \DateTime $dateTime Normalized date.
	 *
	 * @return array Localized date.
	 *
	 * @throws TransformationFailedException If the given value is not an
	 *                                       instance of \DateTime or if the
	 *                                       output timezone is not supported.
	 */
	public function transform($dateTime)
	{
		if (null === $dateTime) {
			return array_intersect_key(array(
				'year'    => '',
				'month'   => '',
				'day'     => '',
				'hour'    => '',
				'minute'  => '',
				'second'  => '',
			), array_flip($this->fields));
		}

		if (!$dateTime instanceof \DateTime) {
			throw new TransformationFailedException('Expected a \DateTime.');
		}

		$dateTime = clone $dateTime;
		if ($this->inputTimezone !== $this->outputTimezone) {
			try {
				$dateTime->setTimezone(new \DateTimeZone($this->outputTimezone));
			} catch (\Exception $e) {
				throw new TransformationFailedException($e->getMessage(), $e->getCode(), $e);
			}
		}

		$result = array_intersect_key(array(
			'year'    => $this->formatDateTime($dateTime, 'YYYY'),
			'month'   => $this->formatDateTime($dateTime, 'MM'),
			'day'     => $this->formatDateTime($dateTime, 'dd'),
			'hour'    => $this->formatDateTime($dateTime, 'HH'),
			'minute'  => $this->formatDateTime($dateTime, 'mm'),
			'second'  => $this->formatDateTime($dateTime, 'ss'),
		), array_flip($this->fields));

		if (!$this->pad) {
			foreach ($result as &$entry) {
				// remove leading zeros
				$entry = (string) (int) $entry;
			}
		}

		return $result;
	}

	private function formatDateTime($dateTime, $format)
	{
		$this->formatter->setPattern($format);
		return $this->formatter->format($dateTime);
	}

	/**
	 * Transforms a localized date into a normalized date.
	 *
	 * @param array $value Localized date
	 *
	 * @return \DateTime Normalized date
	 *
	 * @throws TransformationFailedException If the given value is not an array,
	 *                                       if the value could not be transformed
	 *                                       or if the input timezone is not
	 *                                       supported.
	 */
	public function reverseTransform($value)
	{
		if (null === $value) {
			return;
		}

		if (!is_array($value)) {
			throw new TransformationFailedException('Expected an array.');
		}

		if ('' === implode('', $value)) {
			return;
		}

		$emptyFields = array();

		foreach ($this->fields as $field) {
			if (!isset($value[$field])) {
				$emptyFields[] = $field;
			}
		}

		if (count($emptyFields) > 0) {
			throw new TransformationFailedException(
				sprintf('The fields "%s" should not be empty', implode('", "', $emptyFields)
				));
		}

		if (isset($value['month']) && !ctype_digit((string) $value['month'])) {
			throw new TransformationFailedException('This month is invalid');
		}

		if (isset($value['day']) && !ctype_digit((string) $value['day'])) {
			throw new TransformationFailedException('This day is invalid');
		}

		if (isset($value['year']) && !ctype_digit((string) $value['year'])) {
			throw new TransformationFailedException('This year is invalid');
		}

		if (!empty($value['month']) && !empty($value['day']) && !empty($value['year']) && false === checkdate($value['month'], $value['day'], $value['year'])) {
			throw new TransformationFailedException('This is an invalid date');
		}

		if (isset($value['hour']) && !ctype_digit((string) $value['hour'])) {
			throw new TransformationFailedException('This hour is invalid');
		}

		if (isset($value['minute']) && !ctype_digit((string) $value['minute'])) {
			throw new TransformationFailedException('This minute is invalid');
		}

		if (isset($value['second']) && !ctype_digit((string) $value['second'])) {
			throw new TransformationFailedException('This second is invalid');
		}

		try {
			$dateTime = $this->toDateTime($value['year'], $value['month'] - 1, $value['day']);
			if ($this->inputTimezone !== $this->outputTimezone) {
				$dateTime->setTimezone(new \DateTimeZone($this->inputTimezone));
			}
		} catch (\Exception $e) {
			throw new TransformationFailedException($e->getMessage(), $e->getCode(), $e);
		}
		return $dateTime;
	}


	public function toDateTime($year, $month, $day){
		/**
		 * @var $cal \IntlCalendar
		 */
		$cal = $this->formatter->getCalendarObject();
		$cal->set(\IntlCalendar::FIELD_YEAR, $year);
		$cal->set(\IntlCalendar::FIELD_MONTH, $month);
		$cal->set(\IntlCalendar::FIELD_DAY_OF_MONTH, $day);
		return $cal->toDateTime();
	}

} 