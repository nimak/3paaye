<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/17/2014
 * Time: 12:01 PM
 */

namespace Rasen\NineGagBundle\Form\EventListener;

use FOS\UserBundle\FOSUserEvents;
//use FOS\UserBundle\Event\FormEvent;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Rasen\NineGagBundle\Entity\User;
use JMS\DiExtraBundle\Annotation as DI;
/**
 * Class RegistrationSubscriber
 *
 * @DI\Service("event_listener.user_registration_subscriber")
 * @DI\Tag("kernel.event_subscriber")
 *
 * @package Rasen\NineGagBundle\EventListener
 */
class RegistrationSubscriber implements EventSubscriberInterface
{

	/**
	 * {@inheritDoc}
	 */
	public static function getSubscribedEvents()
	{
		return array(
			FormEvents::PRE_SUBMIT => 'onPreSubmit',
		);
	}

	public function onPreSubmit(FormEvent $event)
	{
		/**
		 * @var User
		 */
		$user=$event->getForm()->getData();

		$user->setIsTrustedUser(true);
		$user->setGender(User::GENDER_NOT_KNOWN);

		$event->getForm()->setData($user);
	}
} 