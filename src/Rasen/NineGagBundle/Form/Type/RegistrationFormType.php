<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/17/2014
 * Time: 12:37 PM
 */

namespace Rasen\NineGagBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use JMS\DiExtraBundle\Annotation as DI;
use Rasen\NineGagBundle\Form\EventListener\RegistrationSubscriber;
/**
 * Class RegistrationFormType
 *
 * @DI\FormType
 *
 * @package Rasen\NineGagBundle\Form\Type
 */
class RegistrationFormType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder->addEventSubscriber(new RegistrationSubscriber());
	}

	public function getParent()
	{
		return 'fos_user_registration';
	}

	public function getName()
	{
		return 'rasen_nine_gag_registration';
	}
} 