<?php
namespace Rasen\NineGagBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('firstName', 'text', array('required'=>false));
        $builder->add('lastName', 'text', array('required'=>false));
        $builder->add('phone', 'text', array('required'=>false));
        $builder->add('gender', 'choice', array(
                'choices'   => array('1' => 'form.gender.male', '2' => 'form.gender.female', '0' => 'form.gender.not_specified'),
                'required'  => true,
                'translation_domain' =>'RasenNineGagBundle'
            ));
        $builder->add('profilePic', 'file', array('required'=>false));
        $builder->add('profileHeaderPic', 'file', array('required'=>false));
        $builder->add('username','text');
        $builder->add('profileBio','textarea', array('required'=>false));
        $builder->add('birthday','jalali_birthday', array('required'=>false));



    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                'data_class' => 'Rasen\NineGagBundle\Entity\User',
            ));
    }

    public function getName()
    {
        return 'userProfile';
    }
}