<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 11/14/2014
 * Time: 5:46 PM
 */

/* @TODO: THIS THING HAS SOME SERIOUS PROBLEMS (FIX BEFORE USE) */

namespace Rasen\NineGagBundle\Form\Type;

use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToArrayTransformer;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToLocalizedStringTransformer;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToStringTransformer;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToTimestampTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\ReversedTransformer;
use Symfony\Component\OptionsResolver\Exception\InvalidOptionsException;
use Symfony\Component\Form\Extension\Core\Type\DateType;

use Rasen\NineGagBundle\Form\DataTransformer\JalaliDateTimeToArrayTransformer;

use JMS\DiExtraBundle\Annotation as DI;
/**
 * Class JalaliDateType
 *
 * @DI\Service("rasen_ninegag.jalali_date_type")
 * @DI\Tag("form.type", attributes={"alias": "jalali_date"})
 *
 * @package Rasen\NineGagBundle\Form\Extension
 */
class JalaliDateType extends DateType
{
	private static $jalaaliMonths = array(
		1 => 'فروردین',
		2 => 'اردیبهشت',
		3 => 'خرداد',
		4 => 'تیر',
		5 => 'مرداد',
		6 => 'شهریور',
		7 => 'مهر',
		8 => 'آبان',
		9 => 'آذر',
		10 => 'دی',
		11 => 'بهمن',
		12 => 'اسفند',
	);

	private static $acceptedFormats = array(
		\IntlDateFormatter::FULL,
		\IntlDateFormatter::LONG,
		\IntlDateFormatter::MEDIUM,
		\IntlDateFormatter::SHORT,
	);

	/**
	 * {@inheritdoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$dateFormat = is_int($options['format']) ? $options['format'] : self::DEFAULT_FORMAT;
		$timeFormat = \IntlDateFormatter::NONE;
		$calendar = \IntlDateFormatter::TRADITIONAL;
		$pattern = is_string($options['format']) ? $options['format'] : null;

		if (!in_array($dateFormat, self::$acceptedFormats, true)) {
			throw new InvalidOptionsException('The "format" option must be one of the IntlDateFormatter constants (FULL, LONG, MEDIUM, SHORT) or a string representing a custom format.');
		}

		if (null !== $pattern && (false === strpos($pattern, 'y') || false === strpos($pattern, 'M') || false === strpos($pattern, 'd'))) {
			throw new InvalidOptionsException(sprintf('The "format" option should contain the letters "y", "M" and "d". Its current value is "%s".', $pattern));
		}

		if ('single_text' === $options['widget']) {
			$builder->addViewTransformer(new DateTimeToLocalizedStringTransformer(
				$options['model_timezone'],
				$options['view_timezone'],
				$dateFormat,
				$timeFormat,
				$calendar,
				$pattern
			));
		} else {
			$yearOptions = $monthOptions = $dayOptions = array(
				'error_bubbling' => true,
			);

			$formatter = new \IntlDateFormatter(
				\Locale::getDefault(),
				$dateFormat,
				$timeFormat,
				'UTC',
				$calendar,
				$pattern
			);

			// new \intlDateFormatter may return null instead of false in case of failure, see https://bugs.php.net/bug.php?id=66323
			if (!$formatter) {
				throw new InvalidOptionsException(intl_get_error_message(), intl_get_error_code());
			}

			$formatter->setLenient(false);

			if ('choice' === $options['widget']) {
				// Only pass a subset of the options to children
				$yearOptions['choices'] = $this->formatTimestamps($formatter, '/y+/', $this->listYears($options['years']));
				$yearOptions['empty_value'] = $options['empty_value']['year'];
				$monthOptions['choices'] = $this->listMonths($options['months']);//$this->formatTimestamps($formatter, '/[M|L]+/', $this->listMonths($options['months']));
				$monthOptions['empty_value'] = $options['empty_value']['month'];
				$dayOptions['choices'] = $this->listDays($options['days']);//$this->formatTimestamps($formatter, '/d+/', $this->listDays($options['days']));
				$dayOptions['empty_value'] = $options['empty_value']['day'];
			}

			// Append generic carry-along options
			foreach (array('required', 'translation_domain') as $passOpt) {
				$yearOptions[$passOpt] = $monthOptions[$passOpt] = $dayOptions[$passOpt] = $options[$passOpt];
			}

			$builder
				->add('year', $options['widget'], $yearOptions)
				->add('month', $options['widget'], $monthOptions)
				->add('day', $options['widget'], $dayOptions)
				->addViewTransformer(new JalaliDateTimeToArrayTransformer(
					$options['model_timezone'], $options['view_timezone'], array('year', 'month', 'day')
				))
				->setAttribute('formatter', $formatter)
			;
		}

		if ('string' === $options['input']) {
			$builder->addModelTransformer(new ReversedTransformer(
				new DateTimeToStringTransformer($options['model_timezone'], $options['model_timezone'], 'Y-m-d')
			));
		} elseif ('timestamp' === $options['input']) {
			$builder->addModelTransformer(new ReversedTransformer(
				new DateTimeToTimestampTransformer($options['model_timezone'], $options['model_timezone'])
			));
		} elseif ('array' === $options['input']) {
			$builder->addModelTransformer(new ReversedTransformer(
				new JalaliDateTimeToArrayTransformer($options['model_timezone'], $options['model_timezone'], array('year', 'month', 'day'))
			));
		}
	}


	/**
	 * {@inheritdoc}
	 */
	public function getName()
	{
		return 'jalali_date';
	}

	private function formatTimestamps(\IntlDateFormatter $formatter, $regex, array $timestamps)
	{
		$pattern = $formatter->getPattern();
		$timezone = $formatter->getTimezoneId();

		if ($setTimeZone = method_exists($formatter, 'setTimeZone')) {
			$formatter->setTimeZone('UTC');
		} else {
			$formatter->setTimeZoneId('UTC');
		}

		if (preg_match($regex, $pattern, $matches)) {
			$formatter->setPattern($matches[0]);

			foreach ($timestamps as $key => $timestamp) {
				$timestamps[$key] = $formatter->format($timestamp);
			}

			// I'd like to clone the formatter above, but then we get a
			// segmentation fault, so let's restore the old state instead
			$formatter->setPattern($pattern);
		}

		if ($setTimeZone) {
			$formatter->setTimeZone($timezone);
		} else {
			$formatter->setTimeZoneId($timezone);
		}

		return $timestamps;
	}

	private function listYears(array $years)
	{
		$result = array();

		$formatter = new \IntlDateFormatter(
			'en_US@calendar=persian',
			\IntlDateFormatter::SHORT,
			\IntlDateFormatter::NONE,
			'UTC',
			\IntlDateFormatter::TRADITIONAL
		);
		$formatter->setPattern('YYYY');

		foreach (array_reverse($years) as $year) {
			if (false !== $y = gmmktime(0, 0, 0, 6, 15, $year)) {
				$fy = $formatter->format($y);
				$result[$fy] = $y;
			}
		}
		return $result;
	}

	private function listMonths(array $months)
	{
		$result = array();
		foreach ($months as $month) {
			//$m = $this->toDateTime(1393,$month,'15');
			$m = self::$jalaaliMonths[$month];
			//die(var_dump($m));
			$result[$month] = $m;
		}

		return $result;
	}

	private function listDays(array $days)
	{
		$result = array();

		foreach ($days as $day) {
			$result[$day] = $day;//gmmktime(0, 0, 0, 5, $day);
		}

		return $result;
	}

	public function toDateTime($year, $month, $day){
		$formatter = new \IntlDateFormatter(
			'fa_IR@calendar=persian',
			\IntlDateFormatter::SHORT,
			\IntlDateFormatter::NONE,
			'Asia/Tehran',
			\IntlDateFormatter::TRADITIONAL
		);
		/**
		 * @var $cal \IntlCalendar
		 */
		$cal = $formatter->getCalendarObject();
		$cal->set(\IntlCalendar::FIELD_YEAR, $year);
		$cal->set(\IntlCalendar::FIELD_MONTH, $month);
		$cal->set(\IntlCalendar::FIELD_DAY_OF_MONTH, $day);
		return $cal->toDateTime();
	}

	public function toTimestamp($year, $month, $day){
		return $this->toDateTime($year, $month, $day)->getTimestamp();
	}
} 