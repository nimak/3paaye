<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 11/14/2014
 * Time: 5:46 PM
 */

namespace Rasen\NineGagBundle\Form\Type;

use Symfony\Component\Form\Extension\Core\Type\BirthdayType;

use JMS\DiExtraBundle\Annotation as DI;
/**
 * Class JalaliBirthdayType
 *
 * @DI\Service("rasen_ninegag.jalali_birthday_type")
 * @DI\Tag("form.type", attributes={"alias": "jalali_birthday"})
 *
 * @package Rasen\NineGagBundle\Form\Extension
 */
class JalaliBirthdayType extends BirthdayType
{
	/**
	 * {@inheritdoc}
	 */
	public function getParent()
	{
		return 'jalali_date';
	}

	/**
	 * {@inheritdoc}
	 */
	public function getName()
	{
		return 'jalali_birthday';
	}
} 