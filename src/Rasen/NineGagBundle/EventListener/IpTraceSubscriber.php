<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 11/25/2014
 * Time: 10:23 PM
 */

namespace Rasen\NineGagBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;

use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Rasen\NineGagBundle\Entity\Comment;
use Rasen\NineGagBundle\Entity\CommentVote;
use Rasen\NineGagBundle\Entity\CommentReport;
use Rasen\NineGagBundle\Entity\Post;
use Rasen\NineGagBundle\Entity\PostVote;
use Rasen\NineGagBundle\Entity\PostReport;
use Rasen\NineGagBundle\Entity\User;
use Rasen\NineGagBundle\Entity\UserFollower;
use Rasen\NineGagBundle\Entity\UserPoint;
use Rasen\NineGagBundle\Entity\UserReport;
use Symfony\Component\HttpFoundation\RequestStack;

use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class IpTraceSubscriber
 *
 * Update ip fields in entities
 *
 * @DI\Service("event_listener.ip_trace_subscriber")
 * @DI\Tag("doctrine.event_subscriber", attributes = {"connection" = "default"})
 *
 * @package Rasen\NineGagBundle\EventListener
 */
class IpTraceSubscriber implements EventSubscriber
{

	private $requestStack;

	/**
	 * @DI\InjectParams({
	 *     "requestStack" = @DI\Inject("request_stack")
	 * })
	 *
	 * @param RequestStack $requestStack
	 */
	public function __construct(RequestStack $requestStack)
	{
		$this->requestStack = $requestStack;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getSubscribedEvents()
	{
		return array(
			'prePersist',
		);
	}

	/**
	 * {@inheritdoc}
	 */
	public function prePersist(LifecycleEventArgs $args)
	{
		$entity = $args->getObject();
		if ($entity instanceof Comment) {
			$request = $this->requestStack->getCurrentRequest();
			$entity->setAuthorIP($request->getClientIp());
		}
	}
} 