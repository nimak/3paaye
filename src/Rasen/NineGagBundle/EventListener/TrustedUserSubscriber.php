<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/29/2014
 * Time: 1:20 PM
 */

namespace Rasen\NineGagBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Rasen\NineGagBundle\Entity\Post;
use Rasen\NineGagBundle\Entity\User;
use JMS\DiExtraBundle\Annotation as DI;
/**
 * Class TrustedUserSubscriber
 *
 * @DI\Service("event_listener.trusted_user_subscriber")
 * @DI\Tag("doctrine.event_subscriber", attributes = {"connection" = "default"})
 *
 * @package Rasen\NineGagBundle\EventListener
 */
class TrustedUserSubscriber implements EventSubscriber
{

	/**
	 * {@inheritdoc}
	 */
	public function getSubscribedEvents()
	{
		return array(
			'prePersist',
			'preUpdate'
		);
	}

	/**
	 * {@inheritdoc}
	 */
	public function preUpdate(PreUpdateEventArgs $args)
	{
		$entity = $args->getObject();
		if ($entity instanceof Post) {
			if ($args->hasChangedField('isApproved')){
				$this->updateIsTrustedUser($entity);
			}
		}
	}

	/**
	 * {@inheritdoc}
	 */
	public function prePersist(LifecycleEventArgs $args)
	{
		$entity = $args->getObject();
		$this->updateIsTrustedUser($entity);
	}

	/**
	 * Change a user to trusted
	 *
	 * This will only update isTrustedUser one time, if user has created its "TRUST_POST_COUNT" # post, then will make
	 * him/her a trusted user.
	 * But once the user has been marked as a NOT trusted user (isTrustedUser == false), then even if it has a 100 approved posts,
	 * will NOT mark the user as trusted. (since it has been marked as not trusted by admin)
	 *
	 * @param object $entity
	 */
	public function updateIsTrustedUser($entity)
	{
		if ($entity instanceof Post)
		{
			if ($entity->getIsApproved() && $entity->getCreatedBy()->getApprovedPostsNo() == User::TRUST_POST_COUNT)
			{
				$entity->getCreatedBy()->setIsTrustedUser(true);
			}
		}
	}
} 