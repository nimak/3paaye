<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/14/2014
 * Time: 11:14 AM
 */

namespace Rasen\NineGagBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;

use Gedmo\SoftDeleteable\SoftDeleteableListener;
use Rasen\NineGagBundle\Entity\Comment;
use Rasen\NineGagBundle\Entity\CommentVote;
use Rasen\NineGagBundle\Entity\CommentReport;
use Rasen\NineGagBundle\Entity\Post;
use Rasen\NineGagBundle\Entity\PostVote;
use Rasen\NineGagBundle\Entity\PostReport;
use Rasen\NineGagBundle\Entity\User;
use Rasen\NineGagBundle\Entity\UserFollower;
use Rasen\NineGagBundle\Entity\UserPoint;
use Rasen\NineGagBundle\Entity\UserReport;

use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class SoftDeleteListener
 *
 * Handles giving badges to users.
 *
 * @DI\Service("event_listener.soft_delete_listener")
 * @DI\Tag("doctrine.event_listener", attributes = {"event" = "preSoftDelete", "connection" = "default"})
 *
 * @package Rasen\NineGagBundle\EventListener
 */
class SoftDeleteListener
{
	/**
	 * {@inheritdoc}
	 */
	public function preSoftDelete(LifecycleEventArgs $args)
	{
        $em = $args->getObjectManager();
        $obj = $args->getObject();
        if ($obj instanceof User)
        {
            $postsQ = $em->createQuery('UPDATE RasenNineGagBundle:Post p SET p.deletedAt = CURRENT_TIMESTAMP() WHERE p.createdBy = :userP');
            $postsQ = $postsQ->setParameter('userP', $obj);
            $postsQ->execute();

            $votesQ = $em->createQuery('UPDATE RasenNineGagBundle:PostVote pv SET pv.deletedAt = CURRENT_TIMESTAMP() WHERE pv.votedBy = :userP');
            $votesQ = $votesQ->setParameter('userP', $obj);
            $votesQ->execute();

            $postReportsQ = $em->createQuery('UPDATE RasenNineGagBundle:PostReport pr SET pr.deletedAt = CURRENT_TIMESTAMP() WHERE pr.reporter = :userP');
            $postReportsQ = $postReportsQ->setParameter('userP', $obj);
            $postReportsQ->execute();

            $commentsQ = $em->createQuery('UPDATE RasenNineGagBundle:Comment c SET c.deletedAt = CURRENT_TIMESTAMP() WHERE c.createdBy = :userP');
            $commentsQ = $commentsQ->setParameter('userP', $obj);
            $commentsQ->execute();

            $commentMentionsQ = $em->createQuery('UPDATE RasenNineGagBundle:CommentMention cm SET cm.deletedAt = CURRENT_TIMESTAMP() WHERE cm.user = :userP');
            $commentMentionsQ = $commentMentionsQ->setParameter('userP', $obj);
            $commentMentionsQ->execute();

            $commentReportsQ = $em->createQuery('UPDATE RasenNineGagBundle:CommentReport cr SET cr.deletedAt = CURRENT_TIMESTAMP() WHERE cr.reporter = :userP');
            $commentReportsQ = $commentReportsQ->setParameter('userP', $obj);
            $commentReportsQ->execute();

            $commentVotesQ = $em->createQuery('UPDATE RasenNineGagBundle:CommentVote cv SET cv.deletedAt = CURRENT_TIMESTAMP() WHERE cv.votedBy = :userP');
            $commentVotesQ = $commentVotesQ->setParameter('userP', $obj);
            $commentVotesQ->execute();

            $userBadgesQ = $em->createQuery('UPDATE RasenNineGagBundle:UserBadge ub SET ub.deletedAt = CURRENT_TIMESTAMP() WHERE ub.user = :userP');
            $userBadgesQ = $userBadgesQ->setParameter('userP', $obj);
            $userBadgesQ->execute();

            $userFollowersQ = $em->createQuery('UPDATE RasenNineGagBundle:UserFollower uf SET uf.deletedAt = CURRENT_TIMESTAMP() WHERE uf.user = :userP or uf.follower = :userP');
            $userFollowersQ = $userFollowersQ->setParameter('userP', $obj);
            $userFollowersQ->execute();

            $userPointsQ = $em->createQuery('UPDATE RasenNineGagBundle:UserPoint up SET up.deletedAt = CURRENT_TIMESTAMP() WHERE up.user = :userP');
            $userPointsQ = $userPointsQ->setParameter('userP', $obj);
            $userPointsQ->execute();

            $userReportsQ = $em->createQuery('UPDATE RasenNineGagBundle:UserReport ur SET ur.deletedAt = CURRENT_TIMESTAMP() WHERE ur.user = :userP or ur.reporter = :userP');
            $userReportsQ = $userReportsQ->setParameter('userP', $obj);
            $userReportsQ->execute();
        }
        elseif ($obj instanceof Post)
        {
            $votesQ = $em->createQuery('UPDATE RasenNineGagBundle:PostVote pv SET pv.deletedAt = CURRENT_TIMESTAMP() WHERE pv.post = :post');
            $votesQ = $votesQ->setParameter('post', $obj);
            $votesQ->execute();

            $postReportsQ = $em->createQuery('UPDATE RasenNineGagBundle:PostReport pr SET pr.deletedAt = CURRENT_TIMESTAMP() WHERE pr.post = :post');
            $postReportsQ = $postReportsQ->setParameter('post', $obj);
            $postReportsQ->execute();

            $commentsQ = $em->createQuery('UPDATE RasenNineGagBundle:Comment c SET c.deletedAt = CURRENT_TIMESTAMP() WHERE c.post = :post');
            $commentsQ = $commentsQ->setParameter('post', $obj);
            $commentsQ->execute();
        }
        elseif ($obj instanceof Comment)
        {
            $commentMentionsQ = $em->createQuery('UPDATE RasenNineGagBundle:CommentMention cm SET cm.deletedAt = CURRENT_TIMESTAMP() WHERE cm.comment = :comment');
            $commentMentionsQ = $commentMentionsQ->setParameter('comment', $obj);
            $commentMentionsQ->execute();

            $commentReportsQ = $em->createQuery('UPDATE RasenNineGagBundle:CommentReport cr SET cr.deletedAt = CURRENT_TIMESTAMP() WHERE cr.comment = :comment');
            $commentReportsQ = $commentReportsQ->setParameter('comment', $obj);
            $commentReportsQ->execute();

            $commentVotesQ = $em->createQuery('UPDATE RasenNineGagBundle:CommentVote cv SET cv.deletedAt = CURRENT_TIMESTAMP() WHERE cv.comment = :comment');
            $commentVotesQ = $commentVotesQ->setParameter('comment', $obj);
            $commentVotesQ->execute();
        }
	}
} 