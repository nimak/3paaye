<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 11/6/2014
 * Time: 9:16 PM
 */

namespace Rasen\NineGagBundle\EventListener;

use Rasen\NineGagBundle\Controller\Rest\PostRestStatefulController;
use Rasen\NineGagBundle\Event\PostViewEvent;
use Rasen\NineGagBundle\Lib\PostEvents;
use Rasen\NineGagBundle\Lib\PostStatsUtility;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\SecurityContext;
use HWI\Bundle\OAuthBundle\Security\Core\Authentication\Token\OAuthToken;

/**
 * Class PostViewCounterSubscriber
 *
 * @DI\Service("event_listener.post_view_subscriber", scope="request")
 * @DI\Tag("kernel.event_subscriber")
 *
 * @package Rasen\NineGagBundle\EventListener
 */
class PostViewCounterSubscriber implements EventSubscriberInterface
{
    /**
     * @var PostStatsUtility $postStatsUtility
     */
	private $postStatsUtility;

    /**
     * @var TokenStorage $tokenStorage
     */
	private $tokenStorage;

	/**
	 * @DI\InjectParams({
	 *     "postStatsUtility" = @DI\Inject("rasen_ninegag.post_stats_utility"),
	 *     "tokenStorage" = @DI\Inject("security.token_storage"),
	 * })
	 *
	 * @param PostStatsUtility $postStatsUtility
	 * @param TokenStorage $tokenStorage
	 */
	public function __construct(PostStatsUtility $postStatsUtility,
                                TokenStorage $tokenStorage)
	{
		$this->postStatsUtility = $postStatsUtility;
		$this->tokenStorage = $tokenStorage;
	}

	public function onPostView(PostViewEvent $event)
	{
        $posts = $event->getPosts();

        if (empty($posts)) {
            return;
        }

        $request = $event->getRequest();

        $user = $this->tokenStorage->getToken();
        $cookie = $this->postStatsUtility->increaseView($posts, $user);

        if ($cookie instanceof Cookie) {
            $request->attributes->set('post_view_cookie', $cookie);
        }
	}

	public function onKernelResponse(FilterResponseEvent $event)
	{
        if (!$cookie = $event->getRequest()->attributes->get('post_view_cookie')) {
            return;
        }
        $response = $event->getResponse();
        $response->headers->setCookie($cookie);

	}

	public static function getSubscribedEvents()
	{
		return array(
            PostEvents::POST_VIEW => 'onPostView',
			KernelEvents::RESPONSE => 'onKernelResponse',
		);
	}

} 