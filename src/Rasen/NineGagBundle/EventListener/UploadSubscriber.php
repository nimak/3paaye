<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/28/2014
 * Time: 7:50 PM
 */

namespace Rasen\NineGagBundle\EventListener;

use JMS\DiExtraBundle\Annotation as DI;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Rasen\NineGagBundle\Entity\Advertisement;
use Rasen\NineGagBundle\Entity\PostImage;
use Rasen\NineGagBundle\Entity\Watermark;
use Rasen\NineGagBundle\Lib\ImageInformationLib;
use Rasen\NineGagBundle\Lib\PostImageProcessor;
use Sonata\Cache\CacheManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Vich\UploaderBundle\Event\Events as VichUploaderEvents;
use Vich\UploaderBundle\Event\Event;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;
/**
 * Class UploadSubscriber
 *
 * @DI\Service("event_listener.upload_subscriber")
 * @DI\Tag("kernel.event_subscriber")
 *
 * @package Rasen\NineGagBundle\EventListener
 */
class UploadSubscriber implements EventSubscriberInterface
{
	private $kernelRootDir;

	private $rootUploadDir;
	private $watermarkUploadDir;
	private $advertisementUploadDir;

	/**
	 * @var PostImageProcessor
	 */
	private $postImageProcessor;

	/**
	 * @var CacheManager $cacheManager
	 */
	private $cacheManager;

	/**
	 * @var UploaderHelper $vichUploaderHelper
	 */
	private $vichUploaderHelper;

	/**
	 * @var ImageInformationLib $imageInformation
	 */
	private $imageInformation;


	/**
	 * * @DI\InjectParams({
	 *     "kernelRootDir" = @DI\Inject("%kernel.root_dir%"),
	 *     "postImageProcessor" = @DI\Inject("rasen_ninegag.post_image_processor"),
	 *     "cacheManager" = @DI\Inject("liip_imagine.cache.manager"),
	 *     "vichUploaderHelper" = @DI\Inject("vich_uploader.templating.helper.uploader_helper"),
	 *     "imageInformation" = @DI\Inject("rasen_ninegag.image_information")
	 * })
	 * @param $kernelRootDir
	 * @param PostImageProcessor $postImageProcessor
	 * @param CacheManager $cacheManager
	 * @param UploaderHelper $vichUploaderHelper
	 * @param ImageInformationLib $imageInformation
	 */
	public function __construct(
		$kernelRootDir,
		PostImageProcessor $postImageProcessor,
		CacheManager $cacheManager,
		UploaderHelper $vichUploaderHelper,
        ImageInformationLib $imageInformation)
	{
		$this->kernelRootDir = $kernelRootDir;
		$this->postImageProcessor = $postImageProcessor;
		$this->cacheManager = $cacheManager;
		$this->vichUploaderHelper = $vichUploaderHelper;
		$this->imageInformation = $imageInformation;

		$this->rootUploadDir = $kernelRootDir.PostImageProcessor::POST_UPLOAD_DIR;
		$this->watermarkUploadDir = $kernelRootDir.PostImageProcessor::WATERMARK_UPLOAD_DIR;
		$this->advertisementUploadDir = $kernelRootDir.PostImageProcessor::ADS_UPLOAD_DIR;
	}

	/**
	 * {@inheritdoc}
	 */
	public static function getSubscribedEvents()
	{
		return array(
			VichUploaderEvents::POST_UPLOAD => array('onPostUpload', 0),
			VichUploaderEvents::PRE_REMOVE => array('onPreRemove', 0),
		);
	}

	/**
	 * Do some processing after image upload (remove exif, etc.)
     * @param Event $event
	 */
	public function onPostUpload(Event $event)
	{

		$object= $event->getObject();
		if ($object instanceof PostImage) {

			/**
			 * @var $file \Symfony\Component\HttpFoundation\File\UploadedFile
			 */
			$file = $event->getMapping()->getFile($event->getObject());

            //$rootDir = $this->container->get('kernel')->getRootDir().'/../web/uploads/posts/';
            //$fsIdentifier = 'post_fs';
            //$fileName = preg_replace('#^.*://'.$fsIdentifier."\\".DIRECTORY_SEPARATOR.'#', '', $file->getPath().'/'.$file->getBasename());

            $filePath = $object->getImageUrl();
            $fileAbsoulutePath = $this->rootUploadDir.$filePath;

            //Check if the post is animation
			$object->setIsAnimated($this->imageInformation->isAnimated($fileAbsoulutePath));

            list($width, $height, $type, $attr) = $this->imageInformation->getImageDimensions($fileAbsoulutePath);
            $object->setWidth($width)->setHeight($height);

            $imageSize = $this->imageInformation->getFileSize($fileAbsoulutePath);
            $object->setSize($imageSize);

			$this->postImageProcessor->process($object);
		} elseif ($object instanceof Watermark)
        {
            $filePath = $object->getImageName();
            $fileAbsoulutePath = $this->watermarkUploadDir.$filePath;

            list($width, $height, $type, $attr) = $this->imageInformation->getImageDimensions($fileAbsoulutePath);
            $object->setWidth($width)->setHeight($height);
		}
		//$this->container->get('doctrine.orm.entity_manager')->flush()

		//$file->
		//@TODO: Finish this code so that the processed file replaces the original.

		//$mapping = $event->getMapping();
		//$fileDir = $mapping->getUriPrefix();
		//$fileName = $fileDir . '/' . $mapping->getFileName($event->getObject());
		/*$imagemanagerResponse = $this->container
			->get('liip_imagine.controller')
			->filterAction($this->container->get('request'), $fileName, 'posts_image');*/
	}

	/**
	 * Remove the processed (cached) files after it's been removed.
	 * @param Event $event
	 */
	public function onPreRemove(Event $event)
	{

		$object= $event->getObject();
		if ($object instanceof PostImage) {
            if ($object->isDeleted()) {
                $this->postImageProcessor->purge($object);

                $path = $this->vichUploaderHelper->asset($object, 'posts');
                $this->cacheManager->remove($path);
            }
		}
	}
} 