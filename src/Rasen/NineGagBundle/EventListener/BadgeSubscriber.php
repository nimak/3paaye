<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/14/2014
 * Time: 11:14 AM
 */

namespace Rasen\NineGagBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;

use Rasen\NineGagBundle\Entity\Comment;
use Rasen\NineGagBundle\Entity\CommentVote;
use Rasen\NineGagBundle\Entity\CommentReport;
use Rasen\NineGagBundle\Entity\Post;
use Rasen\NineGagBundle\Entity\PostVote;
use Rasen\NineGagBundle\Entity\PostReport;
use Rasen\NineGagBundle\Entity\User;
use Rasen\NineGagBundle\Entity\UserFollower;
use Rasen\NineGagBundle\Entity\UserPoint;
use Rasen\NineGagBundle\Entity\UserReport;

use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class BadgeSubscriber
 *
 * Handles giving badges to users.
 *
 * @DI\Service("event_listener.badge_subscriber")
 * @DI\Tag("doctrine.event_subscriber", attributes = {"connection" = "default"})
 *
 * @package Rasen\NineGagBundle\EventListener
 */
class BadgeSubscriber implements EventSubscriber
{
	/**
	 * {@inheritdoc}
	 */
	public function getSubscribedEvents()
	{
		return array(
			'postPersist',
			'postUpdate',
			'postRemove',
		);
	}

	/**
	 * {@inheritdoc}
	 */
	public function postUpdate(LifecycleEventArgs $args)
	{

	}

	/**
	 * {@inheritdoc}
	 */
	public function postPersist(LifecycleEventArgs $args)
	{

	}

	/**
	 * {@inheritdoc}
	 */
	public function postRemove(LifecycleEventArgs $args)
	{

	}



} 