<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/14/2014
 * Time: 11:14 AM
 */

namespace Rasen\NineGagBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;

use Doctrine\ORM\Event\OnFlushEventArgs;
use Gedmo\SoftDeleteable\SoftDeleteableListener;
use Rasen\NineGagBundle\Entity\Comment;
use Rasen\NineGagBundle\Entity\CommentVote;
use Rasen\NineGagBundle\Entity\CommentReport;
use Rasen\NineGagBundle\Entity\Post;
use Rasen\NineGagBundle\Entity\PostVote;
use Rasen\NineGagBundle\Entity\PostReport;
use Rasen\NineGagBundle\Entity\User;
use Rasen\NineGagBundle\Entity\UserFollower;
use Rasen\NineGagBundle\Entity\UserPoint;
use Rasen\NineGagBundle\Entity\UserReport;

use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class SoftDeleteRecycleListener
 *
 * Handles giving badges to users.
 *
 * @DI\Service("event_listener.soft_delete_recycle_listener")
 * @DI\Tag("doctrine.event_listener", attributes = {"event" = "onFlush", "connection" = "default"})
 *
 * @package Rasen\NineGagBundle\EventListener
 */
class SoftDeleteRecycleListener
{
    /**
     * @param OnFlushEventArgs $args
     */
    public function onFlush(OnFlushEventArgs $args)
	{
        $em = $args->getEntityManager();
        $uow = $em->getUnitOfWork();
        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            $changeSet = $uow->getEntityChangeSet($entity);

            if ( array_key_exists('deletedAt', $changeSet) ) {
                $valueBefore = $changeSet['deletedAt'][0];
                $valueAfter = $changeSet['deletedAt'][1];
                if ( $valueBefore && ! $valueAfter ) {
                    if ($entity instanceof User)
                    {
                        $postsQ = $em->createQuery('UPDATE RasenNineGagBundle:Post p SET p.deletedAt = NULL WHERE p.createdBy = :userP');
                        $postsQ = $postsQ->setParameter('userP', $entity);
                        $postsQ->execute();

                        $votesQ = $em->createQuery('UPDATE RasenNineGagBundle:PostVote pv SET pv.deletedAt = NULL WHERE pv.votedBy = :userP');
                        $votesQ = $votesQ->setParameter('userP', $entity);
                        $votesQ->execute();

                        $postReportsQ = $em->createQuery('UPDATE RasenNineGagBundle:PostReport pr SET pr.deletedAt = NULL WHERE pr.reporter = :userP');
                        $postReportsQ = $postReportsQ->setParameter('userP', $entity);
                        $postReportsQ->execute();

                        $commentsQ = $em->createQuery('UPDATE RasenNineGagBundle:Comment c SET c.deletedAt = NULL WHERE c.createdBy = :userP');
                        $commentsQ = $commentsQ->setParameter('userP', $entity);
                        $commentsQ->execute();

                        $commentMentionsQ = $em->createQuery('UPDATE RasenNineGagBundle:CommentMention cm SET cm.deletedAt = NULL WHERE cm.user = :userP');
                        $commentMentionsQ = $commentMentionsQ->setParameter('userP', $entity);
                        $commentMentionsQ->execute();

                        $commentReportsQ = $em->createQuery('UPDATE RasenNineGagBundle:CommentReport cr SET cr.deletedAt = NULL WHERE cr.reporter = :userP');
                        $commentReportsQ = $commentReportsQ->setParameter('userP', $entity);
                        $commentReportsQ->execute();

                        $commentVotesQ = $em->createQuery('UPDATE RasenNineGagBundle:CommentVote cv SET cv.deletedAt = NULL WHERE cv.votedBy = :userP');
                        $commentVotesQ = $commentVotesQ->setParameter('userP', $entity);
                        $commentVotesQ->execute();

                        $userBadgesQ = $em->createQuery('UPDATE RasenNineGagBundle:UserBadge ub SET ub.deletedAt = NULL WHERE ub.user = :userP');
                        $userBadgesQ = $userBadgesQ->setParameter('userP', $entity);
                        $userBadgesQ->execute();

                        $userFollowersQ = $em->createQuery('UPDATE RasenNineGagBundle:UserFollower uf SET uf.deletedAt = NULL WHERE uf.user = :userP or uf.follower = :userP');
                        $userFollowersQ = $userFollowersQ->setParameter('userP', $entity);
                        $userFollowersQ->execute();

                        $userPointsQ = $em->createQuery('UPDATE RasenNineGagBundle:UserPoint up SET up.deletedAt = NULL WHERE up.user = :userP');
                        $userPointsQ = $userPointsQ->setParameter('userP', $entity);
                        $userPointsQ->execute();

                        $userReportsQ = $em->createQuery('UPDATE RasenNineGagBundle:UserReport ur SET ur.deletedAt = NULL WHERE ur.user = :userP or ur.reporter = :userP');
                        $userReportsQ = $userReportsQ->setParameter('userP', $entity);
                        $userReportsQ->execute();
                    }
                    elseif ($entity instanceof Post)
                    {
                        $votesQ = $em->createQuery('UPDATE RasenNineGagBundle:PostVote pv SET pv.deletedAt = NULL WHERE pv.post = :post');
                        $votesQ = $votesQ->setParameter('post', $entity);
                        $votesQ->execute();

                        $postReportsQ = $em->createQuery('UPDATE RasenNineGagBundle:PostReport pr SET pr.deletedAt = NULL WHERE pr.post = :post');
                        $postReportsQ = $postReportsQ->setParameter('post', $entity);
                        $postReportsQ->execute();

                        $commentsQ = $em->createQuery('UPDATE RasenNineGagBundle:Comment c SET c.deletedAt = NULL WHERE c.post = :post');
                        $commentsQ = $commentsQ->setParameter('post', $entity);
                        $commentsQ->execute();
                    }
                    elseif ($entity instanceof Comment)
                    {
                        $commentMentionsQ = $em->createQuery('UPDATE RasenNineGagBundle:CommentMention cm SET cm.deletedAt = NULL WHERE cm.comment = :comment');
                        $commentMentionsQ = $commentMentionsQ->setParameter('comment', $entity);
                        $commentMentionsQ->execute();

                        $commentReportsQ = $em->createQuery('UPDATE RasenNineGagBundle:CommentReport cr SET cr.deletedAt = NULL WHERE cr.comment = :comment');
                        $commentReportsQ = $commentReportsQ->setParameter('comment', $entity);
                        $commentReportsQ->execute();

                        $commentVotesQ = $em->createQuery('UPDATE RasenNineGagBundle:CommentVote cv SET cv.deletedAt = NULL WHERE cv.comment = :comment');
                        $commentVotesQ = $commentVotesQ->setParameter('comment', $entity);
                        $commentVotesQ->execute();
                    }
                }
                $uow->computeChangeSets();
            }
        }
	}
} 