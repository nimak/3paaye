<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 11/6/2014
 * Time: 9:16 PM
 */

namespace Rasen\NineGagBundle\EventListener;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\SecurityContext;
use HWI\Bundle\OAuthBundle\Security\Core\Authentication\Token\OAuthToken;

/**
 * Class RedisSessionSubscriber
 *
 * @DI\Service("event_listener.redis_session_subscriber")
 * @DI\Tag("kernel.event_subscriber")
 *
 * @package Rasen\NineGagBundle\EventListener
 */
class RedisSessionSubscriber implements EventSubscriberInterface
{
	private $redisStorage;

	private $maxLifetime;

	/**
	 * @DI\InjectParams({
	 *     "redisStorage" = @DI\Inject("snc_redis.default"),
	 *     "context" = @DI\Inject("security.context")
	 * })
	 *
	 * @param $redisStorage
	 * @param SecurityContext $context
	 */
	public function __construct($redisStorage, SecurityContext $context)
	{
		$this->redisStorage = $redisStorage;
		$this->context = $context;
		$this->maxLifetime = ini_get('session.gc_maxlifetime');
	}

	public function onKernelResponse(FilterResponseEvent $event)
	{
        $request = $event->getRequest();
        if ($event->isMasterRequest() &&
            $this->context->getToken()
        ) {
            if ($this->context->getToken() instanceof OAuthToken) {
                $this->saveSessionId($request);
                return;
            }
            if ( !($this->context->getToken() instanceof AnonymousToken) &&
                $this->context->getToken()->getProviderKey() == 'main' &&
                $this->context->isGranted('ROLE_USER')) {

                $this->saveSessionId($request);
            }
        }

	}

    private function saveSessionId(Request $request)
    {
        $session = $request->getSession();
        $sessionId = $session->getId();
        $prefix = 'JSON_PHPSESSION:';
        //json_encode($session->all()));
        $vars = array(
            'username' => $this->context->getToken()->getUsername()
        );
        $this->redisStorage->set( $prefix.$sessionId, json_encode($vars));
        $this->redisStorage->expire($prefix.$sessionId, $this->maxLifetime);
    }
	public static function getSubscribedEvents()
	{
		return array(
			KernelEvents::RESPONSE => 'onKernelResponse',
		);
	}

} 