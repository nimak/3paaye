<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/30/2014
 * Time: 1:35 PM
 */

namespace Rasen\NineGagBundle\Controller;

use Rasen\NineGagBundle\Entity\Post;
use Rasen\NineGagBundle\Entity\PostVote;
use Rasen\NineGagBundle\Entity\PostReport;
use Rasen\NineGagBundle\Entity\Comment;
use Rasen\NineGagBundle\Entity\CommentVote;
use Rasen\NineGagBundle\Entity\CommentReport;
use Rasen\NineGagBundle\Entity\User;
use Rasen\NineGagBundle\Entity\UserFollower;
use Rasen\NineGagBundle\Entity\UserPoint;
use Rasen\NineGagBundle\Entity\UserReport;
use Sonata\AdminBundle\Admin\AdminHelper;
use Sonata\AdminBundle\Admin\Pool;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sonata\AdminBundle\Exception\ModelManagerException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyPath;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\DiExtraBundle\Annotation as DI;
use \Symfony\Component\Validator\ValidatorInterface;

/**
 * Class CountableAdminHelperController
 *
 * @package Rasen\NineGagBundle\Controller
 */
class CountableAdminHelperController extends Controller
{
	/**
	 * @var \Twig_Environment
	 */
	protected $twig;

	/**
	 * @var \Sonata\AdminBundle\Admin\AdminHelper
	 */
	protected $helper;

	/**
	 * @var \Sonata\AdminBundle\Admin\Pool
	 */
	protected $pool;

	/**
	 * @var \Symfony\Component\Validator\ValidatorInterface
	 */
	protected $validator;

	/**
	 * @param  \Symfony\Component\HttpFoundation\Request $request
	 *
	 * @Route("/admin/set-object-field-value")
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function setObjectFieldValueAction(Request $request)
{

		/**
		 * @var \Twig_Environment $twig
		 */
		$this->twig      = $this->get('twig');
		/**
		 * @var \Sonata\AdminBundle\Admin\Pool $pool
		 */
		$this->pool      = $this->get('sonata.admin.pool');
		/**
		 * @var \Sonata\AdminBundle\Admin\AdminHelper $helper
		 */
		$this->helper    = $this->get('sonata.admin.helper');
		/**
		 * @var \Symfony\Component\Validator\ValidatorInterface $validator
		 */
		$this->validator = $this->get('validator');;

		//die('1');
		$field    = $request->get('field');
		$code     = $request->get('code');
		$objectId = $request->get('objectId');
		$value    = $request->get('value');
		$context  = $request->get('context');

		$admin = $this->pool->getInstance($code);
		$admin->setRequest($request);

		// alter should be done by using a post method
		if (!$request->isXmlHttpRequest()) {
			return new JsonResponse(array('status' => 'KO', 'message' => 'Expected a XmlHttpRequest request header'));
		}

		if ($request->getMethod() != 'POST') {
			return new JsonResponse(array('status' => 'KO', 'message' => 'Expected a POST Request'));
		}

		$rootObject = $object = $admin->getObject($objectId);

		if (!$object) {
			return new JsonResponse(array('status' => 'KO', 'message' => 'Object does not exist'));
		}

		//get the value before submit
		$objBefore = null;
		$isApprovedBefore = null;
		$voteBefore = null;
		$pointBefore = null;
		$followerBefore = null;
		if ($object instanceof Post)
		{
			$objBefore = $object->getCreatedBy();
			$isApprovedBefore = $object->getIsApproved();
		} elseif ($object instanceof PostVote)
		{
			$objBefore = $object->getPost();
			$voteBefore = $object->getVote();
		} elseif ($object instanceof PostReport)
		{
			$objBefore = $object->getPost();
		} elseif ($object instanceof Comment)
		{
			$objBefore = $object->getCreatedBy();
			$isApprovedBefore = $object->getApproved();
		} elseif ($object instanceof CommentVote)
		{
			$objBefore = $object->getComment();
			$voteBefore = $object->getVote();
		} elseif ($object instanceof CommentReport)
		{
			$objBefore = $object->getComment();
		} elseif ($object instanceof UserReport)
		{
			$objBefore = $object->getUser();
		} elseif ($object instanceof UserPoint)
		{
			$objBefore = $object->getUser();
			$pointBefore = $object->getPoint();
		} elseif ($object instanceof UserFollower)
		{
			$objBefore = $object->getUser();
			$followerBefore = $object->getFollower();
		}


		// check user permission
		if (false === $admin->isGranted('EDIT', $object)) {
			return new JsonResponse(array('status' => 'KO', 'message' => 'Invalid permissions'));
		}

		if ($context == 'list') {
			$fieldDescription = $admin->getListFieldDescription($field);
		} else {
			return new JsonResponse(array('status' => 'KO', 'message' => 'Invalid context'));
		}

		if (!$fieldDescription) {
			return new JsonResponse(array('status' => 'KO', 'message' => 'The field does not exist'));
		}

		if (!$fieldDescription->getOption('editable')) {
			return new JsonResponse(array('status' => 'KO', 'message' => 'The field cannot be edit, editable option must be set to true'));
		}

		$propertyAccessor = PropertyAccess::createPropertyAccessor();
		$propertyPath     = new PropertyPath($field);

		// If property path has more than 1 element, take the last object in order to validate it
		if ($propertyPath->getLength() > 1) {
			$object = $propertyAccessor->getValue($object, $propertyPath->getParent());

			$elements     = $propertyPath->getElements();
			$field        = end($elements);
			$propertyPath = new PropertyPath($field);
		}

		$propertyAccessor->setValue($object, $propertyPath, '' !== $value ? $value : null);

		$violations = $this->validator->validateProperty($object, $field);

		if (count($violations)) {
			$messages = array();

			foreach ($violations as $violation) {
				$messages[] = $violation->getMessage();
			}

			return new JsonResponse(array('status' => 'KO', 'message' => implode("\n", $messages)));
		}


		$admin->update($object);

		$doctrine = $this->pool->getContainer()->get('doctrine');
		//Update counter
		if ($object instanceof Post)
		{
			$isApprovedAfter = $object->getIsApproved();
			$object->getCreatedBy()->changeApprovedPostsNoOnUpdate($isApprovedBefore, $isApprovedAfter);
			$doctrine->getManager()->flush();
		} elseif ($object instanceof PostVote)
		{
			$voteAfter = $object->getVote();
			$objAfter = $object->getPost();
			$object->getPost()->changeVotesNoOnUpdate($objBefore, $objAfter, $voteBefore, $voteAfter);
			$doctrine->getManager()->flush();
		} elseif ($object instanceof PostReport)
		{
			$objAfter = $object->getPost();
			$object->getPost()->changeReportsNoOnUpdate($objBefore, $objAfter);
			$doctrine->getManager()->flush();
		} elseif ($object instanceof Comment)
		{
			$isApprovedAfter = $object->getApproved();
			$object->getCreatedBy()->changeApprovedCommentsNoOnUpdate($isApprovedBefore, $isApprovedAfter);
			$doctrine->getManager()->flush();
		} elseif ($object instanceof CommentVote)
		{
			$voteAfter = $object->getVote();
			$objAfter = $object->getComment();
			$object->getComment()->changeVotesNoOnUpdate($objBefore, $objAfter, $voteBefore, $voteAfter);
			$doctrine->getManager()->flush();
		} elseif ($object instanceof CommentReport)
		{
			$objAfter = $object->getComment();
			$object->getComment()->changeReportsNoOnUpdate($objBefore, $objAfter);
			$doctrine->getManager()->flush();
		} elseif ($object instanceof UserReport)
		{
			$objAfter = $object->getUser();
			$object->getUser()->changeReportsNoOnUpdate($objBefore, $objAfter);
			$doctrine->getManager()->flush();
		} elseif ($object instanceof UserPoint)
		{
			$pointAfter = $object->getPoint();
			$objAfter = $object->getUser();
			$object->getUser()->changePointsNoOnUpdate($objBefore, $objAfter, $pointBefore, $pointAfter);
			$doctrine->getManager()->flush();
		} elseif ($object instanceof UserFollower)
		{
			$followerAfter =  $object->getFollower();
			$objAfter = $object->getUser();
			$object->getUser()->changeFollowersNoOnUpdate($objBefore, $objAfter, $followerBefore, $followerAfter);
			$doctrine->getManager()->flush();
		}

		// render the widget
		// todo : fix this, the twig environment variable is not set inside the extension ...
		$extension = $this->twig->getExtension('sonata_admin');
		$extension->initRuntime($this->twig);

		$content = $extension->renderListElement($rootObject, $fieldDescription);

		return new JsonResponse(array('status' => 'OK', 'content' => $content));
	}
} 