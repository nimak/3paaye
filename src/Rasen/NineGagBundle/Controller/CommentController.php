<?php

namespace Rasen\NineGagBundle\Controller;

use Rasen\NineGagBundle\Lib\NotificationUtility;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CommentController
 * @package Rasen\NineGagBundle\Controller
 */
class CommentController extends Controller
{
	/**
	 * @param $id
	 *
	 * @Route("/{id}/")
	 * @return array
	 */
    public function viewAction($id)
    {
        $response = new Response($id);
        return $response;
    }

}
