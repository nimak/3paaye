<?php

namespace Rasen\NineGagBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Csrf\CsrfProvider\CsrfProviderInterface;
use Rasen\NineGagBundle\Lib\NotificationUtility;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class NotificationController
 * @package Rasen\NineGagBundle\Controller
 */
class NotificationController extends Controller
{
	/**
	 * @Security("has_role('ROLE_USER')")
	 *
	 * @Route("/all")
	 * @return array
	 */
    public function allAction()
    {
	    $user = $this->getUser();
	    if (!$user) {
		    throw new AccessDeniedException();
	    }

	    /**
	     * @var NotificationUtility $notifUtil
	     */
	    $notifUtil = $this->get('rasen_ninegag.notification_utility');

	    $notifications = $notifUtil->getAllNotificationMessages($user->getId(), false, 50);
        return $this->render('RasenNineGagBundle:Notification:all.html.twig', array(
	        'notifications' => $notifications['messages'],
	        'unread_count' => $notifications['totalUnreadCount']
        ));
    }
}
