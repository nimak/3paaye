<?php

namespace Rasen\NineGagBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Acl\Exception\AclNotFoundException;

/**
 * Class PostCategoryController
 * @package Rasen\NineGagBundle\Controller
 */
class PostCategoryController extends Controller
{
	/**
	 * @Route("/{slug}/")
	 * @Route("/{slug}/hot")
	 * @Route("/{slug}/trending")
	 * @Route("/{slug}/fresh")
	 * @param $slug
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
    public function viewAction($slug)
    {
        $category = $this->getDoctrine()->getRepository('RasenNineGagBundle:PostCategory')->findOneBy(
            array('slug' => $slug)
        );
        if (!$category){
            throw new NotFoundHttpException ();
        }
	    return $this->render("RasenNineGagBundle:Category:single_category.html.twig", array(
		   "category" => $category
	    ));
    }
}
