<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 11/4/2014
 * Time: 4:08 PM
 */

namespace Rasen\NineGagBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;

class OAuthSecurityController extends Controller
{
	public function loginAction()
	{
		$request = $this->get('request');
		/* @var $request \Symfony\Component\HttpFoundation\Request */
		$session = $request->getSession();
		/* @var $session \Symfony\Component\HttpFoundation\Session\Session */

		// get the error if any (works with forward and redirect -- see below)
		if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
			$error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
		} elseif (null !== $session && $session->has(SecurityContext::AUTHENTICATION_ERROR)) {
			$error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
			$session->remove(SecurityContext::AUTHENTICATION_ERROR);
		} else {
			$error = '';
		}

		if ($error) {
			// TODO: this is a potential security risk (see http://trac.symfony-project.org/ticket/9523)
			$error = $error->getMessage();
		}
		// last username entered by the user
		$lastUsername = (null === $session) ? '' : $session->get(SecurityContext::LAST_USERNAME);

		$csrfToken = $this->container->get('form.csrf_provider')->generateCsrfToken('authenticate');

		return $this->render('RasenNineGagBundle:OAuth:login.html.twig',array(
			'last_username' => $lastUsername,
			'error'         => $error,
			'csrf_token' => $csrfToken,
		));
	}

	public function checkAction()
	{
		throw new \RuntimeException('You must configure the check path to be handled by the firewall using form_login in your security firewall configuration.');
	}
} 