<?php

namespace Rasen\NineGagBundle\Controller\Rest;

use JMS\Serializer\SerializationContext;
use Rasen\NineGagBundle\Entity\Post;
use Rasen\NineGagBundle\Entity\PostImage;
use Rasen\NineGagBundle\Entity\PostReport;
use Rasen\NineGagBundle\Entity\PostText;
use Rasen\NineGagBundle\Entity\PostVote;

use Rasen\NineGagBundle\Entity\User;
use Rasen\NineGagBundle\Entity\UserFollower;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Form\Extension\Csrf\CsrfProvider\CsrfProviderInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

use FOS\RestBundle\Controller\Annotations as Rest,
	FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Component\Security\Core\SecurityContext;

/**
 * Class UserRestStatefulController
 * @package Rasen\NineGagBundle\Controller\Rest
 */
class UserRestStatefulController extends FOSRestController
{

	/**
	 *
	 * @Rest\Get("/users/{username}/followers")
	 * @Rest\RequestParam(name="username", description="username")
	 *
	 * @param Request $request
	 * @param $username
	 *
	 * @return Response
	 */
	public function getUserFollowersAction(Request $request, $username)
	{
		$user = $this->getDoctrine()->getRepository("RasenNineGagBundle:User")->findOneBy(array("username" => $username ));
		if (!$user) {
			throw new NotFoundHttpException();
		}

        $query = $this->getDoctrine()->getRepository("RasenNineGagBundle:User")->findAllFollowersQ($user);

        $pagination = $this->get('rasen_ninegag.cursor_paginator')->cursorPaginate($request, $query, 'users');

        $resp = $this->view($pagination, 200);

		return $this->handleView($resp);
	}

	/**
	 *
	 * @Rest\Get("/users/{username}/following")
	 * @Rest\RequestParam(name="username", description="username")
	 *
     * @param Request $request
	 * @param $username
	 *
	 * @return Response
	 */
	public function getUserFollowingAction(Request $request, $username)
	{
		$user = $this->getDoctrine()->getRepository("RasenNineGagBundle:User")->findOneBy(array("username" => $username ));
		if (!$user) {
			throw new NotFoundHttpException();
		}

        $query = $this->getDoctrine()->getRepository("RasenNineGagBundle:User")->findAllFollowingsQ($user);
        $pagination = $this->get('rasen_ninegag.cursor_paginator')->cursorPaginate($request, $query, 'users');

        $resp = $this->view($pagination, 200);
		return $this->handleView($resp);
	}

	/**
	 *
	 * @Rest\Get("/users/{username}/points")
	 * @Rest\RequestParam(name="username", description="username")
	 *
	 * @param $username
	 *
	 * @return Response
	 */
	public function getUserPointsAction($username)
	{
		$user = $this->getDoctrine()->getRepository("RasenNineGagBundle:User")->findOneBy(array("username" => $username ));
		if (!$user) {
			throw new NotFoundHttpException();
		}

		if ($user == $this->getUser()) {
			throw new AccessDeniedException();
		}

		$userPoints = $this->getDoctrine()->getRepository('RasenNineGagBundle:UserPoint')->findBy(array(
			'user' => $user
		));

		$resp = $this->view($userPoints, 200);
		return $this->handleView($resp);
	}

	/**
	 *
	 * @Rest\Get("/users/{username}/badges")
	 * @Rest\RequestParam(name="username", description="username")
	 *
	 * @param $username
	 *
	 * @return Response
	 */
	public function getUserBadgesAction($username)
	{
		$user = $this->getDoctrine()->getRepository("RasenNineGagBundle:User")->findOneBy(array("username" => $username ));
		if (!$user) {
			throw new NotFoundHttpException();
		}
		$userBadges = $this->getDoctrine()->getRepository('RasenNineGagBundle:UserBadge')->findBy(array(
			'user' => $user,
            'isClaimed' => true
		));

        $badges = array();
        foreach ($userBadges as $userBadge) {
            $badges[] = $userBadge->getBadge();
        }

		$resp = $this->view($badges, 200);
		return $this->handleView($resp);
	}

	/**
	 * @Rest\Get("/users/search")
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function getUserSearchAction(Request $request)
	{
		$query = $request->query->get('q', null);
		$users = $this->getDoctrine()->getRepository("RasenNineGagBundle:User")->findByQuery($query);
		if (count($users) < 1) {
			throw new NotFoundHttpException();
		}

		$resp = $this->view($users, 200);
		return $this->handleView($resp);
	}

	/**
	 *
	 * @Rest\Get("/users/{username}")
	 * @Rest\RequestParam(name="username", description="username")
	 *
	 * @param $username
	 *
	 * @return Response
	 */
	public function getUserProfileAction($username)
	{
		$user = $this->getDoctrine()->getRepository("RasenNineGagBundle:User")->findOneBy(array("username" => $username ));
		if (!$user) {
			throw new NotFoundHttpException();
		}

		$resp = $this->view($user, 200);
		return $this->handleView($resp);
	}

	/**
	 * @Security("has_role('ROLE_USER')")
	 *
	 * @Rest\Post("/users/{username}/follow")
	 * @Rest\RequestParam(name="username", description="username")
	 *
	 * @param $username
	 *
	 * @return Response
	 */
	public function followUserAction($username)
	{
		/**
		 * @var SecurityContext $securityContext
		 */
		$securityContext = $this->container->get('security.context');
		if (!$securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
			throw new AccessDeniedException();
		}

		$thisUser = $this->getUser();

		/**
		 * @var User $toFollowUser
		 */
		$toFollowUser = $this->getDoctrine()->getRepository("RasenNineGagBundle:User")->findOneBy(array("username" => $username ));
		if (!$toFollowUser) {
			throw new NotFoundHttpException();
		}
		if ($toFollowUser === $thisUser) {
			throw new AccessDeniedException();
		}

		$userFollower = new UserFollower();
		$userFollower->setFollower($thisUser);
		$userFollower->setUser($toFollowUser);
		$em = $this->getDoctrine()->getManager();
		$em->persist($userFollower);
		$em->flush();

		$okView = $this->view('ok', 204);
		return $this->handleView($okView);
	}

	/**
	 * @Security("has_role('ROLE_USER')")
	 *
	 * @Rest\Post("/users/{username}/unfollow")
	 * @Rest\RequestParam(name="username", description="username")
	 *
	 * @param $username
	 *
	 * @return Response
	 */
	public function unFollowUserAction($username)
	{
		$thisUser = $this->getUser();

		$toFollowUser = $this->getDoctrine()->getRepository("RasenNineGagBundle:User")->findOneBy(array("username" => $username ));
		if (!$toFollowUser) {
			throw new NotFoundHttpException();
		}
		if ($toFollowUser === $thisUser) {
			throw new AccessDeniedException();
		}


		$userFollower = $this->getDoctrine()->getRepository('RasenNineGagBundle:UserFollower')->findOneBy(array(
			'user' => $toFollowUser,
			'follower' => $thisUser
		));
		if (!$userFollower || !($userFollower instanceof UserFollower)) {
			throw new AccessDeniedException();
		}
		$em = $this->getDoctrine()->getManager();
		$em->remove($userFollower);
		$em->flush();

		$okView = $this->view('ok', 204);
		return $this->handleView($okView);
	}

}
