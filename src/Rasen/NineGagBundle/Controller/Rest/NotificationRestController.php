<?php

namespace Rasen\NineGagBundle\Controller\Rest;

use Rasen\NineGagBundle\Lib\NotificationUtility;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Bundle\FrameworkBundle\Templating\TemplateReference,
	Symfony\Component\Routing\Exception\ResourceNotFoundException,
	Symfony\Component\Validator\ValidatorInterface;
use Doctrine\Common\Cache\Cache;
use FOS\RestBundle\View\View,
	FOS\RestBundle\View\ViewHandler,
	FOS\RestBundle\View\RouteRedirectView;
use JMS\Serializer\SerializationContext;

/**
 * Class NotificationRestController
 * @package Rasen\NineGagBundle\Controller\Rest
 */
class NotificationRestController extends FOSRestController
{

	public function getNotificationsAction()
	{
		$user = $this->getUser();
		if ($user) {
			/**
			 * @var NotificationUtility $notifUtil
			 */
			$notifUtil = $this->get('rasen_ninegag.notification_utility');
			$notifications = $notifUtil->getAllNotificationMessages($user->getId());
			$view = $this->view($notifications, 200);
			return $this->handleView($view);
		}


		$response = new Response('LOGIN FIRST');
		return $response;
	}

}
