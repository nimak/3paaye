<?php

namespace Rasen\NineGagBundle\Controller\Rest;

use Doctrine\ORM\Query;
use Hateoas\Representation\CollectionRepresentation;
use Rasen\NineGagBundle\Entity\Comment;
use Rasen\NineGagBundle\Entity\CommentReport;
use Rasen\NineGagBundle\Entity\CommentVote;
use Rasen\NineGagBundle\Entity\Post;
use Rasen\NineGagBundle\Entity\PostImage;
use Rasen\NineGagBundle\Entity\PostReport;
use Rasen\NineGagBundle\Entity\PostText;
use Rasen\NineGagBundle\Entity\PostVote;

use Rasen\NineGagBundle\Entity\User;
use Rasen\NineGagBundle\Entity\UserFollower;
use Rasen\NineGagBundle\Hateoas\Representation\CursorPaginatedRepresentation;
use Rasen\NineGagBundle\Hateoas\Representation\OffsetPaginatedRepresentation;
use Rasen\NineGagBundle\Lib\CursorPagination\Cursor;
use Rasen\NineGagBundle\Lib\CursorPagination\CursorPaginator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Form\Extension\Csrf\CsrfProvider\CsrfProviderInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

use FOS\RestBundle\Controller\Annotations as Rest,
	FOS\RestBundle\Request\ParamFetcherInterface;

/**
 * Class CommentRestStatefulController
 * @package Rasen\NineGagBundle\Controller\Rest
 */
class CommentRestStatefulController extends FOSRestController
{
	/**
	 * @Security("has_role('ROLE_USER')")
	 *
	 * @param Request $request
	 * @param $hashId
	 *
	 * @Rest\Put("/comments/{hashId}")
	 * @Rest\RequestParam(name="hashId", description="Comment hash id")
	 *
	 * @return Response
	 */
	public function commentEditAction(Request $request, $hashId)
	{
		$comment = $this->getComment($hashId);
		if (false === $this->get('security.authorization_checker')->isGranted('edit', $comment)) {
			throw new AccessDeniedException('Unauthorized access!');
		}

		if (!($comment instanceof Comment)) {
			throw new NotFoundHttpException();
		}

		$content = $request->request->get('content');
		if (!empty($content)) {
			$comment->setContent($content);
		}
		$errors = $this->get('validator')->validate($comment);
		if (count($errors)>0){
			$commentValidationErrors = $this->view($errors, 400);
			return $this->handleView($commentValidationErrors);
		}

		$this->getDoctrine()->getManager()->flush();
		$okView = $this->view('ok', 204);
		return $this->handleView($okView);
	}

	/**
	 *
	 * @Rest\Get("/posts/{postId}/comments/all")
	 * @Rest\RequestParam(name="postId", description="postId")
	 *
	 * @param Request $request
	 * @param $postId
	 *
	 * @return Response
	 */
	public function getPostCommentsAllAction(Request $request, $postId)
	{
		$id = $this->container->get('hashids')->decode($postId);

		$this->disableSoftDeleteFilter();

		$commentRepo = $this->getDoctrine()->getRepository("RasenNineGagBundle:Comment");

		$results['items'] = $commentRepo->findAllApprovedByPost($id, $this->getUser());

		$resp = $this->view($results, 200);

		return $this->handleView($resp);
	}

	/**
	 *
	 * @Rest\Get("/posts/{postId}/comments")
	 * @Rest\RequestParam(name="postId", description="postId")
	 *
	 * @param Request $request
	 * @param $postId
	 *
	 * @return Response
	 */
	public function getPostCommentsAction(Request $request, $postId)
	{
		$id = $this->container->get('hashids')->decode($postId);

		$this->disableSoftDeleteFilter();

		$commentRepo = $this->getDoctrine()->getRepository("RasenNineGagBundle:Comment");

		$offset = $request->query->get('offset', 0);
		$limit = $request->query->get('limit', 10);

		$results = $commentRepo->findAllApprovedByPost($id, $this->getUser(), $limit, $offset);

		/*$paginator  = $this->get('knp_paginator');
		$pagination = $paginator->paginate(
			$results,
			$page,
			$limit
		);
		$paginationData = $pagination->getPaginationData();*/

		/*if ($offset + count($results) >= $totalCount) {
			array_splice($results, count($results) - 2, 2); // Remove the last 2 items, since they've been already loaded in another action (overview)
		}*/


		//$results['offset'] = $offset + count($results['items']);

		$pagination = $this->paginate($request, $results);

		$resp = $this->view($pagination, 200);
		return $this->handleView($resp);
	}

	public static function compare_user_ids($a, $b)
	{
		return ($a->getId() - $b->getId());
	}

	public static function compare_user_score($a, $b) {
		$aScore = $a['score'];
		$bScore = $b['score'];
		if ($aScore == $bScore) {
			return 0;
		}
		return ($aScore < $bScore) ? 1 : -1; //DESC
	}

	/**
	 *
	 * @Security("has_role('ROLE_USER')")
	 *
	 * @Rest\Get("/posts/{postId}/comments/mentionables")
	 * @Rest\RequestParam(name="postId", description="postId")
	 *
	 * @param Request $request
	 * @param $postId
	 *
	 * @return Response
	 */
	public function getPostMentionableUsersAction(Request $request, $postId)
	{
		$limit = min($request->query->get('limit', 10), 100); //max limit = 100

		$query = $request->query->get('q', null);
		$user = $this->getUser();
		$id = $this->container->get('hashids')->decode($postId);

		$usersSearch = $this->getDoctrine()->getRepository("RasenNineGagBundle:User")->findByQuery($query);
		$usersCommented = $this->getDoctrine()->getRepository('RasenNineGagBundle:User')->findAllCommentedOnPost($id);
		$usersFollowing = $this->getDoctrine()->getRepository("RasenNineGagBundle:User")->findAllFollowings($user);
		$usersMentionedBefore = $this->getDoctrine()->getRepository("RasenNineGagBundle:User")->findAllUsersMentioned($user);

		/*$usersSearchAndFollowing = array_intersect($usersSearch, $usersFollowing);
		$usersSearchAndMentionedAndFollowing = array_intersect($usersSearch, $usersMentionedBefore, $usersFollowing);
		$usersSearchAndCommented = array_intersect($usersSearch, $usersCommented);
		$usersSearchAndMentioned = array_intersect($usersSearch, $usersMentionedBefore);

		$mentionables = array_unique(array_merge($usersSearchAndMentionedAndFollowing, $usersSearchAndFollowing, $usersSearchAndMentioned, $usersSearchAndCommented));
*/
		$sortableUsers = array();
		foreach ($usersSearch as $userSearch) {
			if ($userSearch->getId() == $user->getId()) continue;
			$sortableUsers[] = array(
				'user' => $userSearch,
				'score' => 0
			);
		}
		for ( $i=0;$i<count($sortableUsers);$i++ ) {
			$sortableUser = $sortableUsers[$i];
			$score = $sortableUser['score'];

			//die(var_dump(in_array($sortableUser['user'], $usersFollowing)));
			if (in_array($sortableUser['user'], $usersFollowing)) {
				$score++;
			}
			if (in_array($sortableUser['user'], $usersMentionedBefore)) {
				$score++;
			}
			if (in_array($sortableUser['user'], $usersCommented)) {
				$score++;
			}
			$sortableUsers[$i]['score'] = $score;
		}
		uasort($sortableUsers, array($this,'compare_user_score'));

		$mentionables = array_column($sortableUsers, 'user');

		$output = array_slice($mentionables, 0, $limit);

		//$mentionables = array_values($mentionables);

		//$mentionables = array_udiff($mentionables, array($user), array($this,'compare_user_ids'));

		$resp = $this->view($output, 200);
		return $this->handleView($resp);
	}

	/**
	 *
	 * @Rest\Get("/comments/{commentId}")
	 * @Rest\RequestParam(name="commentId", description="commentId")
	 *
	 * @param Request $request
	 * @param $commentId
	 *
	 * @return Response
	 */
	public function getCommentAction(Request $request, $commentId)
	{
		$id = $this->container->get('hashids')->decode($commentId);

		$this->disableSoftDeleteFilter();

		$commentRepo = $this->getDoctrine()->getRepository("RasenNineGagBundle:Comment");

		$results = $commentRepo->findOneById($id);

		$resp = $this->view($results, 200);
		return $this->handleView($resp);
	}

	/**
	 *
	 * @Rest\Get("/comments/{commentId}/votes")
	 * @Rest\RequestParam(name="commentId", description="commentId")
	 *
	 * @param Request $request
	 * @param $commentId
	 *
	 * @return Response
	 */
	public function getCommentVotesAction(Request $request, $commentId)
	{
        //ONLY UPVOTES

        $comment = $this->getComment($commentId);

        $userRepo = $this->getDoctrine()->getRepository("RasenNineGagBundle:User");
        $query = $userRepo->findVotedOnCommentQ($comment, true);

        $pagination = $this->get('rasen_ninegag.cursor_paginator')->cursorPaginate($request, $query, 'users');

		$resp = $this->view($pagination, 200);
		return $this->handleView($resp);
	}

	/**
	 *
	 * @Rest\Get("/comments/{commentId}/replies")
	 * @Rest\RequestParam(name="commentId", description="commentId")
	 *
	 * @param Request $request
	 * @param $commentId
	 *
	 * @return Response
	 */
	public function getCommentRepliesAction(Request $request, $commentId)
	{
		$id = $this->container->get('hashids')->decode($commentId);

		$this->disableSoftDeleteFilter();

		$commentRepo = $this->getDoctrine()->getRepository("RasenNineGagBundle:Comment");

		$offset = $request->query->get('offset', 0);
		$limit = $request->query->get('limit', 10);

		$results = $commentRepo->findAllByParent($id, $limit, $offset);

		/*if ($offset + count($results) >= $totalCount) {
			array_splice($results, count($results) - 2, 2); // Remove the last 2 items, since they've been already loaded in another action (overview)
		}*/


		$pagination = $this->paginate($request, $results);

		$resp = $this->view($pagination, 200);
		return $this->handleView($resp);
	}

	/**
	 *
	 * @Rest\Get("/comments/{commentId}/replies/all")
	 * @Rest\RequestParam(name="commentId", description="commentId")
	 *
	 * @param Request $request
	 * @param $commentId
	 *
	 * @return Response
	 */
	public function getCommentRepliesAllAction(Request $request, $commentId)
	{
		$id = $this->container->get('hashids')->decode($commentId);

		$this->disableSoftDeleteFilter();

		$commentRepo = $this->getDoctrine()->getRepository("RasenNineGagBundle:Comment");

		$results['items'] = $commentRepo->findAllByParent($id);

		$resp = $this->view($results, 200);
		return $this->handleView($resp);
	}

	/**
	 * @Security("has_role('ROLE_USER')")
	 *
	 * @param Request $request
	 * @param $commentId
	 *
	 * @Rest\Post("/comments/{commentId}/replies")
	 * @Rest\RequestParam(name="commentId", description="comment hash id")
	 *
	 * @return Response
	 * @throws AccessDeniedException
	 * @throws NotFoundHttpException
	 */
	public function postReplyAction(Request $request, $commentId)
	{
		$user = $this->getUser();
		$parent = $this->getComment($commentId);
		$post = $parent->getPost();

		$content = $request->request->get('content');

		$newComment = new Comment();

		$newComment->setPost($post)
		           ->setContent($content)
		           ->setParent($parent)
		           ->setApproved(true)
		;

		$validator = $this->get('validator');
		$errors = $validator->validate($newComment);
		if (count($errors) > 0) {
			$view = $this->view($errors, 400);
			return $this->handleView($view);
		}

		$em = $this->getDoctrine()->getManager();

		$em->persist($newComment);
		$em->flush();

		$view = $this->view($newComment, 201);
		$commentHashId = $this->container->get('hashids')->encode($newComment->getId());
		$view->setHeader('Location', $this->generateUrl('ajax_comment_get_comment', array('commentId'=>$commentHashId)));
		return $this->handleView($view);
	}

	/**
	 * @Security("has_role('ROLE_USER')")
	 *
	 * @Rest\Post("/comments/{hashId}/report")
	 * @Rest\RequestParam(name="hashId", description="hashId")
	 *
	 * @param $hashId
	 *
	 * @return Response
	 */
	public function reportCommentAction($hashId)
	{
		$user = $this->getUser();
		$comment = $this->getComment($hashId);

		if ($user === $comment->getCreatedBy()) {
			return $this->handleView($this->view('You can\'t report your own comment!', 400));
		}

		$commentReport = new CommentReport();
		$commentReport->setComment($comment)->setReporter($user);

		$em = $this->getDoctrine()->getManager();
		$em->persist($commentReport);
		$em->flush();

		$okView = $this->view('ok', 204);
		return $this->handleView($okView);
	}

	/**
	 * @Security("has_role('ROLE_USER')")
	 *
	 * @param Request $request
	 * @param $postHashId
	 *
	 * @Rest\Post("/posts/{postHashId}/comments")
	 * @Rest\RequestParam(name="postHashId", description="Post hash id")
	 *
	 * @return Response
	 * @throws AccessDeniedException
	 * @throws NotFoundHttpException
	 */
	public function postCommentAction(Request $request, $postHashId)
	{
		$user = $this->getUser();
		$post = $this->getPost($postHashId);

		$content = $request->request->get('content');
		$parent = $request->request->get('parent', null);

		$newComment = new Comment();

		$newComment->setPost($post)
			->setContent($content)
			->setParent($parent)
			->setApproved(true)
			;

		$validator = $this->get('validator');
		$errors = $validator->validate($newComment);
		if (count($errors) > 0) {
			$view = $this->view($errors, 400);
			return $this->handleView($view);
		}

		$em = $this->getDoctrine()->getManager();

		$em->persist($newComment);
		$em->flush();

		$view = $this->view($newComment, 201);
		$commentHashId = $this->container->get('hashids')->encode($newComment->getId());
		$view->setHeader('Location', $this->generateUrl('ajax_comment_get_comment', array('commentId'=>$commentHashId)));
		return $this->handleView($view);
	}

	/**
	 * @Security("has_role('ROLE_USER')")
	 *
	 * @param Request $request
	 * @param $hashId
	 *
	 * @Rest\Post("/comments/{hashId}/votes")
	 * @Rest\RequestParam(name="hashId", description="Comment hash id")
	 *
	 * @return Response
	 * @throws AccessDeniedException
	 * @throws NotFoundHttpException
	 */
	public function postVoteAction(Request $request, $hashId)
	{
		//if (! $request->isXmlHttpRequest()) throw new AccessDeniedException();

		$user = $this->getUser();
		$comment = $this->getComment($hashId);

		$vote = $request->request->get('vote');

		/**
		 * @var CommentVote $commentVote
		 */
		$commentVote = $this->getDoctrine()->getRepository('RasenNineGagBundle:CommentVote')->findBy(array(
			'comment' => $comment,
			'votedBy' => $user
		));
		$em = $this->getDoctrine()->getManager();

		if (empty($commentVote)) {
			$commentVote = new CommentVote();
			$commentVote->setComment($comment)->setVotedBy($user)->setVote($vote);
			$em->persist($commentVote);
			$em->flush();
			return $this->handleView($this->view('ok', 204));
		} elseif ($commentVote[0] instanceof CommentVote) {
			$commentVote[0]->setVote($vote);
			$em->flush();
			return $this->handleView($this->view('ok', 204));
		}

		return $this->handleView($this->view('ko', 400));
	}

	/**
	 * @Security("has_role('ROLE_USER')")
	 *
	 * @param $hashId
	 *
	 * @Rest\Delete("/comments/{hashId}/votes")
	 * @Rest\RequestParam(name="hashId", description="Comment hash id")
	 *
	 * @return Response
	 * @throws AccessDeniedException
	 * @throws NotFoundHttpException
	 */
	public function deleteVoteAction($hashId)
	{
		//if (! $request->isXmlHttpRequest()) throw new AccessDeniedException();

		$user = $this->getUser();
		$comment = $this->getComment($hashId);

		/**
		 * @var CommentVote $commentVote
		 */
		$commentVote = $this->getDoctrine()->getRepository('RasenNineGagBundle:CommentVote')->findBy(array(
			'comment' => $comment,
			'votedBy' => $user
		));

		if (!$commentVote) {
			throw new NotFoundHttpException;
		}

		$em = $this->getDoctrine()->getManager();

		if ($commentVote[0] instanceof CommentVote) {
			$em->remove($commentVote[0]);
			$em->flush();
			$okView = $this->view('ok', 204);
			return $this->handleView($okView);
		}

		return $this->handleView($this->view('ko', 400));
	}

	/**
	 * @Security("has_role('ROLE_USER')")
	 *
	 * @param $hashId
	 * @param Request $request
	 *
	 * @Rest\Delete("/comments/{hashId}")
	 * @Rest\RequestParam(name="hashId", description="Comment hash id")
	 *
	 * @return Response
	 * @throws AccessDeniedException
	 * @throws NotFoundHttpException
	 */
	public function deleteAction(Request $request, $hashId)
	{
		$comment = $this->getComment($hashId);
		if (false === $this->get('security.authorization_checker')->isGranted('delete', $comment)) {
			throw new AccessDeniedException('Unauthorized access!');
		}


		$em = $this->getDoctrine()->getManager();

		if ($comment instanceof Comment) {

			if ($request->query->has('report') && $request->query->get('report')) {
				if ($comment->getCreatedBy() !== $this->getUser()) {
					$previousReport = $em->getRepository('RasenNineGagBundle:CommentReport')->findOneBy(array('comment'=>$comment));
					if (!$previousReport) {
						$report = new CommentReport();
						$report->setComment($comment);
						$em->persist($report);
					}
				}
			}

			$em->remove($comment);
			$em->flush();
			$okView = $this->view('ok', 204);
			return $this->handleView($okView);
		}

		$koView = $this->view('ko', 400);
		return $this->handleView($koView);
	}

    /**
	 * @param Request $request
	 * @param $results
	 * @return array
	 */
	private function paginate(Request $request, $results)
	{
		$offset = $request->query->get('offset', 0);
		$limit = $request->query->get('limit', 10);

		$collectionRepresentation = new CollectionRepresentation(
			$results['items'],
			'comments'
		);
		$offsetPaginatedRepresentation = new OffsetPaginatedRepresentation(
			$collectionRepresentation,
			$request->get('_route'),
			$request->get('_route_params'),
			$offset,
			$results['offset'],
			$limit,
			$results['totalCount']
		);

		return $offsetPaginatedRepresentation;
	}

	/**
	 * Returns comment
	 *
	 * @param $hashId
	 *
	 * @return Comment
	 */
	private function getComment($hashId)
	{
		$this->disableSoftDeleteFilter();

		$id = $this->container->get('hashids')->decode($hashId);
		/**
		 * @var Comment $comment
		 */
		$comment = $this->getDoctrine()->getRepository('RasenNineGagBundle:Comment')->findOneBy(array(
			'id' => $id
		));
		if (!$comment) {
			throw new NotFoundHttpException();
		}
		return $comment;
	}

	/**
	 * Returns post
	 *
	 * @param $hashId
	 *
	 * @return Post
	 */
	private function getPost($hashId)
	{
		$id = $this->container->get('hashids')->decode($hashId);

		/**
		 * @var Post $post
		 */
		$post = $this->getDoctrine()->getRepository('RasenNineGagBundle:Post')->findOneBy(array(
			'id' => $id
		));
		if (!$post) {
			throw new NotFoundHttpException();
		}
		return $post;
	}

	private function disableSoftDeleteFilter()
	{
		if (array_key_exists('softdeleteable',$this->getDoctrine()->getManager()->getFilters()->getEnabledFilters())) {
			$this->getDoctrine()->getManager()->getFilters()->disable('softdeleteable');
		}
	}

}
