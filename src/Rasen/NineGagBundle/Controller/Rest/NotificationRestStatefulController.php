<?php

namespace Rasen\NineGagBundle\Controller\Rest;

use Rasen\NineGagBundle\Lib\NotificationUtility;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\Extension\Csrf\CsrfProvider\CsrfProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Bundle\FrameworkBundle\Templating\TemplateReference,
	Symfony\Component\Routing\Exception\ResourceNotFoundException,
	Symfony\Component\Validator\ValidatorInterface;
use Doctrine\Common\Cache\Cache;
use FOS\RestBundle\View\View,
	FOS\RestBundle\View\ViewHandler,
	FOS\RestBundle\View\RouteRedirectView;
use JMS\Serializer\SerializationContext;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\RestBundle\Controller\Annotations as Rest,
	FOS\RestBundle\Request\ParamFetcherInterface;

/**
 * Class NotificationRestStatefulController
 * @package Rasen\NineGagBundle\Controller\Rest
 */
class NotificationRestStatefulController extends FOSRestController
{



	/**
	 * @Security("has_role('ROLE_USER')")
	 *
	 * @Rest\Get("/notifications/latest", options={"expose"=true})
	 *
	 * @return array
	 * @throws AccessDeniedException
	 */
	public function latestAction()
	{
		$user = $this->getUser();
		if (!$user) {
			throw new AccessDeniedException();
		}

		/**
		 * @var NotificationUtility $notifUtil
		 */
		$notifUtil = $this->get('rasen_ninegag.notification_utility');

		$notifications = $notifUtil->getAllNotificationMessages($user->getId(), false, 10);

		$resp = $this->view($notifications, 200);
		return $this->handleView($resp);

		/*$notifJSON = $this->get('jms_serializer')->serialize($notifications, 'json');
		$resp =  new Response($notifJSON);
		//$resp->headers->set('Content-Type', 'application/json');
		return $resp;*/
	}

	/**
	 * Marks all notifications as read
	 *
	 * @Security("has_role('ROLE_USER')")
	 *
	 * @Rest\Post("/notifications/mark-read", options={"expose"=true})
	 *
	 * @param Request $request
	 *
	 * @return mixed
	 * @throws AccessDeniedException
	 */
	public function markAllReadAction(Request $request)
	{
		$okView = $this->view('ok', 200);
		$koView = $this->view('ko', 500);

		$user = $this->getUser();
		if (!$user) {
			throw new AccessDeniedException();
		}

		if (! $request->isXmlHttpRequest()) throw new AccessDeniedException();

		/**
		 * @var CsrfProviderInterface $csrf
		 */
		$csrf = $this->get('form.csrf_provider');
		if (!$csrf->isCsrfTokenValid('ajax', $request->get('_token'))) throw new AccessDeniedException();

		/**
		 * @var NotificationUtility $notifUtil
		 */
		$notifUtil = $this->get('rasen_ninegag.notification_utility');

		$result = $notifUtil->markAllAsRead($user->getId());

		return $this->handleView($okView);
	}

}
