<?php

namespace Rasen\NineGagBundle\Controller\Rest;

use Rasen\NineGagBundle\Lib\NotificationUtility;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CommentRestController
 * @package Rasen\NineGagBundle\Controller\Rest
 */
class CommentRestController extends FOSRestController
{

}
