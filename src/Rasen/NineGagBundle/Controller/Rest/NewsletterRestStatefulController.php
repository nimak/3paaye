<?php

namespace Rasen\NineGagBundle\Controller\Rest;

use JMS\Serializer\SerializationContext;
use Rasen\NineGagBundle\Entity\NewsletterSubscriber;
use Rasen\NineGagBundle\Entity\Post;
use Rasen\NineGagBundle\Entity\PostImage;
use Rasen\NineGagBundle\Entity\PostReport;
use Rasen\NineGagBundle\Entity\PostText;
use Rasen\NineGagBundle\Entity\PostVote;

use Rasen\NineGagBundle\Entity\User;
use Rasen\NineGagBundle\Entity\UserFollower;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Form\Extension\Csrf\CsrfProvider\CsrfProviderInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

use FOS\RestBundle\Controller\Annotations as Rest,
	FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Component\Security\Core\SecurityContext;

/**
 * Class NewsletterRestStatefulController
 * @package Rasen\NineGagBundle\Controller\Rest
 */
class NewsletterRestStatefulController extends FOSRestController
{
	/**
	 *
	 * @Rest\Post("/newsletter-subscribers", options={"expose"=true})
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function postNewsletterSubscriberAction(Request $request)
	{
		$okView = $this->view('ok', 200);
		$koView = $this->view('ko', 400);

		$translator = $this->container->get('translator');

		$email = $request->request->get('email');
		$errors = array();
		if (empty($email)){
			$errors[] = array('message'=>$translator->trans("newsletters.subscribe.errors.empty_email", array(), "RasenNineGagBundle"));
			$koView = $this->view($errors, 400);
			return $this->handleView($koView);
		}

		$subscriber = new NewsletterSubscriber();
		$subscriber = $subscriber->setEmail($email)->setIsActiveSubscription(true);

		$errors = $this->get('validator')->validate($subscriber);
		if (count ($errors)>0){
			$koView = $this->view($errors, 400);
			return $this->handleView($koView);
		}

		$em = $this->getDoctrine()->getManager();
		$em->persist($subscriber);
		$em->flush();

		return $this->handleView($okView);
	}

}
