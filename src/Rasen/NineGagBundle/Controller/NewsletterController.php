<?php

namespace Rasen\NineGagBundle\Controller;

use JMS\Serializer\SerializationContext;
use Rasen\NineGagBundle\Entity\NewsletterSubscriber;
use Rasen\NineGagBundle\Entity\Post;
use Rasen\NineGagBundle\Entity\PostImage;
use Rasen\NineGagBundle\Entity\PostReport;
use Rasen\NineGagBundle\Entity\PostText;
use Rasen\NineGagBundle\Entity\PostVote;
use Rasen\NineGagBundle\Form\Type\PostImageType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Csrf\CsrfProvider\CsrfProviderInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class NewsletterController
 * @package Rasen\NineGagBundle\Controller
 */
class NewsletterController extends Controller
{
	/**
	 * @param Request $request
	 *
	 * @Route("/subscribe/")
	 * @Method({"POST", "GET"})
	 * @return array
	 */
	public function subscribeAction(Request $request)
	{
		$email = $request->request->get('email');
		$subscribe = $request->request->get('subscribe');

		if ($subscribe) {
			$csrfValid = $this->get('form.csrf_provider')->isCsrfTokenValid('newsletter',$request->request->get('_token'));
			if(!$csrfValid){
				throw $this->createAccessDeniedException();
			}

			$newPost = new NewsletterSubscriber();
			$newPost->setEmail($email)->setIsActiveSubscription(true);
			$errors = $this->get('validator')->validate($newPost);
			if (count ($errors)>0){
				$this->render('RasenNineGagBundle:Newsletter:subscribe.html.twig', array(
					'errors' => $errors
				));
			}

			$em = $this->getDoctrine()->getManager();
			$em->persist($newPost);
			$em->flush();
			return $this->redirect($this->generateUrl('rasen_ninegag_newsletter_subscribe'));
		}

		return $this->render('RasenNineGagBundle:Newsletter:subscribe.html.twig');
	}
}
