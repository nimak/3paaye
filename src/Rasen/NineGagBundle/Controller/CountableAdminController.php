<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/30/2014
 * Time: 1:35 PM
 */

namespace Rasen\NineGagBundle\Controller;

use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManager;
use Rasen\NineGagBundle\Entity\Post;
use Rasen\NineGagBundle\Entity\PostVote;
use Rasen\NineGagBundle\Entity\PostReport;
use Rasen\NineGagBundle\Entity\Comment;
use Rasen\NineGagBundle\Entity\CommentVote;
use Rasen\NineGagBundle\Entity\CommentReport;
use Rasen\NineGagBundle\Entity\User;
use Rasen\NineGagBundle\Entity\UserFollower;
use Rasen\NineGagBundle\Entity\UserPoint;
use Rasen\NineGagBundle\Entity\UserReport;
use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Sonata\AdminBundle\Exception\ModelManagerException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
/**
 * Class CountableAdminController
 * @package Rasen\NineGagBundle\Controller
 */
class CountableAdminController extends Controller
{

	/**
	 * @return RedirectResponse
	 */
	public function recycleAction()
	{
		$id = $this->get('request')->get($this->admin->getIdParameter());
		$object = $this->admin->getObject($id);
		if (!$object) {
			throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
		}
		if (false === $this->admin->isGranted('DELETE', $object)) {
			throw new AccessDeniedException();
		}

		if ($object instanceof Post || $object instanceof Comment || $object instanceof User) {
			$this->admin->delete($object);
			$this->addFlash( 'sonata_flash_success', $this->admin->trans('flash_delete_success', array(), 'RasenNineGagBundle'));
		}
		return new RedirectResponse($this->admin->generateUrl('list'));
	}

	/**
	 * @return RedirectResponse
	 */
	public function restoreAction()
	{
		$id = $this->get('request')->get($this->admin->getIdParameter());
		$object = $this->admin->getObject($id);
		if (!$object) {
			throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
		}
		if (false === $this->admin->isGranted('DELETE', $object)) {
			throw new AccessDeniedException();
		}

		if ($object instanceof Post || $object instanceof Comment || $object instanceof User) {
			$object->setDeletedAt(null);
			$this->admin->update($object);
			$this->addFlash( 'sonata_flash_success', $this->admin->trans('flash_restore_success', array(), 'RasenNineGagBundle'));
		}
		return new RedirectResponse($this->admin->generateUrl('list'));
	}

	/**
	 * @return RedirectResponse
	 */
	public function approveAction()
	{
		$id = $this->get('request')->get($this->admin->getIdParameter());
		$object = $this->admin->getObject($id);
		if (!$object) {
			throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
		}

		if ($object instanceof Post) {
			$object->setIsApproved(true);
			$this->admin->update($object);
			$this->addFlash( 'sonata_flash_success', $this->admin->trans('flash_approve_success', array(), 'RasenNineGagBundle'));
		} elseif ($object instanceof Comment) {
			$object->setApproved(true);
			$this->admin->update($object);
			$this->addFlash('sonata_flash_success', $this->admin->trans('flash_approve_success', array(), 'RasenNineGagBundle'));
		}
		return new RedirectResponse($this->admin->generateUrl('list'));
	}

	/**
	 * @return RedirectResponse
	 */
	public function disapproveAction()
	{
		$id = $this->get('request')->get($this->admin->getIdParameter());
		$object = $this->admin->getObject($id);
		if (!$object) {
			throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
		}

		if ($object instanceof Post) {
			$object->setIsApproved(false);
			$this->admin->update($object);
			$this->addFlash( 'sonata_flash_success', $this->admin->trans('flash_disapprove_success', array(), 'RasenNineGagBundle'));
		} elseif ($object instanceof Comment) {
			$object->setApproved(false);
			$this->admin->update($object);
			$this->addFlash('sonata_flash_success', $this->admin->trans('flash_disapprove_success', array(), 'RasenNineGagBundle'));
		}
		return new RedirectResponse($this->admin->generateUrl('list'));
	}

	/**
	 * @param ProxyQueryInterface $query
	 *
	 * @return RedirectResponse
	 */
	public function batchActionRecycle(ProxyQueryInterface $query)
	{
		if (false === $this->admin->isGranted('DELETE')) {
			throw new AccessDeniedException();
		}

		$modelManager = $this->admin->getModelManager();
		try {
			$modelManager->batchDelete($this->admin->getClass(), $query);
			$this->addFlash('sonata_flash_success', 'flash_batch_recycle_success');
		} catch (ModelManagerException $e) {
			$this->addFlash('sonata_flash_error', 'flash_batch_recycle_error');
		}

		return new RedirectResponse($this->admin->generateUrl('list', array('filter' => $this->admin->getFilterParameters())));
	}

	/**
	 * @param ProxyQueryInterface $query
	 *
	 * @return RedirectResponse
	 */
	public function batchActionRestore(ProxyQueryInterface $query)
	{
		if (false === $this->admin->isGranted('DELETE')) {
			throw new AccessDeniedException();
		}

		$modelManager = $this->admin->getModelManager();

		$query->select('DISTINCT '.$query->getRootAlias());

		try {
			$entityManager = $modelManager->getEntityManager($this->admin->getClass());
			/*$entityManager->getConnection()
				->getConfiguration()
				->setSQLLogger(new \Doctrine\DBAL\Logging\EchoSQLLogger())
			;*/
			//$entityManager->getFilters()->enable('softdeleteable');
			$i = 0;
			foreach ($query->getQuery()->iterate() as $pos => $object) {
				if ($object[0] instanceof Post || $object[0] instanceof Comment || $object[0] instanceof User) {
					$object[0]->setDeletedAt(null);
					$entityManager->persist($object[0]);
					if ((++$i % 20) == 0) {
						$entityManager->flush();
						$entityManager->clear();
					}
				}
			}
			$entityManager->flush();
			$entityManager->clear();
			//die();
		} catch (\PDOException $e) {
			//throw new ModelManagerException('', 0, $e);
			$this->addFlash('sonata_flash_error', $this->admin->trans('flash_batch_restore_error', array(), 'RasenNineGagBundle'));
			return new RedirectResponse(
				$this->admin->generateUrl('list',$this->admin->getFilterParameters())
			);
		} catch (DBALException $e) {
			//throw new ModelManagerException('', 0, $e);
			$this->addFlash('sonata_flash_error', $this->admin->trans('flash_batch_restore_error', array(), 'RasenNineGagBundle'));
			return new RedirectResponse(
				$this->admin->generateUrl('list',$this->admin->getFilterParameters())
			);
		}
		$this->addFlash('sonata_flash_success', $this->admin->trans('flash_batch_restore_success', array(), 'RasenNineGagBundle'));
		return new RedirectResponse(
			$this->admin->generateUrl('list',$this->admin->getFilterParameters())
		);
	}

	/**
	 * @param ProxyQueryInterface $selectedModelQuery
	 *
	 * @return RedirectResponse
	 */
	public function batchActionApprove(ProxyQueryInterface $selectedModelQuery)
	{
		if (!$this->admin->isGranted('EDIT'))
		{
			throw new AccessDeniedException();
		}

		$modelManager = $this->admin->getModelManager();
		$selectedModels = $selectedModelQuery->execute();
		// do the work here
		try {
			foreach ($selectedModels as $selectedModel) {
				if ($selectedModel instanceof Post) {
					$selectedModel->setIsApproved(true);
				} elseif ($selectedModel instanceof Comment) {
					$selectedModel->setApproved(true);
				}
			}
			$modelManager->update($selectedModel);
		} catch (\Exception $e) {
			$this->addFlash('sonata_flash_error', $this->admin->trans('flash_batch_approve_error', array(), 'RasenNineGagBundle'));

			return new RedirectResponse(
				$this->admin->generateUrl('list',$this->admin->getFilterParameters())
			);
		}

		$this->addFlash('sonata_flash_success', $this->admin->trans('flash_batch_approve_success', array(), 'RasenNineGagBundle'));

		return new RedirectResponse(
			$this->admin->generateUrl('list',$this->admin->getFilterParameters())
		);
	}

	/**
	 * @param ProxyQueryInterface $selectedModelQuery
	 *
	 * @return RedirectResponse
	 */
	public function batchActionDisapprove(ProxyQueryInterface $selectedModelQuery)
	{
		if (!$this->admin->isGranted('EDIT'))
		{
			throw new AccessDeniedException();
		}

		$modelManager = $this->admin->getModelManager();
		$selectedModels = $selectedModelQuery->execute();
		// do the work here
		try {
			foreach ($selectedModels as $selectedModel) {
				if ($selectedModel instanceof Post) {
					$selectedModel->setIsApproved(false);
				} elseif ($selectedModel instanceof Comment) {
					$selectedModel->setApproved(false);
				}
			}
			$modelManager->update($selectedModel);
		} catch (\Exception $e) {
			$this->addFlash('sonata_flash_error', $this->admin->trans('flash_batch_disapprove_error', array(), 'RasenNineGagBundle'));

			return new RedirectResponse(
				$this->admin->generateUrl('list',$this->admin->getFilterParameters())
			);
		}

		$this->addFlash('sonata_flash_success', $this->admin->trans('flash_batch_disapprove_success', array(), 'RasenNineGagBundle'));

		return new RedirectResponse(
			$this->admin->generateUrl('list',$this->admin->getFilterParameters())
		);
	}

//	/**
//	 * {@inheritdoc}
//	 */
//	public function editAction($id = null)
//	{
//		// the key used to lookup the template
//		$templateKey = 'edit';
//
//		$id = $this->get('request')->get($this->admin->getIdParameter());
//		$object = $this->admin->getObject($id);
//
//		if (!$object) {
//			throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
//		}
//
//		if (false === $this->admin->isGranted('EDIT', $object)) {
//			throw new AccessDeniedException();
//		}
//
//		$this->admin->setSubject($object);
//
//		//get the value before submit
//		$objBefore = null;
//		$isApprovedBefore = null;
//		$voteBefore = null;
//		$pointBefore = null;
//		$followerBefore = null;
//		if ($object instanceof Post)
//		{
//			$objBefore = $object->getCreatedBy();
//			$isApprovedBefore = $object->getIsApproved();
//		} elseif ($object instanceof PostVote)
//		{
//			$objBefore = $object->getPost();
//			$voteBefore = $object->getVote();
//		} elseif ($object instanceof PostReport)
//		{
//			$objBefore = $object->getPost();
//		} elseif ($object instanceof Comment)
//		{
//			$objBefore = $object->getCreatedBy();
//			$isApprovedBefore = $object->getApproved();
//		} elseif ($object instanceof CommentVote)
//		{
//			$objBefore = $object->getComment();
//			$voteBefore = $object->getVote();
//		} elseif ($object instanceof CommentReport)
//		{
//			$objBefore = $object->getComment();
//		} elseif ($object instanceof UserReport)
//		{
//			$objBefore = $object->getUser();
//		} elseif ($object instanceof UserPoint)
//		{
//			$objBefore = $object->getUser();
//			$pointBefore = $object->getPoint();
//		} elseif ($object instanceof UserFollower)
//		{
//			$objBefore = $object->getUser();
//			$followerBefore = $object->getFollower();
//		}
//
//		/** @var $form \Symfony\Component\Form\Form */
//		$form = $this->admin->getForm();
//		$form->setData($object);
//
//		if ($this->getRestMethod() == 'POST') {
//			$form->submit($this->get('request'));
//
//			$isFormValid = $form->isValid();
//
//			// persist if the form was valid and if in preview mode the preview was approved
//			if ($isFormValid && (!$this->isInPreviewMode() || $this->isPreviewApproved())) {
//
//				try {
//					$object = $this->admin->update($object);
//
//					//Update counter
//					if ($object instanceof Post)
//					{
//						$isApprovedAfter = $form->get('isApproved')->getData();
//						$object->getCreatedBy()->changeApprovedPostsNoOnUpdate($isApprovedBefore, $isApprovedAfter);
//						$this->getDoctrine()->getManager()->flush();
//					} elseif ($object instanceof PostVote)
//					{
//						$voteAfter = $form->get('vote')->getData();
//						$objAfter = $object->getPost();
//						$object->getPost()->changeVotesNoOnUpdate($objBefore, $objAfter, $voteBefore, $voteAfter);
//						$this->getDoctrine()->getManager()->flush();
//					} elseif ($object instanceof PostReport)
//					{
//						$objAfter = $object->getPost();
//						$object->getPost()->changeReportsNoOnUpdate($objBefore, $objAfter);
//						$this->getDoctrine()->getManager()->flush();
//					} elseif ($object instanceof Comment)
//					{
//						$isApprovedAfter = $form->get('approved')->getData();
//						$object->getCreatedBy()->changeApprovedCommentsNoOnUpdate($isApprovedBefore, $isApprovedAfter);
//						$this->getDoctrine()->getManager()->flush();
//					} elseif ($object instanceof CommentVote)
//					{
//						$voteAfter = $form->get('vote')->getData();
//						$objAfter = $object->getComment();
//						$object->getComment()->changeVotesNoOnUpdate($objBefore, $objAfter, $voteBefore, $voteAfter);
//						$this->getDoctrine()->getManager()->flush();
//					} elseif ($object instanceof CommentReport)
//					{
//						$objAfter = $object->getComment();
//						$object->getComment()->changeReportsNoOnUpdate($objBefore, $objAfter);
//						$this->getDoctrine()->getManager()->flush();
//					} elseif ($object instanceof UserReport)
//					{
//						$objAfter = $object->getUser();
//						$object->getUser()->changeReportsNoOnUpdate($objBefore, $objAfter);
//						$this->getDoctrine()->getManager()->flush();
//					} elseif ($object instanceof UserPoint)
//					{
//						$pointAfter = $form->get('point')->getData();
//						$objAfter = $object->getUser();
//						$object->getUser()->changePointsNoOnUpdate($objBefore, $objAfter, $pointBefore, $pointAfter);
//						$this->getDoctrine()->getManager()->flush();
//					} elseif ($object instanceof UserFollower)
//					{
//						$followerAfter =  $object->getFollower();
//						$objAfter = $object->getUser();
//						$object->getUser()->changeFollowersNoOnUpdate($objBefore, $objAfter, $followerBefore, $followerAfter);
//						$this->getDoctrine()->getManager()->flush();
//					}
//
//					if ($this->isXmlHttpRequest()) {
//						return $this->renderJson(array(
//							'result'    => 'ok',
//							'objectId'  => $this->admin->getNormalizedIdentifier($object)
//						));
//					}
//
//					$this->addFlash('sonata_flash_success', $this->admin->trans('flash_edit_success', array('%name%' => $this->admin->toString($object)), 'SonataAdminBundle'));
//
//					// redirect to edit mode
//					return $this->redirectTo($object);
//
//				} catch (ModelManagerException $e) {
//
//					$isFormValid = false;
//				}
//			}
//
//			// show an error message if the form failed validation
//			if (!$isFormValid) {
//				if (!$this->isXmlHttpRequest()) {
//					$this->addFlash('sonata_flash_error', $this->admin->trans('flash_edit_error', array('%name%' => $this->admin->toString($object)), 'SonataAdminBundle'));
//				}
//			} elseif ($this->isPreviewRequested()) {
//				// enable the preview template if the form was valid and preview was requested
//				$templateKey = 'preview';
//				$this->admin->getShow();
//			}
//		}
//
//		$view = $form->createView();
//
//		// set the theme for the current Admin Form
//		$this->get('twig')->getExtension('form')->renderer->setTheme($view, $this->admin->getFormTheme());
//
//		return $this->render($this->admin->getTemplate($templateKey), array(
//			'action' => 'edit',
//			'form'   => $view,
//			'object' => $object,
//		));
//	}
} 