<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 12/3/2014
 * Time: 6:41 PM
 */

namespace Rasen\NineGagBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Controller\RegistrationController as BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class RegistrationController
 * @package Rasen\NineGagBundle\Controller
 */
class RegistrationController extends BaseController
{
	/**
	 * @return RedirectResponse
	 */
	public function registerAction()
	{
		$form = $this->container->get('fos_user.registration.form');
		$formHandler = $this->container->get('fos_user.registration.form.handler');
		$confirmationEnabled = $this->container->getParameter('fos_user.registration.confirmation.enabled');

		$csrfValid = $this->container->get('form.csrf_provider')->isCsrfTokenValid('register',$this->container->get('request')->request->get('_token'));
		if(!$csrfValid){
			throw new AccessDeniedException();
		}

		$process = $formHandler->process($confirmationEnabled);
		if ($process) {
			$user = $form->getData();

			$authUser = false;
			if ($confirmationEnabled) {
				$this->container->get('session')->set('fos_user_send_confirmation_email/email', $user->getEmail());
				$route = 'fos_user_registration_check_email';
			} else {
				$authUser = true;
				$route = 'fos_user_registration_confirmed';
			}

			$this->setFlash('fos_user_success', 'registration.flash.user_created');
			$url = $this->container->get('router')->generate($route);
			$response = new RedirectResponse($url);

			if ($authUser) {
				$this->authenticateUser($user, $response);
			}

			return $response;
		}

		return $this->container->get('templating')->renderResponse('FOSUserBundle:Registration:register.html.'.$this->getEngine(), array(
			'form' => $form->createView(),
		));
	}
}