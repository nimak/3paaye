<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/8/2014
 * Time: 12:14 AM
 */

namespace Rasen\NineGagBundle\Model;

use Doctrine\ORM\Mapping as ORM;

use JMS\Serializer\Annotation as Serializer;
/**
 * Class VotableReportable
 *
 * Provides shared methods and properties for entities which are both votable and reportable (such as Post, Comment etc.)
 *
 * @ORM\MappedSuperclass
 *
 * @Serializer\ExclusionPolicy("none")
 *
 * @package Rasen\NineGagBundle\Model
 */
class VotableReportable
{
	/**
	 * UpVotes count
	 *
	 * @var integer
	 *
	 * @Serializer\Groups({"default", "own"})
	 *
	 * @ORM\Column(name="upvotes_no", type="integer", nullable=true, options={"unsigned":true, "default":0})
	 */
	protected $upvotesNo;

	/**
	 * DownVotes count
	 *
	 * @var integer
	 *
	 * @Serializer\Groups({"default", "own"})
	 *
	 * @ORM\Column(name="downvotes_no", type="integer", nullable=true, options={"unsigned":true, "default":0})
	 */
	protected $downvotesNo;

	/**
	 * Reports (inappropriate flags) count
	 *
	 * @var integer
	 *
	 * @Serializer\Exclude
	 *
	 * @ORM\Column(name="reports_no", type="integer", nullable=true, options={"unsigned":true, "default":0})
	 */
	protected $reportsNo;


	/**
	 * Set upvotesNo
	 *
	 * @param integer $upvotesNo
	 * @return VotableReportable
	 */
	public function setUpvotesNo($upvotesNo)
	{
		$this->upvotesNo = $upvotesNo;

		return $this;
	}

	/**
	 * Get upvotesNo
	 *
	 * @return integer
	 */
	public function getUpvotesNo()
	{
		return $this->upvotesNo;
	}

	/**
	 * Set downvotesNo
	 *
	 * @param integer $downvotesNo
	 * @return VotableReportable
	 */
	public function setDownvotesNo($downvotesNo)
	{
		$this->downvotesNo = $downvotesNo;

		return $this;
	}

	/**
	 * Get downvotesNo
	 *
	 * @return integer
	 */
	public function getDownvotesNo()
	{
		return $this->downvotesNo;
	}

	/**
	 * Set reportsNo
	 *
	 * @param integer $reportsNo
	 * @return VotableReportable
	 */
	public function setReportsNo($reportsNo)
	{
		$this->reportsNo = $reportsNo;

		return $this;
	}

	/**
	 * Get reportsNo
	 *
	 * @return integer
	 */
	public function getReportsNo()
	{
		return $this->reportsNo;
	}

	/**
	 * Update upvotesNo
	 *
	 * Increase or Decrease one step
	 *
	 * @param bool $decrease
	 * @return VotableReportable
	 */
	public function updateUpvotesNo($decrease = false)
	{
		if ($this->upvotesNo == null) $this->upvotesNo = 0;
		$this->upvotesNo = $decrease ? $this->upvotesNo - 1 : $this->upvotesNo + 1;

		return $this;
	}

	/**
	 * Update downvotesNo
	 *
	 * Increase or Decrease one step
	 *
	 * @param bool $decrease
	 * @return VotableReportable
	 */
	public function updateDownvotesNo($decrease = false)
	{
		if ($this->downvotesNo == null) $this->downvotesNo = 0;
		$this->downvotesNo = $decrease ? $this->downvotesNo - 1 : $this->downvotesNo + 1;

		return $this;
	}

	/**
	 * Update vote
	 *
	 * Increase or Decrease upVote/downVote one step depending on the vote
	 *
	 * @param bool $decrease
	 * @return VotableReportable
	 */
	public function updateVotesNo($vote, $decrease = false)
	{
		if ($vote == true) {
			$this->updateUpvotesNo($decrease);
		} else {
			$this->updateDownvotesNo($decrease);
		}

		return $this;
	}

	/**
	 * Update reportsNo
	 *
	 * Increase or Decrease one step
	 *
	 * @param bool $decrease
	 * @return VotableReportable
	 */
	public function updateReportsNo($decrease = false)
	{
		if ($this->reportsNo == null) $this->reportsNo = 0;
		$this->reportsNo = $decrease ? $this->reportsNo - 1 : $this->reportsNo + 1;

		return $this;
	}

	/**
	 * Updates votes counter on entity updates
	 *
	 * @param VotableReportable $objBefore
	 * @param VotableReportable $objAfter
	 * @param $voteBefore
	 * @param $voteAfter
	 */
	public function changeVotesNoOnUpdate(VotableReportable $objBefore, VotableReportable $objAfter, $voteBefore, $voteAfter)
	{
		if ($objBefore !== $objAfter) {
			$objBefore->updateVotesNo($voteBefore, true); // decrease last post's vote count
			$objAfter->updateVotesNo($voteAfter, false); // increase new post's vote count
		} else {
			if ($voteBefore && !$voteAfter) {
				$this->updateUpvotesNo(true); // upVotes--
				$this->updateDownvotesNo(false); // downVotes++
			} elseif (!$voteBefore && $voteAfter) {
				$this->updateUpvotesNo(false); // upVotes++
				$this->updateDownvotesNo(true); // downVotes--
			}

		}
	}

	/**
	 * Updates reports counter on entity updates
	 *
	 * @param VotableReportable $objBefore
	 * @param VotableReportable $objAfter
	 */
	public function changeReportsNoOnUpdate(VotableReportable $objBefore, VotableReportable $objAfter)
	{
		if ($objBefore !== $objAfter) {
			$objBefore->updateReportsNo(true); // decrease last object's report count
			$objAfter->updateReportsNo(false); // increase new object's report count
		}
	}

	/**
	 * Total # of votes (aka voters).
	 *
	 * @return int
	 */
	public function getTotalVotersNo() {
		return $this->getUpvotesNo() + $this->getDownvotesNo();
	}

	/**
	 * Total sum of votes (score)
	 *
	 * @Serializer\VirtualProperty
	 * @Serializer\Groups({"default", "own"})
	 *
	 * @return int
	 */
	public function getTotalVotes() {
		return $this->getUpvotesNo() - $this->getDownvotesNo();
	}

	public function resetCounters()
	{
		$this->downvotesNo = $this->upvotesNo = $this->reportsNo = 0;
	}
} 