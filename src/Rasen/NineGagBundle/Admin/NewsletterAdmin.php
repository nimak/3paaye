<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/23/2014
 * Time: 11:09 AM
 */

namespace Rasen\NineGagBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
/**
 * Class NewsletterAdmin
 *
 * @package Rasen\NineGagBundle\Admin
 */
class NewsletterAdmin extends Admin
{
	/**
	 * {@inheritdoc}
	 */
	protected $datagridValues = array(
		'_page' => 1,
		'_sort_order' => 'DESC',
		'_sort_by' => 'createdTime'
	);

	/**
	 * {@inheritdoc}
	 */
	public function getBaseRoutePattern()
	{
		if (!$this->baseRoutePattern) {
			$baseRoute = 'newsletter';
			if ($this->isChild()) { // the admin class is a child, prefix it with the parent route name
				$this->baseRoutePattern = sprintf('%s/{id}/%s',
					$this->getParent()->getBaseRoutePattern(),
					$baseRoute
				);
			} else {
				$this->baseRoutePattern = $baseRoute;
			}
		}
		return $this->baseRoutePattern;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureFormFields(FormMapper $formMapper)
	{
		$formMapper
			->with('newsletter',
				array(
					'class' => 'col-md-8 col-md-offset-2'
				))
			->add('content', 'textarea', array('help'=>'form.description.newsletter.content'))
		;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureDatagridFilters(DatagridMapper $datagridMapper)
	{
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureListFields(ListMapper $listMapper)
	{
		$listMapper
			->addIdentifier('id')
			->add('content')
		;
	}
} 