<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/24/2014
 * Time: 12:29 PM
 */

namespace Rasen\NineGagBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
/**
 * Class PostVoteAdmin
 *
 * @package Rasen\NineGagBundle\Admin
 */
class PostVoteAdmin extends Admin
{
	/**
	 * {@inheritdoc}
	 */
	public function getParentAssociationMapping()
	{
		$em = $this->modelManager->getEntityManager('Rasen\NineGagBundle\Entity\PostVote');
		$className = $em->getClassMetadata(get_class($this->getParent()->getObject($this->getParent()->getRequest()->get('id'))))->getTableName();

		if (strtolower( $className ) == 'users'){
			$className = 'votedBy';
		} elseif (strtolower( $className ) == 'posts') {
			$className = 'post';
		} else {
			$className = strtolower( $className );
		}

		return $className;
	}

	/**
	 * {@inheritdoc}
	 */
	protected $datagridValues = array(
		'_page' => 1,
		'_sort_order' => 'DESC',
		'_sort_by' => 'votedTime'
	);

	/**
	 * {@inheritdoc}
	 */
	public function getBaseRoutePattern()
	{
		if (!$this->baseRoutePattern) {
			$baseRoute = 'post-vote';
			if ($this->isChild()) { // the admin class is a child, prefix it with the parent route name
				$this->baseRoutePattern = sprintf('%s/{id}/%s',
					$this->getParent()->getBaseRoutePattern(),
					$baseRoute
				);
			} else {
				$this->baseRoutePattern = $baseRoute;
			}
		}
		return $this->baseRoutePattern;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureFormFields(FormMapper $formMapper)
	{
		$formMapper
			->with('post_vote',
				array(
					'class' => 'col-md-8 col-md-offset-2'
				));
		if (!$this->hasParentFieldDescription() && !($this->getParent() instanceof PostAdmin)){
			$formMapper->add('post', 'sonata_type_model_list');
		}
		$formMapper->add('vote', 'checkbox', array('required'=>false, 'help'=>'form.description.vote'))
		;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureDatagridFilters(DatagridMapper $datagridMapper)
	{
		$datagridMapper
			->add('post.id')
			->add('vote')
			->add('votedTime')
			->add('votedBy.id')
		;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureListFields(ListMapper $listMapper)
	{
		$listMapper
			->addIdentifier('id')
			->add('post')
			->add('vote', 'boolean', array('editable'=>true))
			->add('votedTime')
			->add('votedBy')
		;
	}
} 