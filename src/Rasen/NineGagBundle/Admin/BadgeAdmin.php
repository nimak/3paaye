<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/23/2014
 * Time: 10:39 AM
 */

namespace Rasen\NineGagBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
/**
 * Class BadgeAdmin
 *
 * @package Rasen\NineGagBundle\Admin
 */
class BadgeAdmin extends Admin
{
	/**
	 * {@inheritdoc}
	 */
	protected $datagridValues = array(
		'_page' => 1,
		'_sort_order' => 'DESC',
		'_sort_by' => 'code'
	);

	/**
	 * {@inheritdoc}
	 */
	public function getBaseRoutePattern()
	{
		if (!$this->baseRoutePattern) {
			$baseRoute = 'badge';
			if ($this->isChild()) { // the admin class is a child, prefix it with the parent route name
				$this->baseRoutePattern = sprintf('%s/{id}/%s',
					$this->getParent()->getBaseRoutePattern(),
					$baseRoute
				);
			} else {
				$this->baseRoutePattern = $baseRoute;
			}
		}
		return $this->baseRoutePattern;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureFormFields(FormMapper $formMapper)
	{
        $subject = $this->getSubject();
        $imageFileFieldOptions = array('required' => false);
        // Show image preview as helper
        if ($subject && ($webPath = $subject->getImageUrl())) {
            // get the container so the full path to the image can be set
            $container = $this->getConfigurationPool()->getContainer();
            $helper = $container->get('vich_uploader.templating.helper.uploader_helper');
            $path = $helper->asset($subject, 'badge_images');
            $cachedFilterUrl = $container->get('liip_imagine.cache.manager')->getBrowserPath($path, 'square_48');

            // add a 'help' option containing the preview's img tag
            $imageFileFieldOptions['help'] = '<img src="'.$cachedFilterUrl.'" class="admin-preview" />';
        }
		$formMapper
			->with('badge',
			array(
				'class' => 'col-md-8 col-md-offset-2'
			))
			->add('code', 'text', array('help'=>'form.description.badge.code'))
			->add('name', 'text')
            ->add('imageFile', 'file', $imageFileFieldOptions)
			->add('description', 'textarea', array('required'=>false))
		;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureDatagridFilters(DatagridMapper $datagridMapper)
	{
		$datagridMapper
			->add('code')
			->add('name')
		;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureListFields(ListMapper $listMapper)
	{
		$listMapper
			->addIdentifier('code')
			->add('name')
            ->add('imageUrl', 'string', array('template'=>'RasenNineGagBundle:Admin/CRUD:list_badge_image.html.twig'))
			->add('description')
		;
	}
} 