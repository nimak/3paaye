<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/23/2014
 * Time: 11:04 AM
 */

namespace Rasen\NineGagBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
/**
 * Class PostCategoryAdmin
 *
 * @package Rasen\NineGagBundle\Admin
 */
class PostCategoryAdmin extends Admin
{
	/**
	 * {@inheritdoc}
	 */
	protected $datagridValues = array(
		'_page' => 1,
		'_sort_order' => 'DESC',
		'_sort_by' => 'name'
	);

	/**
	 * {@inheritdoc}
	 */
	public function getBaseRoutePattern()
	{
		if (!$this->baseRoutePattern) {
			$baseRoute = 'post-category';
			if ($this->isChild()) { // the admin class is a child, prefix it with the parent route name
				$this->baseRoutePattern = sprintf('%s/{id}/%s',
					$this->getParent()->getBaseRoutePattern(),
					$baseRoute
				);
			} else {
				$this->baseRoutePattern = $baseRoute;
			}
		}
		return $this->baseRoutePattern;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureFormFields(FormMapper $formMapper)
	{
		$formMapper
			->with('post_category',
				array(
					'class' => 'col-md-8 col-md-offset-2'
				))
			->add('menuOrder', 'text', array('required'=>false))
			->add('name', 'text')
			->add('slug', 'text', array('help'=>'form.description.post_category.slug'))
			->add('description', 'textarea', array('required'=>false))
			->add('mainMenu', 'checkbox', array('required'=>false))
            ->add('watermark', 'entity', array('required'=>false, 'class' => 'Rasen\NineGagBundle\Entity\Watermark', 'property'=>'id'))
		;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureDatagridFilters(DatagridMapper $datagridMapper)
	{
		$datagridMapper
			->add('name')
			->add('menuOrder')
			->add('slug')
			->add('mainMenu')
		;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureListFields(ListMapper $listMapper)
	{
		$listMapper
			->addIdentifier('slug')
			->add('name')
			->add('menuOrder')
			->add('description')
			->add('mainMenu', 'boolean', array('editable'=>true))
            ->add('watermark.id')
		;
	}
} 