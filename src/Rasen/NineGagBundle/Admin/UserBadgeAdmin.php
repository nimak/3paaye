<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/24/2014
 * Time: 12:29 PM
 */

namespace Rasen\NineGagBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
/**
 * Class UserBadgeAdmin
 *
 * @package Rasen\NineGagBundle\Admin
 */
class UserBadgeAdmin extends Admin
{
	protected $parentAssociationMapping = 'user';

	/**
	 * {@inheritdoc}
	 */
	protected $datagridValues = array(
		'_page' => 1,
		'_sort_order' => 'DESC',
		'_sort_by' => 'givenTime'
	);

	/**
	 * {@inheritdoc}
	 */
	public function getBaseRoutePattern()
	{
		if (!$this->baseRoutePattern) {
			$baseRoute = 'user-badge';
			if ($this->isChild()) { // the admin class is a child, prefix it with the parent route name
				$this->baseRoutePattern = sprintf('%s/{id}/%s',
					$this->getParent()->getBaseRoutePattern(),
					$baseRoute
				);
			} else {
				$this->baseRoutePattern = $baseRoute;
			}
		}
		return $this->baseRoutePattern;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureFormFields(FormMapper $formMapper)
	{
		$formMapper
			->with('user_badge',
				array(
					'class' => 'col-md-8 col-md-offset-2'
				))
			->add('user', 'sonata_type_model_list')
			->add('badge', 'sonata_type_model')
			->add('isClaimed', 'checkbox', array('required'=>false))
		;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureDatagridFilters(DatagridMapper $datagridMapper)
	{
		$datagridMapper
			->add('user.id')
			->add('badge.id')
			->add('isClaimed')
			->add('badgeCode')
			->add('givenTime')
			->add('givenBy.id')
		;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureListFields(ListMapper $listMapper)
	{
		$listMapper
			->addIdentifier('id')
			->add('user')
			->add('badge')
			->add('isClaimed')
			->add('badgeCode')
			->add('givenTime')
			->add('givenBy')
		;
	}
} 