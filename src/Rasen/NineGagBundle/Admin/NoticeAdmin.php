<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 3/16/2015
 * Time: 9:01 PM
 */

namespace Rasen\NineGagBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
/**
 * Class NoticeAdmin
 *
 * @package Rasen\NineGagBundle\Admin
 */
class NoticeAdmin extends Admin
{
	/**
	 * {@inheritdoc}
	 */
	protected $datagridValues = array(
		'_page' => 1,
		'_sort_order' => 'DESC',
		'_sort_by' => 'createdTime'
	);

    /**
     * {@inheritdoc}
     */
    public function getBaseRoutePattern()
    {
        if (!$this->baseRoutePattern) {
            $baseRoute = 'notice';
            if ($this->isChild()) { // the admin class is a child, prefix it with the parent route name
                $this->baseRoutePattern = sprintf('%s/{id}/%s',
                    $this->getParent()->getBaseRoutePattern(),
                    $baseRoute
                );
            } else {
                $this->baseRoutePattern = $baseRoute;
            }
        }
        return $this->baseRoutePattern;
    }

	/**
	 * {@inheritdoc}
	 */
	protected function configureFormFields(FormMapper $formMapper)
	{
        $subject = $this->getSubject();
        $imageFileFieldOptions = array('required' => false);
        // Show image preview as helper
        if ($subject && ($webPath = $subject->getImageUrl())) {
            // get the container so the full path to the image can be set
            $container = $this->getConfigurationPool()->getContainer();
            $helper = $container->get('vich_uploader.templating.helper.uploader_helper');
            $path = $helper->asset($subject, 'notice_images');
            $cachedFilterUrl = $container->get('liip_imagine.cache.manager')->getBrowserPath($path, 'w300');

            // add a 'help' option containing the preview's img tag
            $imageFileFieldOptions['help'] = '<img src="'.$cachedFilterUrl.'" class="admin-preview" />';
        }
		$formMapper
			->with('notice',
				array(
					'class' => 'col-md-8 col-md-offset-2'
				))
			->add('title', 'text', array('required' => false))
			->add('content', 'textarea', array('required' => false, 'attr'=>array('class'=>'tinymce')))
            ->add('imageFile', 'file', $imageFileFieldOptions)
            ->add('active', 'checkbox', array('required'=>false))
            ->add('icon', 'text', array(
                'required'=>false,
                'help' => 'Any <a href="http://fortawesome.github.io/Font-Awesome/icons/">font-awesome</a> or <a href="http://ionicons.com/">ionicons</a> class. </br> Add "blinker" class to make it blink and fa-spin to spin!'
            ))
            ->add('iconColor', 'text', array('required'=>false, 'attr'=>array('class'=>'colorpicker')))
		;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureDatagridFilters(DatagridMapper $datagridMapper)
	{
        $datagridMapper
            ->add('active')
            ->add('title')
            ->add('content')
            ->add('icon')
            ->add('iconColor')
            ->add('createdTime')
            ->add('createdBy.id')
            ;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureListFields(ListMapper $listMapper)
	{
		$listMapper
			->addIdentifier('id')
            ->add('imageUrl', 'string', array('template'=>'RasenNineGagBundle:Admin/CRUD:list_notice_image.html.twig'))
            ->add('title')
            ->add('content')
            ->add('icon', 'string', array('template'=>'RasenNineGagBundle:Admin/CRUD:list_notice_icon.html.twig'))
            ->add('iconColor', 'string', array('template'=>'RasenNineGagBundle:Admin/CRUD:list_notice_icon_color.html.twig'))
            ->add('active', 'boolean', array('editable'=>true))
            ->add('createdTime')
            ->add('createdBy')
		;
	}
} 