<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 3/16/2015
 * Time: 9:01 PM
 */

namespace Rasen\NineGagBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
/**
 * Class AdvertisementAdmin
 *
 * @package Rasen\NineGagBundle\Admin
 */
class AdvertisementAdmin extends Admin
{
	/**
	 * {@inheritdoc}
	 */
	protected $datagridValues = array(
		'_page' => 1,
		'_sort_order' => 'DESC',
		'_sort_by' => 'createdTime'
	);

    /**
     * {@inheritdoc}
     */
    public function getBaseRoutePattern()
    {
        if (!$this->baseRoutePattern) {
            $baseRoute = 'advertisement';
            if ($this->isChild()) { // the admin class is a child, prefix it with the parent route name
                $this->baseRoutePattern = sprintf('%s/{id}/%s',
                    $this->getParent()->getBaseRoutePattern(),
                    $baseRoute
                );
            } else {
                $this->baseRoutePattern = $baseRoute;
            }
        }
        return $this->baseRoutePattern;
    }

	/**
	 * {@inheritdoc}
	 */
	protected function configureFormFields(FormMapper $formMapper)
	{
		$formMapper
			->with('advertisement',
				array(
					'class' => 'col-md-8 col-md-offset-2'
				))
            ->add('content', 'textarea', array('required' => true, 'attr'=>array(
                'class' => 'dir-ltr'
            )))
            ->add('advertisementTitle', 'text', array('required' => false))
            ->add('link', 'text', array(
                'required'=>false,
            ))
            ->add('position', 'text', array(
                'required' => false,
                'help' => 'form.description.ad.position'
            ))
            ->add('appearanceChance', 'text', array(
                'required' => false,
                'help' => 'form.description.ad.appearanceChance'
            ))
            ->add('active', 'checkbox', array('required'=>false))
		;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureDatagridFilters(DatagridMapper $datagridMapper)
	{
        $datagridMapper
            ->add('advertisementTitle')
            ->add('content')
            ->add('link')
            ->add('position')
            ->add('appearanceChance')
            ->add('active')
            ->add('createdTime')
            ->add('createdBy.id')
            ;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureListFields(ListMapper $listMapper)
	{
		$listMapper
			->addIdentifier('id')
            ->add('content')
            ->add('advertisementTitle')
            ->add('link')
            ->add('position')
            ->add('appearanceChance')
            ->add('active')
            ->add('createdTime')
            ->add('createdBy')
		;
	}
} 