<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/21/2014
 * Time: 4:27 PM
 */

namespace Rasen\NineGagBundle\Admin;


use Sonata\AdminBundle\Admin\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

/**
 * Class UserFollower
 * @package Rasen\NineGagBundle\Admin
 */
class UserFollowerAdmin extends Admin
{
	/**
	 * {@inheritdoc}
	 */
	protected $datagridValues = array(
		'_page' => 1,
		'_sort_order' => 'DESC',
		'_sort_by' => 'followedAt'
	);

	/**
	 * {@inheritdoc}
	 */
	public function getBaseRoutePattern()
	{
		if (!$this->baseRoutePattern) {
			$baseRoute = 'user-follower';
			if ($this->isChild()) { // the admin class is a child, prefix it with the parent route name
				$this->baseRoutePattern = sprintf('%s/{id}/%s',
					$this->getParent()->getBaseRoutePattern(),
					$baseRoute
				);
			} else {
				$this->baseRoutePattern = $baseRoute;
			}
		}
		return $this->baseRoutePattern;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getParentAssociationMapping()
	{
		$className = 'user';
		if ($this->getRequest()->query->has('type'))
		{
			if ($this->getRequest()->query->get('type') == 'following') {
				$className = 'follower';
			} elseif ($this->getRequest()->query->get('type') == 'followers') {
				$className = 'user';
			}
		}
		return $className;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getClassnameLabel()
	{
		if ($this->getRequest()->query->has('type'))
		{
			$className = $this->classnameLabel;
			if ($this->getRequest()->query->get('type') == 'following') {
				$className = 'user_following';
			} elseif ($this->getRequest()->query->get('type') == 'followers') {
				$className = 'user_followers';
			}
			return $className;
		} else {
			return $this->classnameLabel;
		}
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureShowFields(ShowMapper $showMapper)
	{
		if ($this->getParent() && $this->getRequest()->query->has('type'))
		{
			if ($this->getRequest()->query->get('type') == 'following') {
				$showMapper
					->add('user')
				;
			} elseif ($this->getRequest()->query->get('type') == 'followers') {
				$showMapper
					->add('follower')
				;
			}
		} else
		{
			$showMapper
				->add('follower')
				->add('user')
				;
		}
		$showMapper
			->add('followedAt')
			->end()
		;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureFormFields(FormMapper $formMapper)
	{

		$formMapper
			->with('user_follower',
				array(
					'class' => 'col-md-8 col-md-offset-2'
				))
			->add('follower', 'sonata_type_model_list')
			->add('user', 'sonata_type_model_list')
			->end()
		;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureDatagridFilters(DatagridMapper $filterMapper)
	{
		if ($this->getParent() && $this->getRequest()->query->has('type'))
		{
			if ($this->getRequest()->query->get('type') == 'following') {
				$filterMapper
					->add('user.id')
				;
			} elseif ($this->getRequest()->query->get('type') == 'followers') {
				$filterMapper
					->add('follower.id')
				;
			}
		} else
		{
			$filterMapper
				->add('follower.id')
				->add('user.id')
			;
		}
		$filterMapper
			->add('followedAt')
		;
	}
	/**
	 * {@inheritdoc}
	 */
	protected function configureListFields(ListMapper $listMapper)
	{
		$listMapper
			->addIdentifier('id');
		if ($this->getParent() && $this->getRequest()->query->has('type'))
		{
			if ($this->getRequest()->query->get('type') == 'following') {
				$listMapper
					->add('user')
				;
			} elseif ($this->getRequest()->query->get('type') == 'followers') {
				$listMapper
					->add('follower')
				;
			}
		} else
		{
			$listMapper
				->add('follower')
				->add('user')
			;
		}
		$listMapper
			->add('followedAt')
		;
	}
}