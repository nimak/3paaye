<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 3/16/2015
 * Time: 9:01 PM
 */

namespace Rasen\NineGagBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
/**
 * Class WatermarkAdmin
 *
 * @package Rasen\NineGagBundle\Admin
 */
class WatermarkAdmin extends Admin
{
	/**
	 * {@inheritdoc}
	 */
	protected $datagridValues = array(
		'_page' => 1,
		'_sort_order' => 'DESC',
		'_sort_by' => 'createdTime'
	);

    /**
     * {@inheritdoc}
     */
    public function getBaseRoutePattern()
    {
        if (!$this->baseRoutePattern) {
            $baseRoute = 'watermark';
            if ($this->isChild()) { // the admin class is a child, prefix it with the parent route name
                $this->baseRoutePattern = sprintf('%s/{id}/%s',
                    $this->getParent()->getBaseRoutePattern(),
                    $baseRoute
                );
            } else {
                $this->baseRoutePattern = $baseRoute;
            }
        }
        return $this->baseRoutePattern;
    }

	/**
	 * {@inheritdoc}
	 */
	protected function configureFormFields(FormMapper $formMapper)
	{
        $subject = $this->getSubject();
        $imageFileFieldOptions = array('required' => false);
        // Show image preview as helper
        if ($subject && ($webPath = $subject->getImageName())) {
            // get the container so the full path to the image can be set
            $container = $this->getConfigurationPool()->getContainer();
            $helper = $container->get('vich_uploader.templating.helper.uploader_helper');
            $path = $helper->asset($subject, 'watermarks');
            //$cachedFilterUrl = $container->get('liip_imagine.cache.manager')->getBrowserPath($path, 'w300');

            // add a 'help' option containing the preview's img tag
            $imageFileFieldOptions['help'] = '<img src="'.$path.'" class="admin-preview" />';
        }
		$formMapper
			->with('watermark',
				array(
					'class' => 'col-md-8 col-md-offset-2'
				))
            ->add('imageFile', 'file', $imageFileFieldOptions)
		;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureDatagridFilters(DatagridMapper $datagridMapper)
	{
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureListFields(ListMapper $listMapper)
	{
		$listMapper
			->addIdentifier('id')
            ->add('imageUrl', 'string', array('template'=>'RasenNineGagBundle:Admin/CRUD:list_watermark_image.html.twig'))
            ->add('createdTime')
            ->add('createdBy')
		;
	}
} 