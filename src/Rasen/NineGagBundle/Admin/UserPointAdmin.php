<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/24/2014
 * Time: 12:29 PM
 */

namespace Rasen\NineGagBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
/**
 * Class UserPointAdmin
 *
 * @package Rasen\NineGagBundle\Admin
 */
class UserPointAdmin extends Admin
{
	protected $parentAssociationMapping = 'user';

	/**
	 * {@inheritdoc}
	 */
	protected $datagridValues = array(
		'_page' => 1,
		'_sort_order' => 'DESC',
		'_sort_by' => 'createdTime'
	);

	/**
	 * {@inheritdoc}
	 */
	public function getBaseRoutePattern()
	{
		if (!$this->baseRoutePattern) {
			$baseRoute = 'user-point';
			if ($this->isChild()) { // the admin class is a child, prefix it with the parent route name
				$this->baseRoutePattern = sprintf('%s/{id}/%s',
					$this->getParent()->getBaseRoutePattern(),
					$baseRoute
				);
			} else {
				$this->baseRoutePattern = $baseRoute;
			}
		}
		return $this->baseRoutePattern;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureFormFields(FormMapper $formMapper)
	{
		$formMapper
			->with('user_point',
				array(
					'class' => 'col-md-8 col-md-offset-2'
				))
			->add('user', 'sonata_type_model_list')
			->add('point', 'integer')
			->add('description', 'textarea', array('help'=>'form.description.user_point.description'))
		;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureDatagridFilters(DatagridMapper $datagridMapper)
	{
		$datagridMapper
			->add('user.id')
			->add('point')
			->add('createdTime')
			->add('lastModifiedTime')
		;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureListFields(ListMapper $listMapper)
	{
		$listMapper
			->addIdentifier('id')
			->add('user')
			->add('point', 'integer', array('editable'=>true))
			->add('description')
			->add('createdTime')
			->add('lastModifiedTime')
		;
	}
} 