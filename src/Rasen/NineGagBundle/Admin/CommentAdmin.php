<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/23/2014
 * Time: 12:02 AM
 */

namespace Rasen\NineGagBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

/**
 * Class CommentAdmin
 *
 * @package Rasen\NineGagBundle\Admin
 */
class CommentAdmin extends Admin
{
	/**
	 * {@inheritdoc}
	 */
	protected $datagridValues = array(
		'_page' => 1,
		'_sort_order' => 'DESC',
		'_sort_by' => 'createdTime'
	);

	/**
	 * {@inheritdoc}
	 */
	public function getBaseRoutePattern()
	{
		if (!$this->baseRoutePattern) {
			$baseRoute = 'comment';
			if ($this->isChild()) { // the admin class is a child, prefix it with the parent route name
				$this->baseRoutePattern = sprintf('%s/{id}/%s',
					$this->getParent()->getBaseRoutePattern(),
					$baseRoute
				);
			} else {
				$this->baseRoutePattern = $baseRoute;
			}
		}
		return $this->baseRoutePattern;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureRoutes(RouteCollection $collection)
	{
		$collection->add('restore', $this->getRouterIdParameter().'/restore');
		$collection->add('approve', $this->getRouterIdParameter().'/approve');
		$collection->add('disapprove', $this->getRouterIdParameter().'/disapprove');
	}

	/**
	 * {@inheritdoc}
	 */
	public function getBatchActions()
	{
		// retrieve the default batch actions
		$actions = parent::getBatchActions();

		if (
			$this->hasRoute('delete') && $this->isGranted('DELETE')
		) {
			$actions['restore'] = array(
				'label' => $this->trans('action_restore', array(), 'RasenNineGagBundle'),
				'ask_confirmation' => true
			);
		}
		if (
			$this->hasRoute('edit') && $this->isGranted('EDIT')
		) {
			$actions['approve'] = array(
				'label' => $this->trans('action_approve', array(), 'RasenNineGagBundle'),
				'ask_confirmation' => true
			);
			$actions['disapprove'] = array(
				'label' => $this->trans('action_disapprove', array(), 'RasenNineGagBundle'),
				'ask_confirmation' => true
			);

		}

		return $actions;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getParentAssociationMapping()
	{
		$em = $this->modelManager->getEntityManager('Rasen\NineGagBundle\Entity\Comment');
		$className = $em->getClassMetadata(get_class($this->getParent()->getObject($this->getParent()->getRequest()->get('id'))))->getTableName();

		if (strtolower( $className ) == 'users'){
			$className = 'createdBy';
		} elseif (strtolower( $className ) == 'posts') {
			$className = 'post';
		} else {
			$className = strtolower( $className );
		}

		return $className;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureFormFields(FormMapper $formMapper)
	{
		$formMapper
			->with('comment',
				array(
					'class' => 'col-md-8 col-md-offset-2'
				))
			->add('content', 'textarea');
		if (!$this->hasParentFieldDescription() && !($this->getParent() instanceof PostAdmin)){
			$formMapper->add('post', 'sonata_type_model');
		}

		$formMapper->add('approved', 'checkbox', array('required'=>false))
		;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureDatagridFilters(DatagridMapper $datagridMapper)
	{
		$datagridMapper
			->add('createdBy.id')
			->add('isDeleted', 'doctrine_orm_callback', array(
//                'callback'   => array($this, 'getWithOpenCommentFilter'),
				'callback' => function($queryBuilder, $alias, $field, $value) {
					if (!$value['value']) {
						return;
					}
					if ($value['value'] == 1) {
						$queryBuilder->andWhere($alias.'.deletedAt is NOT NULL');
					} else {
						$queryBuilder->andWhere($alias.'.deletedAt is NULL');
					}

					return true;
				},
				'field_type' => 'sonata_type_boolean'
			))
			->add('content')
			->add('approved')
			->add('createdTime')
			->add('authorIP')
			->add('approvedTime')
			->add('approvedBy.id')
		;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureListFields(ListMapper $listMapper)
	{
		$listMapper
			->addIdentifier('id')
			->add('content')
			->add('createdBy')
			->add('post')
			->add('approved', 'boolean', array('editable'=>true))
			->add('downvotesNo', 'url', array(
				'route' => array(
					'name' => 'admin_rasen_ninegag_comment_commentvote_list',
					'parameters' => array('filter[vote][value]' => 2),
					'identifier_parameter_name' => 'id'
				)
			))
			->add('upvotesNo', 'url', array(
				'route' => array(
					'name' => 'admin_rasen_ninegag_comment_commentvote_list',
					'parameters' => array('filter[vote][value]' => 1),
					'identifier_parameter_name' => 'id'
				)
			))
			->add('reportsNo', 'url', array(
				'route' => array(
					'name' => 'admin_rasen_ninegag_comment_commentreport_list',
					'parameters' => array(),
					'identifier_parameter_name' => 'id'
				)
			))
			->add('createdTime')
			->add('lastModifiedTime')
			->add('lastModifiedBy')
			->add('authorIP')
			->add('_action', 'actions', array(
				'actions' => array(
					'approve' => array('template'=>'RasenNineGagBundle:Admin/CRUD:list__action_approve.html.twig'),
					'disapprove' => array('template'=>'RasenNineGagBundle:Admin/CRUD:list__action_disapprove.html.twig'),
				)
			))
		;
	}
} 