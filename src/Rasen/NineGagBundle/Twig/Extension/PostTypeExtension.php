<?php
/**
 * Created by PhpStorm.
 * User: AmiR
 * Date: 11/8/14
 * Time: 8:52 PM
 */

namespace Rasen\NineGagBundle\Twig\Extension;


use Rasen\NineGagBundle\Entity\Post;
use Rasen\NineGagBundle\Entity\PostImage;
use Rasen\NineGagBundle\Entity\PostText;

use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class PostTypeExtension
 *
 * @DI\Service("rasen_ninegag.twig.post_type")
 * @DI\Tag("twig.extension")
 *
 * @package Rasen\NineGagBundle\Twig\Extension
 */
class PostTypeExtension extends \Twig_Extension {

    public function getTests()
    {
        return [
            new \Twig_SimpleTest('image', function (Post $post) { return $post instanceof PostImage; }),
            new \Twig_SimpleTest('text', function (Post $post) { return $post instanceof PostText; })
        ];
    }

    public function getName()
    {
        return 'rasen_ninegag.post_type';
    }
} 