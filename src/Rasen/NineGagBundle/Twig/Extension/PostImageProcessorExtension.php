<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 11/14/14
 * Time: 11:52 AM
 */

namespace Rasen\NineGagBundle\Twig\Extension;


use Rasen\NineGagBundle\Entity\PostImage;
use Rasen\NineGagBundle\Lib\PostImageProcessor;

use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class PostImageProcessorExtension
 *
 * @DI\Service("rasen_ninegag.twig.post_image_processor")
 * @DI\Tag("twig.extension")
 *
 * @package Rasen\NineGagBundle\Twig\Extension
 */
class PostImageProcessorExtension extends \Twig_Extension
{
	/**
	 * @var PostImageProcessor
	 */
	private $postImageProcessor;

	/**
	 * Constructor.
	 *
	 * @DI\InjectParams({
	 *     "postImageProcessor" = @DI\Inject("rasen_ninegag.post_image_processor")
	 * })
	 *
	 * @param PostImageProcessor $postImageProcessor
	 */
	public function __construct(PostImageProcessor $postImageProcessor)
	{
		$this->postImageProcessor = $postImageProcessor;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getFunctions()
	{
		$names = array(
			'post_image' => 'postImage'
		);

		$funcs = array();
		foreach ($names as $twig => $local) {
			$funcs[$twig] = new \Twig_Function_Method($this, $local);
		}

		return $funcs;
	}

	/**
	 * Creates hashes from numeric ids
	 *
	 * @param PostImage $postImage
	 * @param $filter
	 *
	 * @return string
	 */
	public function postImage(PostImage $postImage, $filter) {
		return $this->postImageProcessor->getProcessedImage($postImage, $filter);
	}

	/**
	 * Name of this extension
	 *
	 * @return string
	 */
	public function getName()
	{
		return 'rasen_ninegag.post_image_processor';
	}
} 