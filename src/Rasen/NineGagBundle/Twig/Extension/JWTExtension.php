<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 11/7/2014
 * Time: 3:11 PM
 */

namespace Rasen\NineGagBundle\Twig\Extension;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class JWTExtension
 *
 * @DI\Service("rasen_ninegag.twig.jwt_extension")
 * @DI\Tag("twig.extension")
 *
 * @package Rasen\NineGagBundle\Twig\Extension
 */
class JWTExtension extends \Twig_Extension
{
	private $secret;

	/**
	 * @DI\InjectParams({
	 *     "secret" = @DI\Inject("%jwt_secret%")
	 * })
	 *
	 * @param $secret
	 */
	public function __construct($secret)
	{
		$this->secret = $secret;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getFilters()
	{
		return array(
			new \Twig_SimpleFilter('jwt_encode', array($this, 'encode')),
		);
	}

	/**
	 * @param $payload
	 *
	 * @return string
	 */
	public function encode($payload)
	{
		return \JWT::encode($payload, $this->secret);
	}

	/**
	 * {@inheritdoc}
	 */
	public function getName()
	{
		return 'rasen_ninegag.twig.jwt_extension';
	}
} 