<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 02/14/15
 * Time: 11:56 PM
 */

namespace Rasen\NineGagBundle\Twig\Extension;


use Hashids\Hashids;

use JMS\DiExtraBundle\Annotation as DI;
use Rasen\NineGagBundle\Entity\Post;
use Rasen\NineGagBundle\Entity\PostImage;
use Rasen\NineGagBundle\Lib\UrlSlugGenerator;
use Symfony\Component\Routing\Router;

/**
 * Class PostUrlExtension
 *
 * @DI\Service("rasen_ninegag.twig.post_url")
 * @DI\Tag("twig.extension")
 *
 * @package Rasen\NineGagBundle\Twig\Extension
 */
class PostUrlExtension extends \Twig_Extension
{
	/**
	 * @var UrlSlugGenerator $urlSlugGenerator
	 */
	protected $urlSlugGenerator;

	/**
	 * @DI\InjectParams({
	 *     "urlSlugGenerator" = @DI\Inject("rasen_ninegag.url_slug_generator")
	 * })
	 * @param UrlSlugGenerator $urlSlugGenerator
	 */
	public function __construct(UrlSlugGenerator $urlSlugGenerator)
	{
		$this->urlSlugGenerator = $urlSlugGenerator;
	}

	/**
	 * Returns a list of filters.
	 *
	 * @return array
	 */
	public function getFilters()
	{
		return array(
			new \Twig_SimpleFilter('post_url', array($this, 'postUrl')),
		);
	}

	/**
	 * Generates url for a given post (slug url if possible)
	 *
	 * @param $post
	 *
	 * @return string
	 */
	public function postUrl(Post $post) {
		return $this->urlSlugGenerator->generateUrl($post);
	}

	/**
	 * Name of this extension
	 *
	 * @return string
	 */
	public function getName()
	{
		return 'rasen_ninegag.post_url';
	}
} 