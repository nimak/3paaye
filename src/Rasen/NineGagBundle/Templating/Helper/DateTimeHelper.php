<?php

namespace Rasen\NineGagBundle\Templating\Helper;

use Sonata\IntlBundle\Locale\LocaleDetectorInterface;
use Sonata\IntlBundle\Timezone\TimezoneDetectorInterface;

use Sonata\IntlBundle\Templating\Helper\DateTimeHelper as SonataDateTimeHelper;

/**
 * {@inheritdoc}
 */
class DateTimeHelper extends SonataDateTimeHelper
{

    /**
     * {@inheritdoc}
     */
    public function formatDate($date, $locale = null, $timezone = null, $dateType = null)
    {
        $date = $this->getDatetime($date, $timezone);

        $formatter = new \IntlDateFormatter(
            $locale ?: $this->localeDetector->getLocale(),
            null === $dateType ? \IntlDateFormatter::MEDIUM : $dateType,
            \IntlDateFormatter::NONE,
            $timezone ?: $this->timezoneDetector->getTimezone(),
            \IntlDateFormatter::TRADITIONAL
        );

        return $this->process($formatter, $date);
    }


	/**
	 * {@inheritdoc}
	 */
    public function formatDateTime($datetime, $locale = null, $timezone = null, $dateType = null, $timeType = null)
    {
        $date = $this->getDatetime($datetime, $timezone);

        $formatter = new \IntlDateFormatter(
            $locale ?: $this->localeDetector->getLocale(),
            null === $dateType ? \IntlDateFormatter::MEDIUM : $dateType,
            null === $timeType ? \IntlDateFormatter::MEDIUM : $timeType,
            $timezone ?: $this->timezoneDetector->getTimezone(),
            \IntlDateFormatter::TRADITIONAL
        );

        return $this->process($formatter, $date);
    }


	/**
	 * {@inheritdoc}
	 */
    public function formatTime($time, $locale = null, $timezone = null, $timeType = null)
    {
        $date = $this->getDatetime($time, $timezone);

        $formatter = new \IntlDateFormatter(
            $locale ?: $this->localeDetector->getLocale(),
            \IntlDateFormatter::NONE,
            null === $timeType ? \IntlDateFormatter::MEDIUM : $timeType,
            $timezone ?: $this->timezoneDetector->getTimezone(),
            \IntlDateFormatter::TRADITIONAL
        );

        return $this->process($formatter, $date);
    }


	/**
	 * {@inheritdoc}
	 */
    public function format($datetime, $pattern, $locale = null, $timezone = null)
    {
        $date = $this->getDatetime($datetime, $timezone);

        $formatter = new \IntlDateFormatter(
            $locale ?: $this->localeDetector->getLocale(),
            \IntlDateFormatter::FULL,
            \IntlDateFormatter::FULL,
            $timezone ?: $this->timezoneDetector->getTimezone(),
            \IntlDateFormatter::TRADITIONAL,
            $pattern
        );

        return $this->process($formatter, $date);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rasen_nine_gag_intl_datetime';
    }
}
