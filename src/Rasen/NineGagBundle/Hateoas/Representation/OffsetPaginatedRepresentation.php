<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 2/19/2015
 * Time: 1:19 AM
 */

namespace Rasen\NineGagBundle\Hateoas\Representation;

use Hateoas\Configuration\Annotation as Hateoas;
use Hateoas\Representation\RouteAwareRepresentation;
use JMS\Serializer\Annotation as Serializer;
/**
 * Class OffsetPaginatedRepresentation
 * @package Rasen\NineGagBundle\Hateoas\Representation
 * /**
 * @Serializer\ExclusionPolicy("all")
 * @Serializer\XmlRoot("collection")
 *
 * @Hateoas\Relation(
 *      "first",
 *      href = @Hateoas\Route(
 *          "expr(object.getRoute())",
 *          parameters = "expr(object.getParameters(0))",
 *          absolute = "expr(object.isAbsolute())"
 *      ),
 *      exclusion = @Hateoas\Exclusion(
 *          excludeIf = "expr((object.getNextOffset() - object.getLimit()) <= 0)"
 *      )
 * )
 * @Hateoas\Relation(
 *      "last",
 *      href = @Hateoas\Route(
 *          "expr(object.getRoute())",
 *          parameters = "expr(object.getParameters(object.getTotal() - object.getLimit()))",
 *          absolute = "expr(object.isAbsolute())"
 *      ),
 *      exclusion = @Hateoas\Exclusion(
 *          excludeIf = "expr(object.getNextOffset() >= object.getTotal())"
 *      )
 * )
 * @Hateoas\Relation(
 *      "next",
 *      href = @Hateoas\Route(
 *          "expr(object.getRoute())",
 *          parameters = "expr(object.getParameters(object.getNextOffset()))",
 *          absolute = "expr(object.isAbsolute())"
 *      ),
 *      exclusion = @Hateoas\Exclusion(
 *          excludeIf = "expr(object.getNextOffset() >= object.getTotal())"
 *      )
 * )
 * @Hateoas\Relation(
 *      "previous",
 *      href = @Hateoas\Route(
 *          "expr(object.getRoute())",
 *          parameters = "expr(object.getParameters((object.getOffset() - object.getLimit()) > 0 ? (object.getOffset() - object.getLimit()) : 0 ))",
 *          absolute = "expr(object.isAbsolute())"
 *      ),
 *      exclusion = @Hateoas\Exclusion(
 *          excludeIf = "expr((object.getNextOffset() - object.getLimit()) <= 0)"
 *      )
 * )
 *
 * @author Nima Karimi <nima.k68@gmail.com>
 */
class OffsetPaginatedRepresentation extends RouteAwareRepresentation
{
    /**
     * @var int
     *
     * @Serializer\Expose
     * @Serializer\XmlAttribute
     */
    private $offset;

    /**
     * @var int
     *
     */
    private $nextOffset;

    /**
     * @var int
     *
     * @Serializer\Expose
     * @Serializer\XmlAttribute
     */
    private $limit;

    /**
     * @var int|null
     *
     * @Serializer\Expose
     * @Serializer\XmlAttribute
     */
    private $total;

    /**
     * @var string
     */
    private $offsetParameterName;

    /**
     * @var string
     */
    private $limitParameterName;

    public function __construct(
        $inline,
        $route,
        array $parameters        = array(),
        $offset,
        $nextOffset,
        $limit,
        $total,
        $offsetParameterName      = null,
        $limitParameterName      = null,
        $absolute                = false
    ) {
        parent::__construct($inline, $route, $parameters, $absolute);

        $this->offset             = $offset;
        $this->nextOffset         = $nextOffset;
        $this->total              = $total;
        $this->limit              = $limit;
        $this->offsetParameterName  = $offsetParameterName  ?: 'offset';
        $this->limitParameterName = $limitParameterName ?: 'limit';
    }

    /**
     * @return int
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * @return int
     */
    public function getNextOffset()
    {
        return $this->nextOffset;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param  null  $offset
     * @param  null  $limit
     * @return array
     */
    public function getParameters($offset = null, $limit = null)
    {
        $parameters = parent::getParameters();

        $parameters[$this->offsetParameterName]  = null === $offset ? $this->getOffset() : $offset;
        $parameters[$this->limitParameterName] = null === $limit ? $this->getLimit() : $limit;

        return $parameters;
    }

    /**
     * @return int|null
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @return string
     */
    public function getOffsetParameterName()
    {
        return $this->offsetParameterName;
    }

    /**
     * @return string
     */
    public function getLimitParameterName()
    {
        return $this->limitParameterName;
    }
}