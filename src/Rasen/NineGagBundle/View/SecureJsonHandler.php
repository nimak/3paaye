<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 2/8/2015
 * Time: 12:35 PM
 */

namespace Rasen\NineGagBundle\View;

use JMS\DiExtraBundle\Annotation as DI;
use FOS\RestBundle\View\View,
    FOS\RestBundle\View\ViewHandler,
    Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\Response;
/**
 * Class SecureJsonHandler
 *
 * @DI\Service("rasen_ninegag.secure_json_handler")
 *
 * @package Rasen\NineGagBundle\View
 */
class SecureJsonHandler {
    /**
     * @param ViewHandler $handler
     * @param View $view
     * @param Request $request
     * @param string $format
     *
     * @return Response
     */
    public function createResponse(ViewHandler $handler, View $view, Request $request, $format)
    {
        $prefix = ")]}',\n";
        $view->setData(
            array('result' => $view->getData())
        );
        $response = $handler->createResponse($view, $request, $format);
        $content = $response->getContent();
        $response->setContent($prefix.$content);

        return $response;
    }
}