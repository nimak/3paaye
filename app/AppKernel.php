<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(

            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new Rasen\NineGagBundle\RasenNineGagBundle(),
            new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
            new FOS\UserBundle\FOSUserBundle(),
            new JMS\DiExtraBundle\JMSDiExtraBundle($this),
            new JMS\AopBundle\JMSAopBundle(),
	        new FOS\OAuthServerBundle\FOSOAuthServerBundle(),
	        new JMS\SerializerBundle\JMSSerializerBundle(),
	        new FOS\RestBundle\FOSRestBundle(),
	        new Sonata\CoreBundle\SonataCoreBundle(),
	        new Sonata\BlockBundle\SonataBlockBundle(),
	        new Knp\Bundle\MenuBundle\KnpMenuBundle(),
	        new Sonata\DoctrineORMAdminBundle\SonataDoctrineORMAdminBundle(),
	        new Sonata\AdminBundle\SonataAdminBundle(),
	        new Sonata\UserBundle\SonataUserBundle(),
	        new Vich\UploaderBundle\VichUploaderBundle(),
	        new Knp\Bundle\GaufretteBundle\KnpGaufretteBundle(),
	        new Sonata\IntlBundle\SonataIntlBundle(),
            new HWI\Bundle\OAuthBundle\HWIOAuthBundle(),
	        new Liip\ImagineBundle\LiipImagineBundle(),
	        new OldSound\RabbitMqBundle\OldSoundRabbitMqBundle(),
	        new Snc\RedisBundle\SncRedisBundle(),
	        new FOS\JsRoutingBundle\FOSJsRoutingBundle(),
	        new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
	        new Dunglas\AngularCsrfBundle\DunglasAngularCsrfBundle(),
	        new cayetanosoriano\HashidsBundle\cayetanosorianoHashidsBundle(),
            new Bazinga\Bundle\HateoasBundle\BazingaHateoasBundle(),
            /*new Nelmio\SecurityBundle\NelmioSecurityBundle(),
            new FOS\CommentBundle\FOSCommentBundle(),*/
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }

    public function getCacheDir()
    {
        if (in_array($this->environment, array('dev', 'test'))) {
            return '/dev/shm/9gag/cache/' .  $this->environment;
        }

        return parent::getCacheDir();
    }

    public function getLogDir()
    {
        if (in_array($this->environment, array('dev', 'test'))) {
            return '/dev/shm/9gag/logs';
        }

        return parent::getLogDir();
    }
}
